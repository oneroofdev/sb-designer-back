﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace UploadImage
{
    public class Upload
    {
        protected APIResultResponse myReturn = new APIResultResponse();
        public string fileName { get; set; }
        public string fileThumpnailName { get; set; }

        public APIResultResponse UpProfileImg(IEnumerable<HttpPostedFileBase> files, string fileName, string fullPath, Dictionary<string, string> isThumpnail)
        { 
            try
            {
                List<Upload> result = new List<Upload>();

                int i = 0;
                foreach (var file in files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        i = i + 1;
                        string ext = Path.GetExtension(file.FileName);
                        if (HasImageExtension(ext))
                        {
                            string FileNameNew = fileName + "_" + i.ToString() + ext;
                            file.SaveAs(fullPath + FileNameNew);
                            foreach (KeyValuePair<string, string> items in isThumpnail)
                            {
                                if (items.Key == "Y")
                                {
                                    int width = 120;
                                    int height = 120;
                                    var temp = items.Value.ToString().Split('X');
                                    if (temp.Count() == 2)
                                    {
                                        width = int.Parse(temp[0]);
                                        height = int.Parse(temp[1]);
                                    }
                                    Image image = Image.FromFile(fullPath + FileNameNew);
                                    Image thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero); 
                                    thumb.Save(fullPath + "thump_" + FileNameNew);
                                    result.Add(new Upload()
                                    {
                                        fileName = FileNameNew,
                                        fileThumpnailName = "thump_" + FileNameNew
                                    });
                                }
                                else
                                {
                                    result.Add(new Upload()
                                    {
                                        fileName = FileNameNew,
                                        fileThumpnailName = null
                                    });
                                } 
                            }
                           
                        }
                        else
                        {
                            result.Add(new Upload()
                            {
                                fileName = null,
                                fileThumpnailName = null
                            });
                        }

                    }
                }

                myReturn.success = true;
                myReturn.code = "200";
                myReturn.message = "OK";
                myReturn.description = "";
                myReturn.items = result; 
                return myReturn;

            }
            catch (Exception ex)
            {
                myReturn.success = false;
                myReturn.code = "500";
                myReturn.message = "ERROR";
                myReturn.description = ex.ToString();
                myReturn.items = null; 
                return myReturn;

            }
        }
        public bool HasImageExtension(string source)
        {
            return (source.EndsWith(".png") || source.EndsWith(".jpg"));
        }
    }
    public class APIResultResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public object items { get; set; }
        public APIResultResponse()
        {
            code = "000";
            message = "OK";
            description = "";
            items = new object();
        }

        public static bool save_transaction(APIResultResponse xx)
        {
            xx.code = "ssss";
            return true;
        }
    }
}
