﻿
var GetMenu = function () { 
    $.ajax({
        url: baseUrl + "User/GetProgramPermission",//
        dataType: 'json',
        type: "POST",
        async: true,
        cache: false,
        traditional: true,
        data: {
            fromType: "B" 
        },
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data != null) {
                //console.log(data)
                var strMenu = "";
                var main = "";
                var sub = "";
                var subGrp = "";
                $.each(data, function (i, e) {
                    sub = "";
                    $.each(e.Menu, function (j, value) {
                        if (value.IS_GRP_MENU == "Y") {
                            main = '<a href="' + baseUrl + (value.URL == null ? '#' : value.URL) + '">' +
                                      '<i class="' + value.ICON + '"></i> <span>' + value.PROGRAM_NAME + '</span>' +
                                      (e.Menu.length > 1 ? '<span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>' : '') +
                                  '</a>';
                        } else {
                            sub += '<li><a href="' + baseUrl + value.URL + '">' + value.PROGRAM_NAME + '</a></li>';
                        }
                    })
                    if (e.Menu.length > 1) {
                        subGrp = '<ul class="' + (e.Menu.length > 1 ? 'treeview-menu' : '') + '">' + sub + '</ul>';
                    }
                    strMenu += '<li class="' + (e.Menu.length > 1 ? 'treeview' : '') + '">' + main + subGrp + '</li>';
                });
                $('#list-menu').html(strMenu);
            } else {
                alert("Error =>" + data.description);
                return false;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        },
        complete: function () { closeLoading() }
    });
}
GetMenu();
