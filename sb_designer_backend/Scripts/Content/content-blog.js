﻿$(document).ready(function () {

    var clearFormInput = function () {
        $('#contentCode').val("0")
        $('#select_con_type').val($('#select_con_type option:first').val()).trigger('change');
        $('#content_hearder_th').val("");
        $('#content_hearder_en').val("");
        $('#contentDescTh').summernote('code', '');
        $('#contentDescEn').summernote('code', '');
        $('#link_url').val("");
        $('#limit_register').val("")
        $('#start_date').val("")
        $('#end_date').val("")
        $('#content_image').val("")
        $("#summernote_th").summernote('code', '');
        $("#summernote_en").summernote('code', '');
        $('#content-form input[type=radio][value="Y"]').iCheck('check');
        $('input[name=is_register]').iCheck('uncheck');
    }
    var editContent = function () {
        $("#content_image_").rules("remove", "required");

        //$("#txtLastName").rules("add", { required: true, minlength: 2 });
        //$("#txtEmail").rules("add", "required");
        //$("#txtLastName").rules("remove", "required minlength");

        if ($('#content-form').valid() && !$('.summernote').summernote('isEmpty')) {
            var valueFromTextEditorTH = $("#summernote_th").summernote('code')
            //var valueFromTextEditorEN = $("#summernote_en").summernote('code')
            valueFromTextEditorTH = valueFromTextEditorTH.replace(/\n/g, "<br />").replace(/\"/g, "'");
            //valueFromTextEditorEN = valueFromTextEditorEN.replace(/\n/g, "<br />").replace(/\"/g, "'");

            var descTH = $("#contentDescTh").summernote('code')
            //var descEN = $("#contentDescEn").summernote('code')
            descTH = descTH.replace(/\n/g, "<br />").replace(/\"/g, "'");
            //descEN = descEN.replace(/\n/g, "<br />").replace(/\"/g, "'");

            ///console.log(valueFromTextEditorTH)
            ///console.log(valueFromTextEditorEN)

            /*$.ajax({
                type: 'post',
                dataType: 'json',
                url: baseUrl + "content/updateContent",
                async: true,
                cache: false,
                crossDomain: true,
                withCredentials: true,
                */
            $.ajax({
                url: sessionStorage.apiUrl + "/content/update_content",//
                dataType: 'json',
                type: "POST",
                async: true,
                cache: false,
                crossDomain: true,
                withCredentials: true,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                    , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: {
                    fromType: "B",
                    contentCode: $('#contentCode').val(),
                    contentType: $('#select_con_type').val(),
                    contentHeaderTh: $('#content_hearder_th').val(),
                    contentHeaderEn: $('#content_hearder_en').val(),
                    contentDescTh: descTH,
                    //contentDescEn: descEN,
                    linkUrl: $('#link_url').val(),
                    isRegister: $('input[name=is_register]:checked', '#content-form').val(),
                    isLimitRegister: $('input[name=is_limit]:checked', '#content-form').val(),
                    LimitRegister: $('#limit_register').val(),
                    contentImage: $('#content_image').val(),
                    contentTextTh: valueFromTextEditorTH,
                    //contentTextEn: valueFromTextEditorEN,
                    startDate: $('#start_date').val(),
                    endDate: $('#end_date').val(),
                    active: $('input[name=active]:checked', '#content-form').val(),
                    createBy: sessionStorage.UserName
                },
                beforeSend: function () { showLoading() },
                success: function (data) {
                    if (data.success) {
                        alert(data.message);
                        getListContent()
                        clearFormInput();
                        $("#content_image_").rules("add", "required");
                    } else {
                        alert(data.description);
                        return false;
                    }
                },
                complete: function () {
                    closeLoading()
                }
            });
        }
        else {
            alert("กรุณากรอกข้อมูลในช่องที่มี * ให้ครบถ้วน");
        }
    }
    //$('#add-q-answer').val(valueFromTextEditor);
    $('#content-form').validate({
        lang: 'th',
        rules: {
            content_type: {
                required: true,
            },
            content_hearder_th: {
                required: true,
            },
            content_hearder_en: {
                required: true,
            },
            content_image_: {
                required: true,
            },
            content_image: {
                required: true,
            },
            content_image: {
                required: true,
            },
            content_text_th: {
                required: true,
            },
            content_text_en: {
                required: true,
            },
        }
    })
    $('#content-form input[type=file]').change(function () {

        listImage = []
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', "activity");
        var file_name = file.name;
        console.log(data.get('file'))
        /* $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "upload/UploadListImage",  */
        $.ajax({
            url: "/upload/UploadListImage",
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function (result) {
            //alert(result);
            $('#content_image').val(result)
            $('#img_preview').attr('src', baseUrl + "Content/img/activity/" + result)
            listImage = result
            console.log(listImage);
        }).fail(function (a, b, c) {
            console.log(a, b, c);
        });
    })
    $('#btn_save').click(function () {
        if ($('#contentCode').val() != 0) {
            editContent()
        }
        else {
            if ($('#content-form').valid() && !$('.summernote').summernote('isEmpty')) {
                var valueFromTextEditorTH = $("#summernote_th").summernote('code')
                //var valueFromTextEditorEN = $("#summernote_en").summernote('code')
                valueFromTextEditorTH = valueFromTextEditorTH.replace(/\n/g, "<br />").replace(/\"/g, "'");
                //valueFromTextEditorEN = valueFromTextEditorEN.replace(/\n/g, "<br />").replace(/\"/g, "'");
                var descTH = $("#contentDescTh").summernote('code')
                //var descEN = $("#contentDescEn").summernote('code')
                descTH = descTH.replace(/\n/g, "<br />").replace(/\"/g, "'");
                //descEN = descEN.replace(/\n/g, "<br />").replace(/\"/g, "'");
                ///console.log(valueFromTextEditorTH)
                ///console.log(valueFromTextEditorEN)
                /*
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: baseUrl + "content/createContent",
                    async: true,
                    cache: false,
                    crossDomain: true,
                    withCredentials: true,*/
                $.ajax({
                    url: sessionStorage.apiUrl + "/content/create_content",//
                    dataType: 'json',
                    type: "POST",
                    async: true,
                    cache: false,
                    crossDomain: true,
                    withCredentials: true,
                    headers: {
                        'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                        , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
                    },
                    data: {
                        fromType: "B",
                        contentType: $('#select_con_type').val(),
                        contentHeaderTh: $('#content_hearder_th').val(),
                        contentHeaderEn: $('#content_hearder_en').val(),
                        contentDescTh: descTH,
                        //contentDescEn: descEN,
                        linkUrl: $('#link_url').val(),
                        isRegister: $('input[name=is_register]:checked', '#content-form').val(),
                        isLimitRegister: $('input[name=is_limit]:checked', '#content-form').val(),
                        LimitRegister: $('#limit_register').val(),
                        contentImage: $('#content_image').val(),
                        contentTextTh: valueFromTextEditorTH,
                        //contentTextEn: valueFromTextEditorEN,
                        startDate: $('#start_date').val(),
                        endDate: $('#end_date').val(),
                        active: $('input[name=active]:checked', '#content-form').val()
                    },
                    beforeSend: function () { showLoading() },
                    success: function (data) {
                        if (data.success) {
                            alert(data.message);
                            getListContent()
                            clearFormInput();
                        } else {
                            alert(data.description);
                            return false;
                        }
                    },
                    complete: function () {
                        closeLoading()
                    }
                });
            }
            else {
                alert("กรุณากรอกข้อมูลในช่องที่มี * ให้ครบถ้วน");
            }
        }
    })
    //$('#content_table').on('click', '.btn_preview', function () {
    //    alert($(this).data("contentcode"))
    //})
    $('#content_table').on('click', '.btn_edit', function () {
        var contentCode = $(this).data("contentcode")
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "content/getContent",/*
        $.ajax({
            url: sessionStorage.apiUrl + "content/get_content",//
            dataType: 'json',
            type: "POST",
            crossDomain: true,
            withCredentials: true,
            headers: {
                'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            },*/
            data: {
                fromType: "B",
                contentType: 0,
                contentCode: contentCode
            },
            async: false,
            cache: false,
            beforeSend: function () { showLoading() },
            success: function (data) {
                if (data.success) {
                    console.log(data.items)
                    $('#contentCode').val(data.items.contentCode)
                    $('#select_con_type').val(data.items.contentType).trigger('change');
                    $('#content_hearder_th').val(data.items.contentHeaderTh);
                    $('#content_hearder_en').val(data.items.contentHeaderEn);
                    $('#contentDescTh').summernote('code', data.items.contentDescTh);
                    $('#contentDescEn').summernote('code', data.items.contentDescEn);
                    $('#link_url').val(data.items.linkUrl);
                    $('#limit_register').val(data.items.LimitRegister)
                    $('#start_date').val(ConvertDate2(data.items.startDate))
                    $('#end_date').val(ConvertDate2(data.items.endDate))
                    $('#content_image').val(data.items.contentImage)
                    $('#content_image_').removeAttr('required')
                    $('#img_preview').attr('src', "/Content/img/activity/" + data.items.contentImage)
                    $("#summernote_th").summernote('code', data.items.textItems[0]["contentTextTh"])
                    $("#summernote_en").summernote('code', data.items.textItems[0]["contentTextEn"])
                    $('#content-form input[type=radio][name=active][value="' + data.items.active + '"]').iCheck('check');
                    $('#content-form input[type=radio][name=is_limit][value="' + data.items.isLimitRegister + '"]').iCheck('check');
                    if (data.items.isRegister == "Y") {
                        $('input[name=is_register]').iCheck('check');
                    }
                    else {
                        $('input[name=is_register]').iCheck('uncheck');
                    }
                    //$('#start_date').datepicker().datepicker('setDate', $(this).val());
                    //$('#end_date').datepicker().datepicker('setDate', $(this).val());
                } else {
                    alert(data.description);
                    return false;
                }
            },
            complete: function () {
                closeLoading()
            }
        });
    })
    $('input[type=radio][name=is_limit]').on('ifChecked', function (event) {
        if ($('input[name=is_limit]:checked', '#content-form').val() == "Y") {
            $("#limit_register").rules("add", "required");
        }
        else {
            $("#limit_register").rules("remove", "required");
        }
    })
    $('#btn_clear').click(function () {
        $("#summernote_th").summernote("code", "");
        $("#summernote_en").summernote("code", "");
        $("#summernote_th").summernote("reset");
        $("#summernote_en").summernote("reset");
    })

    $('.select2').select2();
    $('.summernote').summernote({
        placeholder: 'write here...',
        maximumImageFileSize: 10485760,// # 512KB
        height: "300",
        callbacks: {
            onImageUpload: function (files) {
                var $editor = $(this);
                var data = new FormData();
                data.append('file', files[0]);
                data.append('path', "activity");
                ImageUpload(data, $editor);
            }
        },
        toolbar: [
        ['style', ['style']],
        ['view', ['undo', 'redo']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname', 'DBGillSiam']],

        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['picture']],
        ['view', ['fullscreen', 'help', 'codeview']]
        ],
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        }
    });
    $('.summernote2').summernote({
        placeholder: 'write here...',
        height: "100",
        toolbar: [
        ['view', ['undo', 'redo']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['view', ['fullscreen', 'help']]
        ],
    });
    var ImageUpload = function (data, $editor) {
        console.log(data)
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "upload/UploadListImage",/*
        $.ajax({
            url: sessionStorage.mainUrl + "upload/UploadListImage",
            method: 'POST',*/
            data: data,
            processData: false,
            contentType: false,
            success: function (url) {
                $editor.summernote('insertImage', baseUrl + "Content/img/activity/" + url);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                showAlert("Error", thrownError);
            },
        });
    }
    $('.img-side-show').click(function () {
        $('#img_showpicdamage').attr('src', $(this).attr('src'));
        $('.img-side-show').css('border', 'none');
        $(this).css('border', '5px solid #80b636');
        var id = $(this).data("tamplateid");
        for (var i = 1; i <= 4; i++) {
            if (id == i) {
                $('.tamplate' + i).removeClass("hidden");
            }
            else {
                $('.tamplate' + i).addClass("hidden");
            }
        }
    });
    getListContent()
    $(".link-dialog").remove();
})

var DestroyDataTable = function (element) {
    if ($.fn.DataTable.isDataTable(element)) {
        $(element).DataTable().destroy();
    }
}
var getListContent = function () {
    var row_ = "";
    DestroyDataTable($('#content_table'))
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: baseUrl + "content/listContent",/*
    $.ajax({

        url: sessionStorage.apiUrl + "/content/list_content",//
        dataType: 'json',
        type: "POST",
        crossDomain: true,
        withCredentials: true,
        headers: {
            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        },*/
        data: {
            fromType: "B",
            contentType: 0
        },
        async: false,
        cache: false,
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                $('#content_table tbody tr').remove();
                console.log(data.items)
                $.each(data.items, function (i, e) {
                    row_ += '<tr>' +
                                    '<td class="text-center">' + (i + 1) + '</td>' +
                                    '<td>' + (e.contentType == 1 ? "ข่าวสาร" : "กิจกรรม") + '</td>' +
                                    '<td>' + e.contentHeaderTh + '</td>' +
                                    '<td>' + e.contentHeaderEn + '</td>' +
                                    '<td class="text-center">' + (e.active == "Y" ? "ใช้งาน" : "ยกเลิก") + '</td>' +
                                    '<td class="text-center">' +
                                        '<a class="btn btn-sm btn-info btn_preview" href="/Content/content_example?id=' + e.contentCode + '" target="_blank" data-contentcode="' + e.contentCode + '"><i class="fa fa-search"></i> ตัวอย่าง</a>&nbsp;' +
                                        '<button class="btn btn-sm btn-warning btn_edit" data-contentcode="' + e.contentCode + '"><i class="fa fa-pencil"></i> แก้ไข</button>' +
                                    '</td>' +
                                  '</tr>';
                })
                $('#content_table tbody').html(row_);
                $('#content_table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false
                });
            } else {
                alert(data.description);
                return false;
            }
        },
        complete: function () {
            closeLoading()
        }
    });
}
$('.date').attr('data-date-format', 'dd/mm/yyyy');
var DestroyDataTable = function (element) {
    if ($.fn.DataTable.isDataTable(element)) {
        $(element).DataTable().destroy();
    }
}