﻿$(document).ready(function () {

    var type_report = $("#ddl_type_report option").html();
    $('#txt_reportname').val(type_report);
    $('#grp_date').css('display', 'block');
    //$('#txt_reportname').prop('readonly', true);
    $('#search_tier').css('display', 'none');
    $('#tier').css('display', 'none');
    $('#view_data').css('display', 'block');
    $("#date_btw").html('วันที่แลก');
    $("#ddl_type_report").on('change', function () {
        type_report = $('#ddl_type_report option:selected').html();
        $('#txt_reportname').val(type_report);
    });
    //SET DATE
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy;
    $('#txt_datef').val("01/01/2019");
    $('#txt_datet').val(today);

    $("#bt_ExportExcel").on('click', function () {
        var datef = $('#txt_datef').val().split('/');
        var txtDatef = datef[2] + "-" + datef[1] + "-" + datef[0];
        var datet = $('#txt_datet').val().split('/');
        var txtDatet = datet[2] + "-" + datet[1] + "-" + datet[0];
        window.location = sessionStorage.mainUrl + '/ReportRedeem/ExportExcel?start_date=' + $('#txt_datef').val() + '&end_date=' + $('#txt_datet').val();
    }); // on click export excel

    $("#bt_ViewData").on('click', function () {
        //alert("AAA");
        showLoading();
        var data_period = $("#round_num").find(':selected').attr('data-period');
        $.ajax({
            url: sessionStorage.apiUrl + '/Report/GetReportRedeem_graph',//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            //},
            data: {
                start_date: $('#txt_datef').val(),
                end_date: $('#txt_datet').val()
            },
            success: function (data) {
                console.log(data);
                $("#graph").hide();
                ////TEST GRAPH BARR
                var color = [
                       "#f1948a",
                       "#bb8fce",
                       "#85c1e9",
                       "#73c6b6",
                       "#82e0aa",
                       "#f8c471",
                       "#e59866",
                       "#d7dbdd",
                       "#b2babb",
                       "#808b96",
                       "#FFFF33",
                       "#918ef1",
                       "#f49fd2",
                       "#8ef292",
                       "#FF6699",
                       "#FF9999",
                       "#FFCC99",
                       "#FFFF99",
                       "#FFFFCC"
                ];
                if (data.items.length > 0) {
                    $("#graph").show();
                    var data_labels = [];
                    var data_point = [];

                    $.each(data.items, function (index, value) {
                        data_labels.push(value.CATEGORY_NAME);
                        data_point.push(value.POINT);
                    });

                    //// GRAPH CAT POINT ////
                    createChart(data_labels, data_point, 'cat_point', 'pie', {
                        responsive: true,
                        maintainAspectRatio: false,
                        title: {
                            display: true,
                            fontSize: 18,
                            text: 'คะแนนที่มีการแลก (แยกตามประเภทของรางวัล)'
                        },
                        legend: {
                            display: true,

                        },
                        plugins: {
                            labels: [
                          {
                              fontColor: "black",
                              render: function (args) {
                                  //return args.value;
                                  var value = args.value;
                                  value = value.toString();
                                  value = value.split(/(?=(?:...)*$)/);
                                  value = value.join(',');
                                  return value;
                              },
                              arc: false
                          }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    var value = data.datasets[0].data[tooltipItem.index];
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    value = value.join(',');
                                    return value;
                                }
                            } // end callbacks:
                        } //end tooltips
                    }); //// GRAPH CAT POINT ////
                } // ถ้ามี Data ในช่วงที่เลือก จะสร้าง graph
                closeLoading();
            } //success
        }); //ajax
    }); // on click view data

}); //document ready

function createChart(label, val, id, type, options) {
    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var coloR = [];
    for (var i in val) {
        coloR.push(dynamicColors());
    }
    var color = [
               "#f1948a",
               "#bb8fce",
               "#85c1e9",
               "#73c6b6",
               "#82e0aa",
               "#f8c471",
               "#e59866",
               "#d7dbdd",
               "#b2babb",
               "#808b96",
               "#FFFF33",
               "#918ef1",
               "#f49fd2",
               "#8ef292",
               "#FF6699",
               "#FF9999",
               "#FFCC99",
               "#FFFF99",
               "#FFFFCC"
    ]
    var data = {
        labels: label,
        datasets: [
          {
              label: 'จำนวน',
              data: val,
              backgroundColor: color,
          }
        ]
    };
    new Chart(document.getElementById(id), {
        type: type,
        data: data,
        options: options
    });
    Chart.defaults.global.defaultFontColor = '#000';
}
function createCharts(label, val, id, type, colors, options) {

    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var coloR = [];
    for (var i in val) {
        coloR.push(dynamicColors());
    }
    var data = {
        //labels: ['January', 'February', 'March'],
        labels: label,
        datasets: [
          {
              label: 'จำนวน',
              //data: [50445, 33655, 15900],
              data: val,
              backgroundColor: colors,
              //backgroundColor: [
              //  '#FF6384',
              //  '#36A2EB',
              //  '#FFCE56'
              //]
          }
        ]
    };
    new Chart(document.getElementById(id), {
        type: type,
        data: data,
        options: options
    });
    Chart.defaults.global.defaultFontColor = '#000';
}