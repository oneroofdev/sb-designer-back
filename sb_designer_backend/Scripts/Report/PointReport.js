﻿$(document).ready(function () {

    $('.date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

    /*var table = $('#tbl_point').DataTable({
        "ajax": {
            'type': 'POST',
            'url': '/PointReport/GetDetail',
            'dataSrc': "",
            //'data': {
            //    start_date: "01/07/2018",
            //    end_date: "31/08/2018",
            //}
            'data': function (d) {
                let startDate = null;
                let endDate = null;
                if ($('#date_from').val()) {
                    let start_split = $('#date_from').val().split('/');
                    startDate = start_split[1] + '/' + start_split[0] + '/' + start_split[2];
                }
                if ($('#date_to').val()) {
                    let end_split = $('#date_to').val().split('/');
                    endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];
                }
                d.start_date = startDate;
                d.end_date = endDate;
            }
        },
        "columns": [
            { data: null },
            { data: 'Cust_Code' },
            { data: 'Name' },
            {
                data: 'IS_VIP',
                render: function (data, type, row) {
                    let text = '';
                    if (data) {
                        text = 'VIP'
                    }
                    return text;
                },
            },
            { data: 'BalancePoint' },
            { data: 'Mobile_Phone' },
            { data: 'Email' },
            { data: 'Sap_Code' },
            { data: 'TotalRefer' },
            { data: 'Process' },
            { data: 'Point' },
            { data: 'Amount' },
        ],
        "order": [[1, "asc"]]
    });

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#bt_Search').on('click', function () {
        table.ajax.reload();
    });
*/
    $("#bt_Export").on("click", function () {
        let startDate = '';
        let endDate = '';
        if ($('#date_from').val()) {
            /*let start_split = $('#date_from').val().split('/');
            startDate = start_split[1] + '/' + start_split[0] + '/' + start_split[2];*/
            startDate = $('#date_from').val();
        }
        if ($('#date_to').val()) {
            /* let end_split = $('#date_to').val().split('/');
             endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];*/
            endDate = $('#date_to').val();
        }

        window.location = baseUrl + "PointReport/ExportExcel?start_date=" + startDate + "&end_date=" + endDate;
    });

});