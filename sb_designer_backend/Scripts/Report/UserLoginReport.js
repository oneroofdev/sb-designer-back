﻿$(document).ready(function () { 
    $('.date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

    $("#bt_Clear").on("click", function () {
        $("#date_from").val('');
        $("#date_to").val(''); 
    });

    var table = $('#tbl_user_login').DataTable({
        "ajax": {
            'type': 'POST',
            'url': baseUrl + 'UserLoginReport/GetDetail',
            //'data': {
            //    start_date: $('#date_from').val(),
            //    end_date: $('#date_to').val(),
            //}
            'data': function (d) {
                let startDate = null;
                let endDate = null;
                if ($('#date_from').val()) {
                    //let start_split = $('#date_from').val().split('/');
                    //startDate = start_split[0] + '/' + start_split[0] + '/' + start_split[2];
                    startDate = $('#date_from').val();
                }
                if ($('#date_to').val()) {
                    //let end_split = $('#date_to').val().split('/');
                    //endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];
                    endDate = $('#date_to').val();
                }
                d.start_date = startDate;
                d.end_date = endDate;
            },
            "dataSrc": function (d) {

                var uniqueNames = [];
                $.each($(d), function (key, value) { 
                    if (uniqueNames.indexOf(value.Cust_ID) === -1) {
                        uniqueNames.push(value.Cust_ID);
                    }
                });
                $("#dataUserLog").text(uniqueNames.length);/*
                for (i = 0; i < uniqueNames.length; i++) {
                    alert(uniqueNames[i]);
                }*/
                return d;
            }
        },
        "columns": [
            { data: null },
            { data: 'Cust_ID' },
            { data: 'Name' },
            {
                data: 'VIP',
                render: function (data, type, row) {
                    let text = '';
                    if (data) {
                        text = data
                    }
                    return text;
                },
            },
            {
                data: 'LogDate',
                render: function (data, type, row) {
                    let timestamp = data.split(' ');
                    let date = timestamp[0].split('/');
                    return timestamp[0];
                },
            },
            { data: 'QTY' },
        ],
        /*"columnDefs": [
             { className: "dt-right", "targets": [2, 5] },
            { type: 'date-uk', targets: 4 }

        ],*/
        'drawCallback': function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        'columnDefs': [
                 {
                     "targets": 0,  
                     "type": "date-uk"
                 },
                {
                    "targets": 2,
                    "className": "text-left",
                } 
        ],
        "columnDefs": [ 
            {
                "targets": [0, 4],
                "orderable": false
            }
        ],
    });

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
            if (a == null || a == "") {
                return 0;
            }
            var ukDatea = a.split('/');
            return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        },

        "date-uk-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "date-uk-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });

    $('#bt_Search').on('click', function () {
        table.ajax.reload();
    });

    $("#bt_Export").on("click", function () {
        let startDate = '';
        let endDate = '';
        if ($('#date_from').val()) {
            //let start_split = $('#date_from').val().split('/');
            //startDate = start_split[1] + '/' + start_split[0] + '/' + start_split[2];
            startDate = $('#date_from').val();
        }
        if ($('#date_to').val()) {
            //let end_split = $('#date_to').val().split('/');
            //endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];
            endDate = $('#date_to').val();
        }

        window.location = baseUrl + "UserLoginReport/ExportExcel?start_date=" + startDate + "&end_date=" + endDate;
    });

});