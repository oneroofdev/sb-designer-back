﻿$(document).ready(function () {

    var type_report = $("#ddl_type_report option").html();
    $('#txt_reportname').val(type_report);
    $('#grp_date').css('display', 'block');
    //$('#txt_reportname').prop('readonly', true);
    $('#search_tier').css('display', 'none');
    $('#tier').css('display', 'none');
    $('#view_data').css('display', 'block');
    $("#date_btw").html('วันที่แลก');
    $("#ddl_type_report").on('change', function () {
        type_report = $('#ddl_type_report option:selected').html();
        $('#txt_reportname').val(type_report);
    });
    //SET DATE
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy;
    $('#txt_datef').val("19/02/2018");
    $('#txt_datet').val(today);

    $("#bt_ExportExcel").on('click', function () {
        if ($("#ddl_type_report").val() == '1') {
            window.location = baseUrl + '/ReportRedeem/ReportRedeemRewardStatusExcel?&report_h=' + $('#txt_reportname').val() + '&start_date=' + $('#txt_datef').val() + '&end_date=' + $('#txt_datet').val() + '&status=' + $("#status_redeem").val();
        }
    }); // on click export excel

    $("#bt_ViewData").on('click', function () {
        //alert("AAA");
        showLoading();
        $.ajax({
            url: baseUrl + '/ReportRedeem/GetDataReportRedeemStatusGraph',
            headers: {
                'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
            },
            method: 'POST',
            dataType: 'json',
            crossDomain: true,
            withCredentials: true,
            data: {
                start_date: $('#txt_datef').val(),
                end_date: $('#txt_datet').val(),
                status: $("#status_redeem").val(),
                fromType: "B"
            },
            success: function (data) {
                console.log(data);
                $("#count_redeem_all").html(commaSeparateNumber(data.items.countOfAllRedeem));
                $("#redeem_delivery").html(commaSeparateNumber(data.items.countOfDelivery));
                $("#redeem_no_delivery").html(commaSeparateNumber(data.items.countOfNoDelivery));
                ////TEST GRAPH BARR
                var color = [
               "#f1948a",
               "#bb8fce",
               "#85c1e9",
               "#73c6b6",
               "#82e0aa",
               "#f8c471",
               "#e59866",
               "#d7dbdd",
               "#b2babb",
               "#808b96",
               "#FFFF33",
               "#918ef1",
               "#f49fd2",
               "#8ef292",
               "#FF6699",
               "#FF9999",
               "#FFCC99",
               "#FFFF99",
               "#FFFFCC"
                ];

                label_delivery = ['1-15', '15-30', '31-45', '45-60', '>60'];
                data_delivery = [data.items.count_of_15, data.items.count_of_30, data.items.count_of_45, data.items.count_of_60, data.items.count_of_61];
                data_no_delivery = [data.items.no_of_15, data.items.no_of_30, data.items.no_of_45, data.items.no_of_60, data.items.no_of_61];

                //// GRAPH CAT POINT ////
                createChart(label_delivery, data_delivery, 'delivery', 'pie', {
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: true,
                        fontSize: 18,
                        text: ['จัดส่งแล้ว : ' + commaSeparateNumber(data.items.countOfDelivery) + ' รายการ', ' ระยะเวลาการจัดส่ง(วัน)'],
                    },
                    legend: {
                        display: true,

                    },
                    plugins: {
                        labels: [
                      {
                          fontColor: "black",
                          render: function (args) {
                              //return args.value;
                              var value = args.value;
                              value = value.toString();
                              value = value.split(/(?=(?:...)*$)/);
                              value = value.join(',');
                              return value;
                          },
                          arc: false
                      }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var value = data.datasets[0].data[tooltipItem.index];
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return value;
                            }
                        } // end callbacks:
                    } //end tooltips
                }); //// GRAPH CAT POINT ////

                //// GRAPH CAT PRIZE REWARD ////
                createChart(label_delivery, data_no_delivery, 'no_delivery', 'pie', {
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: true,
                        fontSize: 18,
                        text: ['รอการจัดส่ง : ' + commaSeparateNumber(data.items.countOfNoDelivery) + ' รายการ', 'ระยะเวลานับจากวันที่แลกถึงปัจจุบัน(วัน)']
                    },
                    legend: {
                        display: true,

                    },
                    plugins: {
                        labels: [
                      {
                          fontColor: "black",
                          render: function (args) {
                              //return args.value;
                              var value = args.value;
                              value = value.toString();
                              value = value.split(/(?=(?:...)*$)/);
                              value = value.join(',');
                              return value;
                          },
                          arc: false
                      }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var value = data.datasets[0].data[tooltipItem.index];
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return value;
                            }
                        } // end callbacks:
                    } //end tooltips
                }); //// GRAPH CAT PRIZE REWARD ////
                closeLoading();
            } //success
        }); //ajax
    }); // on click view data

}); //document ready

function createChart(label, val, id, type, options) {
    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var coloR = [];
    for (var i in val) {
        coloR.push(dynamicColors());
    }
    var color = [
               "#f1948a",
               "#bb8fce",
               "#85c1e9",
               "#73c6b6",
               "#82e0aa",
               "#f8c471",
               "#e59866",
               "#d7dbdd",
               "#b2babb",
               "#808b96",
               "#FFFF33",
               "#918ef1",
               "#f49fd2",
               "#8ef292",
               "#FF6699",
               "#FF9999",
               "#FFCC99",
               "#FFFF99",
               "#FFFFCC"
    ]
    var data = {
        //labels: ['January', 'February', 'March'],
        labels: label,
        datasets: [
          {
              label: 'จำนวน',
              //data: [50445, 33655, 15900],
              data: val,
              backgroundColor: color,
              //backgroundColor: [
              //  '#FF6384',
              //  '#36A2EB',
              //  '#FFCE56'
              //]
          }
        ]
    };
    new Chart(document.getElementById(id), {
        type: type,
        data: data,
        options: options
    });
    Chart.defaults.global.defaultFontColor = '#000';
}// create