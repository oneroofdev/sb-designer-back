﻿var frm_validate = "";
$(document).ready(function () {


    $("#bt_Clear").on("click", function () {
        $("#date_from").val('');
        $("#date_to").val('');
        $("#vip").val('').change();
        $("#verifyData").val('').change();
    });
    $('#tb_regis_detail').DataTable({
        'columnDefs': [
                {
                    "targets": 0,
                    "type": "date-uk"
                },
               {
                   "targets": 4,
                   "className": "text-center",
               },
               {
                   "targets": 5,
                   "className": "text-right",
               }
        ]
    });
    buildTB_Register(0);
    $("#bt_Search").on("click", function () {
        buildTB_Register(1);
    });
    $("#bt_Export").on("click", function () {
        frm_validate_call = "";
        var vtxt_date_f = validate_from($("#date_from"), $("#vtxt_datefrom"), "Please insert Date From.");
        var vtxt_date_t = validate_from($("#date_to"), $("#vtxt_dateto"), "Please insert Date To.");
        var date_s = ""; var date_e = "";
        if ($('#date_from').val() == "" && $('#date_to').val() == "") {
            date_s = '01-01-2018';
            date_e = '01-12-2020';
        } else {
            //convert date format
            date_s = $('#date_from').val();
            date_e = $('#date_to').val();
            /* date_s = date_s[1] + '-' + date_s[0] + '-' + date_s[2];
             date_e = date_e[1] + '-' + date_e[0] + '-' + date_e[2];*/
        }
        //download excel
        window.location = baseUrl + "RegisterReport/ExportExcel?CustText=&StartDate=" + date_s + "&EndDate=" + date_e + "&IS_Verify=" + $('#verifyData').val() + "&IS_VIP=" + $('#vip').val();
    });
    $('#tb_regis_total').DataTable();
    $('#tb_regis_detail').DataTable();

    $("#tb_regis_total").on("click", ".dtl_print", function () {
        var data_date = $(this).attr('data_print');
        window.location = baseUrl + "RegisterReport/ExportExcel?CustText=&StartDate=" + data_date + "&EndDate=" + data_date;
    });

    $("#tb_regis_detail").on("click", ".dtl_vq", function () {
        var data_date = $(this).attr('cust_id');
        window.location = "http://sbvisualquestion.com:8080/profile/editprofile?custId=" + btoa(data_date);
        //window.location = baseUrl + "Report_Register/call_ExportExcel?CustText=&StartDate=" + data_date + "&EndDate=" + data_date;
    });

    $("#tb_regis_detail").on("click", ".verify", function () {
        var custID = $(this).attr('custID');
        var name = $(this).attr('name');
        $("#CustIDVerify").val(custID);
        $("#txtName").text(name);


        $('#myModalConfirmDesigner').modal('show');
        return false;
    });

    $("#tb_regis_detail").on("click", ".dtl_regis", function () {
        var data_mobile = $(this).attr('data_regis');
        var data_date = $(this).parents("tr").children("td").eq(1).children("span").text();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "RegisterReport/getDataMember",
            data: { CustText: data_mobile, StartDate: data_date, EndDate: data_date },
            success: function (data) {
                var head = '', rows = '';
                var l = 1;
                if (data != "") {

                    $.each($(data.items), function (key, value) {
                        //for (var j = data.length - 1; j >= 0; j--) {
                        var Name = value.Title_Name + value.Name + " " + value.Surname;
                        //var addr = value.Addrno + " " + value.Moo + " " + value.Soi + " " + value.Road + " " + value.Tambol + " " + value.Amphur + " " + value.Province;
                        var cols = '';
                        var bd = value.Birthday
                        if (bd != null) { bd = SubstringDate(value.Birthday); }
                        var text = "";
                        switch (value.Occupation) {
                            case "D":
                                text = "มัณฑนากร";
                                break;
                            case "A":
                                text = "สถาปนิก";
                                break;
                            case "S":
                                text = "สไตล์ลิส";
                                break;
                            default:
                                text = value.Occupation_other;
                        }
                        var Verify_Date = "";
                        if (value.Verify_Date != null) { Verify_Date = SubstringDate(value.Verify_Date); }
                        $('#tax_name').text(Name);
                        $('#txt_card').text(value.Citizen_ID);
                        $('#tax_Occupation').text(text);
                        $('#txt_gender').text(value.Gender == 'M' ? 'ชาย' : 'หญิง');
                        $('#txt_birthday').text(bd);
                        $('#txt_mobile').text(value.Mobile_Phone);
                        $('#txt_email').text(value.Email);
                        $('#txt_line').text(value.Line_ID);
                        $('#txt_regis').text(SubstringDate(value.Create_Date));
                        $('#txt_recomend').text(value.Recommend_By);
                        $('#txt_verify_date').text(SubstringDate(value.Create_Date));
                        $('#txt_verify_by').text(value.Verify_By);
                        $('#txt_verify_date').text(Verify_Date);


                        $('#myModalRegis').modal('show');

                    });

                }
            },
        });
    });
    $("#tb_regis_detail").on("click", ".dtl_print", function () {
        var data_mobile = $(this).attr('data_print');
        var data_date = $(this).parents("tr").children("td").eq(1).children("span").text();
        window.location = baseUrl + "RegisterReport/ExportExcel?CustText=" + data_mobile + "&StartDate=" + data_date + "&EndDate=" + data_date;
    });
    /* 
    $('.radioVerify').on('ifChanged', function (event) {
        alert($('input[name="verify"]:checked').val());
    });*/
    /* $('.radioVerify').on('ifChecked', function (event) {
         var data = $(this).val(); // alert value
         if (data == "Y") {
             $("#reasonNoVerify").prop("disabled", true);
         } else {
             $("#reasonNoVerify").prop("disabled", false);
         }
     });*/
    $('#verify').on('change', function (event) {
        var data = $(this).val(); // alert value
        if (data != "N") {
            $("#reasonNoVerify").prop("disabled", true);
        } else {
            $("#reasonNoVerify").prop("disabled", false);
        }
    });
    $("#saveVerify").on("click", function () {

        var verify = $('input[name="verify"]:checked').val();
        var reasonNoVerify = $("#reasonNoVerify").val();
        var verify_date = $("#verify_date").val();

        var verify_date_new = $('#verify_date').val().split('-');
        if (verify_date == "") {
            alert("กรุณาระบุวันที่");
        } else if (verify == "N" && reasonNoVerify == "") {
            alert("กรุณาระบุเหตุผล");
        } else {
            $("#myModalConfirmDesigner").modal("hide");

            var post = {
                Cust_ID: $("#CustIDVerify").val(),
                verify_date: verify_date_new[2] + '-' + verify_date_new[1] + '-' + verify_date_new[0],
                reason: reasonNoVerify,
                IS_Verify: $('#verify').val()
                // IS_Verify: $('input[name="verify"]:checked').val()
            }

            $.post(baseUrl + 'RegisterReport/VerifyMember', post, function (data) {
                if (data.status == "True") {
                    alert("บันทึกข้อมูลเรียบร้อย");
                    buildTB_Register(0);
                    $("#myModalConfirmDesigner").modal("hide");
                    return false;
                } else {
                    alert("Error =>" + data.description);
                    return false;
                }
            });
        }
    });

});
function buildTB_Register(n) {
    $('#tb_regis_total').dataTable().fnClearTable();
    $('#tb_regis_detail').dataTable().fnClearTable();
    var date_s = '01-01-2018';
    var date_e = '01-12-2020';
    if ($('#date_from').val() != "" && $('#date_to').val() != "") {
        date_s = $('#date_from').val();
        date_e = $('#date_to').val();
    }

    showLoading();
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: baseUrl + "RegisterReport/getDataMember",
        data: { CustText: '', StartDate: date_s, EndDate: date_e, IS_Verify: $('#verifyData').val(), IS_VIP: $('#vip').val() },
        success: function (data) {
            //console.log(data);   
            var head = '', rows = '';
            var c = 1;
            var l = 1;
            if (data != "") {
                var cc = 0;
                var c_date = "";
                var totalRegis = 0;
                var totalWaitRegis = 0;
                var totalPassRegis = 0;
                var totalNoPassRegis = 0;

                var totalNoContact = 0;
                var totalNoPhone = 0;
                var totalWaitPort = 0;
                //var detail = JSON.parse(data);
                //data = data["CustomerDetail"];


                //var a = data;
                var tmpDate = "";
                var verify = 0;
                var unVerify = 0;
                var waitVerify = 0;
                var noContact = 0;
                var noPhone = 0;
                var waitPort = 0;
                var tmp = "";
                $.each($(data.items), function (key, value) {
                    if (tmpDate == "") {
                        c_date = SubstringDate(value.Create_Date);
                        tmpDate = 1;
                    }
                    //c_date = SubstringDate(value.Create_Date);
                    if (c_date == SubstringDate(value.Create_Date)) {
                        cc++;
                        /*  if (value.IS_Verify != "" && value.Member_Type == "D") {
                              verify++
                          } else if (value.IS_Verify != "" && value.Member_Type == "F") {
                              unVerify++;
                          } else {
                              waitVerify++;
                          }*/
                        if (value.IS_Verify == "Y") {
                            verify++
                        } else if (value.IS_Verify == "N") {
                            unVerify++;
                        } else if (value.IS_Verify == "W") {
                            waitPort++;
                        } else if (value.IS_Verify == "P") {
                            noPhone++;
                        } else if (value.IS_Verify == "C") {
                            noContact++;
                        } else {
                            waitVerify++;
                        }
                    } else {
                        var iconhead =
                           '<a class="dtl_print" data_print="' + c_date +
                           '" style="cursor: pointer;"><span class="fa fa-print" style="font-size:25px;"></span></a>'
                        var tbtotal = $('#tb_regis_total').dataTable().fnAddData([
                            c,
                            c_date,
                            cc,
                            waitVerify,
                            verify,
                            unVerify,
                            waitPort,
                            noPhone,
                            noContact,
                            iconhead
                        ]);
                        totalRegis = totalRegis + cc;

                        totalWaitRegis = totalWaitRegis + waitVerify;
                        totalPassRegis = totalPassRegis + verify;
                        totalNoPassRegis = totalNoPassRegis + unVerify;

                        totalNoContact = totalNoContact + noContact;
                        totalNoPhone = totalNoPhone + noPhone;
                        totalWaitPort = totalWaitPort + waitPort;
                        c++;
                        cc = 1;


                        verify = 0;
                        unVerify = 0;
                        waitVerify = 0;

                        noContact = 0;
                        noPhone = 0;
                        waitPort = 0;
                        /*
                        if (value.IS_Verify != "" && value.Member_Type == "D") {
                            verify++
                        } else if (value.IS_Verify != "" && value.Member_Type == "F") {
                            unVerify++;
                        } else {
                            waitVerify++;
                        }*/

                        if (value.IS_Verify == "Y") {
                            verify++
                        } else if (value.IS_Verify == "N") {
                            unVerify++;
                        } else if (value.IS_Verify == "W") {
                            waitPort++;
                        } else if (value.IS_Verify == "P") {
                            noPhone++;
                        } else if (value.IS_Verify == "C") {
                            noContact++;
                        } else {
                            waitVerify++;
                        }
                        c_date = SubstringDate(value.Create_Date);
                    }
                    var Name = value.Name + " " + value.Surname;
                    var addr = value.Addrno + " " + value.Moo + " " + value.Soi + " " + value.Road + " " + value.Tambol + " " + value.Amphur + " " + value.Province;
                    var icondetail = '<a class="dtl_regis" data_regis="' + value.Mobile_Phone +
                        '" style="cursor: pointer;"><span class="fa fa-file" style="font-size:25px;"></span></a> ' +
                        '<a class="dtl_print" data_print="' + value.Mobile_Phone +
                        '" style="cursor: pointer;"><span class="fa fa-print" style="font-size:25px;"></span></a>' +
                         '<a class="dtl_vq" cust_id="' + value.Cust_ID +
                        '" style="cursor: pointer;"><span class="fa fa-search" style="font-size:25px;"></span></a>';
                    var cols = '';
                    var bd = value.Birthday;
                    var IS_Verify = "<a href='#' class='verify' name = '" + Name + "'  custID='" + value.Cust_ID + "'  >ยังไม่ตรวจสอบ</a>";
                    if (value.IS_Verify == 'Y') {
                        var IS_Verify = "ผ่านการตรวจสอบ";
                    } else if (value.IS_Verify == 'N') {
                        var IS_Verify = "ไม่ผ่านการตรวจสอบ";
                    } else if (value.IS_Verify == 'W') {
                        var IS_Verify = "<a href='#' class='verify' name = '" + Name + "'  custID='" + value.Cust_ID + "'  >รอส่งผลงาน</a>";
                    } else if (value.IS_Verify == 'C') {
                        var IS_Verify = "<a href='#' class='verify' name = '" + Name + "'  custID='" + value.Cust_ID + "'  >ติดต่อไม่ได้</a>";
                    } else if (value.IS_Verify == 'P') {
                        var IS_Verify = "<a href='#' class='verify' name = '" + Name + "'  custID='" + value.Cust_ID + "'  >เบอร์โทรผิด</a>";
                    }

                    var IS_VIP = "";
                    var Verify_Date = "";
                    if (value.IS_VIP == 'Y') {
                        IS_VIP = "VIP";
                    } else if (value.IS_52Week == 'Y') {
                        IS_VIP = "52 Week";
                    } else {
                        IS_VIP = "ปกติ";
                    }
                    if (bd != null) { bd = SubstringDate(value.Birthday); }
                    if (value.Verify_Date != null) { Verify_Date = SubstringDate(value.Verify_Date); }
                    var text = "";
                    switch (value.Occupation) {
                        case "D":
                            text = "มัณฑนากร";
                            break;
                        case "A":
                            text = "สถาปนิก";
                            break;
                        case "S":
                            text = "สไตล์ลิส";
                            break;
                        default:
                            text = value.Occupation_other;
                    }
                    $('#tb_regis_detail').dataTable().fnAddData([
                        l,
                        '<span>' + SubstringDate(value.Create_Date) + '</span>',
                        Name,
                        text,
                        //value.Gender == 'M' ? 'ชาย' : 'หญิง',
                        value.Mobile_Phone,
                        (value.Balance == "0" || value.Balance == "" || value.Balance == "null" || value.Balance == null) ? "-" : commaSeparateNumber(value.Balance),
                        IS_VIP,
                        value.Branch_Apply == 'B' ? 'บางนา กม.4' : value.Branch_Apply == 'R' ? 'เดอะคริสตัล เอสบี ราชพฤกษ' : value.Branch_Apply == 'C' ? 'คริสตัล ดีไซน์ เซ้นเตอร์' : value.Branch_Apply == 'P' ? 'ภูเก็ต' : "-",
                        IS_Verify,
                        Verify_Date,
                        value.referMember,
                        //value.Member_Type == 'D' ? 'Designer' : 'Customer',
                        icondetail
                    ]);
                    l++;
                });
                /*for (var j = data.length - 1; j >= 0; j--) {
                    if (j == data.length - 1) {
                        c_date = SubstringDate(data[j]["Create_Date"]);
                    }
                    if (c_date == SubstringDate(data[j]["Create_Date"])) {
                        cc++;
                    } else {
                        var iconhead =
                            '<a class="dtl_print" data_print="' + c_date +
                            '" style="cursor: pointer;"><span class="fa fa-print" style="font-size:25px;"></span></a>'
                        var tbtotal = $('#tb_regis_total').dataTable().fnAddData([
                            c,
                            c_date,
                            cc,
                            cc,
                            cc,
                            iconhead
                        ]);
                        totalRegis = totalRegis + cc;
                        c++;
                        cc = 1;
                        c_date = SubstringDate(data[j]["Create_Date"]);
                    }
                    var Name = data[j]["Title_Name"] + data[j]["Name"] + " " + data[j]["Surname"];
                    var addr = data[j]["Addrno"] + " " + data[j]["Moo"] + " " + data[j]["Soi"] + " " + data[j]["Road"] + " " + data[j]["Tambol"] + " " + data[j]["Amphur"] + " " + data[j]["Province"];
                    var icondetail = '<a class="dtl_regis" data_regis="' + data[j]["Mobile_Phone"] +
                        '" style="cursor: pointer;"><span class="fa fa-file" style="font-size:25px;"></span></a> ' +
                        '<a class="dtl_print" data_print="' + data[j]["Mobile_Phone"] +
                        '" style="cursor: pointer;"><span class="fa fa-print" style="font-size:25px;"></span></a>'
                    var cols = '';
                    var bd = data[j]["Birthday"];
                    var IS_Verify = "<a href='#' class='verify' name = '" + Name + "'  custID='" + data[j]["Cust_ID"] + "'  >ยังไม่ตรวจสอบ</a>";
                    if (data[j]["IS_Verify"] == 'Y') {
                        var IS_Verify = "ผ่านการตรวจสอบ";
                    } else if (data[j]["IS_Verify"] == 'N') {
                        var IS_Verify = "ไม่ผ่านการตรวจสอบ";
                    }
                    if (bd != null) { bd = SubstringDate(data[j]["Birthday"]); }
                    var text = "";
                    switch (data[j]["Occupation"]) {
                        case "D":
                            text = "มัณฑนากร";
                            break;
                        case "A":
                            text = "สถาปนิก";
                            break;
                        case "S":
                            text = "สไตล์ลิส";
                            break;
                        default:
                            text = data[j]["Occupation_other"];
                    }
                    $('#tb_regis_detail').dataTable().fnAddData([
                        l,
                        '<span>' + SubstringDate(data[j]["Create_Date"]) + '</span>',
                        Name,
                        text,
                        data[j]["Gender"] == 'M' ? 'ชาย' : 'หญิง',
                        data[j]["Mobile_Phone"],
                        bd,
                        IS_Verify,
                        data[j]["Member_Type"] == 'D' ? 'Designer' : 'Customer',
                        icondetail
                    ]);
                    l++;
                }*/
                var iconhead2 =
                    '<a class="dtl_regis" data_print="' + c_date +
                    '" style="cursor: pointer;"><span class="fa fa-print" style="font-size:25px;"></span></a>'
                $('#tb_regis_total').dataTable().fnAddData([
                    c,
                    c_date,
                    cc,
                    waitVerify,
                    verify,
                    unVerify,
                    waitPort,
                    noPhone,
                    noContact,
                    iconhead2
                ]);
                totalRegis = totalRegis + cc;

                totalWaitRegis = totalWaitRegis + waitVerify;
                totalPassRegis = totalPassRegis + verify;
                totalNoPassRegis = totalNoPassRegis + unVerify;

                totalNoContact = totalNoContact + noContact;
                totalNoPhone = totalNoPhone + noPhone;
                totalWaitPort = totalWaitPort + waitPort;

                $('#txt_total').text(data.length);
                $('#totalRegis').val(totalRegis);
                $('#totalPassRegis').val(totalPassRegis);
                $('#totalWaitRegis').val(totalWaitRegis);
                $('#totalNoPassRegis').val(totalNoPassRegis);

                $('#totalNoContactRegis').val(totalNoContact);
                $('#totalPhoneRegis').val(totalNoPhone);
                $('#totalWaitPortRegis').val(totalWaitPort);

                $('#totalRegisDetail').val(totalRegis);
                $('#totalPassRegisDetail').val(totalPassRegis);
                $('#totalWaitRegisDetail').val(totalWaitRegis);
                $('#totalNoPassRegisDetail').val(totalNoPassRegis);

                $('#totalNoContactRegisDetail').val(totalNoContact);
                $('#totalPhoneRegisDetail').val(totalNoPhone);
                $('#totalWaitPortRegisDetail').val(totalWaitPort);
            } else {
                $('#tbody_regis_total').html('<tr><td class="text-center" data-title="Data" colspan="4"> -- ไม่มีข้อมูล -- </td></tr>');
                $('#tbody_regis_detail').html('<tr><td class="text-center" data-title="Data" colspan="8"> -- ไม่มีข้อมูล -- </td></tr>');
            }
            closeLoading();
        },
    });

}
function validate_from(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        frm_validate += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}

function SubstringDate(date) {
    date = date.substring(0, 10);
    return date.substring(8, 10) + "-" + date.substring(5, 7) + "-" + date.substring(0, 4)
}

function commaSeparateNumber(val) {
    var output = numeral(val).format('0,0');
    return output;
}