﻿$(document).ready(function () {
    getBudget();
    var type_report = $("#ddl_type_report option").html();
    $('#txt_reportname').val(type_report);

    $("#ddl_type_report").on('change', function () {
        type_report = $('#ddl_type_report option:selected').html();
        $('#txt_reportname').val(type_report);
    });

    if ($("#budget_id").val() != "0" && $("#budget_id").val() != 'null' && $("#budget_id").val() != "") {
        $("#round_num").val($("#budget_id").val());
    } else {
        $("#round_num").val($("#round_num option:first").val()); //set round num lastest
    }

    $("#bt_ExportExcel").on('click', function () {

        var data_period = $("#round_num").find(':selected').attr('data-period');
        if ($("#ddl_type_report").val() == '1') {
            window.location = baseUrl + '/ReportBudget/ReportBudgetSummaryExcel?&report_h=' + $('#txt_reportname').val() + '&round_num=' + $("#round_num").find(':selected').attr('data-round') + '&period=' + data_period;
        }
        if ($("#ddl_type_report").val() == '2') {
            window.location = baseUrl + '/ReportBudget/ReportBudgetDetailExcel?&report_h=' + $('#txt_reportname').val() + '&round_num=' + $("#round_num").find(':selected').attr('data-round') + '&period=' + data_period;
        }
    }); // on click export excel

    $("#bt_ViewData").on('click', function () {
        //alert("AAA");
        showLoading();
        var data_period = $("#round_num").find(':selected').attr('data-period');
        $.ajax({
            url: baseUrl + '/ReportBudget/GetDataReportBudgetGraph',
            headers: {
                'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
            },
            method: 'POST',
            dataType: 'json',
            data: {
                budget_id: $("#round_num").find(':selected').attr('data-round')
            },
            success: function (data) {
                console.log(data.items);
                $("#graph").hide();
                ////TEST GRAPH BARR
                var color = [
               "#f1948a",
               "#bb8fce",
               "#85c1e9",
               "#73c6b6",
               "#82e0aa",
               "#f8c471",
               "#e59866",
               "#d7dbdd",
               "#b2babb",
               "#808b96",
               "#FFFF33",
               "#918ef1",
               "#f49fd2",
               "#8ef292",
               "#FF6699",
               "#FF9999",
               "#FFCC99",
               "#FFFF99",
               "#FFFFCC"
                ];
                if (data.items.countOfPoint > 0) {
                    $("#graph").show();
                    var data_labels = [];
                    var data_point = [];
                    var data_price = [];

                    $.each(data.items.DataReport, function (index, value) {
                        data_labels.push(value.cat_name);
                        data_point.push(value.point_redeem);
                        data_price.push(value.reward_price);
                    });

                    //// GRAPH CAT POINT ////
                    createChart(data_labels, data_point, 'cat_point', 'pie', {
                        responsive: true,
                        maintainAspectRatio: false,
                        title: {
                            display: true,
                            fontSize: 18,
                            text: 'คะแนนที่มีการแลก (แยกตามประเภทของรางวัล)'
                        },
                        legend: {
                            display: true,

                        },
                        plugins: {
                            labels: [
                          {
                              fontColor: "black",
                              render: function (args) {
                                  //return args.value;
                                  var value = args.value;
                                  value = value.toString();
                                  value = value.split(/(?=(?:...)*$)/);
                                  value = value.join(',');
                                  return value;
                              },
                              arc: false
                          }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    var value = data.datasets[0].data[tooltipItem.index];
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    value = value.join(',');
                                    return value;
                                }
                            } // end callbacks:
                        } //end tooltips
                    }); //// GRAPH CAT POINT ////

                } // ถ้ามี Data ในช่วงที่เลือก จะสร้าง graph
                closeLoading();
            } //success
        }); //ajax
    }); // on click view data graph

}); //document ready

function getBudget() {
    $.ajax({
        url: baseUrl + '/ReportBudget/GetListReportBudget',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        data: {
            fromType: "B",
            budget_id: "A"
        },
        success: function (data) {
            console.log(data.items.AllBudget);
            var i = 1;
            $.each(data.items.AllBudget, function (index, value) {
                i++;
                var budget = value.BUDGET_ID + " (" + value.PERIOD_REDEEM_FROM + " - " + value.PERIOD_REDEEM_TO + ")"
                $("#round_num").append("<option value='" + value.BUDGET_ID + "' data-i='" + i + "' data-round='" + value.BUDGET_ID + "' data-period='" + value.PERIOD_REDEEM_FROM + " - " + value.PERIOD_REDEEM_TO + "'>" + budget + "</option>")
            }); //each budget in options

            if ($("#budget_id").val() == 0 || $("#budget_id").val() == 'null' || $("#budget_id").val() == "") {
                $("#round_num").val($("#round_num option:first").val()); //set round num lastest
            } else {
                $("#round_num").val($("#budget_id").val()); //set round num from generate budget
            }
        }
    });

} //get list budget

function createChart(label, val, id, type, options) {
    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var coloR = [];
    for (var i in val) {
        coloR.push(dynamicColors());
    }
    var color = [
               "#f1948a",
               "#bb8fce",
               "#85c1e9",
               "#73c6b6",
               "#82e0aa",
               "#f8c471",
               "#e59866",
               "#d7dbdd",
               "#b2babb",
               "#808b96",
               "#FFFF33",
               "#918ef1",
               "#f49fd2",
               "#8ef292",
               "#FF6699",
               "#FF9999",
               "#FFCC99",
               "#FFFF99",
               "#FFFFCC"
    ]
    var data = {
        //labels: ['January', 'February', 'March'],
        labels: label,
        datasets: [
          {
              label: 'จำนวน',
              //data: [50445, 33655, 15900],
              data: val,
              backgroundColor: color,
              //backgroundColor: [
              //  '#FF6384',
              //  '#36A2EB',
              //  '#FFCE56'
              //]
          }
        ]
    };
    new Chart(document.getElementById(id), {
        type: type,
        data: data,
        options: options
    });
    Chart.defaults.global.defaultFontColor = '#000';
}

function createCharts(label, val, id, type, colors, options) {

    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var coloR = [];
    for (var i in val) {
        coloR.push(dynamicColors());
    }
    var data = {
        //labels: ['January', 'February', 'March'],
        labels: label,
        datasets: [
          {
              label: 'จำนวน',
              //data: [50445, 33655, 15900],
              data: val,
              backgroundColor: colors,
              //backgroundColor: [
              //  '#FF6384',
              //  '#36A2EB',
              //  '#FFCE56'
              //]
          }
        ]
    };
    new Chart(document.getElementById(id), {
        type: type,
        data: data,
        options: options
    });
    Chart.defaults.global.defaultFontColor = '#000';
}