﻿$(document).ready(function () {

    $('.date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    $("#bt_Clear").on("click", function () {
        $("#date_from").val('');
        $("#date_to").val('');
    });

    var table = $('#tbl_user_login').DataTable({
        "ajax": {
            'type': 'POST',
            'url': '/ReferReport/GetDetail',
            'dataSrc': "",
            //'data': {
            //    start_date: "01/07/2018",
            //    end_date: "31/08/2018",
            //}
            'data': function (d) {
                let startDate = null;
                let endDate = null;
                if ($('#date_from').val()) {
                    //let start_split = $('#date_from').val().split('/');
                    //startDate = start_split[1] + '/' + start_split[0] + '/' + start_split[2];
                    startDate = $('#date_from').val();
                }
                if ($('#date_to').val()) {
                    /*let end_split = $('#date_to').val().split('/');
                    endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];*/
                    endDate = $('#date_to').val();
                }
                d.start_date = startDate;
                d.end_date = endDate;
            }
        },
        "columns": [
            { data: null },
            {
                data: 'Ref_Date',
                render: function (data, type, row) {
                    let timestamp = data.split(' ');
                    let date = timestamp[0].split('/');
                    return timestamp[0];
                },
            },
            { data: 'CustID' },
            { data: 'Name' },
            {
                data: 'VIP',
                render: function (data, type, row) {
                    let text = '';
                    if (data) {
                        text = 'VIP'
                    }
                    return text;
                },
            },
            { data: 'Cust_Phone' },
            { data: 'Cust_Email' },
            { data: 'CustSapCode' },
            { data: 'Ref_Name' },
            { data: 'Ref_Sap_Code' },
            { data: 'Ref_Mobile_1' },
            { data: 'Ref_Email' },
            { data: 'Ref_Mobile_2' },
            { data: 'Status' },
            { data: 'Ref_Total_Buy' },
            { data: 'Ref_Reason' },
        ],
        "columnDefs": [
            { type: 'date-th', targets: 1 }
        ],
        "order": [[1, "desc"]]
    });

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "date-th-pre": function (a) {
            if (a == null || a == "") {
                return 0;
            }
            var thDatea = a.split('/');
            return (thDatea[2] + thDatea[1] + thDatea[0]) * 1;
        },

        "date-th-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "dateth-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });

    $('#bt_Search').on('click', function () {
        table.ajax.reload();
    });

    $("#bt_Export").on("click", function () {
        let startDate = '';
        let endDate = '';
        if ($('#date_from').val()) {
            //let start_split = $('#date_from').val().split('/');
            //startDate = start_split[1] + '/' + start_split[0] + '/' + start_split[2]; 
            startDate = $('#date_from').val();
        }
        if ($('#date_to').val()) {
            //let end_split = $('#date_to').val().split('/');
            //endDate = end_split[1] + '/' + end_split[0] + '/' + end_split[2];
            endDate = $('#date_to').val();
        } 
        window.location = baseUrl + "ReferReport/ExportExcel?start_date=" + startDate + "&end_date=" + endDate;
    });

});