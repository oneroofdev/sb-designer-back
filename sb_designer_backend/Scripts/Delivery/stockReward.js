﻿$(document).ready(function () {
    var tblDataReward = $('#tblDataReward').dataTable({
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        "aaSorting": [[1, 'asc']],
        "pagingType": "full_numbers"
    });

    getCourier();
    function getCourier() {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "Delivery/getRewardCourier",
            data: {
                rewardType: ""
            },
            success: function (data) {
                $.each(data.items, function (key, value) {
                    $("#editCourier").append('<option value="' + value.COURIER_CODE + '" > ' + value.COURIER_NAME + '</option>');
                });
            },
        });
    }
    searchData();
    $('#tblDataReward').on('click', 'tr', function () {
        $("#tblDataReward tr").css('background-color', "#f5f5f5");
        $("#tblDataReward tr").css('color', "#7a878e");
        $(this).css('background-color', "#25476a");
        $(this).css('color', "#f5f5f5");
    });

    $('#tblDataRedeem').on('click', 'tr', function () {
        $("#tblDataRedeem tr").css('background-color', "#f5f5f5");
        $("#tblDataRedeem tr").css('color', "#7a878e");
        $(this).css('background-color', "#25476a");
        $(this).css('color', "#f5f5f5");
    });

    $("#btn_search_Reward").on("click", function () {
        //$.showLoading({ name: 'line-scale', allowHide: false });
        searchData();
    });
    function searchData() {
        showLoading();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "Delivery/getDataRedeemSearch",
            data: {
                rewardType: $("#rewardType").val()
            },
            success: function (data) {
                // $.hideLoading();

                var i = 0;
                $("#tblDataReward").dataTable().fnClearTable();
                $('#tblDataRedeem tbody').empty();
                $.each(data.items, function (key, value) {
                    i++;
                    //$('#tblDataReward  tbody').append('<tr><td style="text-align:center;">' + i + '</td><td>' + value.reward_id + '</td><td>' + value.reward_name_en + '</td><td style="text-align:right;">' + addCommas(value.totReward) + '</td><td style="text-align:right;">' + addCommas(value.totRedeem) + '</td><td style="text-align:right;"></td><td class="text-center"><button type="button" rewardId ="' + value.reward_id + '" class="btn  btn-raised btn-sm btn-danger redeemDetail">Redeem</button></td></tr>');

                    $('#tblDataReward').dataTable().fnAddData([
			            i,
			            value.REWARD_CODE,
			            '<span class="pull-left">' + value.NAME_TH + '</span>',
			            '<span class="pull-right">' + addCommas(value.TOT_REWARD) + '</span>',
			            '<span class="pull-right">' + addCommas(value.TOT_REDEEM) + '</span>',
			            '<span class="pull-right" style="color:blue;"></span>',
			            '<button type="button" rewardqty = "' + value.TOT_REWARD + '" rewardId ="' + value.REWARD_CODE + '" class="btn  btn-raised btn-sm btn-danger redeemDetail text-center">รายการแลก</button>'
                    ]);
                });
                closeLoading();
            },
        });
    }
    $("#tblDataReward tbody").on("click", ".redeemDetail", function () {
        //$.showLoading({ name: 'line-scale', allowHide: false }); 
        var rewardCode = $(this).attr("rewardId");
        var rewardqty = $(this).attr("rewardqty")
        $("#totalStockCal").val(rewardqty);
        $(".chkRedeemAll").prop('checked', false);

        $("#tblDataReward tbody tr").each(function () {
            $(this).children().eq(5).text('');
        });
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "Delivery/getDataRedeem",
            data: {
                rewardCode: rewardCode
            },
            success: function (data) {
                // $.hideLoading();
                $('#tblDataRedeem tbody').empty();
                var totalShop = 0;
                var totalReward = 0;
                var Shop = [];
                $.each(data.items, function (key, value) { 
                    var date = new Date(parseInt(value.REDEEM_DATE.replace("/Date(", "").replace(")/", "")));
                    //REDEEM_DATE = addZeroBefore(date.getDate()) + '/' + addZeroBefore(parseInt(date.getMonth()) + 1) + '/' + date.getFullYear();
                    REDEEM_DATE = value.REDEEM_DATE.substring(8, 10) + "-" + value.REDEEM_DATE.substring(5, 7) + "-" + value.REDEEM_DATE.substring(0, 4);
                    /*var agentcode;
                    var agentname;
                    if (value.delivery_by == "BRT") {
                        agentcode = value.agent_code;
                        agentname = value.agent_name;
                    } else {
                        agentcode = "";
                        agentname = "";
                    }*/
                    $('#tblDataRedeem  tbody').append('<tr><td><input type="checkbox"  class="chkRedeem" /></td><td data-title="Redeem No" style="height:40px;"><span class="pull-center">' + value.REDEEM_NO + '</span> </td><td data-title="Redeem Date" style="height:40px;"><span class="pull-center">' + REDEEM_DATE + '</span> </td><td data-title="Member ID" style="height:40px;"><span class="pull-center">' + value.MEMBER_ID + '</span> </td><td data-title="Member Name" style="height:40px;"><span class="pull-left">' + value.MEMBER_NAME + '</span> </td><td data-title="Reward Code" style="height:40px;"><span class="pull-left">' + value.REWARD_CODE + '</span> </td><td data-title="Reward Name" style="height:40px;"><span class="pull-left">' + value.REWARD_NAME + '</span> </td><td data-title="Qty" style="height:40px;"><span class="pull-right">' + addCommas(value.QTY) + '</span> </td><td data-title="Delivey by" style="height:40px;"><input type="hidden" value = "' + value.DELIVERY_BY + '"><span class="pull-left">' + value.DELIVERY_BY + '</span> </td> <td data-title="Edit" style="height:40px;"><button class="btn btn-md btn-warning editCOURIER" data-member-id="' + value.MEMBER_ID + '" data-shop-name="' + value.MEMBER_NAME + '" data-delivery="' + value.DELIVERY_BY + '" data-reward-code="' + value.REWARD_CODE + '"  data-reward-name="' + value.REWARD_NAME + '" data-redeem-no="' + value.REDEEM_NO + '"  "><i class="fa fa-pencil"></i> แก้ไข</button> </td></tr>');
                    if (jQuery.inArray(value.MEMBER_ID, Shop)) {
                        totalShop += 1;
                    }
                    Shop.push(value.MEMBER_ID);
                    totalReward += parseInt(value.QTY);
                });
                $("#totalShop").val(totalShop);
                $("#totalReward").val(totalReward);

                $('html, body').animate({
                    scrollTop: $("#tblDataRedeem").offset().top
                }, 500);
            },
        });
    });
    $("#tblDataRedeem tbody").on("click", ".editCOURIER", function () {
        var redeemno = $(this).attr('data-redeem-no');
        var memberid = $(this).attr('data-member-id');
        var shopname = $(this).attr('data-shop-name');
        var delivery_by = $(this).attr('data-delivery');
        var rewardcode = $(this).attr('data-reward-code');
        var rewardname = $(this).attr('data-reward-name');
        var agentcode = $(this).attr('data-agent-code');
        var thisQTY = $(this).parents("tr").children("td").eq(7).text();
        var redeemdate = $(this).parents("tr").children("td").eq(2).text();
        $("#editMemberidy").val(memberid);
        $("#editShopName").val(shopname);
        $("#editRedeemNo").val(redeemno);
        $("#editRewardCode").val(rewardcode);
        $("#editRewardName").val(rewardname);
        $("#editQty").val(thisQTY);
        $("#editRedeemDate").val(redeemdate);
        $("#editCourier").val(delivery_by);
        $('html, body').animate({
            scrollTop: $("#editMemberid").offset().top
        }, 500);
    });
    $("#tblDataRedeem tbody").on("click", ".chkRedeem", function () {

        var chkBox = $(this);
        var thisQTY = $(this).parents("tr").children("td").eq(7).text();
        var chk_count = $("#tblDataRedeem tbody input:checked").length;
        var qty = 0;
        var rewardId = "";
        if (chk_count > 0) {
            $("#tblDataRedeem tbody input:checked").each(function () {
                rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
                qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
                qty = qty + parseInt(qtyInLine);
            });
        }

        $("#tblDataReward tbody tr").each(function () {
            rewardIdTmp = $(this).children().eq(1).text();
            stockQty = $(this).children().eq(3).text();
            $(this).children().eq(5).text('');
            $("#totalSendCal").val('');
            if (rewardIdTmp == rewardId) {
                if (qty > stockQty) {
                    alert("สินค้าใน Stock ไม่เพียงพอ");
                    chkBox.prop('checked', false);
                    $(this).children().eq(5).html('<span style="color:blue;">' + (qty - thisQTY) + "</span>");
                    $("#totalSendCal").val(qty - thisQTY);
                    return false;
                } else {
                    $(this).children().eq(5).html('<span style="color:blue;">' + qty + "</span>");
                    $("#totalSendCal").val(qty);
                    return false;
                }
            }
        });
    });

    $('.chkRedeemAll').on("ifChanged", function (event) {
        //$("#tblDataRedeem").on("click", ".chkRedeemAll", function () {
        if ($(this).is(':checked')) {
            $(".chkRedeem").prop('checked', true);
        } else {
            $(".chkRedeem").prop('checked', false);
        }

        var chk_count = $("#tblDataRedeem tbody input:checked").length;
        var qty = 0;
        var rewardId = "";
        if (chk_count > 0) {
            $("#tblDataRedeem tbody input:checked").each(function () {
                rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
                qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
                qty = qty + parseInt(qtyInLine);
            });
        }

        $("#tblDataReward tbody tr").each(function () {
            rewardIdTmp = $(this).children().eq(1).text();
            stockQty = $(this).children().eq(3).text();
            $(this).children().eq(5).text('');
            $("#totalSendCal").val('');
            if (rewardIdTmp == rewardId) {
                if (qty > stockQty) {
                    alert("สินค้าใน Stock ไม่เพียงพอ");
                    $(".chkRedeemAll").prop('checked', false);
                    $(".chkRedeem").prop('checked', false);
                    return false;
                } else {
                    $("#totalSendCal").val(qty);
                    $(this).children().eq(5).text(qty);
                    return false;
                }
            }
        });
    });

    $("#editCOURIER").on("click", function () {
        if ($("#editSubAgentCode").val() == "") {
            alert("กรุณาเลือก รายการ");
        } else {
            $("#editSendAgent").attr('readonly', false);
            $("#editCourier").attr('readonly', false);
        }
    });

    $("#saveCOURIER").on("click", function () {
        //$.showLoading({ name: 'line-scale', allowHide: false });
        if ($("#editSubAgentCode").val() == "") {
            alert("กรุณาเลือก รายการ");
        } else {
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: baseUrl + "Delivery/setEditDeivery",
                data: {
                    editCourier: $("#editCourier").val(), 
                    editRewardCode: $("#editRewardCode").val(),
                    editRedeemNo: $("#editRedeemNo").val()
                },
                success: function (data) { 
                    // $.hideLoading();
                    $('#tblDataRedeem tbody').empty();
                    var totalShop = 0;
                    var totalReward = 0;
                    var Shop = [];
                    $.each(data.items, function (key, value) {
                        var date = new Date(parseInt(value.REDEEM_DATE.replace("/Date(", "").replace(")/", "")));
                        //REDEEM_DATE = addZeroBefore(date.getDate()) + '/' + addZeroBefore(parseInt(date.getMonth()) + 1) + '/' + date.getFullYear();
                        REDEEM_DATE = value.REDEEM_DATE.substring(8, 10) + "-" + value.REDEEM_DATE.substring(5, 7) + "-" + value.REDEEM_DATE.substring(0, 4);
                        /*var agentcode;
                        var agentname;
                        if (value.delivery_by == "BRT") {
                            agentcode = value.agent_code;
                            agentname = value.agent_name;
                        } else {
                            agentcode = "";
                            agentname = "";
                        }*/
                        $('#tblDataRedeem  tbody').append('<tr><td><input type="checkbox"  class="chkRedeem" /></td><td data-title="Redeem No" style="height:40px;"><span class="pull-center">' + value.REDEEM_NO + '</span> </td><td data-title="Redeem Date" style="height:40px;"><span class="pull-center">' + REDEEM_DATE + '</span> </td><td data-title="Member ID" style="height:40px;"><span class="pull-center">' + value.MEMBER_ID + '</span> </td><td data-title="Member Name" style="height:40px;"><span class="pull-left">' + value.MEMBER_NAME + '</span> </td><td data-title="Reward Code" style="height:40px;"><span class="pull-left">' + value.REWARD_CODE + '</span> </td><td data-title="Reward Name" style="height:40px;"><span class="pull-left">' + value.REWARD_NAME + '</span> </td><td data-title="Qty" style="height:40px;"><span class="pull-right">' + addCommas(value.QTY) + '</span> </td><td data-title="Delivey by" style="height:40px;"><input type="hidden" value = "' + value.DELIVERY_BY + '"><span class="pull-left">' + value.DELIVERY_BY + '</span> </td> <td data-title="Edit" style="height:40px;"><button class="btn btn-md btn-warning editCOURIER" data-member-id="' + value.MEMBER_ID + '" data-shop-name="' + value.MEMBER_NAME + '" data-delivery="' + value.DELIVERY_BY + '" data-reward-code="' + value.REWARD_CODE + '"  data-reward-name="' + value.REWARD_NAME + '" data-redeem-no="' + value.REDEEM_NO + '"  "><i class="fa fa-pencil"></i> แก้ไข</button> </td></tr>');
                        if (jQuery.inArray(value.MEMBER_ID, Shop)) {
                            totalShop += 1;
                        }
                        Shop.push(value.MEMBER_ID);
                        totalReward += parseInt(value.QTY);
                    });
                    $("#totalShop").val(totalShop);
                    $("#totalReward").val(totalReward);

                    $('html, body').animate({
                        scrollTop: $("#tblDataRedeem").offset().top
                    }, 500);
                },
            });
            $("#editSendAgent").attr('readonly', false);
            $("#editCourier").attr('readonly', false);
        }
    });

    $("#saveData").on("click", function () {
        var chk_count = $("#tblDataRedeem tbody input:checked").length;
        if (chk_count == 0) {
            alert("กรุณาเลือกข้อมูล");
        } else {
            $("#myModalDelivery").modal("show");
        }
    });
    $("#saveDelivery").on("click", function () {
        var deliveryDate = $("#str_month1").val();
        if (deliveryDate == "") {
            alert("กรุณาเลือกวันที่จัดส่ง");
        } else {
            //$.showLoading({ name: 'line-scale', allowHide: false });
            saveDeliveryData();
        }
    });
    function saveDeliveryData() {
        var deliveryDate = $("#str_month1").val();
        var dataDelivery = "";
        $("#tblDataRedeem tbody input:checked").each(function () {
            if (dataDelivery != "") {
                dataDelivery += "|";
            }
            redeemNo = $.trim($(this).parents("tr").children("td").eq(1).text());
            memberId = $.trim($(this).parents("tr").children("td").eq(3).text());
            delivey = $.trim($(this).parents("tr").children('td').eq(8).children("input").val());
            rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
            qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
            dataDelivery += redeemNo + ":" + memberId + ":" + delivey + ":" + rewardId + ":" + qtyInLine;
        });
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "Delivery/saveDataDelivery",
            data: {
                deliveryDate: deliveryDate,
                dataDelivery: dataDelivery
            },
            success: function (data) {
                // $.hideLoading();
                if (data.STATUS == "TRUE") {
                    alert(data.MESSAGE);
                    location.reload(true);;
                } else if (data.STATUS == "FALSE") {
                    alert(data.MESSAGE);
                }
            },
        });
    }
});