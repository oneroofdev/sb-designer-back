﻿$(document).ready(function () {
    $('input[type="checkbox"]').iCheck('uncheck');
    //$('#myModalEditCourier').modal('show');
    var tblReward = $('#tblDataReward').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columns": [
           { "width": "5%" },
           { "width": "10%" },
           { "width": "30%" },
           { "width": "5%" },
           { "width": "5%" },
           { "width": "5%" },
           { "width": "5%" },
           { "width": "5%" },
           { "width": "10%" }
        ],
        'drawCallback': function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        "columnDefs": [
            { "className": "text-center", "targets": [0, 1, 8] },
            { "className": "text-right", "targets": [3, 4, 5, 6, 7] },
            {
                "targets": [0, 8],
                "orderable": false
            }
        ],
        "order": [[2, "asc"]]
    });

    var tblRedeem = $('#tblDataRedeem').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columns": [
           { "width": "5%" },
           { "width": "5%" },
           { "width": "10%" },
           { "width": "18%" },
           { "width": "7%" },
           { "width": "7%" },
           { "width": "20%" },
           { "width": "5%" },
           { "width": "10%" },
           { "width": "10%" }
        ],
        'drawCallback': function (oSettings) {
            //if (oSettings.bSorted || oSettings.bFiltered) {
            //    for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
            //        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
            //    }
            //}
            $('input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
        },
        "columnDefs": [
            { "className": "text-center", "targets": [0, 1, 2, 5, 9] },
            { "className": "text-right", "targets": [7] },
            {
                "targets": 0,
                "orderable": false
            }
        ],
        "order": [[2, "desc"]]
    });

    var tblBudget = $('#tblDataBudget').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "columns": [
           { "width": "10%" },
           { "width": "15%" },
           { "width": "15%" },
           { "width": "10%" },
           { "width": "10%" },
           { "width": "10%" },
           { "width": "10%" },
           { "width": "10%" },
           { "width": "10%" }
        ],
        'drawCallback': function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        "columnDefs": [
            { "className": "text-center", "targets": [0, 6, 7, 8] },
            { "className": "text-right", "targets": [3, 4, 5] },
        ],
    });
    var getCourier = function () {
        $.ajax({
            url: sessionStorage.apiUrl + '/REW/ListCourier',
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            //crossDomain: true,
            //withCredentials: true,
            //headers: {  'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                language: "TH",
                maxResults: 99,
                fromType: "B"
            }, 
            success: function (data) {
                if (data.success) {
                    $.each(data.items, function (i, e) {
                        rewardCourier += '<option value="' + e.COURIER_CODE + '">' + e.COURIER_NAME + '</option>';
                    });
                    $("#rewardCourier").html(rewardCourier);
                }
            }
        });
    }

    var getCatelory = function () {
        $.ajax({
            url: sessionStorage.apiUrl + '/REW/ListCatelory',
            dataType: 'json',
            type: "POST",
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                language: "TH",
                maxResults: 99,
                fromType: "B"
            },
            async: true,
            cache: false,
            success: function (data) {
                if (data.success) {
                    rewardType = '<option value="A">ทั้งหมด</option>';
                    $.each(data.items, function (i, e) {
                        rewardType += '<option value="' + e.FREFIXES + '">' + e.CATEGORY_NAME + '</option>';
                    });
                    $("#rewardType").html(rewardType);
                    if (sessionStorage.GrpId == '4') {
                        $("#rewardType").attr('disabled', true);
                        $("#rewardType").val('C');
                    } else {
                        $("#rewardType").val('A');
                    }
                }
            }
        });
    }
    getCatelory();

    var geBudgetWait = function () { 
        $.ajax({
            url: sessionStorage.apiUrl + '/budget/GetListBudgetWaitDelivery',
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                language: "TH",
                maxResults: 99,
                fromType: "B"
            }, 
            success: function (data) { 
                if (data.success) {
                    $.each(data.items, function (i, e) {
                        BG_STATUS = '<span style="color:#ff5200;">' + "รอการอนุมัติ" + '</span>';
                        if (e.BUDGET_STATUS == 2) {
                            BG_STATUS = '<span style="color:#3c8dbc;">' + "อนุมัติ/รอรับของ" + '</span>';
                        } else if (e.BUDGET_STATUS == 3) {
                            BG_STATUS = '<span style="color:#80b636;">' + "รับของเรียบร้อย" + '</span>';
                        }


                        DEL_STATUS = '<span style="color:#ff5200;">' + "รอการจัดส่ง" + '</span>';
                        if (e.DELIVERY_STATUS == 1) {
                            DEL_STATUS = '<span style="color:#3c8dbc;">' + "จัดส่งแล้วบางส่วน" + '</span>';
                        } else if (e.DELIVERY_STATUS == 2) {
                            DEL_STATUS = '<span style="color:#80b636;">' + "จัดส่งเรียบร้อย" + '</span>';
                        }
                        tblBudget.row.add([
                             '.1',
                             e.BUDGET_DATE,
                             e.DURATION,
                             commaSeparateNumber(e.TOTAL_MEMBER),
                             commaSeparateNumber(e.TOTAL_POINT),
                             commaSeparateNumber(e.TOTAL_BUDGET),
                             BG_STATUS,
                             DEL_STATUS,
                             "<button class='btn btn-sm btn-info btndetail-reward' data-budget-id ='" + e.BUDGET_ID + "' data-bg-status='" + e.BUDGET_STATUS + "'  > รายละเอียด </button>"
                        ]).draw(false);
                    });
                }
            }
        });
    }

    var geBudgetRedeem = function (budgetId) {
        tblReward.clear().draw();
        $.ajax({
            url: sessionStorage.apiUrl + '/budget/GetListBudgetRedeem',
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                language: "TH",
                maxResults: 99,
                fromType: "B",
                BUDGET_ID: budgetId,
                DELIVERY_TYPE: $("#deliveryType").val(),
                GROUP_ID: $("#rewardType").val()
            }, 
            success: function (data) {
                if (data.success) {
                    $.each(data.items, function (i, e) {
                        tblReward.row.add([
                            '1',
                            e.REWARD_CODE,
                            e.NAME_TH,
                            commaSeparateNumber(e.TOTAL_REDEEM_QTY),
                            commaSeparateNumber(e.TOTAL_DEL_QTY),
                            commaSeparateNumber(e.TOTAL_WAIT_DEL_QTY),
                            commaSeparateNumber(e.TOTAL_STOCK_QTY),
                            '',
                            "<button class='btn btn-sm btn-info btndetail-redeem' data-budget-id ='" + e.BUDGET_ID + "' data-reward-code ='" + e.REWARD_CODE + "' > รายละเอียด </button>"
                        ]).draw(false);
                    });
                }
            }
        });
    }

    var geBudgetRedeemDetail = function (budgetId, rewardCode) {
        tblRedeem.clear().draw();
        $("#totalShop").val('0');
        $("#totalReward").val('0');
        var bgstatus = $("#bgstatus").val();
        $.ajax({
            url: sessionStorage.apiUrl + '/budget/GetListBudgetRedeemDetail',
            dataType: 'json',
            type: "POST",
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                language: "TH",
                maxResults: 99,
                fromType: "B",
                BUDGET_ID: budgetId,
                REWARD_CODE: rewardCode,
                DELIVERY_TYPE: $("#deliveryType").val(),
                GROUP_ID: $("#rewardType").val()
            },
            async: true,
            cache: false,
            success: function (data) {
                $('input[type="checkbox"]').iCheck('uncheck');
                if (data.success) {
                    totalReward = 0;
                    totalShop = 0;
                    $.each(data.items, function (i, e) {
                        totalReward += e.QTY;
                        totalShop++;
                        checkbox = "";
                        edit = "";
                        if ((e.DELIVERY_DATE == "" || e.DELIVERY_DATE == null) && (bgstatus != '' && bgstatus != null && bgstatus != "null")) {
                            tempid = e.REDEEM_NO + "" + e.REWARD_CODE;
                            checkbox = "<input type='checkbox' class='checkRedeem " + tempid + "' data-is-spc='" + e.IS_SPECIAL + "' data-qty='" + e.QTY + "'  data-member-id='" + e.MEMBER_ID + "' data-couier-code='" + e.COURIER_CODE + "' data-budget-id ='" + e.BUDGET_ID + "' data-redeem-no='" + e.REDEEM_NO + "' data-reward-code='" + e.REWARD_CODE + "' />";
                            edit = "<button class='btn btn-sm btn-info btndetail-courier' data-is-spc='" + e.IS_SPECIAL + "' data-reward-name='" + e.NAME_TH + "' data-member-name='" + e.FIRST_NAME_TH + ' ' + e.LAST_NAME_TH + "' data-qty='" + e.QTY + "'  data-member-id='" + e.MEMBER_ID + "' data-couier-code='" + e.COURIER_CODE + "' data-budget-id ='" + e.BUDGET_ID + "' data-redeem-no='" + e.REDEEM_NO + "' data-reward-code='" + e.REWARD_CODE + "' > <i class='fa fa-pencil'></i> </button>";
                        } else {
                            tempid = e.REDEEM_NO + "" + e.REWARD_CODE;
                            checkbox = "";
                            edit = "";

                        }
                        tblRedeem.row.add([
                      checkbox,
                       e.REDEEM_NO,
                       e.REDEEM_DATE,
                       e.FIRST_NAME_TH + ' ' + e.LAST_NAME_TH,
                       (e.TIER_CODE == "GR") ? "GREEN" : (e.TIER_CODE == "GP") ? "GREENPLUS" : (e.TIER_CODE == "PL") ? "PLATINUM" : "",
                       e.REWARD_CODE,
                       e.NAME_TH,
                       commaSeparateNumber(e.QTY),
                       e.COURIER_NAME,
                       e.DELIVERY_DATE
                        ]).draw(false);
                    });
                    $("#totalShop").val(totalShop);
                    $("#totalReward").val(commaSeparateNumber(totalReward, 0));
                }
            }
        });
    }

    var rowRedeem = "";
    var rowReward = "";

    $('#tblDataBudget tbody').on('click', '.btndetail-reward', function (event) {
        let budgetid = $(this).attr('data-budget-id');
        let bgstatus = $(this).attr('data-bg-status');


        $("#bgstatus").val(bgstatus);
        $("#budgetid").val(budgetid);
        if (rowReward != "") {
            rowReward.removeClass('rowSelect');
        }
        rowReward = $(this).parents("tr").addClass('rowSelect');
        geBudgetRedeem(budgetid);
        geBudgetRedeemDetail(budgetid, null);
    });

    $('#tblDataRedeem tbody').on('click', '.btndetail-courier', function (event) {

        let membername = $(this).attr('data-member-name');
        let courier = $(this).attr('data-couier-code');
        let redeemNo = $(this).attr('data-redeem-no');
        let rewardCode = $(this).attr('data-reward-code');
        let rewardName = $(this).attr('data-reward-name');
        let dataisspc = $(this).attr('data-is-spc');

        $("#redeemNoEdit").val(redeemNo);
        $("#rewardCodeEdit").val(rewardCode);
        $("#rewardCourier").val(courier);
        $("#rewardNameEdit").val(rewardName);
        $("#nameEdit").val(membername);
        $('#myModalEditCourier').modal('show');
    });


    $('#tblDataReward tbody').on('click', '.btndetail-redeem', function (event) {
        let budgetid = $(this).attr('data-budget-id');
        let rewardcode = $(this).attr('data-reward-code');
        if (rowRedeem != "") {
            rowRedeem.removeClass('rowSelect');
        }
        rowRedeem = $(this).parents("tr").addClass('rowSelect');
        geBudgetRedeemDetail(budgetid, rewardcode);
    });

    var dataDelivery = [];
    //$('#saveData').on('click', function () {
    //    $('#myModalDelivery').modal('show');
    //});

    $('#tblDataRedeem thead').on('ifChanged', '.chkRedeemAll', function (event) {
        var rows = tblRedeem.rows({ 'search': 'applied' }).nodes();
        if (event.target.checked) {
            $('input[type="checkbox"]', rows).iCheck('check');
        } else {
            $('input[type="checkbox"]', rows).iCheck('uncheck');
        }
    });
    $('#btn_search_Reward').on('click', function (e) {
        geBudgetRedeem($("#budgetid").val());
        geBudgetRedeemDetail($("#budgetid").val(), null);
    });
    $('#saveData').on('click', function (e) {
        dataDelivery = [];
        var redeemNo = "";
        tblRedeem.$('input[type="checkbox"]').each(function () {
            if (this.checked) {
                redeemNo = $(this).attr('data-redeem-no');
                rewardCode = $(this).attr('data-reward-code');
                budgetId = $(this).attr('data-budget-id');
                couierCode = $(this).attr('data-couier-code');
                memberID = $(this).attr('data-member-id');
                qty = $(this).attr('data-qty');
                dataisspc = $(this).attr('data-is-spc');
                asdas = $(this).parents("tr").children().eq(5).text();
                dataDelivery.push({
                    REDEEM_NO: redeemNo,
                    REWARD_CODE: rewardCode,
                    BUDGET_ID: budgetId,
                    COURIER_CODE: couierCode,
                    MEMBER_ID: memberID,
                    MEMBER_ID: memberID,
                    QTY: qty,
                    IS_SPECIAL: dataisspc
                });
            }
        });
        if (redeemNo != "") {
            $("#deliveryDate").val('');
            if ($("#rewardType").val() == "C") {
                $('#myModalDelivery .modal-title').text('กรุณาเลือกวันที่โอน');
            } else {
                $('#myModalDelivery .modal-title').text('กรุณาเลือกวันที่ส่ง');

            }
            $('#myModalDelivery').modal('show');
        } else {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "กรุณาเลือกรายการที่ต้องการจัดส่งของรางวัล",
                size: 'small',
                callback: function () {
                }
            });
        }
        e.preventDefault();
    });

    $('#saveDelivery').on('click', function (e) { 
        if ($("#deliveryDate").val() == "") {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "กรุณาเลือก วันที่",
                size: 'small',
                callback: function () {
                }
            });
        } else {
            $.ajax({
                url: sessionStorage.apiUrl + '/redeem/SaveDelivery',
                dataType: 'json',
                type: "POST",
                //crossDomain: true,
                //withCredentials: true,
                //headers: {
                //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
                //},
                data: {
                    deliveryDate: $("#deliveryDate").val(),
                    deliveryData: JSON.stringify(dataDelivery),
                    createBy: sessionStorage.UserName,
                },
                async: true,
                cache: false,
                success: function (data) {
                    if (data.success) {
                        bootbox.alert({
                            title: "แจ้งเตือน",
                            message: "บันทึกข้อมูลเรียบร้อย",
                            size: 'small',
                            callback: function () {
                            }
                        });
                        tblBudget.clear().draw();
                        geBudgetWait();
                        tblReward.clear().draw();
                        tblRedeem.clear().draw();
                        
                        $('#myModalDelivery').modal('hide');
                        dataDelivery = [];
                    } else {
                        bootbox.alert({
                            title: "แจ้งเตือน",
                            message: "ไม่สามารถบันทึกข้อมูลได้",
                            size: 'small',
                            callback: function () {
                            }
                        });
                    }
                }
            });

        }
    });

    $('#saveCourier').on('click', function (e) {
        $.ajax({
            url: sessionStorage.apiUrl + '/redeem/UpdateCourier',
            dataType: 'json',
            type: "POST",
            //crossDomain: true,
            //withCredentials: true,
            //headers: {
            //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
            //},
            data: {
                courierCode: $("#rewardCourier").val(),
                rewardCode: $("#rewardCodeEdit").val(),
                redeemNo: $("#redeemNoEdit").val(),
                createBy: sessionStorage.UserName,
            },
            async: true,
            cache: false,
            success: function (data) {
                if (data.success) {
                    bootbox.alert({
                        title: "แจ้งเตือน",
                        message: "บันทึกข้อมูลเรียบร้อย",
                        size: 'small',
                        callback: function () {
                        }
                    });
                    geBudgetRedeemDetail($("#budgetid").val(), null);
                    //geBudgetWait();
                    //tblReward.clear().draw();
                    //tblRedeem.clear().draw();
                    $('#myModalEditCourier').modal('hide');
                    dataDelivery = [];
                } else {
                    bootbox.alert({
                        title: "แจ้งเตือน",
                        message: "ไม่สามารถบันทึกข้อมูลได้",
                        size: 'small',
                        callback: function () {
                        }
                    });
                }
            }
        });
    });

    $("#tblDataRedeem tbody").on("ifChecked", ".checkRedeem", function () {
        var thisQTY = $(this).parents("tr").children("td").eq(7).text();
        var rewardIdCheck = $(this).parents("tr").children("td").eq(5).text();
        var redeemno = $(this).parents("tr").children("td").eq(1).text();
        let dataisspcSelect = $(this).attr('data-is-spc');
       // alert(dataisspc);
        var qty = 0;
        var qtyStock = 0;
        var rewardIdCheck = $(this).parents("tr").children("td").eq(5).text();
        var rewardId = "";
        tblRedeem.$('input[type="checkbox"]').each(function () {
            if (this.checked) {
                rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
                qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
                dataisspc = $(this).attr('data-is-spc');
                if (rewardIdCheck == rewardId) {
                    qty = qty + parseInt(qtyInLine);
                    if (dataisspc == "Y") {
                        qtyStock = qtyStock + parseInt(qtyInLine);
                    }
                }

            }
        });
        $("#tblDataReward tbody tr").each(function () {
            rewardIdTmp = $(this).children().eq(1).text();
            rewardName = $(this).children().eq(2).text();
            stockQty = $(this).children().eq(6).text() || 0;
            currentQty = $(this).children().eq(7).text() || 0;
            if (rewardIdTmp == rewardIdCheck) {
                //alert(qtyStock);
                //alert(stockQty);
                //alert(dataisspcSelect);
                if (qtyStock > stockQty && dataisspcSelect == 'Y') {
                    bootbox.alert({
                        title: "แจ้งเตือน",
                        message: "จำนวนสินค้า " + rewardName + "ที่ส่งมีจำนวนสินค้ามากกว่าสินค้าใน สต๊อก",
                        size: 'small',
                        callback: function () {
                            var idTemp = "." + redeemno + rewardIdCheck;
                            $(idTemp).iCheck('uncheck');

                        }
                    });
                    $(this).children().eq(7).html('<span style="color:blue;">' + (qty - parseInt(thisQTY)) + "</span>");
                    $("#totalSendCal").val(qty - thisQTY);
                    return false;
                } else {
                    $(this).children().eq(7).html('<span style="color:blue;">' + parseInt(qty) + "</span>");
                    $("#totalSendCal").val(qty);
                    return false;
                }
            }
        });
    });

    $("#tblDataRedeem tbody").on('ifUnchecked', ".checkRedeem", function (event) {
        var thisQTY = $(this).parents("tr").children("td").eq(7).text();
        var rewardIdCheck = $(this).parents("tr").children("td").eq(5).text();
        var redeemno = $(this).parents("tr").children("td").eq(1).text();
        var qty = 0;
        var rewardIdCheck = $(this).parents("tr").children("td").eq(5).text();
        var rewardId = "";
        tblRedeem.$('input[type="checkbox"]').each(function () {
            if (this.checked) {
                rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
                qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());

                dataisspc = $(this).attr('data-is-spc');
                if (rewardIdCheck == rewardId) {
                    qty = qty + parseInt(qtyInLine);
                }

            }
        });
        $("#tblDataReward tbody tr").each(function () {
            rewardIdTmp = $(this).children().eq(1).text();
            rewardName = $(this).children().eq(2).text();
            stockQty = $(this).children().eq(6).text() || 0;
            currentQty = $(this).children().eq(7).text() || 0;
            if (rewardIdTmp == rewardIdCheck) {
                $(this).children().eq(7).html('<span style="color:blue;">' + parseInt(qty) + "</span>");
                $("#totalSendCal").val(qty);
                return false;
            }
        });
    });
    //$("#tblDataRedeem tbody").on("ifChanged", ".checkRedeem", function () {
    //    var thisQTY = $(this).parents("tr").children("td").eq(7).text();
    //    var rewardIdCheck = $(this).parents("tr").children("td").eq(5).text();
    //    var redeemno = $(this).parents("tr").children("td").eq(1).text();
    //    var chk_count = $("#tblDataRedeem tbody input:checked").length;
    //    var qty = 0;
    //    var rewardId = "";
    //    var dataDeliveryRedeem = [];
    //    chkBox = $.trim($(this).parents("tr").children("td").eq(0).text());
    //    if (chk_count > 0) {
    //        tblRedeem.$('input[type="checkbox"]').each(function () {
    //            if (this.checked) {
    //                rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
    //                qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
    //                if (rewardIdCheck == rewardId) {
    //                    qty = qty + parseInt(qtyInLine);
    //                }

    //            }
    //        });
    //    }

    //    $("#tblDataReward tbody tr").each(function () {
    //        rewardIdTmp = $(this).children().eq(1).text();
    //        rewardName = $(this).children().eq(2).text();
    //        stockQty = $(this).children().eq(6).text(); 
    //        $("#totalSendCal").val('');
    //        if (rewardIdTmp == rewardId) {
    //            if (qty > stockQty) {
    //                bootbox.alert({
    //                    title: "แจ้งเตือน",
    //                    message: "สินค้าใน Stock " + rewardName + "ไม่เพียงพอ",
    //                    size: 'small',
    //                    callback: function () {
    //                        var idTemp = "." + redeemno + rewardId; 
    //                        $(idTemp).iCheck('uncheck');
    //                    }
    //                });
    //                $(this).children().eq(7).html('<span style="color:blue;">' + (qty - thisQTY) + "</span>");
    //                $("#totalSendCal").val(qty - thisQTY);
    //                return false;
    //            } else {
    //                $(this).children().eq(7).html('<span style="color:blue;">' + qty + "</span>");
    //                $("#totalSendCal").val(qty);
    //                return false;
    //            }
    //        }
    //    });

    //});

    //$("#tblDataRedeem tbody").on("ifChanged", ".checkRedeem", function () {
    //    var thisQTY = $(this).parents("tr").children("td").eq(7).text();
    //    var chk_count = tblRedeem.$('input[type="checkbox"] input:checked').length;
    //    var qty = 0;
    //    var rewardId = ""; 

    //    tblRedeem.$('input[type="checkbox"]').each(function () {
    //        if (this.checked) {
    //            var chkBox = $.trim($(this).parents("tr").children("td").eq(0).text());
    //            rewardId = $.trim($(this).parents("tr").children("td").eq(5).text());
    //            qtyInLine = $.trim($(this).parents("tr").children("td").eq(7).text());
    //            qty = qty + parseInt(qtyInLine);
    //            $("#tblDataReward tbody tr").each(function () {
    //                rewardIdTmp = $(this).children().eq(1).text();
    //                stockQty = $(this).children().eq(3).text();
    //                rewardName = $(this).children().eq(2).text();
    //                $(this).children().eq(5).text('');
    //                $("#totalSendCal").val('');
    //                if (rewardIdTmp == rewardId) {
    //                    if (qty > stockQty) {
    //                        //alert("สินค้าใน Stock " + rewardName + "ไม่เพียงพอ"); 
    //                        bootbox.alert({
    //                            title: "แจ้งเตือน",
    //                            message: "สินค้าใน Stock " + rewardName + "ไม่เพียงพอ",
    //                            size: 'small',
    //                            callback: function () {
    //                            }
    //                        });
    //                        chkBox.iCheck('uncheck');
    //                        $(this).children().eq(5).html('<span style="color:blue;">' + (qty - thisQTY) + "</span>");
    //                        $("#totalSendCal").val(qty - thisQTY);
    //                        return false;
    //                    } else {
    //                        $(this).children().eq(5).html('<span style="color:blue;">' + qty + "</span>");
    //                        $("#totalSendCal").val(qty);
    //                        return false;
    //                    }
    //                }
    //            });
    //        }
    //    });

    //    //}

    //});
    $('#deliveryType').on('change', function () {
        geBudgetRedeem($("#budgetid").val());
        geBudgetRedeemDetail($("#budgetid").val(), null);
    });
    $('#rewardType').on('change', function () {
        geBudgetRedeem($("#budgetid").val());
        geBudgetRedeemDetail($("#budgetid").val(), null);
    });



    geBudgetWait();
    getCourier();
});