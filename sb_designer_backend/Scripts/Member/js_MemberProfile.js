﻿$(document).ready(function () {
    $('#tb_activities').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });
    $('#tb_pointrefer').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 2, 5] }
        ]
    });
    $('#tb_historyredeem').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] },
            { className: "text-right", "targets": [4, 5, 6] }
        ]
    });
    $('#tb_detalBuyProject').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] },
            { className: "text-right", "targets": [9] }
        ]
    });
    $('#tb_detalBuyProjectHistory').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] },
            { className: "text-right", "targets": [4] }
        ]
    });
    $('#tb_ReferPaymentDetail').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] },
            { className: "text-right", "targets": [3] }
        ]
    });
    $('#tb_receivedetail_promplay').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] }
        ]
    });
    $('#tb_receivedetail_bank').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] }
        ]
    });
    $('#referfriend-table').DataTable({
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: false,
        info: true,
        autoWidth: false,
        columnDefs: [
            { className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6] }
        ]
    });
    $('#detail-referfriend-table').DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: false,
        info: true,
        autoWidth: false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 4] }
        ]
    });
    if (sessionCust_ID != "") {
        document.getElementById("detail_refer").style.display = "none";
        getRedeemRW(sessionCust_ID);
        getListReferFriend();
        getListPoint();
    }
    $("#ddl_search").on("change", function () {
        sessionCust_ID = $("#ddl_search").val();
        document.getElementById("detail_refer").style.display = "none";
        getRedeemRW(sessionCust_ID);
        getListReferFriend();
        getListPoint();
    });
    $("#tb_historyredeem").on("click", ".cls_redeemRW", function () {
        var str = $(this).attr('data_redeem');
        var str_redeemNo = $(this).attr('data_redeemNo');
        $("#tbody_historyredeem tr").removeAttr("style");
        $(this).parent().parent().css("background-color", "#e1e1e1");
        getListDetailRedeem_byRWCode(str, str_redeemNo);
    });
    //$('#btn_refer_search').on("click", function () {
    //    document.getElementById("detail_refer").style.display = "none";
    //    getListReferFriend();
    //    getListBuyProjectHistory();
    //});
    $("#tb_detalBuyProject").on("click", ".btn_ReferPaymentDetail", function () {
        var contractNo = $(this).attr('data_ReferPaymentDetail');
        $("#tbody_detalBuyProject tr").removeAttr("style");
        $(this).parent().parent().parent().css("background-color", "#e1e1e1");
        getListPayment(contractNo);
    });
    $("#tb_detalBuyProjectHistory").on("click", ".btn_ReferPaymentDetail", function () {
        var contractNo = $(this).attr('data_ReferPaymentDetail');
        $("#tbody_detalBuyProjectHistory tr").removeAttr("style");
        $(this).parent().parent().parent().css("background-color", "#e1e1e1");
        getListPayment(contractNo);
    });
    $("#referfriend-table").on("click", ".bt_cls_refer", function () {
        document.getElementById("detail_refer").style.display = "block";
        var str = $(this).attr('data_refer_date');
        $('#date_refer').text($(this).parent().parent().children('td').eq(1).text());
        $("#referfriend-tbody tr").removeAttr("style");
        $(this).parent().parent().css("background-color", "#e1e1e1");
        getDetailReferFriend(str);
        //getListBuyProject(str, "N");
    });

});
function getRedeemRW(Cust_ID) {
    $.ajax({
        url: 'MemberProfile/GetListRedeem',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                Cust_ID: Cust_ID,
                yearmonth:""
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#tb_historyredeem').dataTable().fnClearTable();
                var totalpoint = 0;
                var c = 1;
                $.each(a.items, function (i, value) {
                    var dateR = value.REDEEM_DATE.split("T");
                    var DateRedeem = value.REDEEM_DATE == "" ? "" : ConvertDateToFormat(dateR[0]);
                    var DateSend = "";//value.DELIVERY_DATE == "" ? "" : ConvertDateToFormat(value.DELIVERY_DATE);
                    var btn_detail = '<button type="button" class="btn btn-info btn-sm cls_redeemRW" data_redeem="' + value.REWARD_CODE + '" data_redeemNo="' + value.REDEEM_NO + '"><i class="fa fa fa-file"></i>&nbsp; รายละเอียด</button>';
                    var CodeR = value.REWARD_CODE == null ? "" : value.REWARD_CODE;
                    var nameR = value.NAME_TH == null ? "" : value.NAME_TH;
                    $('#tb_historyredeem').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + DateRedeem + '</span>',
                        '<span>' + CodeR + '</span>',
                        '<span>' + nameR + '</span>',
                        '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                        '<span>' + commaSeparateNumber(value.POINT_REDEEM) + '</span>',
                        btn_detail
                    ]);
                    totalpoint += value.POINT_REDEEM;
                    c++;
                });
                $('#txt_pointredeem').text(commaSeparateNumber(totalpoint));
            }
        }
    });
}
function getListDetailRedeem_byRWCode(RwCode, str_redeemNo) {
    $.ajax({
        url: 'MemberProfile/ListDetailRedeem_byRWCode',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                Cust_ID: sessionCust_ID,
                RewardCode: RwCode,
                RedeemNo: str_redeemNo
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var rows = '';
                var c = 1;
                $.each(a.items, function (i, e) {
                    var cols = '';
                    var RECEIVE_BY = e.RECEIVE_BY == null ? "-" : e.RECEIVE_BY;
                    var RECEIVE_DATE = e.RECEIVE_DATE == null ? "-" : e.RECEIVE_DATE;
                    var DELIVERY_DATE = e.DELIVERY_DATE == null ? "-" : e.DELIVERY_DATE;
                    var BANK_ACCT_NO = e.BANK_ACCT_NO == null ? "-" : e.BANK_ACCT_NO;
                    var BANK_NAME = e.BANK_NAME == null ? "-" : e.BANK_NAME;
                    var BANK_BRANCH = e.BANK_ACCT_NO == null ? "-" : e.BANK_BRANCH;
                    var PROMPT_PAY = e.PROMPT_PAY_MOBILE == null ? e.PROMPT_PAY_CID : e.PROMPT_PAY_MOBILE;
                    var TRACKING_NO = e.TRACKING_NO == null ? "-" : e.TRACKING_NO;
                    $('#txt_detail_send_address').val(e.ADDRESS);
                    if (e.CATEGORY_CODE == 5 && e.DELIVERY_COURIER_CODE == 1) {
                        document.getElementById("tb_bank").style.display = "block";
                        document.getElementById("tb_send").style.display = "none";
                        document.getElementById("tb_promplay").style.display = "none";
                        rows += '<tr>' +
                                    '<td>' + e.REDEEM_NO + '</td>' +
                                    '<td>' + e.COURIER_NAME + '</td>' +
                                    '<td>' + DELIVERY_DATE + '</td>' +
                                    '<td>' + e.CARD_CODE + '</td>' +
                                    '<td>' + BANK_ACCT_NO + '</td>' +
                                    '<td>' + BANK_NAME + '</td>' +
                                    '<td>' + BANK_BRANCH + '</td>' +
                                 '</tr>';
                        $('#tbody_receivedetail_bank').html(rows);
                    } else if (e.CATEGORY_CODE == 5 && e.DELIVERY_COURIER_CODE == 2) {
                        document.getElementById("tb_promplay").style.display = "block";
                        document.getElementById("tb_send").style.display = "none";
                        document.getElementById("tb_bank").style.display = "none";
                        rows += '<tr>' +
                                    '<td>' + e.REDEEM_NO + '</td>' +
                                    '<td>' + e.COURIER_NAME + '</td>' +
                                    '<td>' + DELIVERY_DATE + '</td>' +
                                    '<td>' + PROMPT_PAY + '</td>' +
                                    '<td>' + e.CARD_CODE + '</td>' +
                                 '</tr>';
                        $('#tbody_receivedetail_promplay').html(rows);
                    } else {
                        document.getElementById("tb_send").style.display = "block";
                        document.getElementById("tb_promplay").style.display = "none";
                        document.getElementById("tb_bank").style.display = "none";
                        rows += '<tr>' +
                                    '<td>' + e.REDEEM_NO + '</td>' +
                                    '<td>' + e.COURIER_NAME + '</td>' +
                                    '<td>' + DELIVERY_DATE + '</td>' +
                                    '<td>' + RECEIVE_BY + '</td>' +
                                    '<td>' + RECEIVE_DATE + '</td>' +
                                    '<td>' + TRACKING_NO + '</td>' +
                                 '</tr>';
                        $('#tbody_receivedetail').html(rows);
                    }
                    c++;
                });
            } else {
                $('#tbody_receivedetail').html('<tr><td class="text-center" data-title="Data" colspan="6"> -- ไม่มีข้อมูล -- </td></tr>');
            }
            $('#myModalRedeemDetail').modal('show');
        }
    });
}
//function getListBuyProject(referDate, genP) {
//    $.ajax({
//        url: sessionStorage.apiUrl + 'refer/getBuyProject',//
//        dataType: 'json',
//        type: "POST",
//        crossDomain: true,
//        withCredentials: true,
//        headers: {
//            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
//            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
//        },
//        data: {
//            MEMBER_ID: sessionCust_ID,
//            refer_date: referDate,
//            CLS_ID: $('#hd_search_refer').val(),
//            ReceivePoint: genP,
//            language: "TH",
//            maxResults: 99,
//            fromType: "B"
//        },
//        async: true,
//        cache: false,
//        success: function (data) {
//            if (data.success) {
//                $('#totRecord_buy').html(data.items.length);
//                $('#tb_detalBuyProject').dataTable().fnClearTable();
//                var c = 1;
//                $.each(data.items, function (i, e) {
//                    var icondetail = '<button type="button" class="btn btn-info btn-sm btn_ReferPaymentDetail" data_ReferPaymentDetail="' + e.CONTRACT_ID +
//    '">รายละเอียด</button>'
//                    var dateRefer = e.DATE_REFER == "" ? "" : ConvertDateToFormat(e.DATE_REFER);
//                    var dateApprove = e.APPROVE_DATE == "" ? "" : ConvertDateToFormat(e.APPROVE_DATE);
//                    $('#tb_detalBuyProject').dataTable().fnAddData([
//                        '<span>' + c + '</span>',
//                        '<span>' + e.REFER_NAME + '</span>',
//                        '<span>' + dateRefer + '</span>',
//                        '<span>' + e.STATUS + '</span>',
//                        '<span>' + e.SBU + '</span>',
//                        '<span>' + e.PROJECT + '</span>',
//                        '<span>' + dateApprove + '</span>',
//                        '<span>' + e.CONTRACT_NUMBER + '</span>',
//                        '<span>' + e.UNIT_NUMBER + '</span>',
//                        '<span>' + commaSeparateNumber(e.TOTAL_NET_PRICE) + '</span>',
//                        '<span>' + icondetail + '</span>'
//                    ]);
//                    c++;
//                });
//            } else {
//                $('#tb_detalBuyProject').dataTable().fnClearTable();
//                $('#tbody_detalBuyProject').html('<tr><td class="text-center" data-title="Data" colspan="10"> -- ไม่มีข้อมูล -- </td></tr>');
//            }
//        }
//    });
//}
//function getListPayment(contractNo) {
//    $.ajax({
//        url: sessionStorage.apiUrl + 'refer/getPaymentDetail',//
//        dataType: 'json',
//        type: "POST",
//        crossDomain: true,
//        withCredentials: true,
//        headers: {
//            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
//            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
//        },
//        data: {
//            CONTRACT_NO: contractNo,
//            language: "TH",
//            maxResults: 99,
//            fromType: "B"
//        },
//        async: true,
//        cache: false,
//        success: function (data) {
//            if (data.success) {
//                $('#tb_ReferPaymentDetail').dataTable().fnClearTable();
//                var c = 1;
//                $.each(data.items, function (i, e) {
//                    var datepaid = e.PAID_DATE == "" ? "" : ConvertDateToFormat(e.PAID_DATE);
//                    $('#tb_ReferPaymentDetail').dataTable().fnAddData([
//                        '<span>' + c + '</span>',
//                        '<span>' + e.TERM_NAME + '</span>',
//                        '<span>' + datepaid + '</span>',
//                        '<span>' + formatter.format(e.PAID_AMOUNT) + '</span>'
//                    ]);
//                    c++;
//                });
//            }
//            $('#myModaltab1').modal("show");
//        }
//    });
//}
//function getListActivity() {
//    $.ajax({
//        url: sessionStorage.apiUrl + 'activity/getListActivity',//
//        dataType: 'json',
//        type: "POST",
//        crossDomain: true,
//        withCredentials: true,
//        headers: {
//            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
//            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
//        },
//        data: {
//            MEMBER_ID: sessionCust_ID,
//            language: "TH",
//            maxResults: 99,
//            fromType: "B"
//        },
//        async: true,
//        cache: false,
//        success: function (data) {
//            if (data.success) {
//                $('#tb_activities').dataTable().fnClearTable();
//                var c = 1;
//                var act_point = 0;
//                $.each(data.items, function (i, e) {
//                    act_point += e.POINT;
//                    var JOINDATE = e.JOIN_DATE == "" ? "" : ConvertDateToFormat(e.JOIN_DATE);
//                    $('#tb_activities').dataTable().fnAddData([
//                        '<span>' + c + '</span>',
//                        '<span>' + JOINDATE + '</span>',
//                        '<span>' + e.ACTIVITY_NAME + '</span>',
//                        '<span>' + e.ACTIVITY_DETAIL + '</span>',
//                        '<span>' + commaSeparateNumber(e.POINT) + '</span>',
//                        '<span>' + e.REMARK + '</span>'
//                    ]);
//                    c++;
//                });
//                $('#txt_pointactivities').val(commaSeparateNumber(parseInt(act_point)));
//                $('#txt_totalpoint').text(commaSeparateNumber(parseInt($('#txt_pointpay').val().replace(",", "")) + parseInt($('#txt_pointactivities').val().replace(",", ""))));
//            }
//        }
//    });
//}
function getListReferFriend() {
    $.ajax({
        url: 'MemberProfile/list_refer_date',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                Cust_ID: sessionCust_ID,
                refer_id: $('#ddl_search_refer').val()
            },
        success: function (data) {
            var a = JSON.parse(data);
            var c = 1;
            $('#referfriend-table').dataTable().fnClearTable();
            if (a.success) {
                $.each(a.items, function (i, e) {
                    var dateR = e.DATE_REFER.split("T");
                    var ref_date = e.DATE_REFER == "" ? "" : ConvertDateToFormat(dateR[0]);
                    var icondetail = '<button type="button" class="btn btn-info btn-sm bt_cls_refer" data_refer_date="' + dateR[0] +
                        '"><i class="fa fa-search"></i> &nbsp; รายละเอียด</button>'
                    $('#referfriend-table').dataTable().fnAddData([
                        '<span title="">' + c + '</span>',
                        '<span title="">' + ref_date + '</span>',
                        '<span title="">' + e.TOTALREFER + '</span>',
                        '<span title="" style="color:#009900;">' + e.COUNTOFAPPROVE + '</span>',
                        '<span title="" style="color:#009900;">' + e.COUNTOFREJECT + '</span>',
                        '<span title="" style="color:red;">' + e.COUNTOFCANCEL + '</span>',
                        '<span title="">' + e.COUNTOFVERIFY + '</span>',
                        icondetail
                    ]);
                    c++;
                });
            } else {
                $('#referfriend-tbody').html('<tr><td class="text-center" colspan="7"> -- ไม่มีข้อมูล -- </td></tr>');
            }
        }
    });
}
function getDetailReferFriend(str_date) {
    $.ajax({
        url: 'MemberProfile/detail_refer_date',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                Cust_ID: sessionCust_ID,
                DATE_REFER: str_date,
                refer_cls_id: ''
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var c = 1;
                $('#detail-referfriend-table').dataTable().fnClearTable();
                $('.totRecord').html(a.items.length);
                $.each(a.items, function (i, e) {
                    var ref_date = e.DATE_REFER == "" ? "" : ConvertDateToFormat(e.DATE_REFER);
                    var txt_color = '';
                    if (e.STATUS == "ยกเลิก") {
                        txt_color = 'style="color:red;"';
                    } else {
                        txt_color = 'style="color:#009900;"';
                    }
                    $('#detail-referfriend-table').dataTable().fnAddData([
                        '<span title="">' + c + '</span>',
                        '<span title="">' + e.FULLNAME + '</span>',
                        '<span title="">' + ref_date + '</span>',
                        '<span title="" ' + txt_color + '>' + e.STATUS + '</span>',
                        '<span title="">' + e.VERIFY_DESCRIPTION + '</span>'
                    ]);
                    c++;
                });
            } else {
                $('#detail-referfriend-tbody').html('<tr><td class="text-center" data-title="Data" colspan="7"> -- ไม่มีข้อมูล -- </td></tr>');
            }
        }
    });
}
function getListPoint() {
    $.ajax({
        url: 'MemberProfile/getDataHistoryPoint',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                Cust_ID: sessionCust_ID,
                yearmonth: ""
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#tb_detalBuyProjectHistory').dataTable().fnClearTable();
                var c = 1;
                var paid_point = 0;
                $.each(a.items, function (i, e) {
                    paid_point += e.Point;
                    $('#tb_detalBuyProjectHistory').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + e.Process + '</span>',
                        '<span>' + e.Calculate_Date + '</span>',
                        '<span>' + e.Sap_Code + '</span>',
                        '<span style="color:blue;">' + commaSeparateNumber(e.Point) + '</span>'
                    ]);
                    c++;
                });
                $('#txt_pointpay').val(commaSeparateNumber(parseInt(paid_point)));
                $('#txt_totalpoint').text(commaSeparateNumber(parseInt($('#txt_pointpay').val().replace(",", "")) + parseInt($('#txt_pointactivities').val().replace(",", ""))));
            }
        }
    });
}
