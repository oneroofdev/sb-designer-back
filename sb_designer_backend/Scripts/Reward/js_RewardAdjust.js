﻿var frm_validate = "";
var ddl_reason = "";
var status_reason = "6";
var r_adj = 1;
$(document).ready(function () {
    $('#gridReward').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        'drawCallback': function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length ; i < iLen ; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 4] },
            {
                "targets": [0, 4],
                "orderable": false
            }
        ]
    });
    $('#tb_StockHistory').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 4] }
        ]
    });
    var tbAdj = $('#tb_adjustReward').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 7] }
        ]
    });
    getListReward();
    //getListProject();
    getListRewardStock(2);
    $('#ddl_stock').val('2').change();
    var dn = new Date();
    $('#str_reward_start_date').val("01/01/2018");
    $('#str_reward_end_date').val(addZeroBefore(dn.getDate()) + "/" + addZeroBefore(dn.getMonth() + 1) + "/" + dn.getFullYear());
    $('#ddl_stock').on('change', function () {
        getListRewardStock($('#ddl_stock').val());
    });
    $('#ddl_Reward_history').on('change', function () {
        getListRewardStockHistory($('input[name="type_stock"]:checked').val());
    });
    $('#str_reward_start_date').on('change', function () {
        getListRewardStockHistory($('input[name="type_stock"]:checked').val());
    });
    $('#str_reward_end_date').on('change', function () {
        getListRewardStockHistory($('input[name="type_stock"]:checked').val());
    });
    $('#filter_stock').on('click', function () {
        $('#ddl_Reward_history').val($('#ddl_Reward_history option:first').val()).trigger('change');
        $('#myModalReward').modal('show');
    });
    $("#export_stock").on('click', function () {
        window.location = sessionStorage.mainUrl + '/RewardAdjust/ReportStockExcel'
    }); // on click export excel
    $('#gridReward').on("click", ".btn_Stock", function () {
        $('#ddl_Reward_history').val($(this).attr('data_StockRWID')).change();
        getListRewardStockHistory($('input[name="type_stock"]:checked').val());
        $('#myModalReward').modal('show');
    });
    $('#gridReward').on("click", ".btn_AdjStock", function () {
        $('#ddl_AdjReward').val($(this).attr('data_AdjStockRWID')).change();
        loadreason_filter();
    });
    $('input[name="type_stock"]').on("ifClicked", function (event) {
        if ($('#ddl_Reward_history').val() != "") {
            getListRewardStockHistory(this.value);
        } else {
            $('#Adjust_Alert').text("กรุณาเลือกของรางวัล");
            $('#alert_modal').modal('show');
        }
    });
    $('input[name="rd_adjReward"]').on("ifClicked", function (event) {
        if (this.value == "I") {
            status_reason = "6";
        } else {
            status_reason = "2";
        }
        loadreason();
        $('#tb_adjustReward').dataTable().fnClearTable();
    });
    $('#filter_stockbyproduct').on('click', function () {
        $('#ddl_AdjReward').val($('#ddl_AdjReward option:first').val()).trigger('change');
        loadreason_filter();
    });
    $('#btn_addRewardAdj').on('click', function () {
        showLoading();
        var rewardCode = $("input[name='rwCode_adj[]']").map(function () { return $(this).val(); }).get();
        var chk_dup = 0;
        for (var i = 0; i < rewardCode.length; i++) {
            if (rewardCode[i] == $('#ddl_AdjReward').val()) {
                chk_dup--;
            } else {
                chk_dup++;
            }
        }
        if (chk_dup == rewardCode.length) {
            $.ajax({
                url: 'RewardAdjust/GetListRewardStockByCode',
                type: "POST",
                async: false,
                cache: false,
                data: { RewardCode: $('#ddl_AdjReward').val() },
                success: function (data) {
                    var a = JSON.parse(data);
                    if (a.success) {
                        $.each(a.items, function (i, value) {
                            var qty = value.QTY == null ? 0 : value.QTY;
                            var icondetail = '<button type="button" class="btn btn-danger btn-sm btn_deleteAdj" data_deleteAdj="' + value.REWARD_CODE +
        '"><i class="fa fa-minus"></i></button>'
                            $('#tb_adjustReward').dataTable().fnAddData([
                                '<span>' + r_adj + '</span>',
                                '<span><input type="hidden" name="rwCode_adj[]" value="' + value.REWARD_CODE + '" />' + value.REWARD_CODE + '</span>',
                                '<span>' + value.NAME_TH + '</span>',
                                '<span><input type="hidden" name="qty_total[]" value="' + qty + '" />' + commaSeparateNumber(qty) + '</span>',
                                '<input type="text" name="qty_adj[]" class="required form-control txt_temp" style = "text-align:center;" maxlength = "7" onkeyup = "this.value=this.value.replace(/[^0-9,]/g,\'\');" />',
                                '<select class="form-control ddl_select required" name="str_reason_adj[]">' + ddl_reason + '</select>',
                                '<input type="text" name="remark_adj[]" class="required form-control txt_temp" maxlength = "200"/>',
                                icondetail
                            ]);
                            r_adj++;
                        });
                    }
                    closeLoading();
                }
            });
        } else {
            closeLoading();
        }
    });
    $('#tb_adjustReward tbody').on('click', '.btn_deleteAdj', function () {
        var target_row = $(this).closest("tr").get(0); // this line did the trick
        var aPos = tbAdj.fnGetPosition(target_row);
        tbAdj.fnDeleteRow(aPos);
    });
    $('#bt_Adjust_reward').on('click', function () {
        showLoading();
        var rewardCode = [];
        var qty_adj = [];
        var qty_total = [];
        var v_remark = [];
        var v_reason = [];
        var txt_reason = [];
        rewardCode = $("input[name='rwCode_adj[]']")
              .map(function () { return $(this).val(); }).get();
        qty_total = $("input[name='qty_total[]']")
              .map(function () { return $(this).val(); }).get();
        qty_adj = $("input[name='qty_adj[]']")
              .map(function () { return $(this).val(); }).get();
        v_remark = $("input[name='remark_adj[]']")
              .map(function () { return $(this).val(); }).get();
        v_reason = $("select[name='str_reason_adj[]']")
              .map(function () { return $(this).val(); }).get();
        txt_reason = $("select[name='str_reason_adj[]']")
              .map(function () { return $(this).find(":selected").text(); }).get();
        var params = [];
        var adjtype = $('input[name="rd_adjReward"]:checked').val();
        var chk = false;
        for (var i = 0; i < rewardCode.length; i++) {
            if (parseInt(qty_adj[i]) > 0) {
                if (adjtype == "O") {
                    if (parseInt(qty_total[i]) < parseInt(qty_adj[i])) {
                        chk = false;
                        break;
                    } else {
                        params.push({
                            str_rwcode: rewardCode[i],
                            str_qty: parseInt(qty_adj[i].replace(',', '') * -1),
                            str_reason: parseInt(v_reason[i]),
                            str_remark: v_remark[i]
                        });
                        chk = true;
                    }
                } else {
                    params.push({
                        str_rwcode: rewardCode[i],
                        str_qty: parseInt(qty_adj[i].replace(',', '')),
                        str_reason: parseInt(v_reason[i]),
                        str_remark: v_remark[i]
                    });
                    chk = true;
                }
            } else {
                chk = false;
                break;
            }
        }
        if (chk) {
            $.ajax({
                url: 'RewardAdjust/SaveAdjustReward',
                type: "POST",
                async: false,
                cache: false,
                data: {
                    DATA_ADJUST: JSON.stringify(params),
                    PROJECT_ID: 0,
                    username: sessionStorage.UserName
                },
                success: function (data) {
                    var a = JSON.parse(data);
                    if (a.success) {
                        r_adj = 1;
                        $('#tb_adjustReward').dataTable().fnClearTable();
                        $('#Adjust_Alert').text("บันทึกข้อมูลการแก้ไขสต็อกของรางวัลเรียบร้อย");
                        $('#alert_modal').modal('show');
                    } else {
                        $('#Adjust_Alert').text("ไม่สามารถบันทึกข้อมูลการแก้ไขสต็อกของรางวัลได้");
                        $('#alert_modal').modal('show');
                    }
                    closeLoading();
                }
            });
        } else {
            closeLoading();
            $('#Adjust_Alert').text("กรุณาตรวจสอบจำนวนของรางวัลที่นำเข้า/นำออก จาก Stock");
            $('#alert_modal').modal('show');
        }
    });
    $('#close_modalAdj').on("click", function () {
        getListRewardStock(2);
        $('#ddl_stock').val('2').change();
        $('#myModalAdjByReward').modal('hide');
    });
    $('#close_alert').on("click", function () {
        $('#alert_modal').modal('hide');
    });
});
function getListProject() {
    $.ajax({
        url: 'RewardAdjust/list_project',
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var rows = '';
                $.each(a.items, function (i, value) {
                    rows += '<option value="' + value.projectId + '">' + value.projectName + '</option>';
                });
                $('#ddl_project').html(rows);
            }
        }
    });
}
function getListReward() {
    $.ajax({
        url: 'RewardAdjust/GetListRewardStock',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Status_Stock: 1
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var rows = '<option value=""> --- กรุณาเลือก --- </option>';
                $.each(a.items, function (i, value) {
                    rows += '<option value="' + value.REWARD_CODE + '">' + value.REWARD_CODE + ' ' + value.NAME_TH + '</option>';
                });
                $('#ddl_Reward_history').html(rows);
                $('#ddl_AdjReward').html(rows);
            }
        }
    });
}
function getListRewardStock(Status_Stock) {
    $.ajax({
        url: 'RewardAdjust/GetListRewardStock',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Status_Stock: Status_Stock
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#gridReward').dataTable().fnClearTable();
                var c = 1;
                $.each(a.items, function (i, value) {
                    var icondetail = '<button type="button" class="btn btn-success btn_Stock" data_StockRWID="' + value.REWARD_CODE +
'">ประวัติ Stock</button>&nbsp;&nbsp;<button type="button" class="btn btn-info btn-sm btn_AdjStock" data_AdjStockRWID="' + value.REWARD_CODE +
'">จัดการ Stock</button>'
                    $('#gridReward').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + value.REWARD_CODE + '</span>',
                        '<span>' + value.NAME_TH + '</span>',
                        '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                        icondetail,
                    ]);
                    c++;
                });
            }
        }
    });
}
function getListRewardStockHistory(typeStock) {
    frm_validate = "";
    showLoading();
    var vtxt_dateF = validate_from_stock($("#str_reward_start_date"), $("#vtxt_reward_start_date"), "กรุณาระบุวันที่เริ่มต้นต้องการค้นหา");
    var vtxt_dateT = validate_from_stock($("#str_reward_end_date"), $("#vtxt_reward_end_date"), "กรุณาระบุวันที่สิ้นสุดต้องการค้นหา");
    if (vtxt_dateF == true && vtxt_dateT == true) {
        $.ajax({
            url: 'RewardAdjust/GetListRewardHistory',
            type: "POST",
            async: false,
            cache: false,
            data: {
                DateF: $('#str_reward_start_date').val(),
                DateT: $('#str_reward_end_date').val(),
                RewardCode: $('#ddl_Reward_history').val(),
                RewardSearch: typeStock
            },
            success: function (data) {
                var a = JSON.parse(data);
                if (a.success) {
                    $('#tb_StockHistory').dataTable().fnClearTable();
                    var c = 1;
                    $.each(a.items, function (i, value) {
                        var reason = value.REASON_DESC_TH == null ? "" : value.REASON_DESC_TH;
                        var remark = value.REMARK == null ? "" : value.REMARK;
                        $('#tb_StockHistory').dataTable().fnAddData([
                            '<span>' + c + '</span>',
                            '<span>' + ConvertDateToFormat(value.STOCK_DATE) + '</span>',
                            '<span>' + value.REWARD_CODE + '</span>',
                            '<span>' + value.NAME_TH + '</span>',
                            '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                            '<span>' + value.CREATE_BY + '</span>',
                            '<span>' + reason + '</span>',
                            '<span>' + remark + '</span>'
                        ]);
                        c++;
                    });
                }
                closeLoading();
            }
        });
    } else {
        bootbox.alert({
            title: "แจ้งเตือน",
            message: "<ul>" + frm_validate + "</ul>",
            size: 'small',
            callback: function () {

            }
        });
    }
}
function validate_from_stock(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        frm_validate += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}
function loadreason() {
    showLoading();
    $.ajax({
        url: 'RewardAdjust/ListReasonAdjReward',
        type: "POST",
        async: false,
        cache: false,
        data: {
            type_id: status_reason
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var rows = '';
                $.each(a.items, function (i, value) {
                    rows += '<option value="' + value.REASON_CODE + '">' + value.REASON_DESC_TH + '</option>';
                });
                ddl_reason = rows;
            }
            closeLoading();
        }
    });
}
function loadreason_filter() {
    showLoading();
    $.ajax({
        url: 'RewardAdjust/ListReasonAdjReward',
        type: "POST",
        async: false,
        cache: false,
        data: {
            type_id: status_reason
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var rows = '';
                $.each(a.items, function (i, value) {
                    rows += '<option value="' + value.REASON_CODE + '">' + value.REASON_DESC_TH + '</option>';
                });
                ddl_reason = rows;
                r_adj = 1;
                $('#tb_adjustReward').dataTable().fnClearTable();
                $('#myModalAdjByReward').modal({ backdrop: "static" });
            }
            closeLoading();
        }
    });
}
