﻿var t_courier = "";
var imgshopHiilight = '';
$(document).ready(function () {
    var imgHiilight = '';
    $("input[name=active][value=Y]").attr('checked', 'checked');
    document.getElementById("grp_logistics").style.display = "none";
    $('#tb_Reward').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-right", "targets": [7] },
            { className: "text-center", "targets": [0, 1, 10] }
        ]
    });
    $('#tb_courier').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 4] },
            { className: "text-left", "targets": [1, 2] }
        ]
    });
    $('#tb_supplier').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 4] },
            { className: "text-left", "targets": [1, 2] }
        ]
    });
    $('#tb_category').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 4] },
            { className: "text-left", "targets": [1, 2] }
        ]
    });
    $('#tb_catalogue').dataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 5, 6] },
            { className: "text-left", "targets": [1, 2] }
        ]
    });
    renderReward('');
    setCatalogueType();
    setCategoryType();
    getCourierType();
    getCourier_All_Type();
    //getListProject();
    getSupplier_active();
    $('.images1').fileinput({
        uploadUrl: "#",
        required: true,
        showUpload: false,
        minFileCount: 1,
        maxFileCount: 4,
        maxFileSize: 10000,
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        uploadExtraData: function (previewId, index) {
            return { key: index };
        },
        overwriteInitial: true,
        showPreview: true,
        initialPreviewAsData: true,
    });

    tinymce.init({
        selector: 'textarea',
        height: 150,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code textcolor colorpicker'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css']
    });
    $('#bt_save_supplier').on('click', function () {
        frm_validate_reward = "";
        var vtxt_supplier_name = validate_data($("#str_supplier_name"), $("#vtxt_supplier_name"), "");
        if (vtxt_supplier_name == true) {
            var txt_title = "คุณต้องการบันทึกข้อมูล Supplier ใช่หรือไม่";
            bootbox.confirm({
                title: "<h4></h4>",
                message: txt_title,
                size: 'small',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i>ยกเลิก'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i>ยืนยัน'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var statuson_off = 'N';
                        if ($("#status_supplier").hasClass("active") == true) { statuson_off = 'Y'; }
                        $.ajax({
                            url: 'RewardSetting/SaveSupplier',//
                            type: "POST",
                            async: false,
                            cache: false,
                            data: {
                                SupplierCode: $('#hd_supplier_code').val(),
                                SupplierTH: $('#str_supplier_name').val(),
                                SupplierEN: $('#str_supplier_name_EN').val(),
                                Suppliercontact: $('#str_supplier_contact').val(),
                                Suppliertel: $('#str_supplier_tel').val(),
                                Supplieremail: $('#str_supplier_email').val(),
                                Supplieraddress: $('#str_supplier_address').val(),
                                Supplierlineid: $('#str_supplier_lineID').val(),
                                Supplierworking: $('#str_supplier_working').val(),
                                Active: statuson_off,
                                MemberName: sessionStorage.UserName
                            },
                            success: function (data) {
                                $('#hd_supplier_code').val("");
                                $('#myModalSupplier').modal('hide');
                            }
                        });
                    }
                }
            });
        } else {
            $('#reward_Alert').text("กรุณากรอกข้อมูลให้ครบ");
            $('#reward_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('#bt_save_courier').on('click', function () {
        frm_validate_reward = "";
        var vtxt_courier_name = validate_data($("#str_courier_name"), $("#vtxt_courier_name"), "");
        var vtxt_courier_type = validate_data($("#ddl_couier_type"), $("#vtxt_courier_type"), "");
        var vtxt_courier_lt = true;
        var vtxt_courier_url = true;
        if ($('#ddl_couier_type').val() == "LG") {
            vtxt_courier_lt = validate_data($("#str_courier_logistics"), $("#vtxt_courier_lt"), "");
            vtxt_courier_url = validate_data($("#str_courier_url"), $("#vtxt_courier_url"), "");
        }
        if (vtxt_courier_name == true && vtxt_courier_type == true && vtxt_courier_lt == true && vtxt_courier_url == true) {
            var txt_title = "คุณต้องการบันทึกข้อมูลการจัดส่งใช่หรือไม่";
            bootbox.confirm({
                title: "<h4></h4>",
                message: txt_title,
                size: 'small',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i>ยกเลิก'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i>ยืนยัน'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var statuson_off = 'N';
                        if ($("#status_courier").hasClass("active") == true) { statuson_off = 'Y'; }
                        $.ajax({
                            url: 'RewardSetting/SaveCourier',//
                            type: "POST",
                            async: false,
                            cache: false,
                            data: {
                                Couriercode: $('#hd_courier_id').val(),
                                CourierTH: $('#str_courier_name').val(),
                                CourierEN: $('#str_courier_name_EN').val(),
                                CourierNum: $('#str_courier_num_delivery').val(),
                                CourierType: $('#ddl_couier_type').val(),
                                CourierLogistic: $('#str_courier_logistics').val(),
                                CourierURL: $('#str_courier_url').val(),
                                Active: statuson_off,
                                MemberName: sessionStorage.UserName
                            },
                            success: function (dataResult) {
                                $('#myModalCourier').modal('hide');
                            }
                        });
                    }
                }
            });
        } else {
            $('#reward_Alert').text("กรุณากรอกข้อมูลให้ครบ");
            $('#reward_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('#bt_save_category').on('click', function () {
        frm_validate_reward = "";
        var vtxt_category_name = validate_data($("#str_category_name"), $("#vtxt_category_name"), "");
        if (vtxt_category_name == true) {
            var txt_title = "คุณต้องการบันทึกข้อมูลประเภทของรางวัลใช่หรือไม่";
            bootbox.confirm({
                title: "<h4></h4>",
                message: txt_title,
                size: 'small',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i>ยกเลิก'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i>ยืนยัน'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var statuson_off = 'N';
                        if ($("#status_category").hasClass("active") == true) { statuson_off = 'Y'; }
                        $.ajax({
                            url: 'RewardSetting/SaveCategory',//
                            type: "POST",
                            async: false,
                            cache: false,
                            data: {
                                CategoryCode: $('#hd_category_id').val(),
                                CategoryTH: $('#str_category_name').val(),
                                CategoryEN: $('#str_category_name_EN').val(),
                                Prefixes: $('#str_category_prefixes').val(),
                                Active: statuson_off,
                                DisplaySEQ: $('#str_seqcate').val(),
                                MemberName: sessionStorage.UserName
                            },
                            success: function (data) {
                                $('#myModalCategory').modal('hide');
                            }
                        });
                        $('#myModalCategory').modal('hide');
                    }
                }
            });
        } else {
            $('#reward_Alert').text("กรุณาเพิ่มชื่อประเภทของรางวัล");
            $('#reward_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('#bt_save_catalogue').on('click', function () {
        frm_validate_reward = "";
        var vtxt_catalogue_name = validate_data($("#str_catalogue_name"), $("#vtxt_catalogue_name"), "");
        var vtxt_catalogue_start_date = validate_data($("#str_catalogue_start_date"), $("#vtxt_catalogue_start_date"), "");
        var vtxt_catalogue_end_date = validate_data($("#str_catalogue_end_date"), $("#vtxt_catalogue_end_date"), "");
        if (vtxt_catalogue_name == true && vtxt_catalogue_start_date == true && vtxt_catalogue_end_date == true) {
            var txt_title = "คุณต้องการยืนยันการบันทึกข้อมูลแค็ตตาล็อก";
            //if ($("#hd_catalogue_id").val() != "") {
            //    txt_title = "Confirmation of record name " + $("#str_catalogue_name").val();
            //}
            bootbox.confirm({
                title: "<h4></h4>",
                message: txt_title,
                size: 'small',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i>ยกเลิก'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i>ยืนยัน'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var statuson_off = 'N';
                        if ($("#status_catalogue").hasClass("active") == true) { statuson_off = 'Y'; }
                        var dates1 = $('#str_catalogue_start_date').val().split("/");
                        var datee1 = $('#str_catalogue_end_date').val().split("/");
                        $.ajax({
                            url: 'RewardSetting/SaveCatalogue',//
                            type: "POST",
                            async: false,
                            cache: false,
                             data: {
                                CatalogueCode: $('#hd_catalogue_id').val(),
                                CatalogueTH: $('#str_catalogue_name').val(),
                                CatalogueEN: $('#str_catalogue_name_EN').val(),
                                Active: statuson_off,
                                DateStart: dates1[2] + "-" + dates1[1] + "-" + dates1[0],
                                DateEnd: datee1[2] + "-" + datee1[1] + "-" + datee1[0],
                                MemberName: sessionStorage.UserName
                            },
                           success: function (data) {
                                $('#myModalCatalogue').modal('hide');
                            }
                        });
                        $('#myModalCatalogue').modal('hide');
                    }
                }
            });
        } else {
            $('#reward_Alert').text("กรุณาเพิ่มชื่อแค็ตตาล็อก และวันที่เริ่มต้นแค็ตตาล็อก");
            $('#reward_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('#bt_newReward').on('click', function () {
        setCatalogueType();
        setCategoryType();
        getCourierType();
        $('#hd_username').val(sessionStorage.UserName);
        $('#myModalReward').modal('show');
        cleardata(1);
    });
    $('#bt_newCatalogue').on('click', function () {
        getCatalogue();
        $('#myModalCatalogue').modal('show');
        cleardata(2);
    });
    $('#bt_newCategory').on('click', function () {
        getCategory();
        $('#myModalCategory').modal('show');
        cleardata(3);
    });
    $('#bt_newCourier').on('click', function () {
        getCourier();
        $('#myModalCourier').modal('show');
        cleardata(4);
    });
    $('#bt_newSupplier').on('click', function () {
        getSupplier();
        $('#myModalSupplier').modal('show');
        cleardata(5);
    });
    $('#bt_newSort').on('click', function () {
        //getSupplier();
        //$('#ArgumentsTable').dataTable().fnClearTable();
        renderRewardSort($('#ddl_display').val(), $('#ddl_type').val());
        $('#myModalSetSortReward').modal('show');
        //cleardata(5);
    });
    $('#bt_clearreward').on('click', function () {
        //alert($('#ddl_courier_type').val());
        setCatalogueType();
        setCategoryType();
        getCourierType();
        cleardata(1);
    });
    $('#bt_clearCatalogue').on('click', function () {
        getCatalogue();
        cleardata(2);
    });
    $('#bt_clearCategory').on('click', function () {
        getCategory();
        cleardata(3);
    });
    $('#bt_clearCourier').on('click', function () {
        getCourier();
        cleardata(4);
    });
    $('#bt_clearSupplier').on('click', function () {
        getSupplier();
        cleardata(5);
    });
    $('#bt_save_reward').on('click', function () {
        //alert($('#ddl_courier_type').val());
        t_courier = "";
        $("#ddl_courier_type option:selected").each(function () {
            if (t_courier == "") {
                t_courier = $(this).val();
            }
            else {
                t_courier += "," + $(this).val();
            }
        });
        $("#str_reward_detail").val(tinyMCE.activeEditor.getContent());//$('#str_reward_detail').code()
        frm_validate_reward = "";
        var vtxt_reward_name = validate_data($("#str_reward_name"), $("#vtxt_reward_name"), "Please insert Reward name.");
        var vtxt_reward_start_date = validate_data($("#str_reward_start_date"), $("#vtxt_reward_start_date"), "Please select Reward start date.");
        var vtxt_reward_end_date = validate_data($("#str_reward_end_date"), $("#vtxt_reward_end_date"), "Please select Reward end date.");
        var vtxt_reward_point = validate_data($("#str_reward_point"), $("#vstr_reward_point"), "Please insert Reward Point.");
        var vtxt_reward_price = validate_data($("#str_reward_price"), $("#vstr_reward_price"), "Please insert Reward Price.");
        var vtxt_reward_catalogue = validate_data($("#ddl_catalogue_type"), $("#vddl_catalogue_type"), "Please select Catalogue.");
        var vtxt_reward_category = validate_data($("#ddl_category_type"), $("#vddl_category_type"), "Please select Category.");
        var vtxt_reward_courier = validate_data($("#ddl_courier_type"), $("#vddl_courier_type"), "Please select Courier.");
        if (vtxt_reward_name == true && vtxt_reward_start_date == true && vtxt_reward_end_date == true && vtxt_reward_point == true && vtxt_reward_price == true && vtxt_reward_catalogue == true && vtxt_reward_category == true && vtxt_reward_courier == true) {
            if ($('input[name="str_hilight"]:checked').val() == "Y") {
                var vtxt_special_start_date = validate_data($("#str_special_start_date"), $("#vtxt_special_start_date"), "Please select special start date.");
                var vtxt_special_end_date = validate_data($("#str_special_end_date"), $("#vtxt_special_end_date"), "Please select special end date.");
                var vtxt_special_point = validate_data($("#str_special_point"), $("#vtxt_special_point"), "Please insert special Point.");
                if (vtxt_special_start_date == true && vtxt_special_end_date == true && vtxt_special_point == true) {
                    saveNewReward();
                } else {
                    $('#myModalSpecialPoint').modal({ backdrop: "static" });
                }
            } else {
                saveNewReward();
            }
        } else {
            bootbox.alert({
                title: "กรุณากรอกข้อมูลให้ครบ",
                message: "<ul>" + frm_validate_reward + "</ul>",
                size: 'small',
                callback: function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                }
            });
            //$('#reward_Alert').text("<ul>" + frm_validate_reward + "</ul>");
            //$('#alert_modal').modal('show');
        }
    });
    $('#bt_save_statusRW').on('click', function () {
        $.ajax({
            url: 'RewardSetting/saveStatusReward',//
            type: "POST",
            async: false,
            cache: false,
            data: {
                rewardCode: $('#hd_status_reward_id').val(),
                rewardStatus: $("#hd_status_reward").val(),
                username: sessionStorage.UserName
            },
            success: function (data) {
                if (data.success) {
                    renderReward('');
                }
            }
        });
        $('#myModalActive').modal('hide');
    });
    $("#tb_Reward").on("click", ".dtl_call", function () {
        var rwid = $(this).attr('data-headcall');
        $("#hd_reward_id").val(rwid);
        $.ajax({
            url: 'RewardSetting/GetEditRewardByCode',//
            type: "POST",
            async: false,
            cache: false,
            data: {
                RewardCode: rwid
            },
            success: function (dataResult) {
                var data = JSON.parse(dataResult);
                if (data.success && data.items != null) {
                    var d = data.items;
                    var arr_courier = [];
                    $("#ddl_project").val(d.PROJECT_ID).change();
                    $("#str_reward_name").val(d.REWARD_NAME);
                    $("#str_reward_name_EN").val(d.REWARD_NAME_EN);
                    $("#str_reward_start_date").val(ConvertDateToFormat(d.START_DATE.toString()));
                    $("#str_reward_end_date").val(d.END_DATE == null ? "" : ConvertDateToFormat(d.END_DATE.toString()));
                    $("#str_reward_point").val(d.POINT);
                    $('#str_reward_price').val(d.PRICE)
                    $("#ddl_catalogue_type").val(d.CATALOGUE_CODE).change();
                    $("#ddl_category_type").val(d.CATEGORY_CODE).change();
                    $.each(d.COURIER, function (i, e) {
                        arr_courier.push(e.COURIER_CODE);
                    });
                    if (d.ACTIVE == "Y") {
                        $("#status_reward").addClass("active");
                    } else {
                        $("#status_reward").removeClass("active");
                    }
                    $("#ddl_courier_type").val(arr_courier).change();
                    $("#str_fixed").val(d.FIXED);
                    $("#ddl_supplier").val(d.SUPPLIER_CODE).change();
                    $("#str_seq").val(d.SEQ);
                    $("#str_reward_brand").val(d.BRAND);
                    $("#str_reward_model").val(d.MODEL);
                    tinyMCE.get('str_reward_detail').setContent(d.REWARD_DESC);
                    tinyMCE.get('str_reward_detailEN').setContent(d.REWARD_DESC_EN);
                    tinyMCE.get('str_condition').setContent(d.REWARD_CONDITION);
                    tinyMCE.get('str_conditionEN').setContent(d.REWARD_CONDITION_EN);
                    tinyMCE.get('str_step').setContent(d.REWARD_STEP);
                    tinyMCE.get('str_stepEN').setContent(d.REWARD_STEP_EN);
                    //var urlIMG = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzkl98D3Nojy83lZkW7GNn_RFlL6X_utqPpO_ROYRsUBmbMscF";
                    //$('.imgAvatar').attr('src', urlIMG);
                    //d.ACTIVE == "Y" ? $('#radio-active1').iCheck('check') : $('#radio-active2').iCheck('check');
                    d.TIER == "Y" ? $('#str_tier1').iCheck('check') : $('#str_tier2').iCheck('check');
                    d.HILIGHT == "Y" ? $('#str_hilight1').iCheck('check') : $('#str_hilight2').iCheck('check');
                    d.MAINPAGE == "Y" ? $('#str_mainpage1').iCheck('check') : $('#str_mainpage2').iCheck('check');
                    //d.STOCK == "Y" ? $('#str_stock1').iCheck('check') : $('#str_stock2').iCheck('check');
                    $.each(d.SpecialPoint, function (i, e) {
                        var datef = new Date(e.START_DATE);
                        var datet = new Date(e.END_DATE);
                        $("#str_special_point").val(e.SPECIAL_POINT);
                        $("#str_special_start_date").val(addZeroBefore(datef.getDate()) + "/" + addZeroBefore(datef.getMonth() + 1) + "/" + datef.getFullYear());
                        $("#str_special_end_date").val(addZeroBefore(datet.getDate()) + "/" + addZeroBefore(datet.getMonth() + 1) + "/" + datet.getFullYear());
                    });


                    var img = [];
                    var imgconfig = [];
                    var firstImage = 'Y';
                    $.each(d.image, function (i, e) {
                        img.push(sessionStorage.backendUrl + "/Content/img/Reward/" + e.imageLarge);
                        if (firstImage == 'Y') {
                            imgshopHiilight = e.imageLarge;
                            firstImage = 'N';
                        }
                        imgconfig.push({
                            type: "image",
                            caption: e.imageLarge,
                            width: "120px",
                            key: i + 1
                        })
                    });
                    $('.images1').fileinput('destroy');
                    $(".images1").fileinput({
                        uploadUrl: "#",
                        required: true,
                        showUpload: false,
                        minFileCount: 1,
                        maxFileCount: 4,
                        maxFileSize: 10000,
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        uploadExtraData: function (previewId, index) {
                            return { key: index };
                        },
                        overwriteInitial: true,
                        initialPreviewAsData: true,
                        initialPreview: img,
                        initialPreviewConfig: imgconfig,
                    })
                    $('#myModalReward').modal('show');
                }
            }
        });
    });
    $('#str_tier2').on('ifChecked', function (event) {
        $('#myModalPointByTier').modal({ backdrop: "static" });
    });
    $('#str_hilight1').on('ifClicked', function (event) {
        if ($('#str_special_point').val() == "") {
            $('#str_special_point').val($('#str_reward_point').val());
        }
        $('#myModalSpecialPoint').modal({ backdrop: "static" });
    });
    $("#tb_Reward").on("click", ".strflag_active", function () {
        $("#hd_status_reward_id").val($(this).attr('data-headcall'));
        $("#hd_status_reward").val("Y");
        $("#status_rw").text("คุณต้องการ เปิดรายการของรางวัลใช่หรือไม่");
        if ($(this).attr('data-extra') == "Y") {
            $("#hd_status_reward").val("N");
            $("#status_rw").text("คุณต้องการ ปิดรายการของรางวัลใช่หรือไม่");
        }
        $('#myModalActive').modal('show');
    });
    $('#filter_all_reward').on('click', function () {
        renderReward('');
    });
    $('#filter_active_reward').on('click', function () {
        renderReward('Y');
    });
    $('#filter_not_active_reward').on('click', function () {
        renderReward('N');
    });
    $('#close_alert').on('click', function () {
        $('#alert_modal').modal("hide");
    });

    $('#reward-date-range').datepicker({ todayHighlight: true, format: "dd/mm/yyyy", autoclose: "true" });
    $('#str_reward_start_date').on('change', function () {
        if ($('#str_reward_end_date').val() < $('#str_reward_start_date').val()) {
            $('#str_reward_end_date').val($('#str_reward_start_date').val()).change;
        }
    });
    $('#str_reward_end_date').on('change', function () {
        if ($('#str_reward_end_date').val() < $('#str_reward_start_date').val()) {
            $('#str_reward_start_date').val($('#str_reward_end_date').val()).change;
        }
    });
    $('#ddl_couier_type').on('change', function () {
        if ($('#ddl_couier_type').val() == "LG") {
            document.getElementById("grp_logistics").style.display = "block";
        } else {
            document.getElementById("grp_logistics").style.display = "none";
        }
    });

    $('#catalogue-date-range').datepicker({ todayHighlight: true, format: "dd/mm/yyyy", autoclose: "true" });
    $('#str_catalogue_start_date').on('change', function () {
        if ($('#str_catalogue_end_date').val() < $('#str_catalogue_start_date').val()) {
            $('#str_catalogue_end_date').val($('#str_catalogue_start_date').val()).change;
        }
    });
    $('#str_catalogue_end_date').on('change', function () {
        if ($('#str_catalogue_end_date').val() < $('#str_catalogue_start_date').val()) {
            $('#str_catalogue_start_date').val($('#str_catalogue_end_date').val()).change;
        }
    });

    $("#tbody_courier").on("click", ".clk_detail_courier", function () {
        var courier_no = $(this).attr('data_courier');
        $("#hd_courier_id").val(courier_no);
        $.ajax({
            url: 'RewardSetting/GetCourierByCode',//
            type: "POST",
            async: false,
            cache: false,
            data: {
                COURIER_CODE: courier_no
            },
            success: function (dataResult) {
                var data = JSON.parse(dataResult);
                if (data.success) {
                    $("#str_courier_name").val(data.items.COURIER_NAME);
                    $("#str_courier_name_EN").val(data.items.COURIER_NAME_EN);
                    $("#str_courier_num_delivery").val(data.items.DELIVERY_DAY);
                    $("#ddl_couier_type").val(data.items.COURIER_TYPE).change;
                    $("#str_courier_logistics").val(data.items.COURIER_LOGISTICS);
                    $("#str_courier_url").val(data.items.URL).change;
                    if (data.items.ACTIVE == "Y") {
                        $("#status_courier").addClass("active");
                    } else {
                        $("#status_courier").removeClass("active");
                    }
                    if ($('#ddl_couier_type').val() == "LG") {
                        document.getElementById("grp_logistics").style.display = "block";
                    } else {
                        document.getElementById("grp_logistics").style.display = "none";
                    }
                }
            }
        });
    });
    $("#tbody_category").on("click", ".clk_detail_category", function () {
        var category_no = $(this).attr('data_category');
        $("#hd_category_id").val(category_no);
        $("#str_category_name").val($(this).parents("tr").children("td").eq(1).children("span").text());
        $("#str_category_name_EN").val($(this).parents("tr").children("td").eq(2).children("span").text());
        if ($(this).parents("tr").children("td").eq(3).children("span").text() == "เปิด") {
            $("#status_category").addClass("active");
        } else {
            $("#status_category").removeClass("active");
        }
        getCategoryByCode(category_no);
    });
    $("#tbody_catalogue").on("click", ".clk_detail_catalogue", function () {
        var category_no = $(this).attr('data_catalogue');
        $("#hd_catalogue_id").val(category_no);
        $("#str_catalogue_name").val($(this).parents("tr").children("td").eq(1).children("span").text());
        $("#str_catalogue_name_EN").val($(this).parents("tr").children("td").eq(2).children("span").text());
        if ($(this).parents("tr").children("td").eq(5).children("span").text() == "เปิด") {
            $("#status_catalogue").addClass("active");
        } else {
            $("#status_catalogue").removeClass("active");
        }
        getCatalogueByCode(category_no);
    });
    $('#ddl_catalogue_type').on('change', function () {
        $.ajax({
            url: 'RewardSetting/ListCatalogueByCode',//
            type: "POST",
            async: false,
            cache: false,
            data: {
                itemecode: $('#ddl_catalogue_type').val()
            },
            success: function (dataResult) {
                var data = JSON.parse(dataResult);
                if (data.success) {
                    $('#str_reward_start_date').val(data.items.START_DATE);
                    $('#str_reward_end_date').val(data.items.END_DATE);
                    $('#str_special_start_date').val(data.items.START_DATE);
                    $('#str_special_end_date').val(data.items.END_DATE);
                }
            }
        });
    });
    $("#tbody_supplier").on("click", ".clk_detail_supplier", function () {
        var supplier_no = $(this).attr('data_supplier');
        $("#hd_supplier_code").val(supplier_no);
        $.ajax({
            url: 'RewardSetting/GetListSupplierByCode',//
            type: "POST",
            async: false,
            cache: false,
            data: {
                SupplierCode: supplier_no
            },
            success: function (dataResult) {
                var data = JSON.parse(dataResult);
                if (data.success) {
                    $.each(data.items, function (i, value) {
                        $("#str_supplier_name").val(value.SUPPLIER_NAME_TH);
                        $("#str_supplier_name_EN").val(value.SUPPLIER_NAME_EN);
                        $("#str_supplier_contact").val(value.SUPPLIER_CONTACT);
                        $("#str_supplier_address").val(value.SUPPLIER_ADDRESS);
                        $("#str_supplier_tel").val(value.SUPPLIER_PHONE);
                        $("#str_supplier_email").val(value.SUPPLIER_EMAIL);
                        $("#str_supplier_lineID").val(value.SUPPLIER_LINE_ID);
                        $("#str_supplier_working").val(value.SUPPLIER_WORKING);
                        if (value.ACTIVE == "Y") {
                            $("#status_supplier").addClass("active");
                        } else {
                            $("#status_supplier").removeClass("active");
                        }
                    });
                }
            }
        });
    });

    $('#images1').change(function () {
        path = "Reward";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            console.log(e)
            data.append('file', e);
            data.append('path', path);
            var imgLg = UploadImage(data, path)
        });
    });
    $('#bt_save_sortReward').on('click', function () {
        saveSort();
    });
    $('#btn_searchSort').on('click', function () {
        $('#ArgumentsTable').dataTable().fnClearTable();
        renderRewardSort($('#ddl_display').val(), $('#ddl_type').val());
    });
});
function renderRewardSort(mainP, hilightP) {
    showLoading();
    $.ajax({
        url: 'RewardSetting/GetListRewardSort',//
        type: "POST",
        async: false,
        cache: false,
        data: {
            Mainpage: mainP,
            Highlight: hilightP
        },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                setSortReward(data.items);
                //var datajson = data.items;
                //$('#ArgumentsTable').dataTable().fnClearTable();
                //$.each(datajson, function (i, e) {
                //    var b_up = '';
                //    if (i > 0) {
                //        b_up = '<span><a class="dtMoveUp">Up</a></span>';
                //    }
                //    $('#ArgumentsTable').dataTable().fnAddData([
                //        (i + 1),
                //        e.REWARD_CODE,
                //        e.NAME_TH,
                //        commaSeparateNumber(e.POINT),
                //        e.START_DATE,
                //        e.END_DATE,
                //        e.IS_HILIGHT,
                //        e.IS_MAINPAGE,
                //        e.DISPLAY_SEQ,
                //        b_up + " " + '<span><a class="dtMoveDown">Down</a></span>'
                //    ]);
                //});
            }
            closeLoading();
        }
    });
}
function setSortReward(data) {
    var dataArguments = data;
    DestroyDataTable($('#ArgumentsTable'));
    var table = $('#ArgumentsTable').DataTable({
        paging: false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 6, 7, 8] },
            { className: "text-right", "targets": [3] }
        ],
        data: dataArguments,
        columns: [
			{
			    name: 'order',
			    title: '#',
			    data: 'orderby',
			    sortable: false
			},
			{
			    name: 'REWARD_CODE',
			    data: 'REWARD_CODE',
			    title: 'รหัสรางวัล',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'NAME_TH',
			    data: 'NAME_TH',
			    title: 'ชื่อรางวัล',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'POINT',
			    data: 'POINT',
			    title: 'คะแนน',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'START_DATE',
			    data: 'START_DATE',
			    title: 'วันที่เริ่ม',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'END_DATE',
			    data: 'END_DATE',
			    title: 'วันที่สิ้นสุด',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'IS_HILIGHT',
			    data: 'IS_HILIGHT',
			    title: 'พิเศษ',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'IS_MAINPAGE',
			    data: 'IS_MAINPAGE',
			    title: 'แสดงหน้าหลัก',
			    searchable: true,
			    sortable: false
			},
			{
			    name: 'action',
			    data: null,
			    title: 'Action',
			    searchable: false,
			    sortable: false,
			    render: function (data, type, full, meta) {
			        if (type === 'display') {
			            var $span = $('<span></span>');

			            if (meta.row > 0) {
			                $('<a class="fa fa-chevron-up dtMoveUp"></a>').appendTo($span);
			            }

			            $('<a class="fa fa-chevron-down dtMoveDown"></a>').appendTo($span);

			            return $span.html();
			        }
			        return data;
			    }
			}
        ],
        'drawCallback': function (settings) {
            $('#ArgumentsTable tr:last .dtMoveDown').remove();

            // Remove previous binding before adding it
            $('.dtMoveUp').unbind('click');
            $('.dtMoveDown').unbind('click');

            // Bind clicks to functions
            $('.dtMoveUp').click(moveUp);
            $('.dtMoveDown').click(moveDown);
        }
    });

    // Move the row up
    function moveUp() {
        var tr = $(this).parents('tr');
        moveRow(tr, 'up');
    }

    // Move the row down
    function moveDown() {
        var tr = $(this).parents('tr');
        moveRow(tr, 'down');
    }

    // Move up or down (depending...)
    function moveRow(row, direction) {
        var index = table.row(row).index();

        var order = -1;
        if (direction === 'down') {
            order = 1;
        }

        var data1 = table.row(index).data();
        data1.order += order;

        var data2 = table.row(index + order).data();
        data2.order += -order;

        table.row(index).data(data2);
        table.row(index + order).data(data1);

        table.page(0).draw(false);
    }
} (jQuery);
function saveSort() {
    showLoading();
    var tableSort = $('#ArgumentsTable').DataTable();
    var params = [];
    tableSort
        .column(1)
        .data()
        .each(function (value, index) {
            //console.log('Data in index: ' + index + ' is: ' + value);
            params.push({
                REWARD_CODE: value,
                SEQ: index + 1
            });
        });
    //console.log(params);
    $.ajax({
        url: 'RewardSetting/updateSEQReward',//
        type: "POST",
        async: false,
        cache: false,
        data: {
            Highlight: $('#ddl_type').val(),
            Mainpage: $('#ddl_display').val(),
            ListSEQ: JSON.stringify(params)
        },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#myModalSetSortReward').modal('hide');
                $('#reward_Alert').text("บันทึกการเรียงลดับการแสดงเรียบร้อย");
                $('#reward_Alert2').text("");
                $('#alert_modal').modal('show');
            }
            closeLoading();
        }
    });
}
function saveNewReward() {
    var txt_title = "คุณต้องการยืนยันการบันทึกของรางวัลใช่หรือไม่";
    $("#hd_img").val(ListImage("images1"));
    var params = [];
    bootbox.confirm({
        title: "<h4></h4>",
        message: txt_title,
        size: 'small',
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i>ยกเลิก'
            },
            confirm: {
                label: '<i class="fa fa-check"></i>ตกลง'
            }
        },
        callback: function (result) {
            if (result) {
                var d1 = $('#str_reward_start_date').val().split("/");
                var d2 = $('#str_reward_end_date').val().split("/");
                var d3 = $('#str_special_start_date').val() == "" ? "" : $('#str_special_start_date').val().split("/");
                var d4 = $('#str_special_end_date').val() == "" ? "" : $('#str_special_end_date').val().split("/");
                var tierbypoint = "";
                tierbypoint = $('#str_tier_pl').val() == "" ? "" : $('#str_tier_pl').val() + "," + $('#str_tier_gp').val() + "," + $('#str_tier_gr').val();
                var specialPoint = $('#str_special_point').val() == "" ? "0" : $('#str_special_point').val();
                var detail_th = tinyMCE.get('str_reward_detail').getContent() == null ? "<p></P>" : tinyMCE.get('str_reward_detail').getContent();
                var detail_en = tinyMCE.get('str_reward_detailEN').getContent() == null ? "<p></P>" : tinyMCE.get('str_reward_detailEN').getContent();
                var Condition_th = tinyMCE.get('str_condition').getContent() == null ? "<p></P>" : tinyMCE.get('str_condition').getContent();
                var Condition_en = tinyMCE.get('str_conditionEN').getContent() == null ? "<p></P>" : tinyMCE.get('str_conditionEN').getContent();
                var step_th = tinyMCE.get('str_step').getContent() == null ? "<p></P>" : tinyMCE.get('str_step').getContent();
                var step_en = tinyMCE.get('str_stepEN').getContent() == null ? "<p></P>" : tinyMCE.get('str_stepEN').getContent();
                var statuson_off = 'N';
                if ($("#status_reward").hasClass("active") == true) { statuson_off = 'Y'; }
                params.push({
                    rewardCode: $('#hd_reward_id').val(),
                    rewardName: $('#str_reward_name').val(),
                    rewardNameEN: $('#str_reward_name_EN').val(),
                    startDate: d1[2] + "-" + d1[1] + "-" + d1[0],
                    endDate: d2[2] + "-" + d2[1] + "-" + d2[0],
                    specialStartDate: d3[2] + "-" + d3[1] + "-" + d3[0],
                    specialEndDate: d4[2] + "-" + d4[1] + "-" + d4[0],
                    point: ($('#str_reward_point').val()),
                    specialpoint: (specialPoint),
                    tierpoint: tierbypoint,
                    price: ($('#str_reward_price').val()),
                    catalogueCode: ($('#ddl_catalogue_type').val()),
                    categoryCode: ($('#ddl_category_type').val()),
                    courierCode: t_courier,
                    rewardDetailTH: detail_th,
                    rewardDetailEN: detail_en,
                    fileNamePic: $("#hd_img").val(),
                    qtyFixed: $('#str_fixed').val(),
                    active: statuson_off,
                    username: sessionStorage.UserName,
                    rewardBrand: $('#str_reward_brand').val(),
                    rewardModel: $('#str_reward_model').val(),
                    tier: "",
                    mainPage: $('input[name="str_mainpage"]:checked').val(),
                    hilight: $('input[name="str_hilight"]:checked').val(),
                    seq: "0",
                    supplier: $('#ddl_supplier').val(),
                    rewardConditionTH: Condition_th,
                    rewardConditionEN: Condition_en,
                    rewardStepTH: step_th,
                    rewardSteplEN: step_en,
                    rewardProject: "0"
                });
                var str_dataRew = JSON.stringify(params);
                var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
                var encodedString = Base64.encode(str_dataRew);
                $.ajax({
                    url: 'RewardSetting/saveNewReward',
                    type: "POST",
                    async: false,
                    cache: false,
                    data: {
                        DataRew: encodedString
                    },
                    success: function (dataResult) {
                        var data = JSON.parse(dataResult);
                        if (data.success) {
                            $('#myModalReward').modal('hide');
                            if ($('input[name="str_hilight"]:checked').val() == "Y") {
                                $('#section-hilight-reward').removeClass('hidden');
                                $('.img_reward_special').attr('src', 'Content/img/Reward/' + imgshopHiilight);
                                // $('.img_reward_special').attr('src', 'Content/img/Reward/2018_APPLE-MacBook-Pro-13_128-GB_03.jpg');
                                $('.txt-hilight-reward').text($('#str_reward_name').val());
                                //genHtmlForEDM_Hilight(imgshopHiilight);

                            } else {
                                //normal 
                                $('#section-reward').removeClass('hidden');
                                $('#img-edm-reward').attr('src', 'Content/img/Reward/' + imgshopHiilight);
                                //  $('#img-edm-reward').attr('src', 'Content/img/Reward/2018_APPLE-MacBook-Pro-13_128-GB_03.jpg');
                                $('#reward-edm').text($('#str_reward_name').val());

                                //genHtmlForEDM(imgshopHiilight);
                            }
                            renderReward('');
                        } else {
                            return false;
                        }
                    }
                });
            }
        }
    });
}
function completePrice(point) {
    $('#str_reward_price').val((point.value * 2));
}

var genHtmlForEDM = function (imgName) {
    //console.log('../../Content/img/privilege/edm/' + imgName)
    //console.log(data.privilegeRibbonTH)
    //console.log($('#shop-privilege option:selected').html())
    //console.log(data.privilegeDescriptionTH)


    html2canvas(document.getElementById("section-reward"), {
        width: 255,
        height: 270
    }).then(function (canvas) {
        var base64image = canvas.toDataURL("image/png");
        var img = canvas.toDataURL().replace(/^data[:]image\/(png|jpg|jpeg)[;]base64,/i, "");
        $.ajax({
            type: "POST",
            url: "Upload/saveImageReward",
            data: {
                imageData: img,
                filename: 'edm_' + imgName
            }
        }).done(function () {
            $('#section-reward').addClass('hidden')
        })
    });
};
var genHtmlForEDM_Hilight = function (imgName) {
    html2canvas(document.querySelector("#section-hilight-reward"), { width: 585, height: 265 })
        .then(function (canvas) {

            var base64image = canvas.toDataURL("image/png");
            var img = canvas.toDataURL().replace(/^data[:]image\/(png|jpg|jpeg)[;]base64,/i, "");
            $.ajax({
                type: "POST",
                url: "Upload/saveImageReward",
                data: {
                    imageData: img,
                    filename: 'edm_hilight_reward_' + imgName
                }
            }).done(function () {
                $('#section-hilight-reward').addClass('hidden')
            })
        });
};

function setCatalogueType() {
    var rows = '';
    $.ajax({
        url: 'Redemption/getListCatalogue',//
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                var todate = new Date();
                var datenow = todate.getFullYear() + "-" + addZeroBefore(todate.getMonth() + 1) + "-" + addZeroBefore(todate.getDate());
                $.each(data.items, function (i, value) {
                    if (value.ACTIVE == "Y" && (value.START_DATE <= datenow && value.END_DATE >= datenow)) {
                        cols = '<option value="' + value.CATALOGUE_CODE + '">' + value.CATALOGUE_NAME + '</option>';
                        rows += cols;
                    }
                });
                $('#ddl_catalogue_type').html(rows);
                $('#ddl_catalogue_type').val($('#ddl_catalogue_type option:first').val()).trigger('change');
            }
        }
    });
}
function setCategoryType() {
    var rows = '<option value="">Please select category type.</option>';
    $.ajax({
        url: 'Redemption/ListCategory',//
        type: "POST",
        async: false,
        cache: false,
        data: {},
        async: true,
        cache: false,
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $.each(data.items, function (i, value) {
                    if (value.ACTIVE == "Y") {
                        cols = '<option value="' + value.CATEGORY_CODE + '">' + value.CATEGORY_NAME + '</option>';
                        rows += cols;
                    }
                });
                $('#ddl_category_type').html(rows);
                $('#ddl_category_type').val("").change();
            }
        }
    });
}
function getCourierType() {
    var rows = '';
    $.ajax({
        url: 'RewardSetting/ListCourier',//
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $.each(data.items, function (i, value) {
                    if (value.ACTIVE == "Y") {
                        cols = '<option value="' + value.COURIER_CODE + '">' + value.COURIER_NAME + '</option>';
                        rows += cols;
                    }
                });
                $('#ddl_courier_type').html(rows);
                $('#ddl_courier_type').val("").change();
            }
        }
    });
}
function getListProject() {
    $.ajax({
        url: 'RewardAdjust/list_project',//
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                var rows = '';
                $.each(data.items, function (i, value) {
                    rows += '<option value="' + value.projectId + '">' + value.projectName + '</option>';
                });
                $('#ddl_project').html(rows);
                $('#ddl_project').val($('#ddl_project option:first').val()).trigger('change');
            }
        }
    });
}
function validate_data(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        frm_validate_reward += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}
function cleardata(caseData) {
    if (caseData == 1) {
        $('#hd_reward_id').val("");
        $("#str_reward_name").val("");
        $("#str_reward_name_EN").val("");
        $("#str_reward_start_date").val("");
        $("#str_reward_end_date").val("");
        $("#str_reward_point").val("");
        $('#str_special_point').val("")
        var $el4 = $('#images1'), initPlugin = function () {
            $el4.fileinput({ previewClass: '' });
        };
        $el4.fileinput('clear');
        $("#ddl_catalogue_type").val("").change();
        $("#ddl_category_type").val("").change();
        $("#ddl_courier_type").val("").change();
        $("#str_fixed").val('');
        $("#str_seq").val('');
        $("#ddl_supplier").val("").change();
        $("#str_reward_brand").val('');
        $("#str_reward_model").val('');
        $("#str_reward_price").val('');
        $("#vstr_reward_name").html('');
        $("#vstr_reward_start_date").html('');
        $("#vstr_reward_end_date").html('');
        $("#vstr_reward_point").html('');
        $("#vstr_reward_catalogue").html('');
        $("#vstr_reward_category").html('');
        $("#vstr_reward_courier").html('');
        //tinyMCE.activeEditor.setContent('');
        tinyMCE.get('str_reward_detail').setContent('');
        tinyMCE.get('str_reward_detailEN').setContent('');
        tinyMCE.get('str_condition').setContent('');
        tinyMCE.get('str_conditionEN').setContent('');
        tinyMCE.get('str_step').setContent('');
        tinyMCE.get('str_stepEN').setContent('');
        //$("#str_reward_detail").code('');
    }
    else if (caseData == 2) {
        $('#hd_catalogue_id').val("");
        $("#str_catalogue_name").val("").change();
        $("#str_catalogue_name_EN").val("").change();
        $("#str_catalogue_start_date").val("").change();
        $("#str_catalogue_end_date").val("").change();
        $("#vtxt_catalogue_name").html('');
        $("#vtxt_catalogue_start_date").html('');
        $("#vtxt_catalogue_end_date").html('');
        $("#status_catalogue").addClass("active");
    }
    else if (caseData == 3) {
        $('#hd_category_id').val("");
        $("#str_category_name").val("");
        $("#str_category_name_EN").val("");
        $('#str_category_prefixes').val("");
        $("#str_seqcate").val("");
        $("#vtxt_category_name").html('');
        $("#status_category").addClass("active");
    }
    else if (caseData == 4) {
        $('#hd_courier_id').val("");
        $("#str_courier_name").val("");
        $("#str_courier_name_EN").val("");
        $("#str_courier_num_delivery").val("");
        $('#ddl_couier_type').val("").change;
        $("#str_courier_logistics").val("");
        $("#str_courier_url").val("");
        $("#vtxt_courier_name").html('');
        $("#status_courier").addClass("active");
    }
    else if (caseData == 5) {
        $('#hd_supplier_code').val("");
        $("#str_supplier_name").val("");
        $("#str_supplier_name_EN").val("");
        $("#vtxt_supplier_name").html('');
        $("#status_supplier").addClass("active");
        $("#str_supplier_contact").val("");
        $("#str_supplier_address").val("");
        $("#str_supplier_tel").val("");
        $("#str_supplier_email").val("");
        $("#str_supplier_lineID").val("");
        $("#str_supplier_working").val("");
    }
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function SelectPicture(ele) {
    var ObjectID = $(ele).attr('id');
    if (ObjectID == 'str_reward_picture') {
        $('.imgAvatar').attr('src', ele.value); // for IE
        if (ele.files && ele.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.imgAvatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(ele.files[0]);
        }
    }
}
function getCourier() {
    $.ajax({
        url: 'RewardSetting/ListCourier',//
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#tb_courier').dataTable().fnClearTable();
                $.each(data.items, function (i, value) {
                    var activeonoff = "<span class=\"label label-danger \">ปิด</span>";
                    if (value.ACTIVE == "Y") { activeonoff = "<span class=\"label label-success\">เปิด</span>"; }
                    var btn_edit = '<button class="btn btn-warning btn-sm clk_detail_courier" style="padding: 0 5px;" data_courier="' + value.COURIER_CODE + '"><i class="fa fa fa-pencil"></i>&nbsp; แก้ไข</button>'
                    $('#tb_courier').dataTable().fnAddData([
                        '<span style="cursor:pointer;" class="clk_detail_courier" data_courier = "' + value.COURIER_CODE + '">' + value.COURIER_CODE + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_courier" data_courier = "' + value.COURIER_CODE + '">' + value.COURIER_NAME + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_courier" data_courier = "' + value.COURIER_CODE + '">' + value.COURIER_NAME_EN + '</span>',
                        activeonoff,
                        btn_edit,
                    ]);
                });
            }
        }
    });
}
function getCourier_All_Type() {
    $.ajax({
        url: 'RewardSetting/ListCourierType',//
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                rows = '<option value=""> ---กรุณาเลือก--- </option>';
                $.each(data.items, function (i, value) {
                    if (value.ACTIVE == "Y") {
                        cols = '<option value="' + value.COURIER_TYPE_CODE + '">' + value.COURIER_TYPE_NAME + '</option>';
                        rows += cols;
                    }
                });
                $('#ddl_couier_type').html(rows);
                $('#ddl_couier_type').val("").change();
            }
        }
    });
}
function getSupplier() {
    $.ajax({
        url: 'RewardSetting/GetListSupplier',//
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#tb_supplier').dataTable().fnClearTable();
                $.each(data.items, function (i, value) {
                    var activeonoff = "<span class=\"label label-danger \">ปิด</span>";
                    if (value.ACTIVE == "Y") { activeonoff = "<span class=\"label label-success\">เปิด</span>"; }
                    var btn_edit = '<button class="btn btn-warning btn-sm clk_detail_supplier" style="padding: 0 5px;" data_supplier="' + value.SUPPLIER_CODE + '"><i class="fa fa fa-pencil"></i>&nbsp; แก้ไข</button>'
                    $('#tb_supplier').dataTable().fnAddData([
                        '<span style="cursor:pointer;" class="clk_detail_supplier" data_supplier = "' + value.SUPPLIER_CODE + '">' + value.SUPPLIER_CODE + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_supplier" data_supplier = "' + value.SUPPLIER_CODE + '">' + value.SUPPLIER_NAME_TH + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_supplier" data_supplier = "' + value.SUPPLIER_CODE + '">' + value.SUPPLIER_NAME_EN + '</span>',
                        activeonoff,
                        btn_edit,
                    ]);
                });
            }
        }
    });
}
function getSupplier_active() {
    var rows = '';
    $.ajax({
        url: 'RewardSetting/GetListSupplier_active',//
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $.each(data.items, function (i, value) {
                    if (value.ACTIVE == "Y") {
                        cols = '<option value="' + value.SUPPLIER_CODE + '">' + value.SUPPLIER_NAME_TH + '</option>';
                        rows += cols;
                    }
                });
                $('#ddl_supplier').html(rows);
                $('#ddl_supplier').val("").change();
            }
        }
    });
}
function getCategory() {
    $.ajax({
        url: 'Redemption/ListCategory',//
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#tb_category').dataTable().fnClearTable();
                $.each(data.items, function (i, value) {
                    var activeonoff = "<span class=\"label label-danger \">ปิด</span>";
                    var cate = value.CATEGORY_NAME_EN==null?"":value.CATEGORY_NAME_EN;
                    if (value.ACTIVE == "Y") { activeonoff = "<span class=\"label label-success\">เปิด</span>"; }
                    var btn_edit = '<button class="btn btn-warning btn-sm clk_detail_category" style="padding: 0 5px;" data_category="' + value.CATEGORY_CODE + '"><i class="fa fa fa-pencil"></i>&nbsp; แก้ไข</button>'
                    $('#tb_category').dataTable().fnAddData([
                        '<span style="cursor:pointer;" class="clk_detail_category" data_category = "' + value.CATEGORY_CODE + '">' + value.CATEGORY_CODE + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_category" data_category = "' + value.CATEGORY_CODE + '">' + cate + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_category" data_category = "' + value.CATEGORY_CODE + '">' + value.CATEGORY_NAME_EN + '</span>',
                        activeonoff,
                        btn_edit,
                    ]);
                });
            }
        }
    });
}
function getCategoryByCode(categorycode) {
    $.ajax({
        url: 'RewardSetting/ListCategoryByCode',//
        type: "POST",
        async: false,
        cache: false,
        data: {
            itemecode: categorycode
        },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#str_category_prefixes').val(data.items.PREFIXES);
                $('#str_seqcate').val(data.items.DISPLAY_SEQ);
            }
        }
    });
}
function getCatalogue() {
    $.ajax({
        url: 'Redemption/getListCatalogue',//
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#tb_catalogue').dataTable().fnClearTable();
                $.each(data.items, function (i, value) {
                    var cata_en = value.CATALOGUE_NAME_EN == null ? "" : value.CATALOGUE_NAME_EN;
                    var activeonoff = "<span class=\"label label-danger \">ปิด</span>";
                    if (value.ACTIVE == "Y") { activeonoff = "<span class=\"label label-success\">เปิด</span>"; }
                    var btn_edit = '<button class="btn btn-warning btn-sm clk_detail_catalogue" style="padding: 0 5px;" data_catalogue="' + value.CATALOGUE_CODE + '"><i class="fa fa fa-pencil"></i>&nbsp; แก้ไข</button>'
                    $('#tb_catalogue').dataTable().fnAddData([
                        '<span style="cursor:pointer;" class="clk_detail_catalogue" data_catalogue = "' + value.CATALOGUE_CODE + '">' + value.CATALOGUE_CODE + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_catalogue" data_catalogue = "' + value.CATALOGUE_CODE + '">' + value.CATALOGUE_NAME + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_catalogue" data_catalogue = "' + value.CATALOGUE_CODE + '">' + cata_en + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_catalogue" data_catalogue = "' + value.CATALOGUE_CODE + '">' + ConvertDateToFormat(value.START_DATE) + '</span>',
                        '<span style="cursor:pointer;" class="clk_detail_catalogue" data_catalogue = "' + value.CATALOGUE_CODE + '">' + ConvertDateToFormat(value.END_DATE) + '</span>',
                        activeonoff,
                        btn_edit,
                    ]);
                });
            }
        }
    });
}
function getCatalogueByCode(cataloguecode) {
    $.ajax({
        url: 'RewardSetting/ListCatalogueByCode',//
        type: "POST",
        async: false,
        cache: false,
        data: {
            itemecode: cataloguecode
        },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            if (data.success) {
                $('#str_catalogue_start_date').val(data.items.START_DATE);
                $('#str_catalogue_end_date').val(data.items.END_DATE);
            }
        }
    });
}
function renderReward(active) {
    showLoading();
    $.ajax({
        url: 'Redemption/ListRewardNew',//
        type: "POST",
        async: false,
        cache: false,
        data: {
            ALL_TIER: "",
            START_DATE: "",
            END_DATE: "",
            CATALOGUE_CODE: "",
            CATEGORY_CODE: "",
            IS_HILIGHT: "",
            IS_MAINPAGE: "",
            TIER: "",
            POINT: 0,
            TAG: "",
            MODEL: "",
            BRAND: "",
            ACTIVE: active
        },
        success: function (dataResult) {
            var data = JSON.parse(dataResult);
            var date_now = new Date();
            var head = '', rows = '';
            if (data.success) {
                var datajson = data.items;
                datajson.sort(function (a, b) {
                    return a.NAME_TH.localeCompare(b.NAME_TH);
                });
                $('#tb_Reward').dataTable().fnClearTable();
                $('#example').dataTable().fnClearTable();
                $.each(datajson, function (i, e) {
                    var cols = '';
                    var date_start = "";
                    var date_end = "";
                    var d1 = e.START_DATE.split("T");
                    var d2 = e.END_DATE == null ? "" : e.END_DATE.split("T");
                    if (d2 != "") {
                        var dateE = new Date(d2);
                        date_end = addZeroBefore(dateE.getDate()) + '/' + addZeroBefore(parseInt(dateE.getMonth()) + 1) + '/' + dateE.getFullYear();
                    }
                    var dateS = new Date(d1[0]);
                    date_start = addZeroBefore(dateS.getDate()) + '/' + addZeroBefore(parseInt(dateS.getMonth()) + 1) + '/' + dateS.getFullYear();
                    var str_status = "<a class=\"strflag_active\" data-headcall= '" + e.REWARD_CODE + "' data-extra='" + e.ACTIVE + "' style=\"cursor: pointer;\">";
                    if (e.ACTIVE == "Y") {
                        str_status += "<span class=\"label label-success label-md\">ใช้งาน</span>";
                    } else {
                        str_status += "<span class=\"label label-danger label-md\">ยกเลิก</span>";
                    }
                    str_status += "</a>";
                    $('#tb_Reward').dataTable().fnAddData([
                        (i + 1),
                        str_status,
                        e.REWARD_CODE,
                        e.NAME_TH,
                        e.NAME_EN,
                        date_start,
                        date_end,
                        commaSeparateNumber(e.POINT),
                        e.CATALOGUE_NAME + '<input type="hidden" value="' + e.CATALOGUE_CODE + '" />',
                        e.CATEGORY_NAME + '<input type="hidden" value="' + e.CATEGORY_CODE + '" />',
                        '<a class="dtl_call" data-headcall="' + e.REWARD_CODE + '" style="cursor: pointer;"><span class="fa fa-file-text-o"></span></a>',
                    ]);
                    $('#example').dataTable().fnAddData([
                        (i + 1),
                        str_status,
                        e.REWARD_CODE,
                        e.NAME_TH,
                        e.NAME_EN,
                        date_start,
                        date_end,
                        commaSeparateNumber(e.POINT),
                        e.CATALOGUE_NAME + '<input type="hidden" value="' + e.CATALOGUE_CODE + '" />',
                        e.CATEGORY_NAME + '<input type="hidden" value="' + e.CATEGORY_CODE + '" />',
                        '<a class="dtl_call" data-headcall="' + e.REWARD_CODE + '" style="cursor: pointer;"><span class="fa fa-file-text-o"></span></a>',
                    ]);
                });
            }
            closeLoading();
        }
    });
}
var ListImage = function (name) {
    var img = new Array();
    img = [];
    $("#myModalReward").find("input[type=file][name=" + name + "]").each(function (index, field) {
        var file = field.files;
        $.each(field.files, function (i, e) {
            img.push(e.name);
        })
    });
    return img;
}
var UploadImage = function (data, path) {
    $.ajax({
        url: "Upload/UploadListImageReward",
        processData: false,
        contentType: false,
        headers: {
            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
            'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        },
        data: data,
        type: 'POST'
    }).done(function (result) {
        imgshopHiilight = result;
        return result;
        //alert(result);
        //$('#shopLogo').val(result)
        //$('.shopImage').attr('src', sessionStorage.mainUrl + "Content/img/" + path + "/" + result);
        //listImage = result
        //console.log(listImage);
    }).fail(function (a, b, c) {
        console.log(a, b, c);
    });
}