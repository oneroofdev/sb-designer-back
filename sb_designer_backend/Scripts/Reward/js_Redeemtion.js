﻿var ddl_reason = '';
var frm_validate = "";
var str_code = "";
var str_catecode = "";
var str_qty = 1;
var str_img = "";
var str_name = "";
var str_reason = "";
var str_point = 0;
var str_cartID = 0;
var rows_prod = 0;
var Total_point = 0;

var rw_code = "";
var frm_validate = "";
var reward_kerry = false;
var reward_cash = false;
var delivery_payment = true;
var dis_id = 0;
var subdis_id = 0;
var img_card = "";
var img_bookbank = "";
var total_p = 0;
var cate_channel = "";

$(document).ready(function () {
    getCategory();
    getCatalogue();
    getProvince();
    getListBank();
    document.getElementById("grp_bookbank").style.display = "none";
    document.getElementById("grp_promptpay").style.display = "block";
    document.getElementById("grp_addfile").style.display = "none";
    document.getElementById("grp_address").style.display = "block";
    document.getElementById("frm_ShowReward").style.display = "none";
    $('#btn_searchRedemption').on("click", function () {
        if ($('#txt_membercard').val() != "") {
            $("#txt_point_balance").val(commaSeparateNumber(parseInt($('#this_point').text().replace(',', ''))));
            showLoading();
            getReward_draw();
            getDataFilter();
        } else {
            $('#Reward_Alert').text("กรุณาเลือกสมาชิกที่ต้องการแลกของรางวัล");
            $('#Reward_Alert2').html("");
            $('#alert-modal').modal('show');
        }

    });
    $('#addRewardRedemption').on("click", ".dtl_rewardRedemp", function () {
        showLoading();
        frm_validate = "";
        var vtxt_searchdata = validate_from_reward($("#MID"), $("#vtxt_searchdata"), "กรุณาเลือกสมาชิก");
        if (vtxt_searchdata == true) {
            str_code = $(this).attr('data_rewardRedemp');
            str_catecode = $(this).attr('data_rewardCate');
            str_img = $(this).parents("ul").children("li").eq(2).children("input").val();
            str_name = $(this).parents("ul").children("li").eq(0).children("input").val();
            str_reason = $(this).parents("ul").children("li").eq(1).children("input").val();
            str_point = $(this).parents("ul").children("li").eq(3).children("input").val();
            $('#txt_qty').val("1");
            showLoading();
            loadreason_redemp(str_code);
            //alert(str_reason);
            //alert(str_code + "," + str_qty + "," + str_name + "," + str_point + "," + $('#hd_Point_sub').val());
        } else {
            closeLoading();
            bootbox.alert({
                title: "กรุณากรอกข้อมูลให้ครบ",
                message: "<ul>" + frm_validate + "</ul>",
                size: 'small',
                callback: function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                }
            });
            //$('#Reward_Alert').text("<ul>" + frm_validate + "</ul>");
            //$('#alert-modal').modal('show');
        }
    });
    $('#btn_choose_reward').on("click", function () {
        showLoading();
        var category_code = $('#hd_category_code').val();
        if (category_code == 5) {
            if (cate_channel == "") {
                cate_channel = $("#ddl_channel").val();
            }
            if (cate_channel == $("#ddl_channel").val()) {
                chooseRewardTocart();
            } else {
                closeLoading();
                $('#Reward_Alert').text("คุณสามารถแลกเงินสดผ่านทางพร้อมเพย์ หรือโอนเงินผ่านบัญชีธนาคารได้อย่างใดอย่างหนึ่งเท่านั้น");
                $('#Reward_Alert2').html("");
                $('#alert-modal').modal('show');
            }
        } else {
            chooseRewardTocart();
        }
    });
    $('#btn_cancalReward').on("click", function () {
        bootbox.confirm({
            title: "<h4>แจ้งเตือน</4>",
            message: "คุณต้องการยกเลิกรายการแลกของรางวัลนี้ใช่หรือไม่",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i>' + "ยกเลิก"
                },
                confirm: {
                    label: '<i class="fa fa-check"></i>' + "ตกลง"
                }
            },
            callback: function (result) {
                if (result) {
                    showLoading();
                    $("#tbody_rewardredemp").empty();
                    clearCartALL();
                    summaryPoint();
                }
            }
        });
    });
    $('#btn_submitReward').on("click", function () {
        frm_validate = "";
        var vtxt_searchdata = validate_from_reward($("#MID"), $("#vtxt_searchdata"), "กรุณาเลือกสมาชิก");
        var Re_code = $("input[name='str_rewardCode[]']").map(function () { return $(this).val(); }).get();
        var str_catecode = $("input[name='str_catecode[]']").map(function () { return $(this).val(); }).get();
        var couriercode = $("input[name='couriercode[]']").map(function () { return $(this).val(); }).get();
        reward_kerry = false;
        //document.getElementById("grp_address").style.display = "none";
        reward_cash = false;
        document.getElementById("grp_addfile").style.display = "none";
        delivery_payment = false;
        for (var i = 0; i < str_catecode.length; i++) {
            if (couriercode[i] == 4) {
                $("#req_email").text("*");
            }
            if (couriercode[i] == 1 || couriercode[i] == 3 || couriercode[i] == 8) {
                reward_kerry = true;
                document.getElementById("grp_address").style.display = "block";
            }
            if (str_catecode[i] == 5) {
                reward_cash = true;
                document.getElementById("grp_addfile").style.display = "block";
                if (couriercode[i] == "1") {
                    delivery_payment = false;
                    document.getElementById("grp_bookbank").style.display = "block";
                    document.getElementById("grp_promptpay").style.display = "none";
                } else if (couriercode[i] == "2") {
                    delivery_payment = true;
                    document.getElementById("grp_bookbank").style.display = "none";
                    document.getElementById("grp_promptpay").style.display = "block";
                }
            }
        }
        if (Re_code.length == 0) {
            $('#Reward_Alert').text('กรุณาเลือกของรางวัล');
            $('#Reward_Alert2').html("");
            $('#alert-modal').modal('show');
        } else if (vtxt_searchdata == true) {
            getProfile(sessionCust_ID);
            $('#ModalDeliveryReward').modal('show');
        } else {
            bootbox.alert({
                title: "กรุณากรอกข้อมูลให้ครบ",
                message: "<ul>" + frm_validate + "</ul>",
                size: 'small',
                callback: function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                }
            });
            //$('#Reward_Alert').text("<ul>" + frm_validate + "</ul>");
            //$('#alert-modal').modal('show');
        }
    });
    $("#txt_province").on("change", function () {
        if ($("#txt_province").val() != "") {
            getDistrict($("#txt_province").val());
        }
    });
    $("#txt_district").on("change", function () {
        if ($("#txt_district").val() != "") {
            getSubDistrict($("#txt_district").val());
        }
    });
    $("#txt_subdistrict").on("change", function () {
        if ($("#txt_subdistrict").val() != "") {
            getZipcode($("#txt_subdistrict").val());
        }
    });
    $('#btn_confirm_redeem').on("click", function () {
        $.ajax({
            url: 'Redemption/CountRedeemForCart',
            type: "POST",
            async: false,
            cache: false,
            data: {
                Cust_ID: sessionCust_ID
            },
            success: function (dataCount) {
                var adataCount = JSON.parse(dataCount);
                if (adataCount.success) {
                    if (adataCount.items.QTY > 0) {
                        var emailx = "";
                        var Re_courier = [];
                        Re_courier = $("input[name='str_reason_rewardRe[]']").map(function () { return $(this).val(); }).get();
                        var params = [];
                        for (var i = 0; i < Re_courier.length; i++) {
                            if (Re_courier[i] == "4") {
                                emailx = "4";
                            }
                        }
                        //alert(emailx);
                        frm_validate = "";
                        var vtxt_name = validate_from_reward($("#txt_fname"), $("#vtxt_fname"), 'กรุณาระบุ ชื่อ');
                        var vtxt_surname = validate_from_reward($("#txt_lname"), $("#vtxt_lname"), 'กรุณาระบุ นามสกุล');
                        var vtxt_tel = validate_from_reward($("#txt_tel_redeem"), $("#vtxt_tel"), 'กรุณาระบุ โทรศัพท์ติดต่อ');
                        var vtxt_mail = true;
                        var vtxt_card = validate_from_reward($("#txt_idcard_redeem"), $("#vtxt_idcard"), 'กรุณาระบุ หมายเลขบัตรประชาชน');
                        var vtxt_copycard = validate_from_reward($("#txt_copyfile"), $("#vtxt_copyfile"), 'สำเนาบัตรประชาชน');
                        var vtxt_addrno = true;
                        var vtxt_subdist = true;
                        var vtxt_dist = true;
                        var vtxt_provice = true;
                        var vtxt_zipecode = true;
                        if (reward_kerry) {
                            vtxt_addrno = validate_from_reward($("#txt_addressno"), $("#vtxt_addressno"), 'กรุณาระบุ บ้านเลขที่');
                            vtxt_subdist = validate_from_reward($("#txt_subdistrict"), $("#vtxt_subdistrict"), 'กรุณาระบุ ตำบล');
                            vtxt_dist = validate_from_reward($("#txt_district"), $("#vtxt_district"), 'กรุณาระบุ อำเภอ');
                            vtxt_provice = validate_from_reward($("#txt_province"), $("#vtxt_province"), 'กรุณาระบุ จังหวัด');
                            vtxt_zipecode = validate_from_reward($("#txt_post"), $("#vtxt_post"), 'กรุณาระบุ รหัสไปรษณีย์');
                        }
                        var vtxt_bank = true;
                        var vtxt_bankBranch = true;
                        var vtxt_cash = true;
                        var vtxt_copycash = true;
                        var vtxt_promptpay = true;
                        if (reward_cash == true) {
                            if (delivery_payment == false) {
                                vtxt_bank = validate_from_reward($("#ddl_bank"), $("#vtxt_bank"), "กรุณาเลือกธนาคาร");
                                vtxt_bankBranch = validate_from_reward($("#txt_bankBranch"), $("#vtxt_bankBranch"), "กรุณากรอกสาขาธนาคาร");
                                vtxt_cash = validate_from_reward($("#txt_bookbank"), $("#vtxt_bookbank"), 'กรุณาระบุ หมายเลขบัญชี');
                                vtxt_copycash = validate_from_reward($("#txt_copybookbank"), $("#vtxt_copybookbank"), 'กรุณาแนบสำเนาหน้าแรกสมุดบัญชี');
                            } else if (delivery_payment == true) {
                                vtxt_promptpay = validate_from_reward($("#txt_promptpay"), $("#vtxt_promptpay"), 'กรุณาระบุรหัสพร้อมเพย์');
                            }
                        }
                        if (vtxt_name == true && vtxt_surname == true && vtxt_addrno == true && vtxt_subdist == true && vtxt_dist == true && vtxt_provice == true && vtxt_bank == true && vtxt_bankBranch == true &&
                            vtxt_zipecode == true && vtxt_tel == true && vtxt_card == true && vtxt_copycard == true && vtxt_cash == true && vtxt_copycash == true && vtxt_promptpay == true) {
                            if (checkID($("#txt_idcard_redeem").val())) {
                                total_p = $('#txt_point_redeem').val();
                                if (emailx == "4") {
                                    vtxt_mail = validate_from_reward($("#txt_email"), $("#vtxt_email"), 'กรุณาระบุ อีเมล์');
                                    if (vtxt_mail == true) {
                                        if (validateEmail($("#txt_email").val())) {
                                            checkFirstRedeem();
                                        } else {
                                            $('#validateAlert').html($("#txt_email").val() + " อีเมล์นี้ไม่ถูกต้อง");
                                            $('#validate_modal').modal('show');
                                        }
                                    } else {
                                        $('#validateAlert').html("");
                                        $('#validate_modal').modal('show');
                                    }
                                } else {
                                    checkFirstRedeem();
                                }
                            } else {
                                $('#Reward_Alert').text('กรุณากรอก หมายเลขบัตรประชาชน ให้ถูกต้อง');
                                $('#Reward_Alert2').text("");
                                $('#alert-modal').modal('show');
                            }
                        } else {
                            $('#validateAlert').html("");
                            $('#validate_modal').modal('show');
                        }
                    } else {
                        $('#Reward_Alert').text('การแลกของรางวัลนี้ ไม่สามารถแลกได้');
                        $('#Reward_Alert2').text("เนื่องจากการแลกของรางวัลที่ถูกยกเลิก");
                        $('#alert-modal').modal('show');
                    }
                }
            }
        });
    });

    $('#txt_copyfile').change(function () {
        path = "Card";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            console.log(e)
            data.append('file', e);
            data.append('path', path);
            $.ajax({
                url: "upload/UploadImageRedeem",
                processData: false,
                contentType: false,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: data,
                type: 'POST'
            }).done(function (result) {
                img_card = result;
                //alert(img_card);
            }).fail(function (a, b, c) {
                img_card = "";
            });

        });
    });
    $('#txt_copybookbank').change(function () {
        path = "BookBank";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            console.log(e)
            data.append('file', e);
            data.append('path', path);
            $.ajax({
                url: "upload/UploadImageRedeem",
                processData: false,
                contentType: false,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: data,
                type: 'POST'
            }).done(function (result) {
                img_bookbank = result;
                //alert(img_bookbank);
            }).fail(function (a, b, c) {
                img_bookbank = "";
            });
        });
    });
    $('#openimg').on("click", function () {
        var copycard = "";
        var fullPath1 = document.getElementById('txt_copyfile').value;
        if (fullPath1) {
            $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/Card/" + img_card);
            $('#modal_openImg').modal('show');
        }
    });
    $('#openbookimg').on("click", function () {
        var fullPath1 = document.getElementById('txt_copybookbank').value;
        if (fullPath1) {
            $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/BookBank/" + img_bookbank);
            $('#modal_openImg').modal('show');
        }
    });
    //$('#rb_address_now').on("click", function () {
    //    getProvince();
    //    getProfile(sessionCust_ID);
    //    //$('#txt_tel').prop('readonly', true);
    //    //$('#txt_email').prop('readonly', true);
    //    //$('#txt_addressno').prop('readonly', true);
    //    //$('#txt_village').prop('readonly', true);
    //    //$('#txt_province').attr('disabled', true);
    //    //$('#txt_district').attr('disabled', true);
    //    //$('#txt_subdistrict').attr('disabled', true);
    //});
    //$('#rb_address_new').on("click", function () {
    //    getProvince();
    //    dis_id = 0;
    //    subdis_id = 0;
    //    //$('#txt_tel').prop('readonly', false);
    //    //$('#txt_email').prop('readonly', false);
    //    //$('#txt_addressno').prop('readonly', false);
    //    //$('#txt_village').prop('readonly', false);
    //    //$('#txt_province').attr('disabled', false);
    //    //$('#txt_district').attr('disabled', false);
    //    //$('#txt_subdistrict').attr('disabled', false);
    //    $('#txt_addressno').val("");
    //    $('#txt_village').val("");
    //    $('#txt_subdistrict').val("");
    //    $('#txt_district').val("");
    //    $('#txt_province').val("");
    //    $('#txt_post').val("");
    //});
});

function chooseRewardTocart() {
    //alert($('#rewardcode').text());
    //alert(sessionCust_ID);
    var skillsSelect = document.getElementById("ddl_channel");
    var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
    //alert(selectedText);
    var rows = '';
    var total_point = 0;
    //alert(str_qty);
    if ($('#txt_qty').val() != "") {
        str_qty = $('#txt_qty').val();
    }
    var corier_code = $("#ddl_channel").val();
    var totalPoint = parseInt(str_qty) * parseInt(str_point);
    total_point = parseInt(total_point) + parseInt(totalPoint);
    var cols = '';
    var r_data = false;
    var dataQty = 1;
    $.ajax({
        url: 'Point/BalancePoint',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Cust_ID: sessionCust_ID
        },
        success: function (dataP) {
            var dataPoint = JSON.parse(dataP);
            if (dataPoint.success) {
                if ($("#ddl_channel").val() != "") {
                    var BP_point = dataPoint.items[0].POINT;
                    if (parseInt(BP_point) >= parseInt(total_point)) {
                        $.ajax({
                            url: 'Redemption/GetCartIDByUser',
                            type: "POST",
                            async: false,
                            cache: false,
                            data: {
                                Cust_ID: sessionCust_ID
                            },
                            success: function (d_CartID) {
                                var dataCartID = JSON.parse(d_CartID);
                                if (dataCartID.success) {
                                    str_cartID = dataCartID.items;
                                } else {
                                    str_cartID = 0;
                                }
                                $.ajax({
                                    url: 'Redemption/addRewardToCart',
                                    type: "POST",
                                    async: false,
                                    cache: false,
                                    data: {
                                        MEMBER_ID: sessionCust_ID,
                                        REWARD_CODE: str_code,
                                        IS_SPECIAL: "N",
                                        QTY: str_qty,
                                        POINT_REDEEM: totalPoint,
                                        CREATE_BY: sessionStorage.UserName,
                                        CREATE_DATE: "",
                                        TRAN_ID: "",
                                        CART_ID: str_cartID,
                                        CHANNEL: "B",
                                        STATUS: "I",
                                        COURIER_CODE: corier_code
                                    },
                                    success: function (data) {
                                        var datasave = JSON.parse(data);
                                        if (datasave.success) {
                                            $('#ModalRewardDetail').modal('hide');
                                            var couriercode = $("#ddl_channel").val();
                                            rows += cols;
                                            //if (couriercode == 4) {
                                            //    $("#req_email").text("*");
                                            //}
                                            //if (couriercode == 3 || couriercode == 8) {
                                            //    reward_kerry = true;
                                            //    document.getElementById("grp_address").style.display = "block";
                                            //}
                                            //if (str_catecode == 5) {
                                            //    reward_cash = true;
                                            //    document.getElementById("grp_addfile").style.display = "block";
                                            //    if (couriercode == "1") {
                                            //        delivery_payment = false;
                                            //        document.getElementById("grp_bookbank").style.display = "block";
                                            //        document.getElementById("grp_promptpay").style.display = "none";
                                            //    } else if (couriercode == "2") {
                                            //        delivery_payment = true;
                                            //        document.getElementById("grp_bookbank").style.display = "none";
                                            //        document.getElementById("grp_promptpay").style.display = "block";
                                            //    }
                                            //}
                                            str_cartID = datasave.items;
                                            $("#no-more-tables tbody tr").each(function () {
                                                if (str_code == $(this).children('td').eq(1).text()) {
                                                    var up_qty = parseInt(str_qty) + parseInt($(this).children('td').eq(4).text().replace(',', ''));
                                                    var up_Point = parseInt(up_qty) * parseInt(str_point);
                                                    $(this).children('td').eq(4).text(commaSeparateNumber(up_qty));
                                                    $(this).children('td').eq(5).text(commaSeparateNumber(up_Point));
                                                    r_data = true;
                                                }
                                            });
                                            //alert(document.getElementById('no-more-tables').getElementsByTagName("tr").length);
                                            if (r_data == false) {
                                                cols = '<td class="text-center" data-title="Picture"><img class="img-responsive displayed" src="' + str_img + '" max-height="30"></td>' +
                                                        '<td class="text-left" data-title="Code" style="vertical-align:middle;">' +
                                                            '<input type="hidden" name="str_rewardCode[]" value="' + str_code + '" />' +
                                                            str_code +
                                                        '</td>' +
                                                        '<td class="text-left" data-title="Name" style="vertical-align:middle;">' +
                                                            '<input type="hidden" name="str_rewardName[]" value="' + str_name + '" />' +
                                                            str_name +
                                                        '</td>' +
                                                        '<td class="text-right" data-title="Point/Unit" style="vertical-align:middle;">' +
                                                            '<input type="hidden" name="str_rewardPoint[]" value="' + str_point + '" />' +
                                                            commaSeparateNumber(str_point) + '</td>' +
                                                        '<td class="text-right" data-title="Quantity" style="vertical-align:middle;">' +
                                                            '<input type="hidden" name="str_qtyRedemp[]" value="' + str_qty + '" />' +
                                                            commaSeparateNumber(str_qty) + '</td>' +
                                                        '<td class="text-right" data-title="Total Point" style="vertical-align:middle;">' +
                                                            commaSeparateNumber(totalPoint) +
                                                        '</td>' +
                                                        '<td class="text-left" data-title="Shipped by" style="vertical-align:middle;">' +
                                                            '<input type="hidden" name="str_category[]" value="' + $('#hd_category_code').val() + '" />' +
                                                            '<input type="hidden" name="str_reason_rewardRe[]" value="' + corier_code + '" />' +
                                                            '<input type="hidden" name="str_catecode[]" value="' + str_catecode + '" />' +
                                                            '<input type="hidden" name="couriercode[]" value="' + couriercode + '" />' +
                                                            selectedText +
                                                        '</td>' +
                                                        '<td class="text-right" data-title="Remark" style="vertical-align:middle;"><input type="text" name="str_rewardRemark[]" maxlength="50" /></td>' +
                                                        '<td class="text-center" data-title="Delete" style="vertical-align:middle;"><a style="cursor: pointer;" onclick="deleteRow(this)"><span class="fa fa-remove" style="font-size:20px;color:red"></span></a></td>';
                                                rows = '<tr>' + cols + '</tr>';
                                                if (rows_prod == 0) {
                                                    $('#tbody_rewardredemp').html(rows);
                                                    rows_prod++;
                                                } else {
                                                    $('#tbody_rewardredemp').append(rows);
                                                }
                                            }
                                            summaryPoint();
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        closeLoading();
                        $('#Reward_Alert').text("คะแนนคงเหลือไม่พอ สำหรับแลกสินค้าชิ้นนี้้");
                        $('#Reward_Alert2').html("");
                        $('#alert-modal').modal('show');
                    }
                } else {
                    closeLoading();
                    $('#Reward_Alert').text("กรุณาเลือกช่องทางการจัดส่งของรางวัล");
                    $('#Reward_Alert2').html("");
                    $('#alert-modal').modal('show');
                }
            }
        }
    });
}
function loadreason_redemp(RewardCode) {
    $.ajax({
        url: 'Redemption/ListCourierByReward',
        type: "POST",
        async: false,
        cache: false,
        data: {
            RewardCode: RewardCode
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                var rows = '';
                $.each(d.items, function (i, value) {
                    rows += '<option value="' + value.COURIER_CODE + '">' + value.COURIER_NAME + '</option>';
                });
                ddl_reason = rows;
                detailRewardByCode(RewardCode);
                closeLoading();
            }
        }
    });
}
function getCategory() {
    $.ajax({
        url: 'Redemption/ListCategory',
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                var rows = '<option value=""> เลือกทั้งหมด </option>';
                $.each(d.items, function (i, value) {
                    rows += '<option value="' + value.CATEGORY_CODE + '">' + value.CATEGORY_NAME + '</option>';
                });
                $('#dll_type_reward').html(rows)
            }
        }
    });
}
function getCatalogue() {
    $.ajax({
        url: 'Redemption/getListCatalogue',
        type: "POST",
        async: false,
        cache: false,
        data: {
           
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                var rows = '';
                $.each(d.items, function (i, value) {
                    rows += '<option value="' + value.CATALOGUE_CODE + '">' + value.CATALOGUE_NAME + '</option>';
                });
                $('#ddl_cat_reward').html(rows);
            }
        }
    });
}
function validate_from_reward(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        frm_validate += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}
function getDataFilter() {
    var str_type = "";
    var str_point = 0;
    var pointMax = $('#hd_BPPoint').val();
    if ($("#dll_type_reward").val() != "") {
        str_type = $('#dll_type_reward').val();
    }
    if ($('#ddl_choice_reward').val() == "0") {
        var bpoint = $('#this_point').text().replace(',', '');
        if (bpoint == 0) {
            str_point = -1;
        } else {
            str_point = bpoint;
        }
    }
    $.ajax({
        url: 'Redemption/ListRewardNew',
        type: "POST",
        async: false,
        cache: false,
        data: {
            ALL_TIER: "",
            START_DATE: "",
            END_DATE: "",
            CATALOGUE_CODE: $("#ddl_cat_reward").val(),
            CATEGORY_CODE: str_type,
            IS_HILIGHT: "",
            IS_MAINPAGE: "",
            TIER: "",
            POINT: str_point,
            TAG: "",
            MODEL: "",
            BRAND: "",
            ACTIVE: "Y"
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                renderReward(d.items);
            }
        }
    });
}
function renderReward(data) {
    if (data != null && data != "") {
        document.getElementById("frm_ShowReward").style.display = "block";
        var allReward = '';
        if ($('#ddl_sort').val() == 1) {
            data.sort(function (a, b) {
                return a.POINT - b.POINT;
            });
        } else if ($('#ddl_sort').val() == 2) {
            data.sort(function (a, b) {
                return b.POINT - a.POINT;
            });
        }
        for (var j = 0; j < data.length; j++) {
            var namePro = data[j]["NAME_TH"];
            var detail = ['รหัสของรางวัล', 'คะแนน', 'เลือก', 'ประเภท', 'แคตตาล็อก'];
            var new_row = "";
            var end_row = "";
            if ((j) % 4 == 0) {
                new_row = "<div class=\"col-md-12\">";
            } if ((j) % 4 == 3) {
                end_row = "</div>";
            }
            var reward = new_row + '<div class="col-md-3" style="padding-bottom: 20px;">' +
                    '<div class="card card-type-pricing text-center" style="border: 1px solid #e6e6e6;">' +
                        '<div class="card-body style-gray" style="height:175px; background-color: #fff; padding:2px !important;">' +
                            '<img class="img-responsive displayed img_center" src="' + sessionStorage.mainUrl + '/Content/img/Reward/' + (data[j]["PATH_IMAGE"] == null ? "img_reward.png" : data[j]["PATH_IMAGE"]) + '" height="300" style="max-height: 175px;">' +
                            //'<img class="img-responsive" src="' + baseUrl + 'Content/img/Reward/' + (data[j]["picture"] == "" ? "noimage.jpg" : data[j]["picture"]) + '" alt="" style="height:250px; background-color: #CCCCCC;">' +
                        '</div><!--end .card-body -->' +
                        '<div class="card-body no-padding">' +
                            '<ul class="list-unstyled" style="text-align:left; line-height: 25px;">' +
                                '<li class="reward_overflow dtl_rewardRedemp" data_rewardRedemp="' + data[j]["REWARD_CODE"] + '" style="cursor: pointer; padding-top:10px;">' +
                                    '<input type="hidden" value="' + namePro + '" />' +
                                    '<b title="' + namePro + '" style="font-size:15px;">' + namePro + '</b>' +
                                    //'<span title="' + namePro + '" class="text-left reward-name h5">' + namePro + '</span>' +
                                '</li>' +
                                '<li style="padding-left:15px;">' +
                                    '<input type="hidden" value="' + data[j]["CATEGORY_CODE"] + '" />' +
                                    detail[3] + ': ' + data[j]["CATEGORY_NAME"] +
                                '</li>' +
                                '<li style="padding-left:15px;">' +
                                    '<input type="hidden" value="' + sessionStorage.mainUrl + '/Content/img/Reward/' + (data[j]["PATH_IMAGE"] == null ? "img_reward.png" : data[j]["PATH_IMAGE"]) + '" />' +
                                    detail[0] + ': ' + data[j]["REWARD_CODE"] +
                                '</li>' +
                                '<li style="padding-left:15px;">' +
                                    '<input type="hidden" value="' + data[j]["POINT"] + '" />' +
                                    detail[1] + ': <span style="color:red;">' + commaSeparateNumber(data[j]["POINT"]) + '</span>' +
                                '</li>' +
                                '<li style="text-align: right;">' +
                                    '<div class="form-group floating-label" style="margin: 5px;">' +
                                        '<div class="input-group">' +
                                            '<div class="input-group-btn">' +
                                                '<a class="btn btn-info dtl_rewardRedemp" data_rewardRedemp="' + data[j]["REWARD_CODE"] + '" data_rewardCate="' + data[j]["CATEGORY_CODE"] + '"><i class="fa fa-plus"></i> ' + detail[2] + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</li>' +
                            '</ul>' +
                        '</div><!--end .card-body -->' +
                    '</div><!--end .card -->' +
                '</div><!--end .col -->' + end_row
            allReward += reward;
        }
        $('.listProd').html(allReward);
    } else {
        $('.listProd').html("");
    }
    //closeLoading();
}
function detailRewardByCode(rewardCode) {
    $.ajax({
        url: 'Redemption/GetRewardDetailByCode',
        type: "POST",
        async: false,
        cache: false,
        data: {
            RewardCode: rewardCode
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                $.each(d.items, function (i, e) {
                    $('#img_reward').html('<img class="img-responsive displayed" src="' + sessionStorage.mainUrl + '/Content/img/Reward/' + (e.IMG == null ? "img_reward.png" : e.IMG) + '" height="300" style="max-height: 300px;">');
                    $('#rewardname').text(e.REWARD_NAME);
                    $('#rewardcode').text(e.REWARD_CODE);
                    $('#point').text((e.POINT != null ? e.POINT.toLocaleString() : "-"));
                    $('#price').text((e.PRICE != null ? e.PRICE.toLocaleString() + " บาท" : "-"));
                    $('#date_from_to').text(ConvertDateToString(e.START_DATE) + " ถึง " + (e.END_DATE == "" ? "ปัจจุบัน" : ConvertDateToString(e.END_DATE)));
                    $('#detail').html(e.REWARD_DETAIL);
                    $('#hd_category_code').val(e.CATEGORY_CODE);
                    $('#catalog_name').text(e.CATALOGUE_NAME);
                    $('#ddl_channel').html(ddl_reason);
                    $('#ModalRewardDetail').modal('show');
                });
            }
            closeLoading();
        }
    });
}
function summaryPoint() {
    var point_use = 0;
    $("#no-more-tables tbody tr").each(function () {
        point_use += parseInt($(this).children('td').eq(5).text().replace(',', ''));
    });
    //$("#no-more-tables1 tbody tr").children('td').eq(1).text(commaSeparateNumber(point_use));
    $("#txt_point_redeem").val(commaSeparateNumber(point_use));
    $('#hd_totalPoint').val(point_use);
    var sumP = parseInt($('#txt_point_balance').val().replace(',', '')) - parseInt(point_use);
    //$("#no-more-tables1 tbody tr").children('td').eq(2).text(commaSeparateNumber(sumP));
    $("#txt_remaining_redeem").val(commaSeparateNumber(sumP));
    getExpirePoint(sessionCust_ID);
    closeLoading();
}
function summaryPoint_2() {
    var point_use = 0;
    $("#no-more-tables tbody tr").each(function () {
        point_use += parseInt($(this).children('td').eq(5).text().replace(',', ''));
    });
    //$("#no-more-tables1 tbody tr").children('td').eq(1).text(commaSeparateNumber(point_use));
    $("#txt_point_redeem").val(commaSeparateNumber(point_use));
    $('#hd_totalPoint').val(point_use);
    var sumP = parseInt($('#this_point').text().replace(',', '')) + parseInt(point_use);
    //$("#no-more-tables1 tbody tr").children('td').eq(2).text(commaSeparateNumber(sumP));
    $('#txt_point_balance').val(commaSeparateNumber(sumP));
    $("#txt_remaining_redeem").val(commaSeparateNumber(parseInt($('#this_point').text().replace(',', ''))));
    closeLoading();
}
function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    var rw_code = document.getElementById("no-more-tables").rows[i].cells[1].innerHTML;
    var rw_code2 = rw_code.split('>');
    var str_catalogueCode = [];
    str_catalogueCode = $("input[name='str_category[]']").map(function () { return $(this).val(); }).get();
    //alert(str_catalogueCode[i-1]);
    //alert(rw_code2[1].trim());
    bootbox.confirm({
        title: "<h4>แจ้งเตือน</4>",
        message: "คุณต้องการลบรายการสินค้านี้ใช่หรือไม่",
        size: 'small',
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i>' + "ยกเลิก"
            },
            confirm: {
                label: '<i class="fa fa-check"></i>' + "ตกลง"
            }
        },
        callback: function (result) {
            if (result) {
                if (str_catalogueCode[i - 1] == 5) {
                    cate_channel = "";
                }
                showLoading();
                $.ajax({
                    url: 'Redemption/deleteRewardFromCart',
                    type: "POST",
                    async: false,
                    cache: false,
                    data: {
                        TRAN_ID: "",
                        CART_ID: str_cartID,
                        MEMBER_ID: sessionCust_ID,
                        REWARD_CODE: rw_code2[1].trim(),
                        IS_SPECIAL: "",
                        QTY: "",
                        POINT_REDEEM: "",
                        CREATE_BY: "",
                        CREATE_DATE: "",
                        CHANNEL: "",
                        STATUS: ""
                    },
                    success: function (data) {
                        var d = JSON.parse(data);
                        if (d.success) {
                            document.getElementById("no-more-tables").deleteRow(i);
                            str_catalogueCode = $("input[name='str_category[]']").map(function () { return $(this).val(); }).get();
                            str_catalogueCode2 = $("input[name='str_reason_rewardRe[]']").map(function () { return $(this).val(); }).get();
                            $.each(str_catalogueCode, function (c, e) {
                                if (e == 5) {
                                    cate_channel = str_catalogueCode2[c];
                                }
                            });
                            summaryPoint();
                        }
                    }
                });
            }
        }
    });
}
function checkFirstRedeem() {
    $.ajax({
        url: 'Redemption/GetFristRedeem',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Cust_ID: sessionCust_ID
        },
        success: function (datacour) {
            var d = JSON.parse(datacour);
            if (d.success) {
                //if (d.items == null && parseInt(total_p.replace(/,/g, '')) < 500) {
                //    var txt = "TH" == "TH" ? "คุณได้ทำการแลกของรางวัลเป็นครั้งแรก ต้องแลกขั้นต่ำ 500 คะแนน" : "You have redeemed the rewards for the first time. Must redeem at least 500 points";
                //    $('#Reward_Alert').html(txt);
                //    $('#Reward_Alert2').html("");
                //    $('#alert-modal').modal('show');
                //} else {
                    saveRedeemReward();
                //}
            }
        },
    });
}
function saveRedeemReward() {
    showLoading();
    var promMobile = "";
    if ($('#txt_promptpay').val().length <= 10) {
        promMobile = $('#txt_promptpay').val();
    }
    var Re_code = [];
    var Re_Reason = [];
    Re_code = $("input[name='str_rewardCode[]']").map(function () { return $(this).val(); }).get();
    Re_Reason = $("input[name='str_reason_rewardRe[]']").map(function () { return $(this).val(); }).get();
    Re_Remark = $("input[name='str_rewardRemark[]']").map(function () { return $(this).val(); }).get();
    var params = [];
    for (var i = 0; i < Re_code.length; i++) {
        //alert(values[i]);
        params.push({
            REWARD_CODE: Re_code[i],
            RewardCourier: Re_Reason[i],
            RewardRewark: Re_Remark[i]
        });
    }
    //var redeemby = sessionStorage.AD == "" ? sessionStorage.UserName : sessionStorage.AD;


    var txt_addr = ($('#txt_addressno').val() == "" ? "" : $('#txt_addressno').val()) +
            ($('#txt_villageNo').val() == null ? "" : " ม." + $('#txt_villageNo').val()) +
            ($('#txt_village').val() == null ? "" : " หมู่บ้าน/อาคาร " + $('#txt_village').val()) +
            ($('#txt_floor').val() == null ? "" : " ชั้น" + $('#txt_floor').val()) +
            ($('#txt_road').val() == null ? "" : " ถ." + $('#txt_road').val()) +
            ($('#txt_soi').val() == null ? "" : " ซ." + $('#txt_soi').val());
    $.ajax({
        url: 'Redemption/saveRedeem_BK',
        type: "POST",
        async: false,
        cache: false,
        data: {
            CART_ID: str_cartID,
            MEMBER_ID: sessionCust_ID,
            REDEEM_BY: sessionStorage.UserName,
            REDEEM_CHANNEL: "2",
            RECEIVE_NAME: $('#txt_fname').val() + " " + $('#txt_lname').val(),
            HOUSE_NO: txt_addr,
            BULDING: "",
            TAMBOL_ID: $('#txt_subdistrict').val(),
            AMPHUR_ID: $('#txt_district').val(),
            PROVINCE_ID: $('#txt_province').val(),
            POST_CODE: $('#txt_post').val(),
            PHONE: $('#txt_tel_redeem').val(),
            EMAIL: $('#txt_email').val(),
            CREATE_BY: sessionStorage.UserName,
            CARD_ID: $('#txt_idcard_redeem').val(),
            COPY_CARD_ID: img_card,
            BANK_CODE: $('#ddl_bank').val(),
            BANK_ACCT_NO: $('#txt_bookbank').val(),
            BANK_BRANCH: $('#txt_bankBranch').val(),
            COPY_BOOK_BANK: img_bookbank,
            PROMPT_PAY_MOBILE: promMobile,
            PROMPT_PAY_CID: $('#txt_promptpay').val(),
            COURIER: JSON.stringify(params)
        },
        success: function (dataResult) {
            var d = JSON.parse(dataResult);
            document.getElementById("frm_ShowReward").style.display = "none";
            $('#ModalDeliveryReward').modal('hide');
            str_cartID = 0;
            $("#tbody_rewardredemp").empty();
            getExpirePoint($('#txt_crmid').val());
            $("#txt_point_redeem").val("0");
            $("#txt_remaining_redeem").val("0");
            $('body,html').animate({
                scrollTop: 0
            }, 500);
            $('.listProd').html("");
            if (d.success) {
                var total_pointuse = 0;
                $.ajax({
                    url: 'Point/BalancePoint',
                    type: "POST",
                    async: false,
                    cache: false,
                    data: {
                        Cust_ID: sessionCust_ID
                    },
                    success: function (datapointuse) {
                        var dpointuse = JSON.parse(datapointuse);
                        if (dpointuse.success) {
                            closeLoading();
                            $('#txt_promptpay').val("");
                            $('#txt_bookbank').val("");
                            $('#Reward_Alert').text('ทำการบันทึกการแลกของรางวัลเรียบร้อย');
                            $('#Reward_Alert2').html("");
                            $('#alert-modal').modal('show');
                            //ส่ง SMS & Email

                            //total_pointuse = datapointuse.items[0].POINT;
                            //cate_channel = "";
                            //var dat = dataResult.items.items;
                            ////console.log(dat);
                            //str_cartID = 0;
                            //var rows = "";
                            //var r = 1;
                            //var borderline = "";
                            //var maxday = 0;
                            //var listRW = "";
                            //var tranferCash = 1;
                            //var typetranfer = "";
                            //$.each(dat.listreward, function (i, e) {
                            //    if (r > 1) {
                            //        borderline = " border-top: 1pt solid #909090; ";
                            //    }
                            //    if (maxday < e.DELIVERY_DAY) {
                            //        maxday = e.DELIVERY_DAY;
                            //    }
                            //    var cols = '<tr style="height: 45px;">' +
                            //                    '<td width="50"  style="text-align: left;padding-left: 10px; width: 50px;' + borderline + '">' + r + '</td>' +
                            //                    '<td width="200" style="text-align: left;padding-left: 10px; width: 200px;' + borderline + '">' + e.REWARD_NAME + '</td>' +
                            //                    '<td width="50"  style="text-align: left;padding-left: 10px; width: 50px;' + borderline + '">' + e.QTY + '</td>' +
                            //                    '<td width="50"  style="text-align: left;padding-left: 10px; width: 50px;' + borderline + '">' + commaSeparateNumber(e.POINT_REDEEM) + '</td>' +
                            //                    '<td width="150" style="text-align: left;padding-left: 10px; width: 150px;' + borderline + '">' + e.COURIER_NAME + '</td>' +
                            //                '</tr>';
                            //    rows += cols;
                            //    //if (listRW == "") {
                            //    //    //listRW += e.SHORT_REWARD_NAME + " จำนวน " + e.QTY + " ชิ้น รวม " + commaSeparateNumber(e.POINT_REDEEM) + " คะแนน";
                            //    //    listRW += e.SHORT_REWARD_NAME;
                            //    //} else {
                            //    //    //listRW += ", " + e.SHORT_REWARD_NAME + " จำนวน " + e.QTY + " ชิ้น รวม " + commaSeparateNumber(e.POINT_REDEEM) + " คะแนน";
                            //    //    listRW += ", " + e.SHORT_REWARD_NAME;
                            //    //}
                            //    if (listRW != "") { listRW += ", "; }
                            //    var txtStr = "";
                            //    var xx = "";
                            //    if (e.CHANNEL_CASH != "" && e.CHANNEL_CASH != null) {
                            //        txtStr = e.CHANNEL_CASH;
                            //        xx = "x" + txtStr.substring(txtStr.length - 6, txtStr.length);
                            //    }
                            //    if (e.COURIER_CODE == 1) {
                            //        if (tranferCash == 1) {
                            //            listRW += e.SHORT_REWARD_NAME + " จะโอนเข้าบัญชีเลขที่ " + xx;
                            //            tranferCash++;
                            //            if (typetranfer == "") {
                            //                typetranfer = "C";
                            //            }
                            //        }
                            //    } else if (e.COURIER_CODE == 2) {
                            //        if (tranferCash == 1) {
                            //            listRW += e.SHORT_REWARD_NAME + " จะโอนผ่านบัญชีพร้อมเพย์ " + xx;
                            //            tranferCash++;
                            //            if (typetranfer == "") {
                            //                typetranfer = "C";
                            //            }
                            //        }
                            //    } else {
                            //        listRW += e.SHORT_REWARD_NAME;
                            //        typetranfer = "R";
                            //    }
                            //    r++;
                            //});
                            //var tbRedeem = '<table class="table" width="520" style="border-spacing: 0px 1px !important;">' +
                            //            '<thead style="background-color: #8dc63f; text-align: left; font-size: 12px; height: 35px;">' +
                            //                '<tr>' +
                            //                    '<th width="50"  style="border-color: #8dc63f; padding-left: 10px; width: 50px;" >ลำดับ</th>' +
                            //                    '<th width="200"  style="border-color: #8dc63f; padding-left: 10px; width: 50px;" >ของรางวัล</th>' +
                            //                    '<th width="50"  style="border-color: #8dc63f; padding-left: 10px; width: 50px;">จำนวน</th>' +
                            //                    '<th width="50"  style="border-color: #8dc63f; padding-left: 10px; width: 50px;">คะแนน</th>' +
                            //                    '<th width="150"  style="border-color: #8dc63f; padding-left: 10px; width: 50px;">ช่องทางการจัดส่ง</th>' +
                            //                '</tr>' +
                            //            '</thead>' +
                            //            '<tbody style="background-color: #fff; font-size: 12px;">' + rows +
                            //            '</tbody>' +
                            //        '</table>';
                            //var tbAddress = '<table class="table" width="520" style="border-spacing: 0px 1px !important;">' +
                            //                    '<thead style="background-color: #8dc63f; text-align:center;font-size: 12px;">' +
                            //                        '<tr style="height:35px;">' +
                            //                            '<th colspan="2" style="border-color: #8dc63f;">ที่อยู่ในการจัดส่ง</th>' +
                            //                        '</tr>' +
                            //                    '</thead>' +
                            //                    '<tbody style="background-color: #fff; font-size: 12px;">' +
                            //                        '<tr style="height:50px;">' +
                            //                            '<td width="100" style="padding-left:10px;">ชื่อ - นามสกุล :</td>' +
                            //                            '<td width="420" style="padding-left:10px;">' + dat.memberName + '</td>' +
                            //                        '</tr>' +
                            //                        '<tr style="height:50px;">' +
                            //                            '<td width="100"  style="padding-left:10px;border-top: 1pt solid #909090;">ที่อยู่ :</td>' +
                            //                            '<td width="420" style="padding-left:10px;border-top: 1pt solid #909090;">' + dat.memberAddress + '</td>' +
                            //                        '</tr>' +
                            //                        '<tr style="height:50px;">' +
                            //                            '<td width="100" style="padding-left:10px;border-top: 1pt solid #909090;">โทร :</td>' +
                            //                            '<td width="420" style="padding-left:10px;border-top: 1pt solid #909090;">' + dat.memberTel + '</td>' +
                            //                        '</tr>' +
                            //                        '<tr style="height:50px;">' +
                            //                            '<td width="100" style="padding-left:10px;border-top: 1pt solid #909090;">อีเมล์ :</td>' +
                            //                            '<td width="420" style="padding-left:10px;border-top: 1pt solid #909090;">' + dat.memberEmail + '</td>' +
                            //                        '</tr>' +
                            //                    '</tbody>' +
                            //                '</table>';
                            //var obj = {
                            //    CF_DETAIL_REWARD: tbRedeem,
                            //    CF_ADDRESS_REWARD: tbAddress,
                            //    CF_POINT_BEF_RD: commaSeparateNumber(parseInt(dat.TOTAL_POINT) + parseInt(total_pointuse)),
                            //    CF_POINT_RD: commaSeparateNumber(dat.TOTAL_POINT),
                            //    CF_POINT_AFTER_RD: commaSeparateNumber(parseInt(total_pointuse)),
                            //    CF_DELIVERY_DAY: maxday
                            //}
                            //var dataTbl = JSON.stringify(obj);
                            //var maxsent = " จะจัดส่งตามช่องทางที่ท่านระบุภายใน ";
                            //if (typetranfer == "C") {
                            //    maxsent = " ภายใน ";
                            //}
                            //var sms_msg = "คุณใช้ " + commaSeparateNumber(dat.TOTAL_POINT) + " คะแนน แลกของรางวัลจำนวน " +
                            //            dat.listreward.length + " รายการ คะแนนสะสมคงเหลือทั้งหมด " +
                            //            commaSeparateNumber(parseInt(total_pointuse)) + " คะแนน ของรางวัล" + listRW +
                            //            maxsent + maxday + " วัน";
                            //$.ajax({
                            //    url: sessionStorage.apiUrl + '/Noti/CreateNotification',//
                            //    dataType: 'json',
                            //    type: "POST",
                            //    crossDomain: true,
                            //    withCredentials: true,
                            //    headers: {
                            //        'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                            //        , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                            //    },
                            //    data: {
                            //        noticeTypeID: "RD",
                            //        memberID: dat.memberID,
                            //        refTranNo: dat.refTranNo,
                            //        noticeLink: "",
                            //        createBy: sessionStorage.UserName,
                            //        messageSendTH: sms_msg,
                            //        messageSendEN: "",
                            //        sentToNow: "Y",
                            //        replaceWord: dataTbl
                            //    },
                            //    async: true,
                            //    cache: false,
                            //    success: function (data) {
                            //        if (data.success) {
                            //            $('#txt_promptpay').val("");
                            //            $('#txt_bookbank').val("");
                            //        }
                            //    }
                            //});


                            //$('#complete_redeem').modal('show');
                        }
                    }
                });
            }
        }
    });
}
function clearCartALL() {
    $.ajax({
        url: 'Redemption/clearCartFirst',
        type: "POST",
        async: false,
        cache: false,
        data: {
            MEMBER_ID: sessionCust_ID
        },
        success: function (data) {
        }
    });
}
function getProfile(memid) {
    showLoading();
    $.ajax({
        url: 'Redemption/get_profile',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Cust_ID: sessionCust_ID
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                $.each(d.items, function (i, e) {
                    $('#txt_fname').val(e.Name == null ? "" : e.Name);
                    $('#txt_lname').val(e.Surname == null ? "" : e.Surname);
                    $('#txt_tel_redeem').val(e.Mobile_Phone == null ? "" : e.Mobile_Phone);
                    $('#txt_email').val(e.Email == null ? "" : e.Email);
                    $('#txt_province').val(e.PRV_ID == null ? "" : e.PRV_ID).change();
                    dis_id = e.AMP_ID;
                    subdis_id = e.TAM_ID;
                    $('#hd_addr_district').val(e.AMP_ID == null ? "" : e.AMP_ID);
                    $('#hd_addr_subdistrict').val(e.TAM_ID == null ? "" : e.TAM_ID);
                    $('#txt_post').val(e.Post_Code == null ? "" : e.Post_Code);
                    $('#txt_idcard_redeem').val(e.Citizen_ID);
                    $('#txt_addressno').val(e.House_No);
                    $('#txt_villageNo').val(e.Moo);
                    $('#txt_village').val(e.Village);
                    $('#txt_floor').val(e.Room);
                    $('#txt_road').val(e.Road);
                    $('#txt_soi').val(e.Soi);
                    $('#ModalDeliveryReward').modal('show');
                });
                closeLoading();
            }
        }
    });
}
function getProvince() {
    $.ajax({
        url: 'Redemption/ListProvince',
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (data) {
            var d = JSON.parse(data);
            $('#txt_province').html('<option value=""> --- จังหวัด --- </option>');
            $.each(d.items, function (i, e) {
                cols = '<option value="' + e.PRV_ID + '">' + e.PRV_NAME + '</option>';
                $('#txt_province').append(cols);
            });
        }
    });
}
function getDistrict(prvID) {
    $.ajax({
        url: 'Redemption/ListDistrict',
        type: "POST",
        async: false,
        cache: false,
        data: {
            prv_id: prvID
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                var dispalylist = false;
                $('#txt_district').html('<option value=""> --- เขต/อำเภอ --- </option>');
                $.each(d.items, function (i, e) {
                    if (e.AMP_ID == dis_id) { dispalylist = true; }
                    cols = '<option value="' + e.AMP_ID + '">' + e.AMP_NAME + '</option>';
                    $('#txt_district').append(cols);
                });
                if (dispalylist) {
                    $('#txt_district').val(dis_id).change();
                } else {
                    $('#txt_district').val("").change();
                }
            }
        }
    });
}
function getSubDistrict(distID) {
    $.ajax({
        url: 'Redemption/ListSubDistrict',
        type: "POST",
        async: false,
        cache: false,
        data: {
            amp_id: distID
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                var dispalylist = false;
                $('#txt_subdistrict').html('<option value=""> --- แขวง/ตำบล --- </option>');
                $.each(d.items, function (i, e) {
                    if (e.TAM_ID == subdis_id) { dispalylist = true; }
                    cols = '<option value="' + e.TAM_ID + '">' + e.TAM_NAME + '</option>';
                    $('#txt_subdistrict').append(cols);
                });
                if (dispalylist) {
                    $('#txt_subdistrict').val(subdis_id).change();
                } else {
                    $('#txt_subdistrict').val("").change();
                }
            }
        }
    });
}
function getZipcode(tamID) {
    $.ajax({
        url: 'Redemption/Zipcode',
        type: "POST",
        async: false,
        cache: false,
        data: {
            tam_id: tamID
        },
        success: function (data) {
            var d = JSON.parse(data);
            if (d.success) {
                $.each(d.items, function (i, e) {
                    $('#txt_post').val(e.TAM_ZIPCODE);
                });
            }
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function getListBank() {
    $.ajax({
        url: 'Redemption/ListBanked',
        type: "POST",
        async: false,
        cache: false,
        data: { },
        success: function (data) {
            var d = JSON.parse(data);
            var rows = '<option value=""> ----- กรุณาเลือกธนาคาร ----- </option>';
            if (d.success) {
                for (var j = 0; j < d.items.length; j++) {
                    cols = '<option value="' + d.items[j]["BANK_CODE"] + '">' + d.items[j]["BANK_NAME"] + '</option>';
                    rows += cols;
                }
                $('#ddl_bank').html(rows);
                $('#ddl_bank').val("").change();
            } else {
                $('#ddl_bank').html(rows);
                $('#ddl_bank').val("").change();
            }

        },
    });
}
function getReward_draw() {
    $.ajax({
        url: 'Redemption/GetListRewardCart_Draw',
        type: "POST",
        async: false,
        cache: false,
        data: { Cust_ID: sessionCust_ID },
        success: function (data) {
            var d = JSON.parse(data);
            $('#tbody_rewardredemp').html("");
            if (d.success) {
                $.each(d.items, function (i, e) {
                    cols = '<td class="text-center" data-title="Picture"><img class="img-responsive displayed" src="' + sessionStorage.mainUrl + '/Content/img/Reward/' + e.PATH_IMAGE + '" max-height="30"></td>' +
                            '<td class="text-left" data-title="Code" style="vertical-align:middle;">' +
                                '<input type="hidden" name="str_rewardCode[]" value="' + e.REWARD_CODE + '" />' +
                                e.REWARD_CODE +
                            '</td>' +
                            '<td class="text-left" data-title="Name" style="vertical-align:middle;">' +
                                '<input type="hidden" name="str_rewardName[]" value="' + e.NAME_TH + '" />' +
                                e.NAME_TH +
                            '</td>' +
                            '<td class="text-right" data-title="Point/Unit" style="vertical-align:middle;">' +
                                '<input type="hidden" name="str_rewardPoint[]" value="' + e.POINT + '" />' +
                                commaSeparateNumber(e.POINT) + '</td>' +
                            '<td class="text-right" data-title="Quantity" style="vertical-align:middle;">' +
                                '<input type="hidden" name="str_qtyRedemp[]" value="' + e.QTY + '" />' +
                                commaSeparateNumber(e.QTY) + '</td>' +
                            '<td class="text-right" data-title="Total Point" style="vertical-align:middle;">' +
                                commaSeparateNumber(e.POINT_REDEEM) +
                            '</td>' +
                            '<td class="text-left" data-title="Shipped by" style="vertical-align:middle;">' +
                                '<input type="hidden" name="str_category[]" value="' + e.CATEGORY_CODE + '" />' +
                                '<input type="hidden" name="str_reason_rewardRe[]" value="' + e.COURIER_TYPE_CODE + '" />' +
                                '<input type="hidden" name="str_catecode[]" value="' + e.CATEGORY_CODE + '" />' +
                                '<input type="hidden" name="couriercode[]" value="' + e.COURIER_TYPE_CODE + '" />' +
                                e.COURIER_TYPE_NAME +
                            '</td>' +
                            '<td class="text-right" data-title="Remark" style="vertical-align:middle;"><input type="text" name="str_rewardRemark[]" maxlength="50" /></td>' +
                            '<td class="text-center" data-title="Delete" style="vertical-align:middle;"><a style="cursor: pointer;" onclick="deleteRow(this)"><span class="fa fa-remove" style="font-size:20px;color:red"></span></a></td>';
                    rows = '<tr>' + cols + '</tr>';
                    $('#tbody_rewardredemp').append(rows);
                });
                summaryPoint_2();
            }
        }
    });
}
