﻿var frm_validate = "";
var bg_id_confirm = 0;
var bg_date_confirm = "";
var bg_rangDate_confirm = "";
var img_PO = "";
var img_card = "";
var img_bookbank = "";
$(document).ready(function () {
    var tblRedeem = $('#tb_waitBudget').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 1, 6, 9, 10] },
            { className: "text-left", "targets": [2, 3, 4, 5] },
            { className: "text-right", "targets": [7, 8] }
        ]
    });
    $('#tb_detailBudget').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 4, 5] },
            { className: "text-left", "targets": [1, 2, 3, 6, 10] },
            { className: "text-right", "targets": [7, 8, 9] }
        ]
    });
    $('#tb_detailByCategory').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 6] },
            { className: "text-left", "targets": [1, 2, 3, 4, 5] },
            { className: "text-right", "targets": [7, 8] }
        ]
    });
    $('#tb_receiveReward').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 11] },
            { className: "text-right", "targets": [3, 4, 5, 6, 7, 8, 9, 10] }
        ]
    });
    $('#tb_budget').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 3, 9] },
            { className: "text-right", "targets": [4, 5] }
        ]
    });
    $('#tb_BudgetForSBU').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 1, 6] },
            { className: "text-right", "targets": [3, 4, 5] }
        ]
    });
    $('#tb_RedeemReward').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0] },
            { className: "text-right", "targets": [3, 4, 5] }
        ]
    });
    $('#reward-date-range').datepicker({ todayHighlight: true, format: "dd/mm/yyyy", autoclose: "true" });
    getListBudget();
    getListWaitBudget();
    getListBank();
    $('#tb_receiveRewardByID_detail').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        columnDefs: [
            { className: "text-center", "targets": [0, 2, 5] }
        ]
    });
    $('#btn_generateBudget').on('click', function () {
        frm_validate = "";
        var vtxt_dateF = validate_from_budget($("#str_budget_start_date"), $("#vtxt_form"), "กรุณาระบุวันที่เริ่มต้นต้องการ Genarate การตั้งเบิก");
        var vtxt_dateT = validate_from_budget($("#str_budget_end_date"), $("#vtxt_form"), "กรุณาระบุวันที่สิ้นสุดต้องการ Genarate การตั้งเบิก");
        if (vtxt_dateF == true && vtxt_dateT == true) {
            var listRedeem = "";
            tblRedeem.$('input[type="checkbox"]').each(function () {
                if (this.checked) {
                    if (listRedeem != "") {
                        listRedeem += "," + $(this).attr('data_ckbbudget');
                    } else {
                        listRedeem = $(this).attr('data_ckbbudget');
                    }
                }
            });
            $.ajax({
                url: 'Budget/checkQtyGenerateBudget',
                type: "POST",
                async: false,
                cache: false,
                data:
                    {
                        DateFrom: $('#str_budget_start_date').val(),
                        DateTo: $('#str_budget_end_date').val()
                    },
                success: function (datacheck) {
                    var acheck = JSON.parse(datacheck);
                    if (acheck.success) {
                        if (acheck.items.C_MEMBER > 0 && listRedeem != "") {
                            var today = new Date();
                            var d_today = today.getFullYear() + "" + addZeroBefore(today.getMonth() + 1) + "" + addZeroBefore(today.getDate());
                            var bg_e = $('#str_budget_end_date').val().split('/');
                            var d_bg_e = bg_e[2] + "" + bg_e[1] + "" + bg_e[0];
                            var bg_s = $('#str_budget_start_date').val().split('/');
                            var d_bg_s = bg_s[2] + "" + bg_s[1] + "" + bg_s[0];
                            var bg_last = $('#hd_date_gen').val().split('/');
                            var d_bg_last = "20181231";
                            if (bg_last != "") {
                                d_bg_last = bg_last[2] + "" + bg_last[1] + "" + bg_last[0];
                            }
                            if (parseInt($('#txt_pointredeem').val()) > 0) {
                                if (d_bg_e < d_today && d_bg_s <= d_bg_e && d_bg_s >= d_bg_last) {
                                    $.ajax({
                                        url: 'Budget/SaveGenerateBudget',
                                        type: "POST",
                                        async: false,
                                        cache: false,
                                        data:
                                            {
                                                DateFrom: $('#str_budget_start_date').val(),
                                                DateTo: $('#str_budget_end_date').val(),
                                                listRedeem: listRedeem,
                                                CreateBy: sessionStorage.UserName
                                            },
                                        success: function (data) {
                                            var a = JSON.parse(data);
                                            if (a.success) {
                                                $('#budget_Alert').text("บันทึกข้อมูลการตั้งเบิกเรียบร้อย");
                                                $('#budget_Alert2').text("");
                                                $('#alert_modal').modal('show');
                                                getListBudget();
                                                getListWaitBudget();
                                                $('#myGenerate').modal('hide');
                                            }
                                        }
                                    });
                                } else {
                                    $('#budget_Alert').text("การตั้งเบิกนี้: คุณไม่สามารถตั้งเบิกได้");
                                    $('#budget_Alert2').text("กรุณาตรวจสอบช่วงวันที่ตั้งเบิก");
                                    $('#alert_modal').modal('show');
                                }
                            } else {
                                $('#budget_Alert').text("การตั้งเบิกนี้: คุณไม่สามารถตั้งเบิกได้");
                                $('#budget_Alert2').text("เนื่องจากไม่พบข้อมูลการแลกของรางวัล");
                                $('#alert_modal').modal('show');
                            }
                        } else {
                            $('#budget_Alert').text("การตั้งเบิกนี้: คุณไม่สามารถตั้งเบิกได้");
                            $('#budget_Alert2').text("เนื่องจากช่วงวันที่ที่ต้องการตั้งเบิก ไม่พบข้อมูลการแลกของรางวัล");
                            $('#alert_modal').modal('show');
                        }
                    }
                }
            });
        } else {
            bootbox.alert({
                title: "กรุณากรอกข้อมูลให้ครบ",
                message: "<ul>" + frm_validate + "</ul>",
                size: 'small',
                callback: function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                }
            });
            //$('#budget_Alert').text("<ul>" + frm_validate + "</ul>");
            //$('#alert_modal').modal('show');
        }
    });
    //$('#tb_budget').on("click", ".cls_budgetSBU", function () {
    //    showLoading();
    //    str_code = $(this).attr('data_budgetSBU');
    //    $('#hd_sbuCode').val(str_code);
    //    $.ajax({
    //        url: sessionStorage.apiUrl + '/budget/GetListBudgetForSBU',//
    //        dataType: 'json',
    //        type: "POST",
    //        crossDomain: true,
    //        withCredentials: true,
    //        headers: {
    //            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
    //            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
    //        },
    //        data: {
    //            SBU: str_code,
    //            language: "TH",
    //            maxResults: 999,
    //            fromType: "B"
    //        },
    //        async: true,
    //        cache: false,
    //        success: function (data) {
    //            closeLoading();
    //            if (data.success) {
    //                $('#tb_BudgetForSBU').dataTable().fnClearTable();
    //                var totalpoint = 0;
    //                var totaluser = 0;
    //                var txtuser = 0;
    //                var c = 1;
    //                $.each(data.items, function (i, value) {
    //                    var SBU = value.SBU == "" ? "" : (value.SBU);
    //                    var SBU_NAME = value.SBU_NAME == null ? "" : value.SBU_NAME;
    //                    var TOTAL_MEMBER = value.TOTAL_MEMBER == null ? "0" : commaSeparateNumber(value.TOTAL_MEMBER);
    //                    var TOTAL_POINT = value.TOTAL_POINT == null ? "0" : commaSeparateNumber(value.TOTAL_POINT);
    //                    var TOTAL_BUDGET = value.TOTAL_BUDGET == null ? "0" : formatter.format(value.TOTAL_BUDGET);
    //                    var btn_detail = '<button type="button" class="btn btn-warning btn-sm cls_detailbudgetBUDGET_ID" data_detailbudgetBUDGET_ID="' + value.BUDGET_ID
    //                        + '" data_detailbudgetSBUshort="' + value.SBU + '" data_detailbudgetSBU="' + value.SBU_ID + '">รายละเอียด SBU</button>';
    //                    $('#tb_BudgetForSBU').dataTable().fnAddData([
    //                        c,
    //                        SBU,
    //                        SBU_NAME,
    //                        '<span>' + TOTAL_MEMBER + '</span>',
    //                        '<span>' + TOTAL_POINT + '</span>',
    //                        '<span>' + TOTAL_BUDGET + '</span>',
    //                        btn_detail
    //                    ]);
    //                    c++;
    //                });
    //                $('#myBudgetForSBU').modal("show");
    //            }
    //        }
    //    });
    //});
    $('#tb_budget').on("click", ".cls_budgetCate", function () {
        showLoading();
        str_code = $(this).attr('data_budgetCate');
        $('#hd_sbuCode').val(str_code);
        $.ajax({
            url: 'Budget/GetListBudgetByCategory',
            type: "POST",
            async: false,
            cache: false,
            data:
                {
                    BUDGET_ID: str_code
                },
            success: function (data) {
                var a = JSON.parse(data);
                closeLoading();
                if (a.success) {
                    $('#tb_detailByCategory').dataTable().fnClearTable();
                    var c = 1;
                    $.each(a.items, function (i, value) {
                        var CATEGORY_NAME = value.CATEGORY_NAME == "" ? "" : (value.CATEGORY_NAME);
                        var MEMBER_CARD = value.MEMBER_ID == null ? "" : value.MEMBER_ID;
                        var RECEIVE_NAME = value.RECEIVE_NAME == null ? "" : value.RECEIVE_NAME;
                        var TIER_NAME = value.TIER_NAME == null ? "" : value.TIER_NAME;
                        var REWARD_CODE = value.REWARD_CODE == null ? "" : value.REWARD_CODE;
                        var REWARD_NAME = value.REWARD_NAME == null ? "" : value.REWARD_NAME;
                        var SBU = value.SBU == null ? "" : value.SBU;
                        var PROJECT = value.PROJECT == null ? "" : value.PROJECT;
                        $('#tb_detailByCategory').dataTable().fnAddData([
                            '<span>' + c + '</span>',
                            '<span>' + CATEGORY_NAME + '</span>',
                            '<span>' + MEMBER_CARD + '</span>',
                            '<span>' + RECEIVE_NAME + '</span>',
                            '<span>' + REWARD_CODE + '</span>',
                            '<span>' + REWARD_NAME + '</span>',
                            '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                            '<span>' + commaSeparateNumber(value.POINT_REDEEM) + '</span>',
                            '<span>' + formatter.format(value.REWARD_PRICE) + '</span>',
                        ]);
                        c++;
                    });
                    $('#myDetailyByCategory').modal("show");
                }
            }
        });
    });
    $('#tb_budget').on("click", ".cls_reportBudget", function () {
        str_code = $(this).attr('data_reportBudget');
        window.open(
            sessionStorage.mainUrl + '/ReportBudget?budgetid=' + str_code,
            '_blank' // <- This is what makes it open in a new window.
        );
    });
    $('#tb_budget').on("change", ".cls_ApproveBudget", function () {
        bg_id_confirm = $(this).attr('data_ApproveBudget');
        bg_date_confirm = $(this).parent().parent().children('td').eq(1).text();
        bg_rangDate_confirm = $(this).parent().parent().children('td').eq(2).text();
        $('#budget_approve_Alert').text('การตั้งเบิก ณ วันที่ ' + bg_date_confirm + ' เป็นรายการแลกวันที่ ' + bg_rangDate_confirm);
        $('#budget_approve_Alert2').text('ยืนยันการอนุมัติการตั้งเบิกนี้');
        $('#alert_approve_modal').modal('show');
    });
    $('#tb_budget').on("click", ".cls_receiveBudget", function () {
        if ($(this).attr('databgStatus') != "1") {
            var bgid_receive = $(this).attr('data_receiveBudget');
            bg_date_confirm = $(this).parent().parent().children('td').eq(1).text();
            bg_rangDate_confirm = $(this).parent().parent().children('td').eq(2).text();
            getListReceiveReward(bgid_receive);
        } else {
            $('#budget_Alert').text("การตั้งเบิกนี้: รอการอนุมัติ คุณไม่สามารถทำการรับของรางวัลเข้าระบบได้");
            $('#budget_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('.btn_confirmApprove').on('click', function () {
        $('#alert_modal').modal('hide');
        showLoading();
        $.ajax({
            url: 'Budget/UpdateApproveBudget',
            type: "POST",
            async: false,
            cache: false,
            data:
                {
                    BUDGET_ID: bg_id_confirm,
                    username: sessionStorage.UserName
                },
            success: function (data) {
                var a = JSON.parse(data);
                if (a.success) {
                    $('#alert_approve_modal').modal('hide');
                    getListBudget();
                }
                closeLoading();
            }
        });
    });
    $('.btn_reportBudget').on('click', function () {
        window.open(
            sessionStorage.backendUrl + 'ReportRedeemBudget?budgetid=' + $('#hd_sbuCode').val(),
            '_blank' // <- This is what makes it open in a new window.
        );
    });
    //$('#tb_BudgetForSBU').on("click", ".cls_detailbudgetBUDGET_ID", function () {
    //    showLoading();
    //    var BUDGET_ID = $(this).attr('data_detailbudgetBUDGET_ID');
    //    var str_code = $(this).attr('data_detailbudgetSBU');
    //    var sbuShortname = $(this).attr('data_detailbudgetSBUshort');
    //    $('#hd_sbuCode').val(BUDGET_ID);
    //    $.ajax({
    //        url: sessionStorage.apiUrl + '/budget/GetListBudgetByBudgetID',//
    //        dataType: 'json',
    //        type: "POST",
    //        crossDomain: true,
    //        withCredentials: true,
    //        headers: {
    //            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
    //            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
    //        },
    //        data: {
    //            BUDGET_ID: BUDGET_ID,
    //            SBU: str_code,
    //            language: "TH",
    //            maxResults: 999,
    //            fromType: "B"
    //        },
    //        async: true,
    //        cache: false,
    //        success: function (data) {
    //            closeLoading();
    //            if (data.success) {
    //                $('#tb_detailBudget').dataTable().fnClearTable();
    //                var c = 1;
    //                $.each(data.items, function (i, value) {
    //                    $('#namesbu').text("{" + sbuShortname + "} " + value.SubBUName);
    //                    var DateRedeem = value.REDEEM_DATE == "" ? "" : ConvertDateToFormat(value.REDEEM_DATE);
    //                    var MEMBER_CARD_NO = value.MEMBER_CARD_NO == null ? "" : value.MEMBER_CARD_NO;
    //                    var RECEIVE_NAME = value.RECEIVE_NAME == null ? "" : value.RECEIVE_NAME;
    //                    var TIER_NAME = value.TIER_NAME == null ? "" : value.TIER_NAME;
    //                    var REWARD_CODE = value.REWARD_CODE == null ? "" : value.REWARD_CODE;
    //                    var REWARD_NAME = value.REWARD_NAME == null ? "" : value.REWARD_NAME;
    //                    var Referfriend = value.REFER_NAME == null ? "" : value.REFER_NAME;
    //                    var PROJECT = value.PROJECT == null ? "" : value.PROJECT;
    //                    $('#tb_detailBudget').dataTable().fnAddData([
    //                        '<span>' + c + '</span>',
    //                        '<span>' + DateRedeem + '</span>',
    //                        '<span>' + MEMBER_CARD_NO + '</span>',
    //                        '<span>' + RECEIVE_NAME + '</span>',
    //                        '<span>' + TIER_NAME + '</span>',
    //                        '<span>' + REWARD_CODE + '</span>',
    //                        '<span>' + REWARD_NAME + '</span>',
    //                        '<span>' + commaSeparateNumber(value.QTY) + '</span>',
    //                        '<span>' + commaSeparateNumber(value.POINT_REDEEM) + '</span>',
    //                        '<span>' + formatter.format(value.TOTAL_BUDGET) + '</span>',
    //                        '<span>' + Referfriend + '</span>'
    //                    ]);
    //                    c++;
    //                });
    //                $('#myDetailBudgetForSBU').modal("show");
    //            }
    //        }
    //    });
    //});
    $('#tb_receiveReward').on("click", ".btn_Receive", function () {
        var bg_id_receive = $(this).attr('data_ReceiveBGID');
        var rw_id_receive = $(this).attr('data_ReceiveRWID');
        $("#tbody_receiveReward tr").removeAttr("style");
        $("#vtxt_receive_no").text("");
        $("#vtxt_copyPO").text("");
        $(this).parent().parent().css("background-color", "#e1e1e1");
        $('#txt_receive_RewardID').val(rw_id_receive + " " + $(this).parent().parent().children('td').eq(2).text());
        $('#hd_receiveRWID').val(rw_id_receive + " " + $(this).parent().parent().children('td').eq(2).text());
        var dn = new Date();
        $('#txt_dateReceive').val(dn.getDate() + '/' + addZeroBefore(dn.getMonth() + 1) + '/' + dn.getFullYear());
        var qty_rec = parseInt($(this).parent().parent().children('td').eq(6).text().replace(',', ''));
        $('#hd_receive_qty').val(qty_rec);
        $('#hd_receive_BgID').val(bg_id_receive);
        $('#hd_receive_RwID').val(rw_id_receive);
        $('#txt_receive_qty').val(qty_rec);
        $('#txt_qty_budget').val(parseInt($(this).parent().parent().children('td').eq(4).text().replace(',', '')));
        $('#txt_receive_success').val(parseInt($(this).parent().parent().children('td').eq(5).text().replace(',', '')));
        if (qty_rec > 0) {
            document.getElementById("grp_recive").style.display = "block";
        } else {
            document.getElementById("grp_recive").style.display = "none";
        }
        $('#txt_receive_no').val("");
        getListReceiveRewardByCode();

    });
    $('#close_ReceiveRewardByID').on('click', function () {
        $('#myReceiveRewardByID').modal('hide');
    });
    $('#close_approve_alert').on('click', function () {
        $('#alert_approve_modal').modal('hide');
        getListBudget();
    });
    $('#close_confirm_alert').on('click', function () {
        $('#alert_confirm_modal').modal('hide');
    });
    $('#close_alert').on('click', function () {
        $('#alert_modal').modal('hide');
    });
    $('.btn_saveReceive').on('click', function () {
        var vtxt_data = validate_from_budget($("#txt_receive_no"), $("#vtxt_receive_no"), "กรุณาระบุเลขที่อ้างอิง(ใบสั่งซื้อ)");
        var vtxt_img = validate_from_budget($("#txt_copyPO"), $("#vtxt_copyPO"), "กรุณาแนบสำเนาเลขที่อ้างอิง(ใบสั่งซื้อ)");
        if (vtxt_data == true && vtxt_img == true) {
            if (parseInt($('#txt_receive_qty').val()) <= 0) {
                $('#budget_Alert').text('กรุณาระบุจำนวนที่รับของรางวัล');
                $('#budget_Alert2').text("");
                $('#alert_modal').modal('show');
            } else if (parseInt($('#hd_receive_qty').val()) >= parseInt($('#txt_receive_qty').val())) {
                $('#budget_confirm_Alert').text('ยืนยันการรับของรางวัล');
                $('#budget_confirm_Alert2').text($('#hd_receiveRWID').val() + " จำนวน " + $('#txt_receive_qty').val() + " ชิ้น");
                $('#alert_confirm_modal').modal('show');
            } else {
                //$('#txt_receive_qty').val($('#hd_receive_qty').val());
                $('#budget_Alert').text('การรับของรางวัลนี้ ไม่สามารถรับของรางวัลได้');
                $('#budget_Alert2').text("เนื่องจากจำนวนที่รับของรางวัลมากกว่าจำนวนที่ตั้งเบิก");
                $('#alert_modal').modal('show');
            }
        } else {
            $('#budget_Alert').text('กรุณาระบุหรือแบบสำเนา เลขที่อ้างอิง(ใบสั่งซื้อ)');
            $('#budget_Alert2').text("");
            $('#alert_modal').modal('show');
        }
    });
    $('.btn_confirmReceive').on('click', function () {
        saveReceive();
    });
    $('#close_modalreceive').on('click', function () {
        $('#myReceiveReward').modal('hide');
        getListBudget();
    });
    $('#btn_myGenerate').on('click', function () {
        $('#myGenerate').modal('show');
        getDataLastDateGenerate();
    });
    $('#tb_waitBudget').on("click", ".cls_openimg", function () {
        var bg_id_receive = $(this).attr('data_openimg');
        var bg_receive_by = $(this).attr('data_user');
        var txtURL = sessionStorage.mainUrl;
        if (bg_receive_by=="2") {
            txtURL = sessionStorage.mainUrl;
        } else {
            txtURL = sessionStorage.frontUrl;
        }
        $('#img_file').attr('src', txtURL + "/Content/img/Card/" + bg_id_receive);
        $('#modal_openImg').modal('show');
    });
    $('#tb_receiveRewardByID_detail').on("click", ".cls_openimg", function () {
        var bg_id_receive = $(this).attr('data_openimg');
        $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/PO/" + bg_id_receive);
        $('#modal_openImg').modal('show');
    });
    $('#tb_waitBudget').on("click", ".cls_openimgBank", function () {
        var bg_id_receive = $(this).attr('data_openimgBank');
        var bg_receive_by = $(this).attr('data_user');
        var txtURL = sessionStorage.mainUrl;
        if (bg_receive_by == "2") {
            txtURL = sessionStorage.mainUrl;
        } else {
            txtURL = sessionStorage.frontUrl;
        }
        $('#img_file').attr('src', txtURL + "/Content/img/BookBank/" + bg_id_receive);
        $('#modal_openImg').modal('show');
    });
    $('#tb_waitBudget').on("click", ".cls_updateimg", function () {
        var redeemNo = $(this).attr('data_updateimg');
        getListUpdateCopy(redeemNo);
        $('#txt_copybookbank').val("");
        $('#txt_copyfile').val("");
        $('#modalUpdateCopy').modal('show');
    });
    $('#tb_waitBudget thead').on('ifChanged', '.chkRedeemAll', function (event) {
        var rows = tblRedeem.rows({ 'search': 'applied' }).nodes();
        if (event.target.checked) {
            $('input[type="checkbox"]', rows).iCheck('check');
        } else {
            $('input[type="checkbox"]', rows).iCheck('uncheck');
        }
    });
    $('#close_updateCopy').on('click', function () {
        $('#modalUpdateCopy').modal('hide');
        getListWaitBudget();
    });
    $('#txt_copyPO').change(function () {
        path = "PO";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            //console.log(e)
            data.append('file', e);
            data.append('path', path);
            $.ajax({
                url: "upload/UploadImageRedeem",
                processData: false,
                contentType: false,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: data,
                type: 'POST'
            }).done(function (result) {
                img_PO = result;
                //alert(img_bookbank);
            }).fail(function (a, b, c) {
                img_PO = "";
            });
        });
    });
    $('#openPOimg').on("click", function () {
        var fullPath1 = document.getElementById('txt_copyPO').value;
        if (fullPath1) {
            $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/PO/" + img_PO);
            $('#modal_openImg').modal('show');
        }
    });
    $('#txt_copyfile').change(function () {
        path = "Card";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            console.log(e)
            data.append('file', e);
            data.append('path', path);
            $.ajax({
                url: "upload/UploadImageRedeem",
                processData: false,
                contentType: false,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: data,
                type: 'POST'
            }).done(function (result) {
                img_card = result;
                //alert(img_card);
            }).fail(function (a, b, c) {
                img_card = "";
            });

        });
    });
    $('#txt_copybookbank').change(function () {
        path = "BookBank";
        var file = $(this)[0].files;
        $.each(file, function (i, e) {
            var data = new FormData();
            console.log(e)
            data.append('file', e);
            data.append('path', path);
            $.ajax({
                url: "upload/UploadImageRedeem",
                processData: false,
                contentType: false,
                headers: {
                    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                },
                data: data,
                type: 'POST'
            }).done(function (result) {
                img_bookbank = result;
                //alert(img_bookbank);
            }).fail(function (a, b, c) {
                img_bookbank = "";
            });
        });
    });
    $('#btn_saveUpdateCopy').on("click", function () {
        frm_validate = "";
        var vtxt_copycard = validate_from_budget($("#txt_copyfile"), $("#vtxt_copyfile"), 'สำเนาบัตรประชาชน');
        var vtxt_copycash = true;
        if ($('#hd_cash').val() == 1) {
            vtxt_copycash = validate_from_budget($("#txt_copybookbank"), $("#vtxt_copybookbank"), 'กรุณาแนบสำเนาหน้าแรกสมุดบัญชี');
        }
        if (vtxt_copycard == true && vtxt_copycash == true) {
            UpdateRedeemCopy();
        } else {
            $('#Reward_Alert').text('กรุณาแนบสำเนาประชาชนหรือสำเนาบัญชีใหครบถ้วน');
            $('#Reward_Alert2').text("");
            $('#alert-modal').modal('show');
        }
    });
    $('#openimg').on("click", function () {
        var copycard = "";
        var fullPath1 = document.getElementById('txt_copyfile').value;
        if (fullPath1) {
            $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/Card/" + img_card);
            $('#modal_openImg').modal('show');
        }
    });
    $('#openbookimg').on("click", function () {
        var fullPath1 = document.getElementById('txt_copybookbank').value;
        if (fullPath1) {
            $('#img_file').attr('src', sessionStorage.mainUrl + "/Content/img/BookBank/" + img_bookbank);
            $('#modal_openImg').modal('show');
        }
    });
});
function getListBudget() {
    showLoading();
    $.ajax({
        url: 'Budget/GetListBudget',
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#tb_budget').dataTable().fnClearTable();
                var totalpoint = 0;
                var totaluser = 0;
                var txtuser = 0;
                var c = 1;
                $.each(a.items, function (i, value) {
                    var BUDGET_DATE = value.BUDGET_DATE == "" ? "" : value.BUDGET_DATE;
                    var DURATION = value.DURATION == null ? "" : value.DURATION;
                    var TOTAL_MEMBER = value.TOTAL_MEMBER == null ? "0" : commaSeparateNumber(value.TOTAL_MEMBER);
                    var TOTAL_POINT = value.TOTAL_POINT == null ? "0" : commaSeparateNumber(value.TOTAL_POINT);
                    var TOTAL_BUDGET = value.TOTAL_BUDGET == null ? "0.00" : formatter.format(value.TOTAL_BUDGET);
                    var TOTAL_BUDGET_SBU = value.TOTAL_BUDGET_SBU == null ? "0" : commaSeparateNumber(value.TOTAL_BUDGET_SBU);
                    var BG_STATUS = '<span style="color:#ff5200;">' + "รอการอนุมัติ" + '</span>';
                    if (value.BUDGET_STATUS == 2) {
                        BG_STATUS = '<span style="color:#3c8dbc;">' + "อนุมัติ/รอรับของ" + '</span>';
                    } else if (value.BUDGET_STATUS == 3) {
                        BG_STATUS = '<span style="color:#80b636;">' + "รับของเรียบร้อย" + '</span>';
                    }
                    var date_approve = '<span style="color:#3c8dbc;">' + value.CL_APPROVE_DATE + '</span>';
                    if (value.CL_APPROVE_DATE == null || value.CL_APPROVE_DATE == "") {
                        date_approve = "<select class='form-control select2 cls_ApproveBudget' data_ApproveBudget='" + value.BUDGET_ID + "' name='ddl_ApproveBudget[]'>" +
                            "<option value='1'>รอการอนุมัติ</option>" +
                            "<option value='2'>อนุมัติการตั้งเบิก</option>" +
                        "</select>";
                    }
                    var btn_detail = '<button type="button" class="btn btn-info btn-xs cls_budgetCate" data_budgetCate="' + value.BUDGET_ID
                        + '">ของรางวัล</button>&nbsp;<button type="button" class="btn btn-primary btn-xs cls_receiveBudget" data_receiveBudget="' + value.BUDGET_ID
                        + '" databgStatus = "' + value.BUDGET_STATUS + '">รับของ</button>&nbsp;<button type="button" class="btn btn-primary btn-xs cls_reportBudget" data_reportBudget="' + value.BUDGET_ID
                        + '">รายงาน</button>';

                    $('#tb_budget').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + BUDGET_DATE + '</span>',
                        '<span>' + DURATION + '</span>',
                        '<span>' + TOTAL_MEMBER + '</span>',
                        '<span>' + TOTAL_POINT + '</span>',
                        '<span>' + TOTAL_BUDGET + '</span>',
                        date_approve,
                        BG_STATUS,
                        value.CREATE_BY,
                        btn_detail
                    ]);
                    c++;
                });
            }
            closeLoading();
        }
    });
}
function getListWaitBudget() {
    $.ajax({
        url: 'Budget/ListWaitBudget',
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            var a = JSON.parse(data);
            $('input[type="checkbox"]').iCheck('uncheck');
            if (a.success) {
                $('#tb_waitBudget').dataTable().fnClearTable();
                var totalpoint = 0;
                var totaluser = 0;
                var txtuser = 0;
                var c = 1;
                $.each(a.items, function (i, value) {
                    var DateRedeem = value.REDEEM_DATE == "" ? "" : ConvertDateToFormat(value.REDEEM_DATE);
                    var MEMBER_CARD = value.MEMBER_ID == null ? "" : value.MEMBER_ID;
                    var RECEIVE_NAME = value.RECEIVE_NAME == null ? "" : value.RECEIVE_NAME;
                    var REWARD_CODE = value.REWARD_CODE == null ? "" : value.REWARD_CODE;
                    var REWARD_NAME = value.REWARD_NAME == null ? "" : value.REWARD_NAME;
                    var SBU = value.SBU == null ? "" : value.SBU;
                    var PROJECT = value.PROJECT == null ? "" : value.PROJECT;
                    var updateImg = '<a style="cursor: pointer;" class="cls_updateimg" data_updateimg="' + value.REDEEM_NO + '"><span class="fa fa-pencil-square-o"></span></a>';
                    var iconImg = '<a style="cursor: pointer;" class="cls_openimg" data_user="' + value.REDEEM_CHANNEL + '" data_openimg="' + value.COPY_CITIZEN_ID + '"><span class="fa fa-id-card-o"></span></a>';
                    if (value.COPY_BOOK_BANK != "") {
                        iconImg += '&nbsp;<a style="cursor: pointer;" class="cls_openimgBank" data_user="' + value.REDEEM_BY + '" data_openimgBank="' + value.COPY_BOOK_BANK + '"><span class="fa fa-book"></span></a>'
                    }
                    var checkbox = '<input type="checkbox" class="cls_ckbbudget" data_ckbbudget="' + value.REDEEM_NO + '" />';
                    $('#tb_waitBudget').dataTable().fnAddData([
                        checkbox,
                        '<span>' + c + '</span>',
                        '<span>' + DateRedeem + '</span>',
                        '<span>' + MEMBER_CARD + '</span>',
                        '<span>' + RECEIVE_NAME + '</span>',
                        '<span>' + REWARD_CODE + ' ' + REWARD_NAME + '</span>',
                        '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                        '<span>' + commaSeparateNumber(value.POINT_REDEEM) + '</span>',
                        '<span>' + formatter.format(value.REWARD_PRICE) + '</span>',
                        updateImg,
                        iconImg
                    ]);
                    if (txtuser != value.MEMBER_ID) {
                        txtuser = value.MEMBER_ID;
                        totaluser++;
                    }
                    totalpoint += value.POINT_REDEEM;
                    c++;
                });
                $('#txt_userredeem').val(commaSeparateNumber(totaluser));
                $('#txt_pointredeem').val(commaSeparateNumber(totalpoint));
            }
        }
    });
}
function getDataLastDateGenerate() {
    showLoading();
    $.ajax({
        url: 'Budget/DataLastDate',
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            closeLoading();
            var a = JSON.parse(data);
            if (a.success) {
                if (a.items == null) {
                    var today = new Date();
                    var d_today = addZeroBefore(today.getDate() - 1) + "/" + addZeroBefore(today.getMonth() + 1) + "/" + today.getFullYear();
                    $('#str_budget_start_date').val("01/01/2019");
                    $('#str_budget_end_date').val(d_today);
                } else {
                    $('#hd_date_gen').val(a.items.LAST_DATE);
                    $('#str_budget_start_date').val(a.items.LAST_DATE);
                    $('#str_budget_end_date').val(a.items.TO_DATE);
                    var bg_e = $('#str_budget_end_date').val().split('/');
                    var d_bg_e = bg_e[2] + "" + bg_e[1] + "" + bg_e[0];
                    var bg_s = $('#str_budget_start_date').val().split('/');
                    var d_bg_s = bg_s[2] + "" + bg_s[1] + "" + bg_s[0];
                    if (d_bg_e < d_bg_s) {
                        $('#budget_Alert').text('การตั้งเบิกนี้ ไม่สามารถตั้งเบิกได้');
                        $('#budget_Alert2').text('เนื่องจากตั้งเบิกจนถึงวันปัจจุบันแล้ว');
                        $('#alert_modal').modal('show');
                        $('#myGenerate').modal('hide');
                    }
                }
            }
        }
    });
}
function validate_from_budget(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        frm_validate += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}
function getListReceiveReward(bgID) {
    $.ajax({
        url: 'Budget/getReceiveByBudgetID',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                BUDGET_ID: bgID
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#tb_receiveReward').dataTable().fnClearTable();
                var totalpoint = 0;
                var totaluser = 0;
                var txtuser = 0;
                var c = 1;
                $('.txt_receiveBudgetID').text(" วันที่ตั้งเบิก " + bg_date_confirm + " ช่วงวันที่ " + bg_rangDate_confirm);
                $.each(a.items, function (i, value) {
                    var icondetail = '';
                    var receive = '-';
                    var txt_receive = "รับของ";
                    if (value.TOTAL_MUST_GET_STOCK == value.GET_QTY) {
                        txt_receive = "รายละเอียด";
                    }
                    if (value.TOTAL_MUST_GET_STOCK > 0) {//value.CHECK_STOCK == "Y"
                        icondetail = '<button type="button" class="btn btn-info btn-sm btn_Receive" data_ReceiveRWID="' + value.REWARD_CODE +
                        '" data_ReceiveBGID="' + bgID + '">' + txt_receive + '</button>';
                        receive = '<span>' + commaSeparateNumber(value.QTY_RECEIVE).toString() + '</span>';
                        if (value.QTY_RECEIVE != value.QTY_REDEEM) {
                            receive = '<span style="color:red;">' + commaSeparateNumber(value.QTY_RECEIVE).toString() + '</span>';
                        }
                    }
                    $('#tb_receiveReward').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + value.REWARD_CODE + '</span>',
                        '<span>' + value.NAME_TH + '</span>',
                        '<span>' + commaSeparateNumber(value.BUDGET_QTY) + '</span>',
                        '<span style="color:blue;">' + commaSeparateNumber(value.TOTAL_MUST_GET_STOCK) + '</span>',
                        '<span style="color:blue;">' + commaSeparateNumber(value.GET_QTY) + '</span>',
                        '<span style="color:blue;">' + commaSeparateNumber(value.WAIT_QTY) + '</span>',
                        commaSeparateNumber(value.TOTAL_NOT_GET_STOCK),
                        commaSeparateNumber(value.VENDOR_QTY),
                        commaSeparateNumber(value.SHOP_QTY),
                        commaSeparateNumber(value.CASH_QTY),
                        icondetail,
                    ]);
                    c++;
                });
            }
            $('#myReceiveReward').modal('show');
        }
    });
}
function getListReceiveRewardByCode() {
    $.ajax({
        url: 'Budget/GetListReceiveRewardID',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                BUDGET_ID: $('#hd_receive_BgID').val(),
                REWARD_CODE: $('#hd_receive_RwID').val()
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#tb_receiveRewardByID_detail').dataTable().fnClearTable();
                var totalpoint = 0;
                var totaluser = 0;
                var txtuser = 0;
                var c = 1;
                $.each(a.items, function (i, value) {
                    var iconImg = '';
                    if (value.COPY_PO != "") {
                        iconImg = '<a style="cursor: pointer;" class="cls_openimg" data_user="2" data_openimg="' + value.COPY_PO + '"><span class="fa fa-file-photo-o"></span></a>';
                    }
                    $('#tb_receiveRewardByID_detail').dataTable().fnAddData([
                        '<span>' + c + '</span>',
                        '<span>' + ConvertDateToFormat(value.STOCK_DATE) + '</span>',
                        '<span>' + commaSeparateNumber(value.QTY) + '</span>',
                        '<span>' + value.REF_DOC_NO + '</span>',
                        value.CREATE_BY,
                        iconImg
                    ]);
                    c++;
                });
            }
            $('#myReceiveRewardByID').modal('show');
        }
    });
}
function saveReceive() {
    showLoading();
    $.ajax({
        url: 'Budget/SaveReceiveReward',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                BUDGET_ID: $('#hd_receive_BgID').val(),
                REWARD_CODE: $('#hd_receive_RwID').val(),
                PROJECT_ID: 1,
                QTY: parseInt($('#txt_receive_qty').val()),
                REF_DOC_NO: $('#txt_receive_no').val(),
                COPY_PO: img_PO,
                username: sessionStorage.UserName
            },
        success: function (data) {
            $('#alert_confirm_modal').modal('hide');
            $('#myReceiveRewardByID').modal('hide');
            var a = JSON.parse(data);
            if (a.success) {
                getListReceiveReward($('#hd_receive_BgID').val());
            }
            closeLoading();
        }
    });
}
function getListUpdateCopy(redeemNo) {
    showLoading();
    img_card = "";
    img_bookbank = "";
    $.ajax({
        url: 'Budget/GetListUpdateCopy',
        type: "POST",
        async: false,
        cache: false,
        data:
            {
                REDEEM_NO: redeemNo
            },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $('#hd_cash').val("0");
                $('#tb_RedeemReward').dataTable().fnClearTable();
                $('#hd_redeemNo').val(redeemNo);
                $.each(a.items, function (i, e) {
                    var name = e.RECEIVE_NAME.split(' ');
                    $('#txt_fname').val(name[0]);
                    $('#txt_lname').val(name[1]);
                    $('#txt_idcard_redeem').val(e.CARD_CODE);
                    $('#txt_tel_redeem').val(e.PHONE);
                    $('#txt_email').val(e.EMAIL);
                    $('#txt_addr').val(e.HOUSE_NO);
                    if (e.DELIVERY_COURIER_CODE == 99) {
                        document.getElementById("grp_bookbank").style.display = "block";
                        $('#hd_cash').val(e.DELIVERY_COURIER_CODE);
                        $('#ddl_bank').val(e.BANK_CODE).change();
                        $('#txt_bookbank').val(e.BANK_ACCT_NO);
                        $('#txt_bankBranch').val(e.BANK_BRANCH);
                    } else {
                        document.getElementById("grp_bookbank").style.display = "none";
                    }
                    $('#tb_RedeemReward').dataTable().fnAddData([
                        '<span>' + (i + 1) + '</span>',
                        '<span>' + e.REWARD_CODE + '</span>',
                        '<span>' + e.NAME_TH + '</span>',
                        '<span>' + e.QTY + '</span>',
                        '<span>' + commaSeparateNumber(e.POINT_REDEEM) + '</span>',
                        '<span>' + formatter.format(e.REWARD_PRICE) + '</span>'
                    ]);
                });
            }
            closeLoading();
        }
    });
}
function getListBank() {
    $.ajax({
        url: 'Budget/ListBanked',
        type: "POST",
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            var a = JSON.parse(data);
            var rows = '<option value=""> ----- กรุณาเลือกธนาคาร ----- </option>';
            if (a.success) {
                $.each(a.items, function (i, e) {
                    cols = '<option value="' + e.BANK_CODE + '">' + e.BANK_NAME + '</option>';
                    rows += cols;
                });
                $('#ddl_bank').html(rows);
                $('#ddl_bank').val("").change();
            } else {
                $('#ddl_bank').html(rows);
                $('#ddl_bank').val("").change();
            }

        },
    });
}
function UpdateRedeemCopy() {
    showLoading();
    $.ajax({
        url: 'Budget/UpdateRedeemCopy',
        type: "POST",
        async: false,
        cache: false,
        data: {
            REDEEM_NO: $('#hd_redeemNo').val(),
            COPY_CARD: img_card,
            COPY_BANK: img_bookbank,
            username: sessionStorage.UserName
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                getListWaitBudget();
                $('#modalUpdateCopy').modal('hide');
            }
            closeLoading();
        }
    });
}
