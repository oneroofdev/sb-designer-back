﻿$(document).ready(function () {

    ////// PORTFOLIO //////
        getPortfolio("1");
    $("#status_approve").on('change', function () {
        var status = $("#status_approve").val();
        getPortfolio(status);
    }); //get port by approve status

    $("#portfolio-detail").on('click', '.wait_project', function () {
        var project_id = $(this).attr('data-project');
        $("#project_id").val(project_id);
        $("#alert_").modal('show');
        $("#alert_ .remark").css('display', 'none');
    });

    $("#portfolio-detail").on('click', '.preview', function () {
        var project_id = $(this).attr('data-project');
        window.open('http://sbdesignerclub.com:8080/dev/DesignerPortfolio/ProjectInfo/' + project_id + '?_auth_type=admin', '_blank');
    });

    $("#alert_").on('change', '#approve', function () {
        var status = $(this).val();
        if (status == "R") {
            $("#alert_ .remark").css('display', 'block');
        } else {
            $("#alert_ .remark").css('display', 'none');
        }
    });

    $("#alert_").on('click', '#comfirm_save', function () {
        $("#alert_").modal('hide');
        var project_id = $("#project_id").val();
        var status_approve = $("#approve").val();
        var remark = $("#remark").val();
        var user = sessionStorage.UserName;

        UpdateStatus_Approve(project_id, status_approve, remark, user);
    });

    ////// REVIEW Designer //////
    $("#tab_review_cus").on('click', function () {
        getReviewCus("1");
    });
    $("#review_cus_status").on('change', function () {
        var status = $("#review_cus_status").val();
        getReviewCus(status);
    }); //get review by approve status

    $("#review-cus-detail").on('click', '.wait_cus_review', function () {
        var rev_id = $(this).attr('data-review');
        var cus_id = $(this).attr('data-cus');
        $("#rev_id").val(rev_id);
        $("#cus_id").val(cus_id);
        $("#alert_cus_review").modal('show');
        $("#alert_cus_review .remark").css('display', 'none');
    });
    $("#alert_cus_review").on('change', '#approve_cus_review', function () {
        var status = $(this).val();
        //if (status == "P") {
        //    $("#alert_cus_review .remark").css('display', 'block');
        //} else {
        //    $("#alert_cus_review .remark").css('display', 'none');
        //}
    });
    $("#alert_cus_review").on('click', '#comfirm_save', function () {
        $("#alert_cus_review").modal('hide');
        var rev_id = $("#rev_id").val();
        var cus_id = $("#cus_id").val();
        var status_approve = $("#approve_cus_review").val();
        var user = sessionStorage.UserName;
        UpdateStatus_Approve_Review(rev_id, cus_id, status_approve, user);
    });

    ////// REVIEW Project //////
    $("#tab_review_project").on('click', function () {
        getReviewProject("1");
    });
    $("#review_project_status").on('change', function () {
        var status = $("#review_project_status").val();
        getReviewProject(status);
    }); //get review project by status

    $("#review-project-detail").on('click', '.wait_project_review', function () {
        var project_id = $(this).attr('data-project');
        var rev_id = $(this).attr('data-review');
        $("#project_id").val(project_id);
        $("#rev_id").val(rev_id);
        $("#alert_project_review").modal('show');
        $("#alert_project_review .remark").css('display', 'none');
    });
    $("#alert_project_review").on('change', '#approve_project_review', function () {
        var status = $(this).val();
        //if (status == "P") {
        //    $("#alert_project_review .remark").css('display', 'block');
        //} else {
        //    $("#alert_project_review .remark").css('display', 'none');
        //}
    });
    $("#alert_project_review").on('click', '#comfirm_save', function () {
        $("#alert_project_review").modal('hide');
        var project_id = $("#project_id").val();
        var status_approve = $("#approve_project_review").val();
        var remark = $("#remark").val();
        var user = sessionStorage.UserName;
        var rev_id = $("#rev_id").val();
        UpdateStatus_Approve_Review_Project(rev_id,project_id, status_approve, user);
    });

}); //documrnt ready

function getPortfolio(status_approve) {

    var dataTable = $('#portfolio-detail').dataTable({
        "sPaginationType": "full_numbers",
        "retrieve": true,
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "fixedColumns": {
            rightColumns: 1
        },
        "columnDefs": [
           { className: "text-center", "targets": [0] },
        ],
        "order": [[1, 'asc']],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
    });
    $.ajax({
        url: baseUrl + '/ApproveProject/GetDataPortfolio',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            status_approve: status_approve
        },
        success: function (data) {
            console.log(data.items);
            dataTable.fnClearTable();
            $.each(data.items, function (index, value) {
                var status = value.status_approve;
                var preview = "<button class='btn btn-secondary btn-sm preview' data-project='" + value.Project_ID + "'>Preview</button>"
                if (value.status_approve == "รอการตรวจสอบ") {
                    status = "<p class='wait_project wait' data-project = '" + value.Project_ID + "'>รอการตรวจสอบ</p>";
                } else if (value.status_approve == "อนุมัติ") {
                    status = "<p class='approve'>อนุมัติ</p>";
                } else {
                    status = "<p class='no_approve'>ไม่อนุมัติ</p>";
                }

                dataTable.fnAddData([
                       '',
                       value.Name,
                       value.tier_code,
                       value.Project_Name,
                       value.Style_Name,
                       value.Type_Name,
                       value.cteate_date,
                       status,
                       preview
                ]);
            }); //loop
        } //success
    }); //ajax
} // get portfolio

function UpdateStatus_Approve(project_id, status_approve, remark, user) {
    $.ajax({
        url: baseUrl + '/ApproveProject/ApproveProject',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            project_id:project_id,
            status_approve: status_approve,
            remark: remark,
            Approve_By: user
        },
        success: function (data) {
            console.log(data.items);
            if (data.items.status == "TRUE") {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ดำเนินการอนุมัติเรียบร้อย",
                    size: 'small',
                    callback: function () {
                    }
                });
            } else {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ไม่สามารถบันทึกรายการได้",
                    size: 'small',
                    callback: function () {
                    }
                });
            }
            getPortfolio("1");
            $("#status_approve").val("1");
        } //success
    }); //ajax
} // upd status approve

function UpdateStatus_Approve_Review_Project(rev_id,project_id, status_approve, user) {
    $.ajax({
        url: baseUrl + '/ApproveProject/ApproveReviewProject',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            review_id:rev_id,
            project_id: project_id,
            status_approve: status_approve,
            Approve_By: user
        },
        success: function (data) {
            console.log(data.items);
            if (data.responseCode == "200") {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ดำเนินการอนุมัติเรียบร้อย",
                    size: 'small',
                    callback: function () {
                    }
                });
            } else {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ไม่สามารถบันทึกรายการได้",
                    size: 'small',
                    callback: function () {
                    }
                });
            }
            getReviewProject("1");
            $("#review_project_status").val("1");
        } //success
    }); //ajax
} // upd status approve review project

function getReviewCus(status_approve) {

    var dataTable = $('#review-cus-detail').dataTable({
        "sPaginationType": "full_numbers",
        "retrieve": true,
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "fixedColumns": {
            rightColumns: 1
        },
        "columnDefs": [
           { className: "text-center", "targets": [0,2] },
        ],
        "order": [[1, 'asc']],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
    });
    $.ajax({
        url: baseUrl + '/ApproveProject/GetDataReviewDesigner',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            status_approve: status_approve
        },
        success: function (data) {
            console.log(data.items);
            dataTable.fnClearTable();
            $.each(data.items, function (index, value) {
                var status = value.status_approve;
                if (value.status_approve == "รอการตรวจสอบ") {
                    status = "<p class='wait_cus_review wait' data-review = '" + value.Review_ID + "' data-cus = '"+ value.Cust_ID +"'>รอการตรวจสอบ</p>";
                } else if (value.status_approve == "อนุมัติ") {
                    status = "<p class='approve'>อนุมัติ</p>";
                } else {
                    status = "<p class='no_approve'>ไม่อนุมัติ</p>";
                }

                dataTable.fnAddData([
                       '',
                       value.Name,
                       value.Review_Rating,
                       value.Review_Detail,
                       value.Review_Name,
                       value.cteate_date,
                       status
                ]);
            }); //loop
        } //success
    }); //ajax
} // get review cus

function getReviewProject(status_approve) {

    var dataTable = $('#review-project-detail').dataTable({
        "sPaginationType": "full_numbers",
        "retrieve": true,
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "fixedColumns": {
            rightColumns: 1
        },
        "columnDefs": [
           { className: "text-center", "targets": [0,3] },
        ],
        "order": [[1, 'asc']],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
    });
    $.ajax({
        url: baseUrl + '/ApproveProject/GetDataReviewProject',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            status_approve: status_approve
        },
        success: function (data) {
            console.log(data.items);
            dataTable.fnClearTable();
            $.each(data.items, function (index, value) {
                var status = value.status_approve;
                if (value.status_approve == "รอการตรวจสอบ") {
                    status = "<p class='wait_project_review wait' data-project = '" + value.Project_ID + "' data-review ='" + value.Review_ID + "'>รอการตรวจสอบ</p>";
                } else if (value.status_approve == "อนุมัติ") {
                    status = "<p class='approve'>อนุมัติ</p>";
                } else {
                    status = "<p class='no_approve'>ไม่อนุมัติ</p>";
                }

                dataTable.fnAddData([
                       '',
                       value.Project_Name,
                       value.Type_Name,
                       value.Review_Rating,
                       value.Review_Detail,
                       value.Review_Name,
                       value.cteate_date,
                       status
                ]);
            }); //loop
        } //success
    }); //ajax
} // get review project

function UpdateStatus_Approve_Review(rev_id, cus_id, status_approve,username){
    $.ajax({
        url: baseUrl + '/ApproveProject/ApproveReviewDesigner',
        headers: {
            'Authorization': "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
        },
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        withCredentials: true,
        data: {
            Review_ID: rev_id,
            Cust_ID: cus_id,
            Status: status_approve,
            Approve_By: username
        },
        success: function (data) {
            console.log(data);
            if (data.responseCode == "200") {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ดำเนินการอนุมัติเรียบร้อย",
                    size: 'small',
                    callback: function () {
                    }
                });
            } else {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "ไม่สามารถบันทึกรายการได้",
                    size: 'small',
                    callback: function () {
                    }
                });
            }
            getReviewCus("1");
        } //success
    }); //ajax
} // update status approve review designer