﻿$('#change-pw-form').validate({
    lang: 'th',
    rules: {
        user: {
            required: true,
        },
        oldPasswd: {
            required: true,
        },
        newPasswd: {
            minlength: 6,
            required: true,
        },
        pw_confirmPass: {
            equalTo: "#pw-password",
            required: true,
        }
    },
})

$('#btn-save-changePW').click(function () {
    if ($('#change-pw-form').valid()) {
        $.ajax({
            url: baseUrl + "Home/changePasswd",//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            withCredentials: true, 
            data: {
                fromType: "B",
                userId: $('#pw-userId').val(),
                username: $('#pw-username').val(),
                oldPassword: $('#old-password').val(),
                newPassword: $('#pw-password').val(),
                createBy: sessionStorage.UserName
            },
            success: function (data) {
                if (data.success) {
                    AlertNoti(data.message, 'success');
                    $('#old-password').val("")
                    $('#pw-password').val("");
                    $('#pw-confirmPass').val("");
                    $('#Change-pw-Modal').modal('hide')
                } else {
                    AlertNoti(data.description, 'danger');
                    return false;
                }
            }
        });
    }
})