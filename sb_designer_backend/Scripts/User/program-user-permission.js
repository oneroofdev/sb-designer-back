﻿$(window).ready(function () {
    var dataRoot = new Array();
    var children = new Array();
    dataRoot = [];
    children = [];

    $('.select2').select2();
    var UserProgramJStree = function (dataRoot) {
        $('#demo-jstree-1').jstree({
            'core': {
                'check_callback': true,
                'data': dataRoot
            },
            'plugins': ['checkbox'],
        });
        return false;
    };
    var ProgramPerUser = function () {
        $.ajax({
            url: sessionStorage.apiUrl + "program/list_program_usr",//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            withCredentials: true,
            headers: {
                'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            },
            data: {
                fromType: "B",
                userId: sessionStorage.userIdForProgram,
                username: sessionStorage.usernameForProgram
            },
            success: function (data) {
                var idMain = 0;
                if (data != null) {
                    //console.log(data)
                    dataRoot = [];
                    children = [];
                    $.each(data.items, function (i, value) {
                        children = [];
                        $.each(value.subMenu, function (i, e) {
                            //if (e.PROGRAM_NAME != value.mainMenu) {
                                children.push({
                                    "id": e.PROGRAM_ID,
                                    "text": e.PROGRAM_NAME,
                                    "state": { "opened": true },
                                    "state": { "selected": e.ACTIVE == 'Y' ? true : false },
                                    'icon': 'fa fa-file-text-o'
                                })
                            //} else {
                            //    idMain = e.PROGRAM_ID
                            //}
                            
                        });
                        dataRoot.push({
                            //"id": idMain,
                            "text": value.mainMenu,
                            "state": { "opened": false },
                            "state": { "selected": false },
                            //"state": { "selected": value.mainActive == 'Y' ? true : false },
                            "children": children,
                        })
                    });
                    UserProgramJStree(dataRoot);
                } else {
                    alert("Error =>" + data.description);
                    return false;
                }
            }
        });
    }
    ProgramPerUser();
   
    $('#btn-save-prog-perm').click(function () {
        //var arrProgramId = new Array();
        //arrProgramId = [];
        var arrProgramId = $("#demo-jstree-1").jstree('get_checked');
        console.log(arrProgramId);
        sessionStorage.UserName
        sessionStorage.userIdForProgram
        sessionStorage.usernameForProgram

        $.ajax({
            url: sessionStorage.apiUrl + "program/update_program_user",//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            withCredentials: true,
            headers: {
                'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            },
            data: {
                fromType: "B",
                userId: sessionStorage.userIdForProgram,
                userName: sessionStorage.usernameForProgram,
                actionBy: sessionStorage.UserName,
                listProgramId: arrProgramId
            },
            success: function (data) {
                var idMain = 0;
                if (data != null && data.success == true) {
                    alert(data.message);
                    ProgramPerUser();
                } else {
                    alert("Error =>" + data.description);
                    return false;
                }
            }
        });

    })


    $('#saveAddProgram').click(function () {
        var da1 = $('#select-permission').val();
        console.log(da1);
    })
});