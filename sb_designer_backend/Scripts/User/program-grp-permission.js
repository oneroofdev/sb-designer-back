﻿$(window).ready(function () {
    var dataRoot = new Array();
    var children = new Array();
    dataRoot = [];
    children = [];
    var UserGroupProgramJStree = function (dataRoot) {
        console.log(dataRoot)
        $('#userGroup-jstree').jstree("refresh");
        $('#userGroup-jstree').jstree({
            'core': {
                'check_callback': true,
                'data': dataRoot
            },
            'plugins': ['checkbox'],
        });
        return false;
    };
    var ProgramPerUserGrp = function (groupId) {
        $.ajax({
            url: sessionStorage.apiUrl + "program/list_program_usr_grp",//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            withCredentials: true,
            headers: {
                'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            },
            data: {
                fromType: "B",
                groupId: groupId,
            },
            beforeSend: function () {
                $('#userGroup-jstree').jstree("destroy").empty();
                
            },
            success: function (data) {
                var idMain = 0;
                if (data != null) {
                    //console.log(data)
                    dataRoot = [];
                    children = [];
                    $.each(data.items, function (i, value) {
                        children = [];
                        $.each(value.subMenu, function (i, e) {
                            //if (e.PROGRAM_NAME != value.mainMenu) {
                            children.push({
                                "id": e.PROGRAM_ID,
                                "text": e.PROGRAM_NAME,
                                "state": { "opened": true },
                                "state": { "selected": e.ACTIVE == 'Y' ? true : false },
                                'icon': 'fa fa-file-text-o'
                            })
                            //} else {
                            //    idMain = e.PROGRAM_ID
                            //}

                        });
                        dataRoot.push({
                            //"id": idMain,
                            "text": value.mainMenu,
                            "state": { "opened": false },
                            "state": { "selected": false },
                            //"state": { "selected": value.mainActive == 'Y' ? true : false },
                            "children": children,
                        })
                    });

                    UserGroupProgramJStree(dataRoot);
                } else {
                    alert("Error =>" + data.description);
                    return false;
                }
            }
        });
    }
    ProgramPerUserGrp($('#group-id').val());
    
    $('#group-id').change(function () {
        var groupTd = $(this).val();
        //alert(groupTd)
        ProgramPerUserGrp(groupTd);
    });


    $('#btn-save-prog-grp').click(function () {
        //var arrProgramId = new Array();
        //arrProgramId = [];
        var arrProgramId = $("#userGroup-jstree").jstree('get_checked');
        console.log(arrProgramId);
        sessionStorage.UserName
        sessionStorage.userIdForProgram
        sessionStorage.usernameForProgram

        $.ajax({
            url: sessionStorage.apiUrl + "program/update_program_user_grp",//
            dataType: 'json',
            type: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            withCredentials: true,
            headers: {
                'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
                , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
            },
            data: {
                fromType: "B",
                groupId: $('#group-id').val(),
                actionBy: sessionStorage.UserName,
                listProgramId: arrProgramId
            },
            success: function (data) {
                var idMain = 0;
                if (data != null && data.success == true) {
                    alert(data.message);
                    ProgramPerUserGrp($('#group-id').val());
                } else {
                    alert("Error =>" + data.description);
                    return false;
                }
            }
        });

    })


    $('#saveAddProgram').click(function () {
        var da1 = $('#select-permission').val();
        console.log(da1);
    })
})

