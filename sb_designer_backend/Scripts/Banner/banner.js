﻿$(document).ready(function () {
    $('.select2').select2()
    getlistBanner()
    //listAdsToSelect($('#adsId'))
    //listAdsToSelect($('#adsId2'))
    clearInput = function () {
        $('#slideCode').val("0")
        $('#descriptionTH').val("")
        $('#descriptionEN').val("")
        $('#displaySEQ').val("")
        $('#linkUrl').val("")
        $('#strStartDate').val("")
        $('#strEndDate').val("")
        $('#linkUrlMember').val("")
        $('#add-banner-form input[type=radio][name=active][value="Y"]').iCheck('check');
        $('#pathImage').val("")
        $('.pathImage').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageEn').val("")
        $('.pathImageEn').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMobile').val("")
        $('.pathImageMobile').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMobileEn').val("")
        $('.pathImageMobileEn').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMember').val("")
        $('.pathImageMember').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMemberEn').val("")
        $('.pathImageMemberEn').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMemberMobile').val("")
        $('.pathImageMemberMobile').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#pathImageMemberMobileEn').val("")
        $('.pathImageMemberMobileEn').attr('src', baseUrl + "/Content/img/ex2.jpg")
        $('#adsId').val($('#adsId option:first').val()).trigger('change');
        $('#adsId2').val($('#adsId2 option:first').val()).trigger('change');
    }
    $('#add-banner-form').validate({
        lang: 'th',
        roles: {
            linkUrlMember: { required: false },
            linkUrl: { required : false}
        }
    });
    $('#btn-preview').click(function () {
        var b1 = $('.pathImage').attr('src');
        var b2 = $('.pathImageEn').attr('src');
        var b3 = $('.pathImageMobile').attr('src');
        var b4 = $('.pathImageMobileEn').attr('src');
        var b5 = $('.pathImageMember').attr('src');
        var b6 = $('.pathImageMemberEn').attr('src');
        var b7 = $('.pathImageMemberMobile').attr('src');
        var b8 = $('.pathImageMemberMobileEn').attr('src');
        window.open(baseUrl + "banner/preview?bannerDTH=" + b1 + "&bannerDEN=" + b2 + "&bannerMTH=" + b5 + "&bannerMEN=" + b6 + "&bannerDMTH=" + b3 + "&bannerDMEN=" + b4 + "&bannerMMTH=" + b7 + "&bannerMMTH=" + b8, '_blank');
        
    })
    
    $('#pathImage_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImage'), $('#pathImage'))
        //console.log(data.get('file'))

    })
    $('#pathImageEn_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageEn'), $('#pathImageEn'))
        //console.log(data.get('file'))

    })
    $('#pathImageMobile_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageMobile'), $('#pathImageMobile'))
        //console.log(data.get('file'))

    })
    $('#pathImageMobileEn_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageMobileEn'), $('#pathImageMobileEn'))
        //console.log(data.get('file'))

    })
    $('#pathImageMember_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageMember'), $('#pathImageMember'))
        //console.log(data.get('file'))

    })
    $('#pathImageMemberEn_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageMemberEn'), $('#pathImageMemberEn'))
        //console.log(data.get('file'))

    })
    $('#pathImageMemberMobile_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        var file_name = file.name;
        uploadImage(data, $('.pathImageMemberMobile'), $('#pathImageMemberMobile'))
        //console.log(data.get('file'))

    })
    $('#pathImageMemberMobileEn_').change(function () {
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        data.append('path', path);
        //var file_name = file.name;
        uploadImage(data, $('.pathImageMemberMobileEn'), $('#pathImageMemberMobileEn'))
        //console.log(data.get('file'))

    })
    $('#save_banner').click(function () {
        var data = {};
        var id = $('#slideCode').val()
        if ($('#add-banner-form').valid()) {
            data = {
                fromType: "B",
                slideCode: id,
                descriptionTH: $('#descriptionTH').val(),
                descriptionEN: $('#descriptionEN').val(),
                displaySEQ: $('#displaySEQ').val(),
                strStartDate: $('#strStartDate').val(),
                strEndDate: $('#strEndDate').val(),
                linkUrl: $('#linkUrl').val(),
                pathImage: $('#pathImage').val(),
                pathImageEn: $('#pathImageEn').val(),
                pathImageMobile: $('#pathImageMobile').val(),
                pathImageMobileEn: $('#pathImageMobileEn').val(),
                linkUrlMember: $('#linkUrlMember').val(),
                pathImageMember: $('#pathImageMember').val(),
                pathImageMemberEn: $('#pathImageMemberEn').val(),
                pathImageMemberMobile: $('#pathImageMemberMobile').val(),
                pathImageMemberMobileEn: $('#pathImageMemberMobileEn').val(),
                active: $('input[name=active]:checked', '#add-banner-form').val(),
                createBy: sessionStorage.UserName
            };
            if (id != 0) {
                // edit
                updateBanner(data)
            } else {

                insertBanner(data)
            }
        }
    })
    $('#banner-table').on('click', '.brn-edit-see', function () {
        //var data = $(this).cells["0"].innerText();
        //console.log(data);
        var bannerId = $(this).data("bannerid")
        getBanner(bannerId)
    })
    $('#adsId').change(function () {
        $('#linkUrl').val($(this).val())
    })
    $('#adsId2').change(function () {
        $('#linkUrlMember').val($(this).val())
    })
})
var path = "Banner"
var uploadImage = function (data, elementImg, elementText) {
    $.ajax({
        url: baseUrl + "upload/UploadListImage",
        processData: false,
        contentType: false,
        headers: {
            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
            'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        },
        data: data,
        type: 'POST'
    }).done(function (result) {
        //alert(result);
        elementText.val(result)
        elementImg.attr('src', baseUrl + "Content/img/" + path + "/" + result)
        //listImage = result
        //console.log(listImage);
    }).fail(function (a, b, c) {
        console.log(a, b, c);
    });
}
var renderTable = function () {
    $('#banner-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });
};
var getlistBanner = function () {
    var row_ = "";
    DestroyDataTable($('#banner-table'))
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: sessionStorage.apiUrl + "/banner/list_banner",
        //url: sessionStorage.apiUrl + "banner/list_banner",//
        //dataType: 'json',
        //type: "POST",
        //async: true,
        cache: false,
        crossDomain: true,
        withCredentials: true,
        headers: {
            'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        },
        data: {
            fromType: "B",
            language: "TH",
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI");
        },
        success: function (data) {
            if (data.success) {
                //console.log(data.items)
                $('#banner-table tbody tr').remove();
                $.each(data.items, function (i, e) {
                    row_ += '<tr style="text-align:left;">' +
                                    '<td class="text-center">' + (i + 1) + '</td>' +
                                    '<td><img src="' + baseUrl + "Content/img/" + path + "/" + e.pathImage + '" style="width:200px;" /></td>' +
                                    '<td><img src="' + baseUrl + "Content/img/" + path + "/" + e.pathImageEn + '" style="width:200px;" /></td>' +
                                    '<td><img src="' + baseUrl + "Content/img/" + path + "/" + e.pathImageMobile + '" style="width:200px;" /></td>' +
                                    '<td><img src="' + baseUrl + "Content/img/" + path + "/" + e.pathImageMobileEn + '" style="width:200px;" /></td>' +
                                    '<td>' + e.descriptionTH + '</td>' +
                                    '<td>' + (e.startDate == null ? "" : ConvertDate2(e.startDate)) + ' - ' + (e.endDate == null ? "" : ConvertDate2(e.endDate)) + '</td>' +
                                    //'<td>' + (e.endDate == null ? "" : ConvertDate2(e.endDate)) + '</td>' +
                                    '<td>' + (e.active == "Y" ? "ใช้งาน" : "ยกเลิก") + '</td>' +
                                    '<td class="text-center">' +
                                        '<button class="btn btn-warning btn-xs brn-edit-see" data-bannerid="' + e.slideCode + '"  data-toggle="modal" data-target="#addBannerModal"><i class="fa fa fa-pencil"></i>&nbsp; แก้ไข</button>' +
                                    '</td>' +
                                  '</tr>';
                });
                $('#banner-table tbody').html(row_)
                renderTable()
            } else {
                showAlert('danger', data.description);
            }
            return false;
        }
    });
}
var getBanner = function (bannerId) {
    var row_ = "";
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: sessionStorage.apiUrl + "/banner/get_banner",
        cache: false,
        crossDomain: true,
        withCredentials: true,
        headers: {
            'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        },

        //url: sessionStorage.apiUrl + "banner/get_banner",//
        //dataType: 'json',
        //type: "POST",
        //async: true,
        //cache: false,
        //crossDomain: true,
        //withCredentials: true,
        //headers: {
        //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
        //    , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        //},
        data: {
            fromType: "B",
            slideCode: bannerId,
        },
        success: function (data) {
            if (data.success) {
                //console.log(data.items)
                $('#slideCode').val(data.items.slideCode)
                $('#descriptionTH').val(data.items.descriptionTH)
                $('#descriptionEN').val(data.items.descriptionEN)
                $('#displaySEQ').val(data.items.displaySEQ)
                $('#linkUrl').val(data.items.linkUrl)
                $('#strStartDate').val(data.items.startDate == null ? "" : ConvertDate2(data.items.startDate))
                $('#strEndDate').val(data.items.endDate == null ? "" : ConvertDate2(data.items.endDate))
                $('#linkUrlMember').val(data.items.linkUrlMember)
                $('#add-banner-form input[type=radio][name=active][value="' + data.items.active + '"]').iCheck('check');
                $('#pathImage').val(data.items.pathImage)
                $('.pathImage').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImage)
                $('#pathImageEn').val(data.items.pathImageEn)
                $('.pathImageEn').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageEn)
                $('#pathImageMobile').val(data.items.pathImageMobile)
                $('.pathImageMobile').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMobile)
                $('#pathImageMobileEn').val(data.items.pathImageMobileEn)
                $('.pathImageMobileEn').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMobileEn)
                $('#pathImageMember').val(data.items.pathImageMember)
                $('.pathImageMember').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMember)
                $('#pathImageMemberEn').val(data.items.pathImageMemberEn)
                $('.pathImageMemberEn').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMemberEn)
                $('#pathImageMemberMobile').val(data.items.pathImageMemberMobile)
                $('.pathImageMemberMobile').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMemberMobile)
                $('#pathImageMemberMobileEn').val(data.items.pathImageMemberMobileEn)
                $('.pathImageMemberMobileEn').attr('src', baseUrl + "Content/img/" + path + "/" + data.items.pathImageMemberMobileEn)
                //datatableLoad()
            } else {
                showAlert('danger', data.description);
            }
            return false;
        }
    });
}
var insertBanner = function (data) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: sessionStorage.apiUrl + "/banner/create_banner",

        //url: sessionStorage.apiUrl + "banner/create_banner",//
        //dataType: 'json',
        //type: "POST",
        //async: true,
        //cache: false,
        //crossDomain: true,
        //withCredentials: true,
        //headers: {
        //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
        //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        //},
        data: data,
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                //showAlert('success', data.message);
                $('#addBannerModal').modal('hide')
                getlistBanner()
                clearInput();
            } else {
                //showAlert('danger', data.description);
                return false;
            }
        },
        complete: function () {
            closeLoading()
        }
    });
}
var updateBanner = function (data) {
    //console.log(data)
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: sessionStorage.apiUrl + "/banner/update_banner",

        //url: sessionStorage.apiUrl + "banner/update_banner",//
        //dataType: 'json',
        //type: "POST",
        //async: false,
        //cache: false,
        //crossDomain: true,
        //withCredentials: true,
        //headers: {
        //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
        //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        //},
        data: data,
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                //showAlert('success',data.message);
                $('#addBannerModal').modal('hide')
                getlistBanner()
                clearInput();
            } else {
                //showAlert('danger', data.description);
                return false;
            }
        },
        complete: function () {
            closeLoading()
        }
    });
}
var listAdsToSelect = function (element) {
    var row_option;
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: sessionStorage.apiUrl + "/ads/list_ads",
        //url: sessionStorage.apiUrl + "ads/list_ads",//
        //dataType: 'json',
        //type: "GET",
        //async: true,
        //cache: false,
        //crossDomain: true,
        //withCredentials: true,
        //headers: {
        //    'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
        //    , 'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        //},
        data: {
            fromType: "B",
            language: "TH",
            //active: 'Y'
        },
        success: function (data) {
            if (data.success) {
                element.find('option').remove();
                row_option += '<option  value="" >None</option>';
                $.each(data.items, function (i, e) {
                    row_option += '<option  value="promote?ads=' + e.adsId + '&channel=10" >' + e.adsName + '</option>';
                    //$('#shop-category').append('<option  value="' + e.shopCatId + '" >' + e.shopCatDesc + '</option>');
                })
                element.html(row_option)
                element.find("option:first").trigger('change');
            } else {
                showAlert('danger', data.description);
            }
            return false;
        }
    });
}