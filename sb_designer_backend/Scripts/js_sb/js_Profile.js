﻿$(document).ready(function () {
    lenderTable();
    if (sessionCust_ID != "") {
        getDataProfile(sessionCust_ID);
    }
    $("#ddl_search").select2({
        placeholder: "กรุณาเลือกสมาชิก โดยค้นหาจาก ชื่อ-สกุล, เลขบัตรประชาชน, เบอร์โทรศัพท์ หรือ E-mail",
        minimumInputLength: 3,
        allowClear: true,
        ajax:{
            url: 'Profile/FilterMember',
            type: "POST",
            async: false,
            cache: false,
            data: function (params) { // page is the one-based page number tracked by Select2
                return {
                    STR_TEXT: params.term, //search term
                    language: "TH",
                    maxResults: 99,
                    fromType: "B"
                };
            },
            processResults: function (data) {
                var a = JSON.parse(data);
                if (a.success) {
                    var myResults = [];
                    $.each(a.items, function (index, item) {
                        myResults.push({
                            'id': item.ID,
                            'text': item.TEXT
                        });
                    });
                    return {
                        results: myResults
                    };
                }
            }
        },
        escapeMarkup: function (markup) { return markup; } // let our custom formatter work
    });
    $("#ddl_search").on("change", function () {
        sessionCust_ID = $("#ddl_search").val();
        SetValueCusID(sessionCust_ID);
        $('#txt_crmid').val(sessionCust_ID);
        getDataProfile(sessionCust_ID);
        //getPoint(sessionCust_ID);
    });
});
function getDataProfile(sessionCust_ID) {
    showLoading();
    $.ajax({
        url: 'Profile/GetMemberByID',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Cust_ID: sessionCust_ID,
            language: "TH",
            maxResults: 99,
            fromType: "B"
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                $.each(a.items, function (i, e) {
                    dataaddr = e.House_No + " " + e.Moo + " " + e.Village + " " + e.Building + " " +
                        e.Room + " " + e.Road + " " + e.Soi + " " + e.TAM_Name + " " + e.AMP_Name + " " + e.PRV_Name + " " + e.Post_Code;
                    var daterg = new Date();
                    if (e.Verify_Date != null) {
                        daterg = new Date(e.Verify_Date.substring(0, 11));
                    }
                    $('#MID').val(e.Cust_ID);
                    $('#txt_crmid').val(sessionCust_ID);
                    $('#txt_membercard').val(e.Cust_Code);
                    $('#txt_membertype').val(e.Member_Type);
                    $('#txt_nameth').val(e.Name + " " + e.Surname);
                    $('#txt_nameen').val(e.Name_Eng == null ? "" : e.Surname_Eng + " " + e.Surname_Eng);
                    $('#txt_tel').val(e.Mobile_Phone == null ? "" : e.Mobile_Phone);
                    $('#txt_regis').val((e.Verify_Date));
                    $('#txt_passport').val(e.Passport);
                    $('#txt_idcard').val(e.Citizen_ID);
                    $('#txt_address').val(dataaddr);
                    $('#txt_mail').val(e.Email);
                });
                closeLoading();
                getExpirePoint(sessionCust_ID);
            } else {
                closeLoading();
            }
        }
    });
}
function getExpirePoint(Cust_ID) {
    showLoading();
    $.ajax({
        url: 'Profile/GetExpirePoint',
        type: "POST",
        async: false,
        cache: false,
        data: {
            Cust_ID: sessionCust_ID,
            language: "TH",
            maxResults: 99,
            fromType: "B"
        },
        success: function (data) {
            var a = JSON.parse(data);
            if (a.success) {
                var data1 = a.items;
                var p0 = 0;
                var p1 = 0;
                var p2 = 0;
                var p3 = 0;
                var p4 = 0;
                var p5 = 0;
                var today = new Date();
                var dYear = today.getFullYear();
                for (var i = 0; i < data1.length; i++) {
                    var dataYear = parseInt(data1[i]["YEAR_EXPIRE"]);
                    switch (dataYear) {
                        case parseInt(dYear):
                            p0 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                        case parseInt(dYear) +1:
                            p1 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                        case parseInt(dYear) +2:
                            p2 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                        case parseInt(dYear) +3:
                            p3 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                        case parseInt(dYear) +4:
                            p4 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                        case parseInt(dYear) +5:
                            p5 = commaSeparateNumber(data1[i]["POINT"]);
                            break;
                    }
                }
                var datatb = '<thead>' +
                                '<tr>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="15%;">คะแนนสะสม (ปัจจุบัน)</th>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + (today.getFullYear() + 1) + '</th>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + (today.getFullYear() + 2) + '</th>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + (today.getFullYear() + 3) + '</th>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + (today.getFullYear() + 4) + '</th>' +
                                    '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + (today.getFullYear() + 5) + '</th>' +
                                '</tr>' +
                            '</thead>' +
                            '<tbody>' +
                                '<tr style="text-align:center;">' +
                                    '<td><span style="color:red;" id="this_point">' + p0 + '</span></td>' +
                                    '<td>' + p1 + '</td>' +
                                    '<td>' + p2 + '</td>' +
                                    '<td>' + p3 + '</td>' +
                                    '<td>' + p4 + '</td>' +
                                    '<td>' + p5 + '</td>' +
                                '</tr>' +
                             '</tbody>';
                $('#tb_expirepoint').html(datatb);
            }
            closeLoading();
        }
    });
}
function lenderTable() {
    var y1 = new Date().getFullYear() + 1;
    var y2 = new Date().getFullYear() + 2;
    var y3 = new Date().getFullYear() + 3;
    var y4 = new Date().getFullYear() + 4;
    var y5 = new Date().getFullYear() + 5;
    var datatb = '<thead>' +
                    '<tr>' +
                        '<th style="text-align:center; padding: 7px 0;" width="15%;">คะแนนสะสม (ปัจจุบัน)</th>' +
                        '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + y1 + '</th>' +
                        '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + y2 + '</th>' +
                        '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + y3 + '</th>' +
                        '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + y4 + '</th>' +
                        '<th style="text-align:center; padding: 7px 0;" width="17%;">คะแนนหมดอายุสิ้นปี ' + y5 + '</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody></tbody>';
    $('#tb_expirepoint').html(datatb);
}
function getPoint(Cust_ID) {
    var data = new FormData();
    //console.log(e)
    data.append('Cust_ID', Cust_ID);
    $.ajax({
        url: "MemberProfile/GetPoint_Balance",
        processData: false,
        contentType: false,
        headers: {
            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac',
            'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        },
        data: data,
        type: 'POST'
    }).done(function (result) {
        img_PO = result;
        //alert(img_bookbank);
    }).fail(function (a, b, c) {
        img_PO = "";
    });
}
