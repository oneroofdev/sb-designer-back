﻿$(document).ready(function () {

    $('.ddl_select').select2({
        width: '100%'
    });
    $('.select').select2();
    $('.date').datepicker({
        autoclose: true
    });
    $('.tableSearch').dataTable({
        searching: true,
        paging: true,
        info: false
    });
    $(".textauto").attr("autocomplete", "off");

    $('.date').attr('data-date-format', 'dd/mm/yyyy');
    var DestroyDataTable = function (element) {
        if ($.fn.DataTable.isDataTable(element)) {
            $(element).DataTable().destroy();
        }
    }
});
function ConvertDateTimeToFormat(date) {
    var d = date.split("T");
    var str = d[0].split("-");
    return str[2] + "/" + str[1] + "/" + str[0];
}
const formatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2
})
//set 2 digit
function addZeroBefore(n) {
    return (n < 10 ? '0' : '') + n;
}
// Check range qty
function RangeNumber(value1, value2, min, max, name) {
    alert(value1 + "::" + value2);
    var value = parseInt(value1) + parseInt(value2);
    if (value < min || value > max) {
        $(name).val('');
        //alert("จำนวนสินค้าในสต็อกมีน้อยกว่าจำนวนที่ต้องการ กรุณาตรวจสอบจำนวนอีกครั้ง");
    }
}
function commaSeparateNumber(val) {
    var output = numeral(val).format('0,0');
    return output;
}
function ConvertDate2(date) {
    var str = date.split("T");
    str = str[0].split('-');
    return str[2] + "/" + str[1] + "/" + str[0];
}
function validate_from(name, from, response) {
    if ($.trim(name.val()) == "") {
        from.html(response);
        // frm_validate_sidl += "<li>" + response + "</li>";
        return false;
    } else {
        from.html('');
        return true;
    }
    return Boolean;
}

function showAlert(titleHeader, msgDetail) {
    bootbox.alert({
        title: titleHeader,
        message: msgDetail,
        size: 'small',
        callback: function () {
            //$('.nav-tabs > .active').prev('li').find('a').trigger('click');
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 500);
        }
    });
}


function showAlertSuccess(titleHeader, msgDetail) {
    bootbox.alert({
        title: titleHeader,
        message: msgDetail,
        size: 'small',
        callback: function () {
            location.reload(true);
        }
    });
}
function addCommas(str) {
    var output = numeral(str).format('0,0');
    return output;
}
function addCommas1digit(str) {
    var output = numeral(str).format('0,0.0');
    return output;
}

function addCommas2digit(str) {
    var output = numeral(str).format('0,0.00');
    return output;
}
function ConvertYearMonthToString(date) {
    var obj = {
        "01": "Jan",
        "02": "Feb",
        "03": "Mar",
        "04": "Apr",
        "05": "May",
        "06": "Jun",
        "07": "Jul",
        "08": "Aug",
        "09": "Sep",
        "10": "Oct",
        "11": "Nov",
        "12": "Dec",
        "13": "13 /",
    };
    return obj[date.substring(4)] + " " + date.substring(0, 4);
}

function ConvertDateToString(date) {
    var obj = {
        "01": "มกราคม",
        "02": "กุมภาพันธ์",
        "03": "มีนาคม",
        "04": "เมษายน",
        "05": "พฤษภาคม",
        "06": "มิถุนายน",
        "07": "กรกฎาคม",
        "08": "สิงหาคม",
        "09": "กันยายน",
        "10": "ตุลาคม",
        "11": "พฤศจิกายน",
        "12": "ธันวาคม",
        "13": "13 /",
    };
    return obj[date.substring(4)] + " " + date.substring(0, 4);
}
function ConvertDateToString_num(date) {
    var obj = {
        "01": "01",
        "02": "02",
        "03": "03",
        "04": "04",
        "05": "05",
        "06": "06",
        "07": "07",
        "08": "08",
        "09": "09",
        "10": "10",
        "11": "11",
        "12": "12",
        "13": "13",
        "14": "14",
    };
    return obj[date.substring(4)] + " / " + date.substring(0, 4);
}
function setFormatDate(dateIn) {
    var str_date = "";
    if (dateIn === null) {
        return str_date;
    } else {
        var date = new Date(parseInt(dateIn.replace("/Date(", "").replace(")/", "")));
        return addZeroBefore(date.getDate()) + '/' + addZeroBefore(parseInt(date.getMonth()) + 1) + '/' + date.getFullYear();
    }
}
function setFormatDateTime(dateIn) {
    var str_date = "";
    if (dateIn === null) {
        return str_date;
    } else {
        var date = new Date(parseInt(dateIn.replace("/Date(", "").replace(")/", "")));
        return addZeroBefore(date.getDate()) + '/' + addZeroBefore(parseInt(date.getMonth()) + 1) + '/' + date.getFullYear() + ' ' + addZeroBefore(date.getHours()) + ':' + addZeroBefore(date.getMinutes()) + ':' + addZeroBefore(date.getSeconds());
    }
}
//2018-12-31 to 31/12/2561
function ConvertDateToFormat(date) {
    if (date != null && date != "") {
        var str = date.split("-");
        var yy = parseInt(str[0]);// + parseInt(language == "TH" ? 543 : 0)
        return str[2] + "/" + str[1] + "/" + yy;
    } else {
        return "";
    }
}
/*New Tab */
function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}

var ConvertDate = function (_date) {
    if (_date != null) {
        var date = _date.substr(6);
        var currentTime = new Date(parseInt(date));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var returnDate = day + "/" + month + "/" + year;
        return returnDate;
    }
    return null;
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
var DestroyDataTable = function (element) {
    //alert($.fn.DataTable.isDataTable(element))
    if ($.fn.DataTable.isDataTable(element)) {
        $(element).DataTable().destroy();
    }
}
function SetValueCusID(myval) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: "Home/SetSessionCus_ID",
        data: { sessionval: myval },
        success: function (data) {

        },
    });
}
function checkID(id) {
    if (id.length != 13) return false;
    for (i = 0, sum = 0; i < 12; i++)
        sum += parseFloat(id.charAt(i)) * (13 - i); if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
            return false; return true;
}
