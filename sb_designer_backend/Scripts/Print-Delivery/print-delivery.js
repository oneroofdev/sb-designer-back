﻿$(document).ready(function () {
    document.querySelectorAll('input[type=number]')
      .forEach(e => e.oninput = () => {
          // Always 2 digits
          if (e.value.length >= 2) e.value = e.value.slice(0, 2);
          // 0 on the left (doesn't work on FF)
          if (e.value.length === 1) e.value = '0' + e.value;
          // Avoiding letters on FF
          if (!e.value) e.value = '00';
      });
    var path = '/Content/Delivery/Export/';
    $('#detail-delivery-table tbody tr').remove();
    getlistDeliveryAll()
    $('#receive-form').validate();
    $('#money_receive-form').validate();
    //devilery_dataTable()
    $('#delivery-table').on('click', 'tr', function () {
        $("#delivery-table tr").css('background-color', "#fff");
        // $("#delivery-table tr").css('color', "#7a878e");
        $(this).css('background-color', "#EEF0EE");
        //$(this).css('color', "#f5f5f5");

        deliveryNo = $.trim($(this).children("td").eq(2).text());
        deliveryby = $.trim($(this).children("td").eq(0).text());
        tempDeliveryNo = deliveryNo;
        $('#delilveryNoChose').text(deliveryNo)
        $('#deliveryByTemp').val(deliveryby)
        $('#deliveryNoTemp').val(deliveryNo)
        getDeliveryRedeem(deliveryNo)
        //deliveryby = "พร้อมเพย์";
        if (deliveryby == "โอนเงินผ่านบัญชีธนาคาร" || deliveryby == "พร้อมเพย์") {
            $('#div_btn_order_report').addClass('hidden')
            $('#div_btn_label_report').addClass('hidden')
        } else {
            $('#div_btn_order_report').removeClass('hidden')
            $('#div_btn_label_report').removeClass('hidden')
        }
    });
    $('#delivery-table').on('click', '.btn-del-detail', function () {
        //var del = $(this).data("delivery")
        $('#delilveryNoChose').text($(this).data("delivery"))
        $('#deliveryByTemp').val($(this).data("deliveryby"))
        $('#deliveryNoTemp').val($(this).data("delivery"))
        $('#projectTemp').val("SB")
        //getDeliveryRedeem($(this).data("delivery"))
    })
    $('#detail-delivery-table').on('click', '.btn-receive', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var qty = $(this).data('qty')
        $('#redeemNo').val(redeemNo)
        $('#reward').val(rewardCode + ":" + rewardName)
        $('#rewardCode').val(rewardCode)
        $('#qty1').val(qty)
        $('#receiveDate').val("")
        $('#receiveDate').attr('readonly', false)
        $('#receiveBy').val("")
        $('#receiveBy').attr('readonly', false)
        $('#timeHH').val("")
        $('#timeMM').val("")
        $('#timeHH').attr('readonly', false)
        $('#timeMM').attr('readonly', false)
        $('#trackingNo').val("")
        $('#trackingNo').attr('readonly', false)

        $('.datetime1').removeClass('hidden')
        $('.datetime2').addClass('hidden')
        $('#receiveModal').modal('show')
        $('#btn_save_receive').removeClass('hidden')
    })
    $('#detail-delivery-table').on('click', '.btn-date-received', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var qty = $(this).data('qty')
        var receiveBy = $(this).data('receiveby')
        var receiveDate = $(this).data('receivedate')
        var datetime = receiveDate.split('T')
        var date = datetime[0].split('-')
        var time = datetime[1].split(':')
        //console.log(date)
        $('#redeemNo').val(redeemNo)
        $('#reward').val(rewardCode + ":" + rewardName)
        $('#rewardCode').val(rewardCode)
        $('#qty1').val(qty)

        $('#receiveDate').val(date[2] + '/' + date[1] + '/' + date[0])
        $('#receiveDate').attr('readonly', true)
        $('#_receiveDate').val(date[2] + '/' + date[1] + '/' + date[0] + ' ' + datetime[1].substring(0, 5))
        
        $('#receiveBy').val(receiveBy)
        $('#receiveBy').attr('readonly', true)

        $('#timeHH').val(time[0])
        $('#timeMM').val(time[1])
        $('#timeHH').attr('readonly', true)
        $('#timeMM').attr('readonly', true)

        $('#trackingNo').val("")
        $('#trackingNo').attr('readonly', false)

        $('.datetime1').addClass('hidden')
        $('.datetime2').removeClass('hidden')
        $('#receiveModal').modal('show')
        $('#btn_save_receive').removeClass('hidden')
    })
    $('#detail-delivery-table').on('click', '.btn-received', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var qty = $(this).data('qty')
        var tracking = $(this).data('tracking')
        $('#redeemNo').val(redeemNo)
        $('#reward').val(rewardCode + ":" + rewardName)
        $('#rewardCode').val(rewardCode)
        $('#qty1').val(qty)
        $('#receiveDate').val("")
        $('#receiveDate').attr('readonly', false)
        $('#receiveBy').val("")
        $('#receiveBy').attr('readonly', false)
        $('#timeHH').val("")
        $('#timeMM').val("")
        $('#timeHH').attr('readonly', false)
        $('#timeMM').attr('readonly', false)
        $('#trackingNo').val(tracking)
        $('#trackingNo').attr('readonly', true)

        $('.datetime1').removeClass('hidden')
        $('.datetime2').addClass('hidden')
        $('#receiveModal').modal('show')
        $('#btn_save_receive').removeClass('hidden')
    })
    $('#detail-delivery-table').on('click', '.btn-info-receive', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var qty = $(this).data('qty')
        var tracking = $(this).data('tracking')
        var receiveBy = $(this).data('receiveby')
        var receiveDate = $(this).data('receivedate')
        var datetime = receiveDate.split('T')
        var date = datetime[0].split('-')
        var time = datetime[1].split(':')
        $('#qty1').val(qty)

        $('#redeemNo').val(redeemNo)
        $('#reward').val(rewardCode + ":" + rewardName)
        $('#rewardCode').val(rewardCode)

        $('#receiveDate').val(date[2] + '/' + date[1] + '/' + date[0])
        $('#receiveDate').attr('readonly', true)
        $('#_receiveDate').val(date[2] + '/' + date[1] + '/' + date[0] + ' ' + datetime[1].substring(0, 5))

        $('#receiveBy').val(receiveBy)
        $('#receiveBy').attr('readonly', true)

        $('#timeHH').val(time[0])
        $('#timeMM').val(time[1])
        $('#timeHH').attr('readonly', true)
        $('#timeMM').attr('readonly', true)

        $('#trackingNo').val(tracking)
        $('#trackingNo').attr('readonly', true)

        $('.datetime1').addClass('hidden')
        $('.datetime2').removeClass('hidden')
        $('#receiveModal').modal('show')
        $('#btn_save_receive').addClass('hidden')
    })

    $('#detail-delivery-table').on('click', '.btn-cash-receive', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var receiveName = $(this).data('receivename')
        var qty = $(this).data('qty')
        var memberId = $(this).data('member')
        tempMemberName = $(this).data('redeemname')
        tempDeliveryCourierCode = $(this).data('deliverycouriercode')
        $('#memberId').val(memberId);
        $('#money_redeemNo').val(redeemNo)
        $('#money_reward').val(rewardCode + ":" + rewardName)
        $('#money_rewardCode').val(rewardCode)
        $('#money_qty').val(qty)
        $('#money_receiveDate').val("")
        $('#money_receiveDate').attr('readonly', false)
        $('#money_receiveBy').val(receiveName)
        $('#money_receiveBy').attr('readonly', false)
        $('#money_timeHH').val("")
        $('#money_timeMM').val("")
        $('#money_timeHH').attr('readonly', false)
        $('#money_timeMM').attr('readonly', false)
        $('#referNo').val("")
        $('#referNo').attr('readonly', false)

        $('.datetime1').removeClass('hidden')
        $('.datetime2').addClass('hidden')
        $('#btn_save_money_receive').removeClass('hidden')
        $('#receiveMoneyModal').modal('show')
    })
    $('#detail-delivery-table').on('click', '.btn-cash-info', function () {
        var redeemNo = $(this).data('redeem')
        var rewardCode = $(this).data('reward')
        var rewardName = $(this).data('rewardname')
        var qty = $(this).data('qty')
        var tracking = $(this).data('tracking')
        var receiveBy = $(this).data('receiveby')
        var receiveDate = $(this).data('receivedate')
        var datetime = receiveDate.split('T')
        var date = datetime[0].split('-')
        var time = datetime[1].split(':')
        $('#money_redeemNo').val(redeemNo)
        $('#money_reward').val(rewardCode + ":" + rewardName)
        $('#money_rewardCode').val(rewardCode)
        $('#money_qty').val(qty)
        $('#money_receiveDate').val(date[2] + '/' + date[1] + '/' + date[0])
        $('#money_receiveDate').attr('readonly', true)
        $('#txt_money_receiveDate').val(date[2] + '/' + date[1] + '/' + date[0] + ' ' + datetime[1].substring(0,5))

        $('#money_receiveBy').val(receiveBy)
        $('#money_receiveBy').attr('readonly', true)

        $('#money_timeHH').val(time[0])
        $('#money_timeMM').val(time[1])
        $('#money_timeHH').attr('readonly', true)
        $('#money_timeMM').attr('readonly', true)
        $('#referNo').val(tracking)
        $('#referNo').attr('readonly', true)

        $('.datetime1').addClass('hidden')
        $('.datetime2').removeClass('hidden')
        $('#btn_save_money_receive').addClass('hidden')
        $('#receiveMoneyModal').modal('show')
    })

    $("#btn_label_report").on("click", function () {
        var delilveryNoTemp = $("#deliveryNoTemp").val();
        var deliveryByTemp = $("#deliveryByTemp").val();
        var projectTemp = $("#projectTemp").val();
        if (delilveryNoTemp == "") {
            showAlert('แจ้งเตือน',"กรุณาเลือกรายการ");
            //AlertNoti("กรุณาเลือกรายการ", 'danger');
        } else {
            $.ajax({
                "url": sessionStorage.mainUrl + "PrintDelivery/label",
                type: 'POST',
                data: {
                    deliveryNo: delilveryNoTemp,
                    project: projectTemp,
                    deliveryBy: deliveryByTemp,
                },
                beforeSend: function () { showLoading() },
                success: function (data) {
                    window.open(sessionStorage.mainUrl + path + data.message, '_blank');
                }, error: function () { closeLoading() },
                complete: function () { closeLoading() },
            });
        }
    });
    $("#btn_excel_report").on("click", function () {
        var delilveryNoTemp = $("#deliveryNoTemp").val();
        var deliveryByTemp = $("#deliveryByTemp").val();
        //deliveryByTemp = "พร้อมเพย์";
        if (delilveryNoTemp == "") {
            showAlert('แจ้งเตือน',"กรุณาเลือกรายการ");
            //AlertNoti("กรุณาเลือกรายการ", 'danger');
        } else {
            window.open(sessionStorage.mainUrl + "PrintDelivery/DeliveryReport?deliveryNo=" + delilveryNoTemp + "&deliveryBy=" + deliveryByTemp, '_blank');
            // window.location.href = ;
        }
    });
    $("#btn_reward_report").on("click", function () {
        var delilveryNoTemp = $("#deliveryNoTemp").val();
        var deliveryByTemp = $("#deliveryByTemp").val();
        var projectTemp = $("#projectTemp").val();
        if (delilveryNoTemp == "") {
            showAlert('แจ้งเตือน',"กรุณาเลือกรายการ");
            //AlertNoti("กรุณาเลือกรายการ", 'danger');
        } else {
            $.ajax({
                "url": sessionStorage.mainUrl + "PrintDelivery/RewardRedeemSummary",
                type: 'POST',
                data: {
                    deliveryNo: delilveryNoTemp,
                    project: projectTemp
                },
                beforeSend: function () { showLoading() },
                success: function (data) {
                    window.open(sessionStorage.mainUrl + path + data.message, '_blank');
                }, error: function () { closeLoading() },
                complete: function () { closeLoading() },
            });
        }
    });
    $("#btn_detail_report").on("click", function () {
        var delilveryNoTemp = $("#deliveryNoTemp").val();
        var projectTemp = $("#projectTemp").val();
        var deliveryByTemp = $("#deliveryByTemp").val();
        if (delilveryNoTemp == "") {
            showAlert('แจ้งเตือน',"กรุณาเลือกรายการ");
            //AlertNoti("กรุณาเลือกรายการ", 'danger');
        } else {
            
            $.ajax({
                "url": sessionStorage.mainUrl + "PrintDelivery/RedeemDetail",
                type: 'POST',
                data: {
                    deliveryNo: delilveryNoTemp,
                    project: projectTemp,
                    deliveryBy: deliveryByTemp,
                },
                beforeSend: function () { showLoading() },
                success: function (data) {
                    window.open(sessionStorage.mainUrl + path + data.message, '_blank');
                }, error: function () { closeLoading() },
                complete: function () { closeLoading() },
            });
        }
    });
    $("#btn_order_report").on("click", function () {
        var delilveryNoTemp = $("#deliveryNoTemp").val();
        var deliveryByTemp = $("#deliveryByTemp").val();
        var projectTemp = $("#projectTemp").val();
        if (delilveryNoTemp == "") {
            showAlert('แจ้งเตือน',"กรุณาเลือกรายการ");
            //AlertNoti("กรุณาเลือกรายการ", 'danger');
        } else {
            $.ajax({
                "url": sessionStorage.mainUrl + "PrintDelivery/Order",
                type: 'POST',
                data: {
                    deliveryNo: delilveryNoTemp,
                    project: projectTemp,
                    //deliveryBy: deliveryByTemp,
                },
                beforeSend: function () { showLoading() },
                success: function (data) {
                    window.open(sessionStorage.mainUrl + path + data.message, '_blank');
                }, error: function () { closeLoading() },
                complete: function () { closeLoading() },
            });
            //window.open(sessionStorage.mainUrl + path + data.message, '_blank');
            //window.open(sessionStorage.mainUrl + "PrintDelivery/Order/?deliveryNo=" + delilveryNoTemp + "&project=" + projectTemp, '_blank');
        }
    });
    $('#btn_save_receive').click(function () {
        if ($('#receive-form').valid()) {
            data = {
                fromType: "B",
                redeemNo: $('#redeemNo').val(),
                rewardCode: $('#rewardCode').val(),
                receiveDate: $('#receiveDate').val(),
                receiveTime: $('#timeHH').val() + ':'+ $('#timeMM').val(),
                receiveBy: $('#receiveBy').val(),
                trackingNo: $('#trackingNo').val(),
                updateBy: sessionStorage.UserName
            }
            if (data.trackingNo == "" && data.receiveDate == "") {
                showAlert('แจ้งเตือน', "กรุณากรอก Tracking No./ข้อมูลการรับของ")
            }
            else if (data.receiveDate != "" && data.receiveBy == "") {
                showAlert('แจ้งเตือน', "กรุณากรอกชื่อผู้รับ")
            } else {
                updateReceive(data)
            }
        }
    })
    $('#btn_save_money_receive').click(function () {
        if ($('#money_receive-form').valid()) {
            data = {
                fromType: "B",
                DeliveryCourierCode: tempDeliveryCourierCode,
                memberName: tempMemberName,
                memberId: $('#memberId').val(),
                redeemNo: $('#money_redeemNo').val(),
                rewardCode: $('#money_rewardCode').val(),
                receiveDate: $('#money_receiveDate').val(),
                receiveTime: $('#money_timeHH').val() + ':' + $('#money_timeMM').val(),
                receiveBy: $('#money_receiveBy').val(),
                trackingNo: $('#referNo').val(),
                updateBy: sessionStorage.UserName
            }
            if (data.trackingNo == "" && data.receiveDate == "") {
                showAlert('แจ้งเตือน', "กรุณากรอกข้อมูล Tracking No.")
            }
            else if (data.receiveDate != "" && data.receiveBy == "") {
                showAlert('แจ้งเตือน', "กรุณากรอกชื่อผู้รับ")
            } else {
                updateReceive(data)
            }
        }
    })
})
var tempDeliveryNo = ''
var tempDeliveryCourierCode = ''
var tempMemberName = '';
var devilery_dataTable = function () {
    $('#detail-delivery-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });
    $('#delivery-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });
};
var getlistDeliveryAll = function () {
    var row_ = "";
    DestroyDataTable($('#delivery-table'))
    $.ajax({
        url: sessionStorage.apiUrl + "/del/list_delivery",//
        dataType: 'json',
        type: "GET",
        async: true,
        cache: false,
        crossDomain: true,
        withCredentials: true,
        headers: {
            'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        },
        data: {
            fromType: "B",
        },
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                //console.log(data.items)
                $('#delivery-table tbody tr').remove();
                $.each(data.items, function (i, e) {
                    row_ += '<tr>' +
                                    '<td class="">' + e.courierName + '</td>' +
                                    '<td class="text-center">' + ConvertDate2(e.deliveryDate) + '</td>' +
                                    '<td class="text-center">' + e.deliveryNo + '</td>' +
                                    '<td class="text-center">' +
                                        '<button class="btn btn-info btn-sm btn-del-detail" data-delivery="' + e.deliveryNo + '" data-deliveryby="' + e.deliveryBy + '"><i class="fa fa fa-list"></i>&nbsp; รายละเอียด</button>' +
                                    '</td>' +
                                  '</tr>';
                });
                $('#delivery-table tbody').html(row_)
                $('#delivery-table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false
                });
                //renderTable()
                //datatableLoad()
            } else {
                showAlert('แจ้งเตือน', data.description);
            }
            return false;
        }, error: function (a, b, c) {
            console.log(a,b,c)
            closeLoading()
        },
        complete: function () { closeLoading() },
    });
}
var getDeliveryRedeem = function (deliveryNo) {
    var row_ = "";
    DestroyDataTable($('#detail-delivery-table'))
    $.ajax({
        url: sessionStorage.apiUrl + "/del/redeem_delivery",//
        dataType: 'json',
        type: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        withCredentials: true,
        headers: {
            'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        },
        data: {
            fromType: "B",
            deliveryNo: deliveryNo
        },
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                console.log(data.items)
                var button = '';
                $('#detail-delivery-table tbody tr').remove();
                $.each(data.items, function (i, e) {
                    if (e.courierType == "เงินสด") {
                        if (e.receiveBy == null && e.receiveDate == null && e.trackingNo == null) {
                            button = '<button class="btn btn-primary btn-sm btn-cash-receive" data-redeemname="' + e.memberName + '" data-deliverycouriercode="' + e.deliveryCourierCode + '" data-member="' + e.memberId + '" data-receivename="' + e.receiveName + '" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึก Tracking/รับของ</button>';
                        }
                        //else if (e.receiveBy == null && e.receiveDate == null && e.trackingNo != null) {
                        //    button = '<button class="btn btn-primary btn-sm btn-cash-tracked" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึกการรับ</button>';
                        //} else if ((e.receiveBy != null || e.receiveDate != null) && e.trackingNo == null) {
                        //    button = '<button class="btn btn-primary btn-sm btn-cash-date-received" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึก Tracking</button>';
                        //}
                        else {
                            button = '<button class="btn btn-info btn-sm btn-cash-info" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">รายละเอียด</button>';
                        }
                    } else {
                        if (e.receiveBy == null && e.receiveDate == null && e.trackingNo == null) {
                            button = '<button class="btn btn-primary btn-sm btn-receive" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึก Tracking/รับของ</button>';
                        } else if (e.receiveBy == null && e.receiveDate == null && e.trackingNo != null) {
                            button = '<button class="btn btn-primary btn-sm btn-received" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึกการรับ</button>';
                        } else if ((e.receiveBy != null || e.receiveDate != null) && e.trackingNo == null) {
                            button = '<button class="btn btn-primary btn-sm btn-date-received" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึก Tracking</button>';
                        } else {
                            button = '<button class="btn btn-info btn-sm btn-info-receive" data-receiveby="' + e.receiveBy + '" data-receivedate="' + e.receiveDate + '" data-tracking="' + e.trackingNo + '" data-qty="' + e.qty + '" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">รายละเอียด</button>';
                        }
                    }
                    
                    row_ += '<tr>' +
                                    '<td class="text-center">' + e.redeemNo + '</td>' +
                                    '<td class="text-center">' + e.deliveryDate + '</td>' +
                                    '<td class="">' + e.memberName + '</td>' +
                                    '<td class="">' + e.receiveName + '</td>' +
                                    '<td class="">' + e.rewardCode + '</td>' +
                                    '<td class="">' + e.rewardName + '</td>' +
                                    '<td class="text-center">' + e.qty + '</td>' +
                                    '<td class="text-center">' + e.courierName + '</td>' +
                                    '<td class="text-center">' + button +
                                        //((e.receiveBy == null && e.receiveDate == null) ? '<button class="btn btn-info btn-sm btn-update-receive" data-reward="' + e.rewardCode + '" data-redeem="' + e.redeemNo + '" data-rewardname="' + e.rewardName + '">บันทึก Tracking/รับของ</button>' : (e.trackingNo == null) ? "บันทึกแล้ว" : "") +
                                    '</td>' +
                                  '</tr>';
                });
                $('#detail-delivery-table tbody').html(row_)
                $('#totRecord').text(data.pageInfo)
                $('#detail-delivery-table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false
                });
            } else {
                showAlert('แจ้งเตือน', data.description);
            }
            return false;
        }, error: function () { closeLoading() },
        complete: function () { closeLoading() },
    });
}
var updateReceive = function (dataInput) {
    $.ajax({
        url: sessionStorage.apiUrl + "/del/update_receive",//
        dataType: 'json',
        type: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        withCredentials: true,
        headers: {
            'Authorization': 'Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI='
        },
        data: dataInput,
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                showAlert('แจ้งเตือน', data.message);
                $('#receiveModal').modal('hide')
                $('#receiveMoneyModal').modal('hide')
                //listActivity($('#activity-table'))
                getDeliveryRedeem(tempDeliveryNo)
            } else {
                showAlert('แจ้งเตือน', data.description);
                return false;
            }
        },
        complete: function () {
            closeLoading()
        }
    });
}