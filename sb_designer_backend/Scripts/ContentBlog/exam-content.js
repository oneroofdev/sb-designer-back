﻿$(document).ready(function () {
    $.ajax({
        url: sessionStorage.apiUrl + "/content/get_content",//
        dataType: 'json',
        type: "POST",
        crossDomain: true,
        withCredentials: true,
        headers: {
            'APP_TOKEN': 'd750df98-7fbf-49cc-a3b2-0b3611b986ac'
            , 'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        },
        data: {
            language: "TH",
            fromType: "B",
            contentType: 0,
            contentCode: $('#contentCode').val()
        },
        async: false,
        cache: false,
        beforeSend: function () { showLoading() },
        success: function (data) {
            if (data.success) {
                console.log(data.items)
                $('#content_th').html(data.items.textItems[0]["contentTextTh"])
                $('#content_en').html(data.items.textItems[0]["contentTextEn"]);
              
            } else {
                AlertNoti(data.description, 'danger');
                return false;
            }
        },
        complete: function () {
            closeLoading()
        }
    });
})