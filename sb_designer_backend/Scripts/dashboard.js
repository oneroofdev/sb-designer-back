﻿$(document).ready(function () {
    function createChart(label, val, id, type, options) { 
        var color = [
                       "#f1948a",
                       "#bb8fce",
                       "#85c1e9",
                       "#73c6b6",
                       "#82e0aa",
                       "#f8c471",
                       "#e59866",
                       "#d7dbdd",
                       "#b2babb",
                       "#808b96",
                       "#FFFF33",
                       "#918ef1",
                       "#f49fd2",
                       "#8ef292",
                       "#FF6699",
                       "#FF9999",
                       "#FFCC99",
                       "#FFFF99",
                       "#FFFFCC"
        ]
        var dynamicColors = function () {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };
        var coloR = [];
        for (var i in val) {
            coloR.push(dynamicColors());
        }
        var data = {
            //labels: ['January', 'February', 'March'],
            labels: label,
            datasets: [
              {
                  label: 'จำนวน',
                  //data: [50445, 33655, 15900],
                  data: val,
                  backgroundColor: color,
                  //backgroundColor: [
                  //  '#FF6384',
                  //  '#36A2EB',
                  //  '#FFCE56'
                  //]
              }
            ]
        };
        new Chart(document.getElementById(id), {
            type: type,
            data: data,
            options: options
        });
    }
    $("#bt_Search").on("click", function () {
        getDataQuestionnaire();
    });
    getDataQuestionnaire();
    function getDataQuestionnaire() {

        showLoading();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: baseUrl + "Dashboard/getDataQuestionnaire",
            data: { StartDate: "", EndDate: "", IS_Verify: $("#IS_Verify").val() },
            success: function (data) {
                var htmlHead = "";
                var htmlDetail = "";
                var htmlFooter = "";
                var QuestionText = "";
                var QuestionID = "";
                var QuestionGroup = "0";
                var AnwserLabel = [];
                var AnwserVal = [];
                var drawCanvas = [];
                var i = 1;
                var temp = "";
                var ChartType = "pie";
                var results = new Array();
                var totalMember = 0;
                var totalMemberVQ = 0;
                var uniqueNames = []; 
                $.each($(data.items), function (key, value) {
                    if (uniqueNames.indexOf(value.Cust_ID) === -1) {
                        uniqueNames.push(value.Cust_ID);
                    }
                    totalMember = value.CustTotal;
                    totalMemberVQ = value.CustTotalSurvey;
                    if (QuestionGroup != value.QuestionGroup || QuestionGroup == "0") {
                        if (QuestionGroup != "0") {
                            htmlFooter += '</div>';
                            htmlFooter += '</div>';
                            htmlFooter += '</div>';
                        }
                        htmlHead = "";
                        htmlHead += '<div class="col-md-12"><div class="box"><div class="box-header with-border">';
                        QuestionGroupText = (value.QuestionGroup != null) ? value.QuestionGroup : "แบบสอบถาม";
                        htmlHead += '<h3 class="box-title">' + QuestionGroupText + '</h3>';
                        htmlHead += '<div class="box-tools pull-right">';
                        htmlHead += '<button type="button" class="btn btn-box-tool" data-widget="collapse">';
                        htmlHead += '<i class="fa fa-minus"></i>';
                        htmlHead += '</button>';
                        htmlHead += '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
                        htmlHead += '</div>';
                        htmlHead += '</div>';
                        htmlHead += ' <div class="box-body">';


                        // html += '<span style="padding:20px;">  ' + value.QuestionGroup + ' </span>'; 
                        QuestionGroup = value.QuestionGroup;

                        if (QuestionText != "") {
                            var htmlInsert = htmlDetail;
                            temp += htmlInsert;
                            results.push({
                                label: AnwserLabel,
                                val: AnwserVal,
                                question: 'canvas' + QuestionID,
                                QuestionText: QuestionText,
                                ChartType: ChartType
                            });
                            AnwserLabel = [];
                            AnwserVal = [];
                            htmlDetail = "";
                        }
                    } else {
                        if (QuestionText != value.QuestionText) {
                        }
                    }
                    if (QuestionText != value.QuestionText) {
                        if (QuestionText != "") {
                            var htmlInsert = htmlFooter + htmlHead + htmlDetail;
                            temp += htmlInsert;
                            htmlHead = "";
                            htmlFooter = "";
                            if (htmlDetail != "") {
                                results.push({
                                    label: AnwserLabel,
                                    val: AnwserVal,
                                    question: 'canvas' + QuestionID,
                                    QuestionText: QuestionText,
                                    ChartType: ChartType
                                });
                                //$(".datavq").append(htmlInsert); 
                                /*createChart(AnwserLabel, AnwserVal, 'canvas' + QuestionID, 'pie', {
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    title: {
                                        display: true,
                                        fontSize: 18,
                                        text: QuestionText
                                    },
                                    legend: {
                                        display: false
                                    },
                                    plugins: {
                                        labels: [
                                        {
                                            render: function (args) {
                                                return args.value;
                                            },
                                            arc: true
                                        }]
                                    }
                                });*/
                                AnwserLabel = [];
                                AnwserVal = [];
                                htmlDetail = "";
                            }
                        }
                        if (value.Chart_Type == 'pie' || value.Chart_Type == 'doughnut') {
                            htmlDetail += '<div class="col-md-12"> <canvas  class="col-md-6" id="canvas' + value.QuestionId + '"> </canvas></div><div class="col-md-12"> &nbsp;</div>';

                        } else {
                            htmlDetail += '<div class="col-md-12"> <canvas  class="col-md-8" id="canvas' + value.QuestionId + '"> </canvas></div><div class="col-md-12"> &nbsp;</div>';

                        }
                        /* html += '<label class="col-md-10 col-sm-10 col-xs-10 text-left">' + value.QuestionText + ' </label>';
                         html += '<label class="col-md-2 col-sm-2 col-xs-2 text-right"><a href="' + baseUrl + 'questionnaire/Index/?pageId=' + btoa(value.PageSeq) + '&custId=' + $("#CustId").val() + '&SurveyID=' + $("#SurveyID").val() + '&type=E"><i style="cursor:pointer;" class="glyphicon glyphicon-pencil editQuestionnaire"></i></a></label>';
                         html += '<br />';
                         html += '<label class="col-md-12 col-sm-12 col-xs-12"><ul style="list-style-type: disc;margin-left:-25px;"> ';
                         html += '<li style="float: left;  word-wrap: break-word;margin-left:30px;">' + value.AnswerText + AnswerOther + '</li>';*/

                        AnwserLabel.push(value.AnswerText);
                        AnwserVal.push(value.Count);
                        ChartType = value.Chart_Type;
                        QuestionText = value.QuestionText;
                        QuestionID = value.QuestionId;
                    } else {
                        AnwserLabel.push(value.AnswerText);
                        AnwserVal.push(value.Count);
                    }
                });

                temp += htmlDetail;
                temp += '</div>';
                temp += '</div>';
                temp += '</div>';
                $(".datavq").html(temp);
                $("#totalMember").text(totalMember);
                $("#totalMemberVQ").text(totalMemberVQ);
                if ($("#IS_Verify").val() == "") { 
                    $("#textAllMember").hide();
                } else {
                    $("#textAllMember").show();

                }
                
                
                results.push({
                    label: AnwserLabel,
                    val: AnwserVal,
                    question: 'canvas' + QuestionID,
                    QuestionText: QuestionText,
                    ChartType: ChartType
                }); 
                $.each(results, function (index, value) {
                    if (value.ChartType == "pie" || value.ChartType == "doughnut") {
                        createChart(value.label, value.val, value.question, value.ChartType, {
                            responsive: true,
                            maintainAspectRatio: false,
                            title: {
                                display: true,
                                fontSize: 18,
                                text: value.QuestionText
                            },
                            legend: {
                                display: true
                            },
                            plugins: {
                              labels: [

                                 /* {
                                    render: 'label',
                                    position: 'outside',
                                    boxWidth: 10,
                                    arc: false
                                },*/
                                {
                                    render: function (args) {
                                        return args.value;
                                    },
                                    arc: false
                                }]
                            },
                        });
                    } else {
                        createChart(value.label, value.val, value.question, value.ChartType, {
                            responsive: true,
                            maintainAspectRatio: false,
                            title: {
                                display: true,
                                fontSize: 18,
                                text: value.QuestionText
                            },
                            legend: {
                                display: false,
                            },
                            plugins: {
                                labels: [
                                {
                                    render: function (args) {
                                        return args.value;
                                    }, 
                                    arc: false 
                                }]
                            }, scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false,
                                        lineWidth: 1,
                                        zeroLineWidth: 1,
                                        zeroLineColor: '#666666',
                                        drawTicks: false
                                    },
                                    ticks: {
                                        display: true,
                                        stepSize: 0,
                                        min: 0,
                                        autoSkip: false,
                                        fontSize: 11,
                                        padding: 12
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        padding: 5
                                    },
                                    gridLines: {
                                        display: true,
                                        lineWidth: 1,
                                        zeroLineWidth: 2,
                                        zeroLineColor: '#666666'
                                    }
                                }]
                            }

                        });
                    }

                });
                closeLoading();
            },
        });
    }
});