﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace sb_designer_backend.Models
{
    public class DeliveryManagement
    {
        public static List<RedeemDeliveryItem> GetDeliveryRedeem(string deliveryNo, string apiRoute)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["HostApi"].ToString() + apiRoute;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";

            httpWebRequest.Headers.Add("APP_TOKEN", "d750df98-7fbf-49cc-a3b2-0b3611b986ac");
            httpWebRequest.Headers.Add("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");

            var item = new List<RedeemDeliveryItem>();
            try
            {
                var dataRequest = new DeliveryInput();
                dataRequest.fromType = "B";
                dataRequest.deliveryNo = deliveryNo;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<List<RedeemDeliveryItem>>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    item = JsonConvert.DeserializeObject<List<RedeemDeliveryItem>>(JsonConvert.SerializeObject(json.items));
                }
            }
            return item;
        }
    }

    public class DeliveryInput
    {
        [Required]
        public string fromType { get; set; }
        public string language { get; set; }
        public string deliveryNo { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string project { get; set; }
    }

    public class RedeemDeliveryItem
    {
        public string deliveryNo { get; set; }
        public string deliveryDate { get; set; }
        public string deliveryBy { get; set; }
        public string courierName { get; set; }
        public string courierType { get; set; }
        public string rewardCode { get; set; }
        public string rewardName { get; set; }
        public string perLabel { get; set; }
        public string redeemNo { get; set; }
        public string redeemBy { get; set; }
        public string redeemDate { get; set; }
        public string redeemChannel { get; set; }
        public string trackingNo { get; set; }
        public int qty { get; set; }
        public string memberId { get; set; }
        public string memberCardNo { get; set; }
        public string memberName { get; set; }
        public string fullNameTh { get; set; }
        public string fullNameEn { get; set; }
        public string receiveName { get; set; }
        public string email { get; set; }
        public string mobileNo { get; set; }
        public string houseNo { get; set; }
        public string bulding { get; set; }
        public string villageNo { get; set; }
        public string village { get; set; }
        public string floor { get; set; }
        public string soi { get; set; }
        public string road { get; set; }
        public string tambolTh { get; set; }
        public string tambolEn { get; set; }
        public string amphurTh { get; set; }
        public string amphurEn { get; set; }
        public string provNameTh { get; set; }
        public string provNameEn { get; set; }
        public string postCode { get; set; }
        public Nullable<DateTime> receiveDate { get; set; }
        public string receiveBy { get; set; }
        public string bankCode { get; set; }
        public string bankAccNo { get; set; }
        public string bankBranch { get; set; }
        public string copyBookBank { get; set; }
        public string promptPayMobile { get; set; }
        public string promptPayCID { get; set; }
        public int deliveryCourierCode { get; set; }
        public string bankNameTH { get; set; }
        public string bankNameEN { get; set; }
        public string remark { get; set; }
    }
}