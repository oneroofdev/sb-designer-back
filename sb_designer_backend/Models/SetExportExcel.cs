﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_designer_backend.Models
{
    public class SetExportExcel
    {
        private int firstColomn_sum;

        public void setHeaderExcel_Summary(IXLWorksheet ws)
        {
            var lastRows = ws.LastRowUsed().RowNumber();
            for (int i = 1; i <= lastRows; i++)
            {
                ws.Range("A" + i + ":B" + i).Row(1).Merge();
            }
            ws.Range("A1:B" + lastRows).Style.Font.FontSize = 20;
            ws.Range("A1:B" + lastRows).Style.Font.FontName = "DBGillSiam";
            ws.Range("A1:B" + lastRows).Style.Font.Bold = true;
        }

        public void setTitleExcel_Summary(IXLWorksheet ws)
        {
            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            this.firstColomn_sum = lastRows;
            ws.Range("C" + lastRows + ":" + lastColumnName + lastRows).Style.Fill.BackgroundColor = XLColor.Yellow;
            ws.Range("C" + lastRows + ":" + lastColumnName + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("C" + lastRows + ":" + lastColumnName + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        }

        public void setFormatExcel_Summary(IXLWorksheet ws)
        {
            int rowStart = this.firstColomn_sum;
            string lastColumnName = ws.LastColumnUsed().ColumnLetter();
            int lastRows = ws.LastRowUsed().RowNumber();

            ws.Range("C" + rowStart + ":" + lastColumnName + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("C" + rowStart + ":" + lastColumnName + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("C" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontSize = 18;
            ws.Range("C" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontName = "DBGillSiam";

            //ใส่วันที่ Export
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            lastRows = ws.LastRowUsed().RowNumber();
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
        }
    }
}