﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace sb_designer_backend.Models
{
    public static class Account
    {
        public static UserItem VerifyLogin(string username, string password)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["HostApi"].ToString() + "/ums/login";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
             
            httpWebRequest.Headers.Add("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI="); 
            UserItem item = new UserItem();
            try
            {
                var dataRequest = new DataLogin();
                dataRequest.username = username;
                dataRequest.password = password;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                item.isSuccess = false;
                item.message = ex.Message;
                //using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                //{
                //    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                //    item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json.items));
                //    item.isSuccess = false;
                //    item.message = ex.Message;
                //}
            }
            return item;
        }

        public static List<UserListItem> GetListAccount()
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["HostApi"].ToString() + "acc/list_users";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";

            httpWebRequest.Headers.Add("APP_TOKEN", "d750df98-7fbf-49cc-a3b2-0b3611b986ac");
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var listAccount = new List<UserListItem>();
            try
            {
                var dataRequest = new FromInput();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    listAccount = JsonConvert.DeserializeObject<List<UserListItem>>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return listAccount;
        }
    }

    public class UserListItem
    {
        public int userId { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string rememberToken { get; set; }
        public string userType { get; set; }
        public int grpId { get; set; }
        public string grpName { get; set; }
        public string active { get; set; }
        public string createBy { get; set; }
        public string updateBy { get; set; }
        public string daleteBy { get; set; }
        public System.Nullable<DateTime> createDate { get; set; }
        public System.Nullable<DateTime> updateDate { get; set; }
        public System.Nullable<DateTime> deleteDate { get; set; }
    }
    public class UserItem
    {
        public UserItem()
        {
            isSuccess = true;
        }
        public int USER_ID { get; set; }

        public string USERNAME { get; set; }

        //public string PASSWORD { get; set; }
        public string ACTIVE { get; set; }

        public string REMEMBER_TOKEN { get; set; }

        public string USER_TYPE { get; set; }

        public int GRP_ID { get; set; }

        public string CREATED_BY { get; set; }

        public string UPDATED_BY { get; set; }

        public string DELETED_BY { get; set; }

        public System.Nullable<DateTime> CREATED_DATE { get; set; }
        public System.Nullable<DateTime> UPDATED_DATE { get; set; }
        public System.Nullable<DateTime> DELETED_DATE { get; set; }
        public bool isSuccess { get; set; }
        public string message { get; set; }
    }

    public class DataLogin
    {
        public DataLogin()
        {
            fromType = "B";
        }
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        public string fromType { get; set; }
    }

    public class FromInput
    {
        public FromInput()
        {
            fromType = "B";
        }
        public string fromType { get; set; }
    }
}