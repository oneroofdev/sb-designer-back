﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExporHTMLTOFiles;
using ClosedXML.Excel;
using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace sb_designer_backend.Models
{
    public static class Commons
    {
        public static string RemoveStringFromToEnd(string str, string suffix)
        {
            if (str.Contains(suffix))
            {
                str = str.Replace("\r\n", "");
                var a = str.IndexOf(suffix);
                var ss = str.Remove(str.IndexOf(suffix), str.Length - 4);
                return ss;
            }
            if (!str.EndsWith(suffix))
            {
                return str.Substring(0, str.Length - suffix.Length);
            }
            else
            {
                return str;
            }
        }

        public static bool CreateDirectory(string path, HttpServerUtilityBase Server)
        {
            bool exists = Directory.Exists(Server.MapPath(path)); // map path in Server by boolean
            if (!exists)
            {
                // Create folder if it does not exist. 
                //***And your need change permission is full control on thr server***
                Directory.CreateDirectory(Server.MapPath(path));
                exists = true;
            }
            return exists;
        }
    }
    class Exports
    {
        private int firstColomn;
        /// <summary>
        /// export label Page A4 Standard Page
        /// </summary>
        /// <param name="Html">สตริง HTML</param>
        /// <param name="pdfFileName">path บันทึกไฟล์ PDF เช่น ~/a/c/fileName.pdf</param>
        public void PDF(string Html, string pdfFileName, string fontPath)
        {
            Byte[] bytes;
            //FontFactory.Register(fontPath, "DBGillSiamX");
            FontFactory.Register(Path.Combine(fontPath, "DB Gill Siam X v3.2.ttf").Replace('/', '\\'), "db gill siam x");
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 12, Font.ITALIC);
            if (File.Exists(pdfFileName))
            {
                File.Delete(pdfFileName);
            }
            Html = Html.Replace("\r\n", "");
            Html = Html.Replace("\0", "");
            using (MemoryStream stream = new MemoryStream())
            {
                StringReader sr = new StringReader(Html);
                Document pdfDoc = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                //using (var msHTML = new MemoryStream(Encoding.UTF8.GetBytes(Html)))
                //{


                //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, msHTML, null, Encoding.UTF8, FontFactory.FontImp);

                //}
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                bytes = stream.ToArray();
            }
            var testFile = Path.Combine(pdfFileName);
            File.WriteAllBytes(testFile, bytes);
        }

        /// <summary>
        /// export label Page A4 Landscape (horizontal) Page
        /// </summary>
        /// <param name="Html">สตริง HTML</param>
        /// <param name="pdfFileName">path บันทึกไฟล์ PDF เช่น ~/a/c/fileName.pdf</param>
        public void PDFLandscape(string Html, string pdfFileName, string fontPath)
        {
            //Create a byte array that will eventually hold our final PDF
            Byte[] bytes;
            FontFactory.Register(Path.Combine(fontPath, "DB Gill Siam X v3.2.ttf").Replace('/', '\\'), "DBGillSiamX");
            if (File.Exists(pdfFileName))
            {
                File.Delete(pdfFileName);
            }
            Html = Html.Replace("\r\n", "");
            Html = Html.Replace("\0", "");
            using (MemoryStream stream = new MemoryStream())
            {
                StringReader sr = new StringReader(Html);
                Document pdfDoc = new Document(PageSize.A4.Rotate(), 15, 15, 20, 15);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                bytes = stream.ToArray();
            }
            var testFile = Path.Combine(pdfFileName);
            File.WriteAllBytes(testFile, bytes);
        }

        /// <summary>
        /// export label Page A4 กำหนดขอบกระดาษ
        /// </summary>
        /// <param name="Html">String HTML</param>
        /// <param name="pdfFileName">path บันทึกไฟล์ PDF เช่น ~/a/c/fileName.pdf</param>
        /// <param name="manginLeft">ขอบกระดาษซ้าย</param>
        /// <param name="marginRight">ขอบกระดาษขวา</param>
        /// <param name="marginTop">ขอบกระดาษบน</param>
        /// <param name="marginBottom">ขอบกระดาษล่าง</param>
        public void PDF(string Html, string pdfFileName, float manginLeft, float marginRight, float marginTop, float marginBottom, string fontPath)
        {
            //Create a byte array that will eventually hold our final PDF
            Byte[] bytes;
            //FontFactory.Register(Path.Combine(fontPath, "DB Gill Siam X v3.2.ttf").Replace('/', '\\'), "DBGillSiamX");
            if (File.Exists(pdfFileName))
            {
                File.Delete(pdfFileName);
            }

            Html = Html.Replace("\r\n", "");
            Html = Html.Replace("\0", "");
            using (MemoryStream stream = new MemoryStream())
            {
                StringReader sr = new StringReader(Html);
                Document pdfDoc = new Document(PageSize.A4, manginLeft, marginRight, marginTop, marginBottom);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                bytes = stream.ToArray();
            }
            var testFile = Path.Combine(pdfFileName);
            File.WriteAllBytes(testFile, bytes);
        }

        public void PDFiText7(string Html, string pdfFileName, string fontNormal, string fontBold)
        {
            string[] fonts = { fontNormal, fontBold };
            iText.Layout.Font.FontProvider fontProvider = new iText.Html2pdf.Resolver.Font.DefaultFontProvider(false, false, false);
            foreach (String font in fonts)
            {
                iText.IO.Font.FontProgram fontProgram = iText.IO.Font.FontProgramFactory.CreateFont(font);
                fontProvider.AddFont(fontProgram);
            }
            iText.Html2pdf.ConverterProperties properties = new iText.Html2pdf.ConverterProperties();
            //FontProgram fontProgram = FontProgramFactory.CreateFont(font);
            properties.SetFontProvider(fontProvider);
            iText.Html2pdf.HtmlConverter.ConvertToPdf(Html, new iText.Kernel.Pdf.PdfWriter(pdfFileName), properties);
        }

       

        public void SelectPDF(string Html, string pdfFileName)
        {
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;
            converter.Options.MarginLeft = 20;
            converter.Options.MarginRight = 20;

            //// hearder
            //bool showHeaderOnFirstPage = true;
            //bool showHeaderOnOddPages = true;
            //bool showHeaderOnEvenPages = true;
            //int headerHeight = 50;

            //// header settings
            //converter.Options.DisplayHeader = showHeaderOnFirstPage ||
            //    showHeaderOnOddPages || showHeaderOnEvenPages;
            //converter.Header.DisplayOnFirstPage = showHeaderOnFirstPage;
            //converter.Header.DisplayOnOddPages = showHeaderOnOddPages;
            //converter.Header.DisplayOnEvenPages = showHeaderOnEvenPages;
            //converter.Header.Height = headerHeight;

            //SelectPdf.PdfHtmlSection headerHtml = new SelectPdf.PdfHtmlSection("<img src='https://member.pruksa.com/backend/Content/img/new-logo.png' width='60px;'/> ");
            //headerHtml.AutoFitHeight = SelectPdf.HtmlToPdfPageFitMode.AutoFit;

            //converter.Header.Add(headerHtml);



            //footer section
            bool showFooterOnFirstPage = true;
            bool showFooterOnOddPages = true;
            bool showFooterOnEvenPages = true;

            converter.Options.DisplayFooter = showFooterOnFirstPage || showFooterOnOddPages || showFooterOnEvenPages;
            converter.Footer.DisplayOnFirstPage = showFooterOnFirstPage;
            converter.Footer.DisplayOnOddPages = showFooterOnOddPages;
            converter.Footer.DisplayOnEvenPages = showFooterOnEvenPages;
            converter.Footer.Height = 20;

            SelectPdf.PdfHtmlSection footerHtml = new SelectPdf.PdfHtmlSection("", "");
            footerHtml.AutoFitHeight = SelectPdf.HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            SelectPdf.PdfTextSection text = new SelectPdf.PdfTextSection(0, 10,
                "Page: {page_number} of {total_pages}  ",
                new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = SelectPdf.PdfTextHorizontalAlign.Right;
            converter.Footer.Add(text);

            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(Html);
            // save pdf document
            doc.Save(pdfFileName);

            // close pdf document
            doc.Close();
        }
        public void SelectPDF_Landscape(string Html, string pdfFileName)
        {
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            converter.Options.PdfPageSize = SelectPdf.PdfPageSize.A4;
            converter.Options.WebPageWidth = 1100;
            converter.Options.PdfPageOrientation = SelectPdf.PdfPageOrientation.Landscape;
            converter.Options.MarginTop = 0;
            converter.Options.MarginBottom = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            //footer section
            bool showFooterOnFirstPage = true;
            bool showFooterOnOddPages = true;
            bool showFooterOnEvenPages = true;

            converter.Options.DisplayFooter = showFooterOnFirstPage || showFooterOnOddPages || showFooterOnEvenPages;
            converter.Footer.DisplayOnFirstPage = showFooterOnFirstPage;
            converter.Footer.DisplayOnOddPages = showFooterOnOddPages;
            converter.Footer.DisplayOnEvenPages = showFooterOnEvenPages;
            converter.Footer.Height = 20;

            SelectPdf.PdfHtmlSection footerHtml = new SelectPdf.PdfHtmlSection("", "");
            footerHtml.AutoFitHeight = SelectPdf.HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            SelectPdf.PdfTextSection text = new SelectPdf.PdfTextSection(0, 10,
                "Page: {page_number} of {total_pages}  ",
                new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = SelectPdf.PdfTextHorizontalAlign.Right;
            converter.Footer.Add(text);

            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(Html);
            // save pdf document
            doc.Save(pdfFileName);

            // close pdf document
            doc.Close();
        }

        public static void ToZip(List<string> files, string zipDestinationPath, string zipFloderName)
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFiles(files, zipFloderName);
                zip.Save(zipDestinationPath);
            }
            DeleteFile(files);
        }

        public static void DeleteFile(List<string> files)
        {
            foreach (var file in files)
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
        }

        public void ec(string Html, string pdfFileName)
        {

            ExportToFile ex = new ExportToFile();
            ex.ExportPDF(Html, pdfFileName);
        }

        public void Excel(XLWorkbook workbook, string fileName)
        {
            // Create the workbook

            // Prepare the response
            HttpResponse httpResponse = HttpContext.Current.Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //httpResponse.AddHeader("content-disposition", "attachment;filename=\"HelloWorld.xlsx\""); 
            httpResponse.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx;", fileName));

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }

            httpResponse.End();
        }
        public void setHeaderExcel(IXLWorksheet ws)
        {
            var lastRows = ws.LastRowUsed().RowNumber();
            for (int i = 1; i <= lastRows; i++)
            {
                ws.Range("A" + i + ":H" + i).Row(1).Merge();
            }
            ws.Range("A1:H" + lastRows).Style.Font.FontSize = 20;
            ws.Range("A1:H" + lastRows).Style.Font.FontName = "Browallia New";
            ws.Range("A1:H" + lastRows).Style.Font.Bold = true;
        }

        /// <summary>
        /// จัดหัวตาราง
        /// </summary>
        /// <param name="ws">Worksheet</param> 
        public void setTitleExcel(IXLWorksheet ws)
        {
            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            this.firstColomn = lastRows;
            ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Style.Fill.BackgroundColor = XLColor.LightGray;
            ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        }

        /// <summary>
        /// วาดเส้นตาราง
        /// </summary>
        /// <param name="ws">Worksheet</param> 
        public void setFormatExcel(IXLWorksheet ws)
        {
            int rowStart = this.firstColomn;
            string lastColumnName = ws.LastColumnUsed().ColumnLetter();
            int lastRows = ws.LastRowUsed().RowNumber();

            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontSize = 18;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontName = "Browallia New";

            //ใส่วันที่ Export
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            lastRows = ws.LastRowUsed().RowNumber();
            lastRows = lastRows + 1;
            DateTime dateNow = DateTime.Now;
            //ws.Cell(lastRows, 1).Value = "Export date : " + dateNow.ToString();
            //ws.Cell(lastRows, 1).Style.Font.FontSize = 14;
            //ws.Cell(lastRows, 1).Style.Font.FontName = "Browallia New";
            //ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Row(1).Merge();
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
        }
    }

    public class GeneratorHTML
    {
        public string LabelHtml(List<RedeemDeliveryItem> dataDetail, string fontNormal, string fontBold, string project)
        {
            string strListBuilder = string.Empty;


            strListBuilder = @"<!DOCTYPE html> 
                        <html>
                        <head>
                        
                          <style>
                        table {
                            border-collapse: collapse;
                        }
                        th, td {
                            padding: 5px;
                            text-align: left; 
                        }
                         body{
                                font-family: DBGillSiamX, DBGillSiamX-Bold;
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X v3.2.ttf');
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf');
                             }
                        hr {
                                    display: block;
                                    border: 1px solid #000;
                                    padding: 0;
                                    margin-left: 15px;
                            }
                        .hr{
                            margin-top: -25px;
                        }
                           
                        </style>
                        </head>
                         <body>";

            strListBuilder += @"
                    <table id='tb' style='width:950px; font-size:14px; margin-top:0px;' align='center' border='0'>";

            int i = 0;
            int j = 0;
            string addrLine1 = string.Empty;
            string addrLine2 = string.Empty;
            string addrLine3 = string.Empty;
            string addrLine4 = string.Empty;
            //string tel = string.Empty;
            foreach (var item in dataDetail)
            {
                addrLine1 = item.houseNo;
                //addrLine1 += item.bulding != null ? " อาคาร " + item.bulding : "";
                //addrLine2 = item.soi != null ? " ซ." + item.soi : "";
                //addrLine2 += item.road != null ? " ถ." + item.road : "";
                addrLine3 = item.tambolTh + " " + item.amphurTh;
                addrLine4 = item.provNameTh + " " + item.postCode + " โทร : " + item.mobileNo;

                j++;
                //<div style='width:100%;  margin-left:20px;'><span style='font-size:16px;'>" + tel + @"</span></div>
                //<div style='width:100%;  margin-left:15px;'><span style='font-size:16px;'>" + tel + @"</span></div>

                string labelSeq = j.ToString();
                if (i == 14)
                {
                    strListBuilder += @"</table> 
                                                     </body>
                                              </html>
                            <!DOCTYPE html> 
                            <html style='page-break-before:always;'>
                                <body>
                                    <table id='tb' style='page-break-before:always;width:950px; margin-top:0px; font-size:14px;' align='center' border='0'>";
                    i = 0;
                }
                i++;
                if (!string.IsNullOrEmpty(item.perLabel) && item.perLabel.Equals("Y"))
                {
                    for (int k = 0; k < item.qty; k++)
                    {
                        if ((i % 2) == 0)
                        {
                            strListBuilder += @"
                                <td style='width:49%;'>
                                    <div style='width:100%;height:145px;display:inline-block; margin-bottom:-5px;'>
                                        <div style='width:100%; text-align:right; padding-left:-25px;'><span style='font-size:18px;margin-right:1px;'> &lt;" + project + "-" + item.deliveryNo + "-" + item.rewardCode + "-" + labelSeq + @"&gt;</span></div>
                                        <div style='width:100%; margin-left:20px;padding-bottom:0px;'><span style='font-size:18px;'><b>" + (string.IsNullOrEmpty(item.receiveName) ? "คุณเจ้าของบ้าน" : "คุณ" + item.receiveName) + @"</b></span></div>
                                        <div class='hr' style='width:100%; padding-top:10px; margin-left:20px;'><span style='font-size:16px;'>______________________________________________________</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:10px;'><span style='font-size:18px;'>" + addrLine1 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine2 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine3 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine4 + @"</span></div>
                                    </div>
                                </td>
                          </tr>";
                        }
                        else
                        {
                            strListBuilder += @"
                            <tr>
                                <td style='width:49%;'>
                                   <div style='width:100%;height:145px;display:inline-block; margin-bottom:-5px;'>
                                        <div style='width:100%; text-align:right; padding-left:-25px;'><span style='font-size:18px;margin-right:1px;'> &lt;" + project + "-" + item.deliveryNo + "-" + item.rewardCode + "-" + labelSeq + @"&gt;</span></div>
                                        <div style='width:100%; margin-left:15px;padding-bottom:0px;'><span style='font-size:18px;'><b>" + (string.IsNullOrEmpty(item.receiveName) ? "คุณเจ้าของบ้าน" : "คุณ" + item.receiveName) + @"</b></span></div>
                                        <div class='hr' style='width:100%; padding-top:10px; margin-left:15px;'><span style='font-size:16px;'>______________________________________________________</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:10px;'><span style='font-size:18px;'>" + addrLine1 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine2 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine3 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine4 + @"</span></div>
                                    </div>
                                </td><td style='width:2%;'></td> ";
                        }
                        i++;
                        j++;
                        labelSeq = j.ToString();
                    }
                    i--;
                    j--;
                    labelSeq = j.ToString();
                }
                else
                {
                    if ((i % 2) == 0)
                    {
                        strListBuilder += @"
                                <td style='width:49%;'>
                                    <div style='width:100%;height:145px;display:inline-block; margin-bottom:-5px;'>
                                        <div style='width:100%; text-align:right; padding-left:-25px;'><span style='font-size:18px;margin-right:1px;'> &lt;" + project + "-" + item.deliveryNo + "-" + item.rewardCode + "-" + labelSeq + @"&gt;</span></div>
                                        <div style='width:100%; margin-left:20px;padding-bottom:0px;'><span style='font-size:18px;'><b>" + (string.IsNullOrEmpty(item.receiveName) ? "คุณเจ้าของบ้าน" : "คุณ" + item.receiveName) + @"</b></span></div>
                                        <div class='hr' style='width:100%; padding-top:10px; margin-left:20px;'><span style='font-size:16px;'>______________________________________________________</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:10px;'><span style='font-size:18px;'>" + addrLine1 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine2 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine3 + @"</span></div>
                                        <div style='width:100%; margin-left:20px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine4 + @"</span></div>
                                    </div>
                                </td>
                          </tr>";
                    }
                    else
                    {
                        strListBuilder += @"
                            <tr>
                                <td style='width:49%;'>
                                   <div style='width:100%;height:145px;display:inline-block; margin-bottom:-5px;'>
                                        <div style='width:100%; text-align:right; padding-left:-25px;'><span style='font-size:18px;margin-right:1px;'> &lt;" + project + "-" + item.deliveryNo + "-" + item.rewardCode + "-" + labelSeq + @"&gt;</span></div>
                                        <div style='width:100%; margin-left:15px;padding-bottom:0px;'><span style='font-size:18px;'><b>" + (string.IsNullOrEmpty(item.receiveName) ? "คุณเจ้าของบ้าน" : "คุณ" + item.receiveName) + @"</b></span></div>
                                        <div class='hr' style='width:100%; padding-top:10px; margin-left:15px;'><span style='font-size:16px;'>______________________________________________________</span></div>
                                        <div style='width:100%; margin-left:15px; margin-top:10px;'><span style='font-size:18px;'>" + addrLine1 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine2 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine3 + @"</span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'>" + addrLine4 + @"</span></div>
                                    </div>
                                </td><td style='width:2%;'></td> ";
                    }
                }
            }
            if ((i % 2) == 0)
            {
                strListBuilder += @"</table></body></html> ";
            }
            else
            {
                strListBuilder += @"  
                                <td style='width:49%;'>
                                    <div style='width:100%; text-align:right; margin-left:-25px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:10px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'></span></div>
                                        <div style='width:100%; margin-left:15px;margin-top:5px;'><span style='font-size:18px;'></span></div>
                                </td>
                          </tr>
                        </table>
                    </body>
                </html> ";
            }
            return strListBuilder;
        }
        public string RewardRedeemSummary(List<RedeemDeliveryItem> dataDetail, string fontNormal, string fontBold, string img_logo)
        {
            string strListBuilder = string.Empty;
            var data = (from a in dataDetail
                        group a by new { a.deliveryBy, a.deliveryDate, a.deliveryNo, a.courierName } into g
                        select new
                        {
                            g.Key,
                            g
                        }).ToList();
            int totalPage = 0;
            int intPage = dataDetail.Count;
            double doublePage = (double)intPage / 30;
            if (doublePage > intPage / 30)
            {
                totalPage = (intPage / 30) + 1;
            }
            else
            {
                totalPage = intPage / 30;
            }
            strListBuilder = @"<!DOCTYPE html> 
                <html>
                    <head>
                    <style>
                        #tb th, #tb td {
                             border: 0.5px solid black;
                        }
                        table {
                            border-collapse: collapse;
                        }
                        th, td {
                            padding: 5px;
                            text-align: left;
                        }
                        body{
                               font-family: DBGillSiamX, DBGillSiamX-Bold;
                               src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X v3.2.ttf');
                               src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf');
                            }
                    </style>
                    </head>";
            foreach (var item in data)
            {
                string delivery_by = item.Key.courierName;

                //switch (item.Key.deliveryBy.ToLower())
                //{
                //    case "brt":
                //        delivery_by = "ผู้จัดการเขต"; break;
                //    case "post":
                //        delivery_by = "ไปรษณีย์"; break;
                //    case "tran":
                //        delivery_by = "ขนส่ง"; break;
                //    case "nim":
                //        delivery_by = "ขนส่ง (บริษัท นิ่มเอ็กซ์เพรส จำกัด)"; break;
                //    default:
                //        delivery_by = "-"; break;
                //}
                strListBuilder += @" 
                <body style='height:100%;min-height:100%;position:relative;'>
                    <table>
                        <tr>
                            <td style='width:40%;'></td>
                            <td style='width:40%;'></td>
                            <td style='width:10%; text-align: right;'>
                                <span style='width:100%; margin-left: 60%;'>
                                    <img src='" + img_logo + @"' width='60px;'/> 
                                </span>                                 
                            </td>
                        </tr>
                    </table>
                    <div style='width:100%;'><span style='font-size:18px; padding-left:0px;'><b>สรุปรายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                    <table style='margin-top:0px;'>
                        <tr>
                            <td style='width:50%;'>
                                <div style='width:100%; line-height: 0.8;'><span style='font-size:18px;'>เลขที่ใบจัดส่ง : " + item.Key.deliveryNo + @"</span></div>
                            </td>
                            <td style='width:50%;'>
                                <div style='width:100%;line-height: 0.8;'><span style='font-size:18px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td style='width:50%;'>
                                <div style='width:100%;line-height: 0.8;'><span style='font-size:18px;'>วันที่จัดส่ง : " + item.Key.deliveryDate + @"</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td style='width:50%;'>
                               <div style='width:100%;line-height: 0.8;'><span style='font-size:18px;'>วันที่พิมพ์ : " + DateTime.Now.ToString("dd/MM/yyyy") + @"</span></div>
                            </td>
                        </tr>
                    </table>";
                strListBuilder += @"
                    <div style='height: 97%;'>
                        <table id='tb' style='width:100%; font-size:16px;border-collapse: collapse;border: 1px solid black;'>
                            <tr style='background-color:#D1D0CE;'>
                                <th style='width:5%; text-align: center;border: 1px solid black;'>ลำดับ</th>
                                <th style='width:15%; text-align: center;border: 1px solid black;'>รหัสของรางวัล</th>
                                <th style='width:70%; text-align: center;border: 1px solid black;'>ชื่อของรางวัล</th>
                                <th style='width:10%; text-align: center;border: 1px solid black;'>จำนวน</th>
                            </tr> ";
                int i = 1;
                int total = 0;
                int page = 1;
                foreach (var e in item.g)
                {
                    strListBuilder += @"
                          <tr>
                            <td style='text-align: center; font-size:16px;border: 1px solid black;'>" + i + @"</td>
                            <td style='text-align: center; font-size:16px;border: 1px solid black;'>" + e.rewardCode + @"</td>
                            <td style='font-size:16px;border: 1px solid black;'>" + e.rewardName + @"</td>
                            <td style='text-align: right; font-size:16px;border: 1px solid black;'>" + e.qty.ToString("#,##0") + @"</td>
                          </tr>";
                    total += e.qty;
                    if (i % 25 == 0)
                    {
                        strListBuilder += @"</table>
                        </div>
                        <div style='min-height:3%; position:fixed; margin-bottom: 5px; margin-top: 100%;bottom:5;width:100%; text-align: right;'><span style='font-size:16px;'>หน้า " + page + "/" + totalPage + @"</span></div>          
                        
                        </body></html>
                        <!DOCTYPE html> 
                        <html>
                            <head>
                            <style>
                                #tb th, #tb td {
                                     border: 0.5px solid black;
                                }
                                table {
                                    border-collapse: collapse;
                                }
                                th, td {
                                    padding: 5px;
                                    text-align: left;
                                }
                                body{
                                        font-family: Angsana New, Angsana New Bold, Angsana New;
                                        src: url('" + fontNormal + @"');
                                        src: url('" + fontBold + @"');
                                    }
                            </style>
                            </head><body style='height:100%;min-height:100%;position:relative;'><div style='height: 97%;'>
                                 <table id='tb' style='width:100%; font-size:16px; page-break-after:always;margin:0px;'>
                                    <tr style='background-color:#D1D0CE;'>
                                        <th style='width:5%; text-align: center;'>ลำดับ</th>
                                        <th style='width:15%; text-align: center;'>รหัสของรางวัล</th>
                                        <th style='width:70%; text-align: center;'>ชื่อของรางวัล</th>
                                        <th style='width:10%; text-align: center;'>จำนวน</th>
                                    </tr>";
                        page++;
                    }
                    i++;
                }
                strListBuilder += @"
                          <tr style='background-color:#D1D0CE;'>
                            <td colspan='3' style='text-align: center; font-size:16px;'><b>รวมทั้งหมด</b></td>
                            <td style='text-align: right; font-size:16px;'><b><u>" + total.ToString("#,##0") + @"</u></b></td>
                          </tr> 
                    </table>
                    <div style='height:40px;'></div>
                    <table style='width:100%; font-size:16px;'>
                        <tr>
                            <td style='text-align: center; width:50%'>______________________________</td>
                            <td style='text-align: center; width:50%'>______________________________</td>
                        </tr> 
                        <tr>
                            <td style='text-align: center;'>ผู้จัดทำ</td>
                            <td style='text-align: center;'>ผู้อนุมัติ</td>
                        </tr> 
                    </table>
                    </div>
                 </body>
             </html> ";
            }
            return strListBuilder;
        }
        public string RedeemDetail(List<RedeemDeliveryItem> dataDetail, string fontNormal, string fontBold, string img_logo)
        {
            int total_qty = 0;
            int i = 1;
            int page = 1;
            int totalPage = 0;
            int intPage = dataDetail.Count;
            double doublePage = (double)intPage / 10;
            if (doublePage > intPage / 10)
            {
                totalPage = (intPage / 10) + 1;
            }
            else
            {
                totalPage = intPage / 10;
            }
            string strSelectUserListBuilder = "";
            string tel = "";
            string address = "";
            //string newPage = @"style='page-break-before:always;margin:0px;'";

            string headerHtml = @"<!DOCTYPE html> <html> <head> <style> #tb th, #tb td { border: 0.5px solid black; } table { border-collapse: collapse; }
                        th, td { padding: 5px; text-align: left; }
                        body{
                                font-family: DBGillSiamX, DBGillSiamX-Bold;
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X v3.2.ttf');
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf');
                            }
                        </style> </head><body>";
            string tableHeader = @"<div style='height:1px;'></div>
                            <div style='height: 95%;'>
                            <table id='tb' style='width:100%; font-size:12px;border-collapse: collapse;border: 1px solid black;'>
                                  <tr style='background-color:#D1D0CE;'>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>#</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>ใบแลก</th> 
                                    <th style='width:6%; text-align: center;border: 1px solid black;'>เลขที่บัตรสมาชิก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อลูกค้า/ผู้แลก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อผู้รับ</th>
                                    
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>ของรางวัล</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>จำนวน</th>
                                    <th style='width:7%; text-align: center;border: 1px solid black;'>เบอร์โทร</th>
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>ที่อยู่</th>
                                    <th style='width:10%; text-align: center;border: 1px solid black;'>หมายเหตุ</th>
                                  </tr>";

            foreach (var item in dataDetail)
            {
                address = item.houseNo + " "; //+ (!string.IsNullOrEmpty(item.bulding) ? " อาคาร " + item.bulding + " " : " ");
                //address += (!string.IsNullOrEmpty(item.soi) ? " ซ." + item.soi + " " : "");
                //address += (!string.IsNullOrEmpty(item.road) ? " ถ." + item.road + " " : "");
                address += (!string.IsNullOrEmpty(item.tambolTh) ? item.tambolTh + " " : "");
                address += (!string.IsNullOrEmpty(item.amphurTh) ? item.amphurTh + " " : "");
                address += (!string.IsNullOrEmpty(item.provNameTh) ? item.provNameTh + " " : "");
                address += (!string.IsNullOrEmpty(item.postCode) ? item.postCode : "");
                tel = (!string.IsNullOrEmpty(item.mobileNo) ? item.mobileNo : "");
                string delivery_by = item.courierName;

                if (i == 1)
                {
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='width:100%; margin-bottom: 10px;'>
                                <tr>
                                     <td style='width:90%;'>
                                        <div style='width:100%;height:70px;'>
                                            <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วิธีการจัดส่ง : " + delivery_by + @"</span></div>
                                        </div>
                                    </td>
                                   
                                    <td style='width:10%;'>
                                        <span style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:50px; padding-top:10px;'/> 
                                        </span>                                 
                                   </td>
                                </tr>
                            </table>";
                    strSelectUserListBuilder += tableHeader;
                    //agent_code_temp = item.AGENT_CODE;
                }
                if (i >= 1)
                {
                    strSelectUserListBuilder += @"<tr id='tb'>
                            <td style='text-align: center;border: 1px solid black;'>" + i + @"</td>
                            <td style='text-align: center;border: 1px solid black;'>" + item.redeemNo + @"</td> 
                            <td style='border: 1px solid black;'>" + item.memberCardNo + @"</td>
                            <td style='border: 1px solid black;'>" + item.memberName + @"</td>
                            <td style='border: 1px solid black;'>" + (item.receiveName == null ? "เจ้าของบ้าน" : item.receiveName == "" ? "เจ้าของบ้าน" : item.receiveName) + @"</td>
                            
                            <td style='text-align: left;border: 1px solid black;'>" + item.rewardCode + ": " + item.rewardName + @"</td>
                            <td style='text-align: right;border: 1px solid black;'>" + (item.qty).ToString("#,##0") + @"</td>
                            <td style='border: 1px solid black;'>" + tel + @"</td>
                            <td style='border: 1px solid black;'>" + address + @"</td>
                            <td style='border: 1px solid black;'>" + item.remark + @"</td>
                          </tr> ";

                    total_qty += item.qty;
                }
                if (i % 10 == 0)
                {
                    strSelectUserListBuilder += @"
                            </table>   
                        </div>           
                            
                     </body>
                    </html>";
                    page++;
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='page-break-before:always;margin:0px; width:100%;'>
                                    <tr>
                                        <td style='width:90%;'>
                                            <div style='width:100%;height:70px;'>
                                                <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                                            </div>
                                        </td>

                                        <td style='width:10%;'>
                                        <div style='width:100%;height:20px;'></div>
                                        <div style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:90px; padding-top:10px;'/> 
                                        </div>                                 
                                   </td>
                                    </tr>
                                </table>";
                    strSelectUserListBuilder += tableHeader;
                }
                i++;
            }
            strSelectUserListBuilder += @"<tr style='background-color:#D1D0CE;'>
                            <td colspan='6' style='text-align: center;border: 1px solid black;'></td>
                            <td style='text-align: right; text-decoration: underline;border: 1px solid black;'><b>" + total_qty.ToString("#,##0") + @"</b></td>
                            <td colspan='5' style='text-align: right;border: 1px solid black;'></td>
                          </tr> 
                          
                        </table>
                        <table style='width:100%; font-size:12px;'>
                          <tr style='border: none;'>
                              <td style='text-align: center; width:33%'></td>
                              <td style='text-align: center; width:33%'></td>
                              <td style='text-align: center; width:33%'>วันที่จัดทำ " + DateTime.Now.ToString("dd/MM/yyyy") + @"</td>
                          </tr>
                        </table>                   
                        <div style='height:30px;'></div>
				        <table style='width:100%; font-size:12px;'>
                          <tr>
                            <td style='text-align: center; width:33%'>ลงชื่อ ______________________________ ผู้รับของ</td>
                            <td style='text-align: center; width:33%'>ลงชื่อ ______________________________ ผู้จัดทำ</td>
                            <td style='text-align: center; width:33%'>ลงชื่อ ______________________________ ผู้อนุมัติ</td>
                          </tr> 
                          <tr>
                            <td style='text-align: center;'>(ตัวบรรจง)</td>
                            <td style='text-align: center;'></td>
                            <td style='text-align: center;'></td>
                          </tr> 
                        </table>
                        </div>
                        
	                </body>
                </html>";
            return strSelectUserListBuilder;
        }
        public string RedeemDetailForSupplier(List<RedeemDeliveryItem> dataDetail, string fontNormal, string fontBold, string img_logo)
        {
            int total_qty = 0;
            int i = 1;
            int page = 1;
            int totalPage = 0;
            int intPage = dataDetail.Count;
            double doublePage = (double)intPage / 10;
            if (doublePage > intPage / 10)
            {
                totalPage = (intPage / 10) + 1;
            }
            else
            {
                totalPage = intPage / 10;
            }
            string strSelectUserListBuilder = "";
            string tel = "";
            string address = "";
            //string newPage = @"style='page-break-before:always;margin:0px;'";

            string headerHtml = @"<!DOCTYPE html> <html> <head> <style> #tb th, #tb td { border: 0.5px solid black; } table { border-collapse: collapse; }
                        th, td { padding: 5px; text-align: left; }
                        body{
                                font-family: DBGillSiamX, DBGillSiamX-Bold;
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X v3.2.ttf');
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf');
                            }
                        </style> </head><body>";
            string tableHeader = @"<div style='height:1px;'></div>
                            <div style='height: 95%;'>
                            <table id='tb' style='width:100%; font-size:12px;border-collapse: collapse;border: 1px solid black;'>
                                  <tr style='background-color:#D1D0CE;'>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>#</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>ใบแลก</th> 
                                    <th style='width:6%; text-align: center;border: 1px solid black;'>เลขที่บัตรสมาชิก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อลูกค้า/ผู้แลก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อผู้รับ</th>
                                    
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>ของรางวัล</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>จำนวน</th>
                                    <th style='width:7%; text-align: center;border: 1px solid black;'>เบอร์โทร</th>
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>ที่อยู่</th>
                                    <th style='width:10%; text-align: center;border: 1px solid black;'>หมายเหตุ</th>
                                  </tr>";

            foreach (var item in dataDetail)
            {
                address = item.houseNo + " "; //+ (!string.IsNullOrEmpty(item.bulding) ? " อาคาร " + item.bulding + " " : " ");
                //address += (!string.IsNullOrEmpty(item.soi) ? " ซ." + item.soi + " " : "");
                //address += (!string.IsNullOrEmpty(item.road) ? " ถ." + item.road + " " : "");
                address += (!string.IsNullOrEmpty(item.tambolTh) ? item.tambolTh + " " : "");
                address += (!string.IsNullOrEmpty(item.amphurTh) ? item.amphurTh + " " : "");
                address += (!string.IsNullOrEmpty(item.provNameTh) ? item.provNameTh + " " : "");
                address += (!string.IsNullOrEmpty(item.postCode) ? item.postCode : "");
                tel = (!string.IsNullOrEmpty(item.mobileNo) ? item.mobileNo : "");
                string delivery_by = item.courierName;

                if (i == 1)
                {
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='width:100%;'>
                                <tr>
                                     <td style='width:90%;'>
                                        <div style='width:100%;height:70px;'>
                                            <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                                        </div>
                                    </td>
                                   
                                    <td style='width:10%;'>
                                        <span style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:50px; padding-top:10px;'/> 
                                        </span>                                 
                                   </td>
                                </tr>
                            </table>";
                    strSelectUserListBuilder += tableHeader;
                    //agent_code_temp = item.AGENT_CODE;
                }
                if (i >= 1)
                {
                    strSelectUserListBuilder += @"<tr id='tb'>
                            <td style='text-align: center;border: 1px solid black;'>" + i + @"</td>
                            <td style='text-align: center;border: 1px solid black;'>" + item.redeemNo + @"</td> 
                            <td style='border: 1px solid black;'>" + item.memberCardNo + @"</td>
                            <td style='border: 1px solid black;'>" + item.memberName + @"</td>
                            <td style='border: 1px solid black;'>" + (item.receiveName == null ? "เจ้าของบ้าน" : item.receiveName == "" ? "เจ้าของบ้าน" : item.receiveName) + @"</td>
                            
                            <td style='text-align: left;border: 1px solid black;'>" + item.rewardCode + ": " + item.rewardName + @"</td>
                            <td style='text-align: right;border: 1px solid black;'>" + (item.qty).ToString("#,##0") + @"</td>
                            <td style='border: 1px solid black;'>" + tel + @"</td>
                            <td style='border: 1px solid black;'>" + address + @"</td>
                            <td style='border: 1px solid black;'>" + item.remark + @"</td>
                          </tr> ";

                    total_qty += item.qty;
                }
                if (i % 10 == 0)
                {
                    strSelectUserListBuilder += @"
                            </table>   
                        </div>           
                            
                     </body>
                    </html>";
                    page++;
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='page-break-before:always;margin:0px; width:100%;'>
                                    <tr>
                                        <td style='width:90%;'>
                                            <div style='width:100%;height:70px;'>
                                                <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                                            </div>
                                        </td>

                                        <td style='width:10%;'>
                                        <div style='width:100%;height:20px;'></div>
                                        <div style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:90px; padding-top:10px;'/> 
                                        </div>                                 
                                   </td>
                                    </tr>
                                </table>";
                    strSelectUserListBuilder += tableHeader;
                }
                i++;
            }
            strSelectUserListBuilder += @"<tr style='background-color:#D1D0CE;'>
                            <td colspan='6' style='text-align: center;border: 1px solid black;'></td>
                            <td style='text-align: right; text-decoration: underline;border: 1px solid black;'><b>" + total_qty.ToString("#,##0") + @"</b></td>
                            <td colspan='5' style='text-align: right;border: 1px solid black;'></td>
                          </tr> 
                          
                        </table>
                        <table style='width:100%; font-size:12px;'>
                          <tr style='border: none;'>
                              <td style='text-align: left; width:50%'>วันที่จัดทำ " + DateTime.Now.ToString("dd/MM/yyyy") + @"</td>
                              <td style='text-align: center; width:50%'></td>
                          </tr>
                        </table>                   
                        <div style='height:30px;'></div>
				        <table style='width:100%; font-size:12px;'>
                          <tr>
                            <td style='text-align: center; width:50%'>ลงชื่อ ______________________________ ผู้จัดทำ</td>
                            <td style='text-align: center; width:50%'>ลงชื่อ ______________________________ ผู้อนุมัติ</td>
                          </tr> 
                          <tr>
                            <td style='text-align: center;'></td>
                            <td style='text-align: center;'></td>
                          </tr> 
                        </table>
                        </div>
	                </body>
                </html>";
            return strSelectUserListBuilder;
        }
        public string RedeemDetailMoney(List<RedeemDeliveryItem> dataDetail, string fontNormal, string fontBold, string img_logo)
        {
            int total_qty = 0;
            int i = 1;
            int page = 1;
            int totalPage = 0;
            int intPage = dataDetail.Count;
            double doublePage = (double)intPage / 10;
            if (doublePage > intPage / 10)
            {
                totalPage = (intPage / 10) + 1;
            }
            else
            {
                totalPage = intPage / 10;
            }
            string strSelectUserListBuilder = "";
            string tel = "";
            string detail = "";
            //string newPage = @"style='page-break-before:always;margin:0px;'";

            string headerHtml = @"<!DOCTYPE html> <html> <head> <style> #tb th, #tb td { border: 0.5px solid black; } table { border-collapse: collapse; }
                        th, td { padding: 5px; text-align: left; }
                        body{
                                font-family: DBGillSiamX, DBGillSiamX-Bold;
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X v3.2.ttf');
                                src: url('../../backend/content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf');
                            }
                        </style> </head><body>";
            string tableHeader = @"<div style='height:1px;'></div>
                            <div style='height: 95%;'>
                            <table id='tb' style='width:100%; font-size:12px;border-collapse: collapse;border: 1px solid black;'>
                                  <tr style='background-color:#D1D0CE;'>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>#</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>ใบแลก</th> 
                                    <th style='width:6%; text-align: center;border: 1px solid black;'>เลขที่บัตรสมาชิก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อลูกค้า/ผู้แลก</th>
                                    <th style='width:12%; text-align: center;border: 1px solid black;'>ชื่อผู้รับ</th>
                                    
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>ของรางวัล</th>
                                    <th style='width:4%; text-align: center;border: 1px solid black;'>จำนวน</th>
                                    <th style='width:7%; text-align: center;border: 1px solid black;'>เบอร์โทร</th>
                                    <th style='width:20%; text-align: center;border: 1px solid black;'>รายละเอียด</th>
                                    <th style='width:10%; text-align: center;border: 1px solid black;'>หมายเหตู</th>
                                  </tr>";

            foreach (var item in dataDetail)
            {

                tel = (!string.IsNullOrEmpty(item.mobileNo) ? item.mobileNo : "");

                if (item.deliveryCourierCode == 1) //โอน
                {
                    detail = @"ธนาคาร " + item.bankNameTH + "<br/>";
                    detail += @"สาขา " + item.bankBranch + "<br/>";
                    detail += @"เลขบัญชี " + (!string.IsNullOrEmpty(item.bankAccNo) ? "x" + item.bankAccNo.Substring(item.bankAccNo.Length - 6, item.promptPayCID.Length - 6) : "-");
                }
                else if (item.deliveryCourierCode == 2) //พร้อมเพย์
                {
                    detail = @"หมายเลขพร้อมเพย์ " + (!string.IsNullOrEmpty(item.promptPayCID) ? "x" + item.promptPayCID.Substring(item.promptPayCID.Length - 5, item.promptPayCID.Length - 5) : "x" + item.promptPayMobile.Substring(item.promptPayMobile.Length - 5, item.promptPayCID.Length - 5));
                }
                string delivery_by = item.courierName;
                if (i == 1)
                {
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='width:100%;'>
                                <tr>
                                     <td style='width:90%;'>
                                        <div style='width:100%;height:70px;'>
                                            <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                            <div style='width:100%;'><span style='font-size:14px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                                        </div>
                                    </td>
                                   
                                    <td style='width:10%;'>
                                        <span style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:50px; padding-top:10px;'/> 
                                        </span>                                 
                                   </td>
                                </tr>
                            </table>";
                    strSelectUserListBuilder += tableHeader;
                    //agent_code_temp = item.AGENT_CODE;
                }
                if (i >= 1)
                {
                    strSelectUserListBuilder += @"<tr id='tb'>
                            <td style='text-align: center;border: 1px solid black;'>" + i + @"</td>
                            <td style='text-align: center;border: 1px solid black;'>" + item.redeemNo + @"</td> 
                            <td style='border: 1px solid black;'>" + item.memberCardNo + @"</td>
                            <td style='border: 1px solid black;'>" + item.memberName + @"</td>
                            <td style='border: 1px solid black;'>" + (item.receiveName == null ? "เจ้าของบ้าน" : item.receiveName == "" ? "เจ้าของบ้าน" : item.receiveName) + @"</td>
                            
                            <td style='text-align: left;border: 1px solid black;'>" + item.rewardCode + ": " + item.rewardName + @"</td>
                            <td style='text-align: right;border: 1px solid black;'>" + (item.qty).ToString("#,##0") + @"</td>
                            <td style='border: 1px solid black;'>" + tel + @"</td>
                            <td style='border: 1px solid black;'>" + detail + @"</td>
                            <td style='border: 1px solid black;'>" + item.remark + @"</td>
                          </tr> ";

                    total_qty += item.qty;
                }
                if (i % 10 == 0)
                {
                    strSelectUserListBuilder += @"
                            </table>   
                        </div>           
                            
                     </body>
                    </html>";
                    page++;
                    strSelectUserListBuilder += headerHtml;
                    strSelectUserListBuilder += @"<table style='page-break-before:always;margin:0px; width:100%;'>
                                    <tr>
                                        <td style='width:90%;'>
                                            <div style='width:100%;height:70px;'>
                                                <div style='width:100%;'><span style='font-size:14px;'><b>รายการแลกของรางวัล Pruksa Member Rewards</b></span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วันที่ส่งของรางวัล : " + item.deliveryDate + @"</span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>เลขที่ใบส่งของรางวัล : " + item.deliveryNo + @" </span></div>
                                                <div style='width:100%;'><span style='font-size:14px;'>วิธิการจัดส่ง : " + delivery_by + @"</span></div>
                                            </div>
                                        </td>

                                        <td style='width:10%;'>
                                        <div style='width:100%;height:20px;'></div>
                                        <div style='width:100%; height:70px;'>
                                            <img src='" + img_logo + @"' style='width:90px; padding-top:10px;'/> 
                                        </div>                                 
                                   </td>
                                    </tr>
                                </table>";
                    strSelectUserListBuilder += tableHeader;
                }
                i++;
            }
            strSelectUserListBuilder += @"<tr style='background-color:#D1D0CE;'>
                            <td colspan='6' style='text-align: center;border: 1px solid black;'></td>
                            <td style='text-align: right; text-decoration: underline;border: 1px solid black;'><b>" + total_qty.ToString("#,##0") + @"</b></td>
                            <td colspan='5' style='text-align: right;border: 1px solid black;'></td>
                          </tr> 
                          
                        </table>
                        <table style='width:100%; font-size:12px;'>
                          <tr style='border: none;'>
                              <td style='text-align: left; width:50%'>วันที่จัดทำ " + DateTime.Now.ToString("dd/MM/yyyy") + @"</td>
                              <td style='text-align: center; width:50%'></td>
                          </tr>
                        </table>                   
                        <div style='height:30px;'></div>
				        <table style='width:100%; font-size:12px;'>
                          <tr>
                            <td style='text-align: center; width:50%'>ลงชื่อ ______________________________ ผู้จัดทำ</td>
                            <td style='text-align: center; width:50%'>ลงชื่อ ______________________________ ผู้อนุมัติ</td>
                          </tr> 
                          <tr>
                            <td style='text-align: center;'></td>
                            <td style='text-align: center;'></td>
                          </tr> 
                        </table>
                        </div>
                        
	                </body>
                </html>";
            return strSelectUserListBuilder;
        }
        public string OrderDelivery(string imgLogo, string imgIdNo, string deliveryNo, string deliveryDate, int running, string memberName, string receiveName, string memberCardNo, string addr1, string addr2, string mobileNo, string rewards)
        {
            string html = "";
            html += @"<!DOCTYPE html> 
                    <html>
                    <head>
	                    <style>
		                    p{
			                    padding-left:5px;
		                    }
		                    aside{
			                    border: 1px solid black;
		                    }
	                    </style>
                    </head>
                    <body>
                        <div style='page-break-before:always;margin:0px; height: 80px;'>
                            <img style='display: block;margin-left: auto;margin-right: 5px;width: 50px;' src='" + imgLogo + @"'/>
                        </div>
                
                        <div style='height: 150px; border: 1px solid black;'>
	                        <div colspan='12' style='font-size:18px;'>
		                        <div style='width: 100%; text-align: center; padding-bottom:-8px;'> One Roof Co.,Ltd.</div>
		                        <div style='width: 100%; text-align: center; padding-bottom:-8px;'> 428 Sukhumvit 63 (Ekamai 26),</div>
		                        <div style='width: 100%; text-align: center; padding-bottom:-8px;'> Sukhumvit Rd., Klongton-Nua,</div>
		                        <div style='width: 100%; text-align: center;'> Wattana, Bangkok 10110</div>
		                        <div style='width: 100%; text-align: center; padding-bottom:-5px;font-size:20px;'> <b>ใบส่งของรางวัล (Delivery Order) </b></div>
		                        <div style='width: 100%; text-align: center; font-size:20px;'> <b>รายการแลกรับของรางวัล</b></div>
	                        </div>
                            <div style='height: 60px; width: 22%; border: 1px solid black; float:right; margin-top: -100px; margin-right:5px; position: fixed;'>
		                        <div style='width: 100%; text-align: left;padding-left:5px; padding-top:10px;'>เลขที่ใบส่งของ :  <span>" + deliveryNo + @"-" + running + @"</span></div>
		                        <div style='width: 100%; text-align: left;padding-left:5px;'>วันที่ส่ง :  <span>" + deliveryDate + @"</span></div>
	                        </div>
	                    </div>
	
	                    <div style='height:5px; width:100%;'></div>
		                    <!-- Detail Recipient-->
		                    <aside style='height: 30px; width: 40%; border: 1px solid black; float: left;'>
                                <div style='padding-left: 5px;'>
			                         <span style='padding-left: 5px; font-size:16px;'><b>รายละเอียดผู้รับของรางวัล <small>(Details of Recipient)</small></b></span>
                                </div>
		                    </aside>
		                    <!-- Acknowledge and comfirm Rewards-->
		                    <aside style='height: 30px; width: 59%; border: 1px solid black;float:right;'>
                                <div style='padding-left: 5px;'>
			                         <span style='padding-left: 5px; font-size:16px;'><b>การรับทราบและยืนยันการรับของรางวัล <small>(Acknowledge and comfirm Rewards)</small></b></span>
                                </div>
		                    </aside>
		                    <aside style='height: 750px; width: 59%; border: 1px solid black;float:right;'>
			                    <p style='text-indent: 2.5em; font-size:16px; padding-left:5px; padding-top:50px;'>
				                    *** โปรดตรวจเช็คสภาพของรางวัลก่อนลงลายมือชื่อรับของรางวัล<br/>หากพบว่ามีการแตกหักเสียหาย กรุณาโทรศัพท์แจ้งศูนย์บริการหรือปฏิเสธ<br/>การรับของรางวัล
			                    </p>
			                    <p style='text-indent: 2.5em; font-size:16px; padding-left:5px;'>
				                    (You are request to check the product merchandise upon receipt in order to verify
				                     that the merchandise received is in good <br/>condition.)
			                    </p>
			                    <div style='height: 40px;'></div>
			                    
			                    <p style='text-align: center; font-size:16px;'>ลายมือชื่อ ______________________________________ ผู้รับของรางวัล</p>
			                    <p style='text-align: center; font-size:16px; padding-top:-10px;'>(กรุณาเขียนตัวบรรจง)</p>
		                    </aside>
		                    <!-- Detail Recipient and  Address-->
		                    <aside style='height: 180px; width: 40%; border: 1px solid black; float: left;'>
                                <div style='padding-left: 5px;'>
                                    <div style='width: 100%; text-align: left; font-size:16px;'>ชื่อสมาชิก/ผู้แลก <span><u>" + memberName + @"</u></span></div>
                                    <div style='width: 100%; text-align: left; font-size:16px;'>ชื่อผู้รับ <span><u>" + receiveName + @"</u></span></div>
			                        <div style='width: 100%; text-align: left; font-size:16px;'>ที่อยู่ 
                                        <span>
                                            <u>" + addr1 + @"</u><br/>
                                            <u>" + addr2 + @" </u>
                                        </span>
                                    </div>
			                        <div style='width: 100%; text-align: left; font-size:16px;'>โทร. <span><u>" + mobileNo + @"</u></span></div>
                                </div>
		                    </aside>
		                    <!-- Detail of Rewards-->
		                    <aside style='height: 30px; width: 40%; border: 1px solid black; float: left;'>
                                <div style='padding-left: 5px;'>
			                         <span style='padding-left: 5px; font-size:16px;'><b>รายละเอียดของรางวัล (Details of Rewards)</b></span>
                                </div>
		                    </aside>
		                    <!-- Rewards list-->
		                    <aside style='height: 536px; width: 40%; border: 1px solid black; float: left;'>
	                            <div style='padding-left: 5px; font-size:16px;'>" + rewards + @"</div>
		                    </aside>
                    </body>
                    </html>";
            return html;
        }
    }

    public class JsonRespone
    {
        public Object data { get; set; }
        public string message { get; set; }
        public bool status { get; set; }
    }
}