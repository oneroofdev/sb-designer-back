﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_designer_backend.Models
{
    public class APIResultResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public string transactionId { get; set; }
        public string transactionDateTime { get; set; }
        public object items { get; set; }
        public int nextPage { get; set; }
        public int prevPage { get; set; }
        public PageInfoItems pageInfo { get; set; }

        public APIResultResponse()
        {
            code = "200";
            message = "OK";
            description = "";
            items = new object();
            transactionId = Guid.NewGuid().ToString();
            transactionDateTime = DateTime.Now.ToString();
            pageInfo = new PageInfoItems
            {
                resultsPerPage = 25, // 25 per page
                totalResults = 0 // all
            };
        }

        public static bool save_transaction(APIResultResponse xx)
        {
            xx.code = "ssss";
            return true;
        }
    }
    public class PageInfoItems
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }

        public PageInfoItems()
        {
            totalResults = 25;
            resultsPerPage = 1;
        }
    }
}