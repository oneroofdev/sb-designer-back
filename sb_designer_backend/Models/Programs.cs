﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace sb_designer_backend.Models
{
    public static class Programs
    {
        public static List<ProgramDetailModel> getProgramWithPermission(int userId, int groupId)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["HostApi"].ToString() + "/program/list_program_permission";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST"; 

            httpWebRequest.Headers.Add("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");

            var item = new List<ProgramDetailModel>();
            try
            {
                var dataRequest = new UserProgram();
                dataRequest.userId = userId;
                dataRequest.groupId = groupId;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<List<ProgramDetailModel>>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    item = JsonConvert.DeserializeObject<List<ProgramDetailModel>>(JsonConvert.SerializeObject(json.items));
                }
            }
            return item;
        }
    }

    public class UserProgram
    {
        public UserProgram()
        {
            fromType = "B";
        }
        [Required]
        public int userId { get; set; }
        [Required]
        public int groupId { get; set; }
        [Required]
        public string fromType { get; set; }
    }

    public class ProgramDetailModel
    {
        public int PROGRAM_ID { get; set; }
        public string PROGRAM_NAME { get; set; }
        public string URL { get; set; }
        public string IS_GRP_MENU { get; set; }
        public string GRP_MENU_NAME { get; set; }
        public string ICON { get; set; }
        public string ACTIVE { get; set; }
        public int SEQ { get; set; }
    }
}