﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.MemberCard
{
    public class MemberProfileController : BaseController
    {
        // GET: MemberProfile
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string GetPoint_Balance(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/get_member");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListRedeem(string Cust_ID,string yearmonth)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"yearmonth\":\"" + yearmonth + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/get_redeem");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string getDataHistoryPoint(string Cust_ID, string yearmonth)
        {
            string rawXml = "{" +
                "\"Cust_id\": \"" + Cust_ID + "\"," +
                "\"yearmonth\":\"" + yearmonth + "\"" +
                "}";
            var client = new RestClient(EndPoint + "/get_point_history");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        [HttpPost]
        public string getDataHistoryRefer(string Cust_ID, string yearmonth)
        {
            string rawXml = "{" +
                "\"Cust_id\": \"" + Cust_ID + "\"," +
                "\"yearmonth\":\"" + yearmonth + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/get_refer");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        [HttpPost]
        public string ListDetailRedeem_byRWCode(string Cust_ID,string RewardCode,string RedeemNo)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"RewardCode\":\"" + RewardCode + "\"," +
                "\"RedeemNo\":\"" + RedeemNo + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListDetailRedeem_byRWCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string list_refer_date(string Cust_ID,string refer_id)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"refer_id\":\"" + refer_id + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/MEM/list_refer_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string detail_refer_date(string Cust_ID, string DATE_REFER)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"DATE_REFER\":\"" + DATE_REFER + "\"," +
                "\"refer_cls_id\":\"\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/MEM/detail_refer_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}