﻿using Newtonsoft.Json;
using RestSharp;
using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Project
{
    public class ApproveProjectController : BaseController
    {
        // GET: ApproveProject
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string GetDataPortfolio (string status_approve)
        {
            string rawXml = "{" +
                "\"status_approve\": \"" + status_approve + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/Project/getDataPortfolio");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string ApproveProject(string project_id, string status_approve,string remark,string Approve_By)
        {
            string rawXml = "{" +
                "\"status_approve\": \"" + status_approve + "\"," +
                "\"project_id\": \"" + project_id + "\"," +
                "\"remark\": \"" + remark + "\"," +
                "\"Approve_By\": \"" + Approve_By + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/Project/updateStatusApprove");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string GetDataReviewDesigner(string status_approve)
        {
            string rawXml = "{" +
                "\"status_approve\": \"" + status_approve + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/Project/getDataReview");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string ApproveReviewDesigner(string Review_ID,string Cust_ID,string Status,string Approve_By)
        {
            string rawXml = "{" +
                 "\"Review_ID\": \"" + Review_ID + "\"," +
                 "\"Cust_ID\": \"" + Cust_ID + "\"," +
                 "\"Status\": \"" + Status + "\"," +
                 "\"Approve_By\": \"" + Approve_By + "\"" +
             "}";
            var client = new RestClient(EndPointPort + "/API/Customer/ApproveCustomerReview");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string GetDataReviewProject(string status_approve)
        {
            string rawXml = "{" +
                "\"status_approve\": \"" + status_approve + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/Project/getDataReviewProject");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string ApproveReviewProject(string review_id,string project_id, string status_approve, string Approve_By)
        {
            string rawXml = "{" +
                 "\"Review_ID\": \"" + review_id + "\"," +
                 "\"Project_ID\": \"" + project_id + "\"," +
                 "\"Status\": \"" + status_approve + "\"," +
                 "\"Approve_By\": \"" + Approve_By + "\"" +
             "}";

            var client = new RestClient(EndPointPort + "/API/Project/ApproveProjectReview");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    } // approve contorller
    }
