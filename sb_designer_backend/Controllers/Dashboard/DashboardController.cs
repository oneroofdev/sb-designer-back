﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Dashboard
{
    public class DashboardController : BaseController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }
        public string getDataQuestionnaire(string StartDate, string EndDate, string IS_Verify)
        {
            /*string rawXml = "{" +
                "\"CustText\": \"" + CustText + "\"," +
                "\"StartDate\":\"" + StartDate + "\"," +
                "\"EndDate\":\"" + EndDate + "\"," +
                "\"IS_Verify\":\"" + IS_Verify + "\"," +
                "\"IS_VIP\":\"" + IS_VIP + "\"" +
                "}";*/ 
            string rawXml = "{" +
                "\"IS_Verify\":\"" + IS_Verify + "\"," +
                "\"StartDate\":\"" + StartDate + "\"," +
                "\"EndDate\":\"" + EndDate + "\"" +
            "}";
            var client = new RestClient(EndPoint + "/get_questionnaire_by_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
