﻿using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.User
{
    public class UserController : BaseController
    {
        // GET: User
        public ActionResult Index()
        {
            var dataAccount = Account.GetListAccount();
            ViewBag.ListAccount = dataAccount;
            return View();
        }

        public ActionResult ProgramPermission(int userId, string username)
        {
            Session["user_id"] = userId;
            Session["user_name"] = username;
            return View();
        }

        public ActionResult ProgramPermissionGrp()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetProgramPermission(UserProgram model)
        { 
            var getData = Programs.getProgramWithPermission(int.Parse(System.Web.HttpContext.Current.Session["USERID"].ToString()), int.Parse(System.Web.HttpContext.Current.Session["GRP_ID"].ToString()));
            var dataMenu = (from r in getData
                            group r by r.GRP_MENU_NAME into Menu
                            orderby Menu.Select(r => r.PROGRAM_ID).FirstOrDefault()
                            select new
                            {
                                Menu
                            }).ToList();
            return Json(dataMenu);
        }
    }
}