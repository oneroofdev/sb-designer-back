﻿using System;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers
{
    public class ProfileController : BaseController
    {
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string FilterMember(string STR_TEXT)
        {
            string rawXml = "{" +
                "\"STR_TEXT\": \"" + STR_TEXT + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
                "}";

            var client = new RestClient(EndPoint + "/MEM/FilterMember");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetMemberByID(string Cust_ID, string language,int maxResults, string fromType)
        {
            string rawXml = "{" +
                "\"CusID\": \"" + Cust_ID + "\"," +
                "\"language\":\"" + language + "\"," +
                "\"maxResults\":" + maxResults + "," +
                "\"fromType\":\"" + fromType + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/MEM/GetMemberByID");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetExpirePoint(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/point/GetExpirePoint");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}
