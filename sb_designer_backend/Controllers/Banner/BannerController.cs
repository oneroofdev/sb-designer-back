﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Banner
{
    public class BannerController : BaseController
    {
        // GET: Banner
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult preview(string bannerDTH, string bannerDEN, string bannerMTH, string bannerMEN, string bannerDMTH, string bannerDMEN, string bannerMMTH, string bannerMMEN)
        {
            ViewBag.bannerDTH = bannerDTH;
            ViewBag.bannerDEN = bannerDEN;
            ViewBag.bannerMTH = bannerMTH;
            ViewBag.bannerMEN = bannerMEN;
            ViewBag.bannerDMTH = bannerDMTH;
            ViewBag.bannerDMEN = bannerDMEN;
            ViewBag.bannerMMTH = bannerMMTH;
            ViewBag.bannerMMEN = bannerMMEN;

            return View();
        }
    }
}