﻿using ClosedXML.Excel;
using ExporHTMLTOFiles;
using Newtonsoft.Json.Linq;
using RestSharp; 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Report
{
    public class ReferReportController : BaseController
    {
        // GET: ReferReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDetail(string start_date, string end_date)
        {
            string rawXml = "{" +
                    "\"Start_Date\": \"" + start_date + "\"," +
                    "\"End_Date\":\"" + end_date + "\"" +
                    "}";

            var client = new RestClient(EndPoint + "/get_refer_by_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);

            var array_key = 0;
            var items = new Dictionary<int, Dictionary<string, object>>();
            foreach (var user in callback["items"])
            {
                var refer_date = user["Ref_Date"].ToString().Split(' ');
                items.Add(array_key, new Dictionary<string, object>()
                {
                    { "Ref_Date", refer_date[0] },
                    { "CustID", user["CustID"].ToString() },
                    { "Name", user["Cust_Name"].ToString() + " " + user["Cust_Surname"].ToString() },
                    { "Is_VIP", user["Is_VIP"].ToString() },
                    { "Cust_Phone", user["Cust_Phone"].ToString() },
                    { "Cust_Email", user["Cust_Email"].ToString() },
                    { "CustSapCode", user["CustSapCode"].ToString() },
                    { "Ref_Name", user["Ref_First_Name"].ToString() + " " + user["Ref_Last_Name"].ToString() },
                    { "Ref_Sap_Code", user["Ref_Sap_Code"].ToString()},
                    { "Ref_Mobile_1", user["Ref_Mobile_1"].ToString()},
                    { "Ref_Mobile_2", user["Ref_Mobile_2"].ToString()},
                    { "Ref_Email", user["Ref_Email"].ToString()},
                    { "Status", user["Status"].ToString()},
                    { "Ref_Total_Buy", user["Ref_Total_Buy"].ToString()},
                    { "Ref_Reason", user["Ref_Reason"].ToString()},
                });
                array_key++;
            }

            List<object> data = new List<object>();
            foreach (var item in items)
            {
                data.Add(item.Value);
            }

            return Json(data);
        }

        public void ExportExcel(string start_date, string end_date)
        {
            //call api
            string rawXml = "{" +
                "\"Start_Date\": \"" + start_date + "\"," +
                "\"End_Date\":\"" + end_date + "\"" +
                "}";
            var client = new RestClient(EndPoint + "/get_refer_by_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);
            //loop for count login amount
            var array_key = 0;
            var items = new Dictionary<int, Dictionary<string, object>>();
            foreach (var user in callback["items"])
            {
                var refer_date = user["Ref_Date"].ToString().Split(' ');
                items.Add(array_key, new Dictionary<string, object>()
                {
                    { "Ref_Date", refer_date[0] },
                    { "CustID", user["CustID"].ToString() },
                    { "Name", user["Cust_Name"].ToString() + " " + user["Cust_Surname"].ToString() },
                    { "Is_VIP", user["Is_VIP"].ToString() },
                    { "Cust_Phone", user["Cust_Phone"].ToString() },
                    { "Cust_Email", user["Cust_Email"].ToString() },
                    { "CustSapCode", user["CustSapCode"].ToString() },
                    { "Ref_Name", user["Ref_First_Name"].ToString() + " " + user["Ref_Last_Name"].ToString() },
                    { "Ref_Sap_Code", user["Ref_Sap_Code"].ToString()},
                    { "Ref_Mobile_1", user["Ref_Mobile_1"].ToString()},
                    { "Ref_Mobile_2", user["Ref_Mobile_2"].ToString()},
                    { "Ref_Email", user["Ref_Email"].ToString()},
                    { "Status", user["Status"].ToString()}, 
                    { "Ref_Reason", user["Ref_Reason"].ToString()},
                });
                array_key++;
            }
            //generate excel
            ExportToFile ex = new ExportToFile();
            XLWorkbook wb = new XLWorkbook();
            var worksheet = wb.Worksheets.Add("Refer Friend");
            var imagePath = Server.MapPath("~");
            imagePath = imagePath + @"\Content\img\new-logo.png";
            var image = worksheet.AddPicture(imagePath)
                .MoveTo(worksheet.Cell("L1").Address)
                .Scale(0.4); // optional: resize picture
            worksheet.Cell("A1").Value = "รายงานข้อมูลการแนะนำเพื่อน";
            worksheet.Range("A1:E1").Row(1).Merge();
            worksheet.Cell("A2").Value = (start_date == "" && end_date == "") ? "ช่วงวันที่ : ทั้งหมด" : "ช่วงวันที่ : " + start_date + " - " + end_date;
            worksheet.Range("A2:E2").Row(1).Merge();
            //worksheet.Cell("A3").Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            worksheet.Range("A3:E3").Row(1).Merge();
            ex.setHeaderExcel(worksheet);

            var seq = 0;
            var row_amount = 6;
            var header = row_amount + 3;
            var row = header + 2;
            int total = 0;
            int refer_buy = 0;
            int refer_not_buy = 0;
            int refer_cancel = 0;
            int refer_more_than_month = 0;

            worksheet.Cell("A" + header).Value = "No.";
            worksheet.Cell("B" + header).Value = "วันที่แนะนำ";
            worksheet.Cell("C" + header).Value = "ข้อมูลผู้แนะนำ";
            worksheet.Cell("I" + header).Value = "ผู้ถูกแนะนำ";
            worksheet.Cell("C" + (header + 1)).Value = "รหัสสมาชิก";
            worksheet.Cell("D" + (header + 1)).Value = "ผู้แนะนำ";
            worksheet.Cell("E" + (header + 1)).Value = "ระดับ";
            worksheet.Cell("F" + (header + 1)).Value = "Mobile Phone";
            worksheet.Cell("G" + (header + 1)).Value = "Email";
            worksheet.Cell("H" + (header + 1)).Value = "Refer SAP ID";
            worksheet.Cell("I" + (header + 1)).Value = "ผู้ถูกแนะนำ";
            worksheet.Cell("J" + (header + 1)).Value = "REFER SAP ID";
            worksheet.Cell("K" + (header + 1)).Value = "MobilePhone";
            worksheet.Cell("L" + (header + 1)).Value = "Email"; 
            worksheet.Cell("M" + (header + 1)).Value = "สถานะการแนะนำ"; 
            worksheet.Cell("N" + (header + 1)).Value = "เหตุผล";
            //worksheet.Cell("M" + (header + 1)).Value = "Mobile Phone";
            //worksheet.Cell("N" + (header + 1)).Value = "สถานะการแนะนำ";
            //worksheet.Cell("O" + (header + 1)).Value = "ยอดซื้อทั้งหมด (บาท)";
            //worksheet.Cell("P" + (header + 1)).Value = "เหตุผล";

            worksheet.Range("A" + 9 + ":N"   + 10).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            var count_ref = new Dictionary<string, int>();
            foreach (var item in items)
            {
                seq++;
                worksheet.Cell("A" + row).Value = seq;
                foreach (var data in item.Value)
                {
                    if (data.Key == "Ref_Date")
                    {
                        worksheet.Cell("B" + row).Value = data.Value;
                    }
                    else if (data.Key == "CustID")
                    {
                        worksheet.Cell("C" + row).Value = data.Value;

                        // count reference
                        if (count_ref.ContainsKey(data.Value.ToString()))
                        {
                            count_ref[data.Value.ToString()]++;
                        }
                        else
                        {
                            count_ref.Add(data.Value.ToString(), 1);
                        }
                    }
                    else if (data.Key == "Name")
                    {
                        worksheet.Cell("D" + row).Value = data.Value;
                    }
                    else if (data.Key == "VIP")
                    {
                        if (data.Value == "")
                        {
                            worksheet.Cell("E" + row).Value = "";
                        }
                        else
                        {
                            worksheet.Cell("E" + row).Value = "VIP";
                        }

                    }
                    else if (data.Key == "Cust_Phone")
                    {
                        worksheet.Cell("F" + row).Value = "'" + data.Value;
                    }
                    else if (data.Key == "Cust_Email")
                    {
                        worksheet.Cell("G" + row).Value = data.Value;
                    }
                    else if (data.Key == "CustSapCode")
                    {
                        worksheet.Cell("H" + row).Value = data.Value;
                    }
                    else if (data.Key == "Ref_Name")
                    {
                        worksheet.Cell("I" + row).Value = data.Value;
                    }
                    else if (data.Key == "Ref_Sap_Code")
                    {
                        worksheet.Cell("J" + row).Value = data.Value;
                    }
                    else if (data.Key == "Ref_Mobile_1")
                    {
                        worksheet.Cell("K" + row).Value = "'" + data.Value;
                    }
                    else if (data.Key == "Ref_Email")
                    {
                        worksheet.Cell("L" + row).Value = data.Value;
                    }
                    else if (data.Key == "Ref_Mobile_2")
                    {
                       // worksheet.Cell("M" + row).Value = "'" + data.Value;
                    }
                    else if (data.Key == "Status")
                    {
                        worksheet.Cell("M" + row).Value = data.Value;
                        if (data.Value.ToString() == "แนะนำแล้วไปซื้อ")
                        {
                            refer_buy++;
                        }
                        else if (data.Value.ToString() == "แนะนำแล้วยังไม่ไปซื้อ")
                        {
                            refer_not_buy++;
                        }
                        else if (data.Value.ToString() == "ยกเลิก")
                        {
                            refer_cancel++;
                        }
                        else if (data.Value.ToString() == "แนะนำแล้วเกิน 1 เดือนยังไม่ไปซื้อ")
                        {
                            refer_more_than_month++;
                        }
                    }
                    else if (data.Key == "Ref_Total_Buy")
                    {
                        //worksheet.Cell("O" + row).Value = data.Value;
                        //worksheet.Cell(row, "O").Style.NumberFormat.Format = "#,##";
                        //total += Convert.ToInt32(data.Value);
                    }
                    else if (data.Key == "Ref_Reason")
                    {
                        worksheet.Cell("N" + row).Value = data.Value;
                    }
                }
                row++;
            }

            worksheet.Cell("A" + row).Value = "Total";
        /*    worksheet.Cell("O" + row).Value = total;
            worksheet.Cell(row, "O").Style.NumberFormat.Format = "#,##";*/
            setFormatExcelTmp(worksheet);
            worksheet.Range("A" + row + ":N" + row).Row(1).Merge();
            worksheet.Range("C" + header + ":H" + header).Row(1).Merge();
            worksheet.Range("I" + header + ":N" + header).Row(1).Merge();
            worksheet.Range("A" + header + ":A" + (header + 1)).Column(1).Merge();
            worksheet.Range("B" + header + ":B" + (header + 1)).Column(1).Merge();

            worksheet.Cell("A" + row_amount).Value = "จำนวนผู้แนะนำ : " + count_ref.Count() + " ราย";
            worksheet.Cell("D" + row_amount).Value = "จำนวนผู้ถูกแนะนำ : " + seq + " ราย";
            worksheet.Range("A" + row_amount + ":C" + row_amount).Row(1).Merge();
            worksheet.Range("D" + row_amount + ":F" + row_amount).Row(1).Merge();

            worksheet.Cell("A" + (row_amount + 1)).Value = "สถานะผู้ถูกแนะนำ";
            worksheet.Range("A" + (row_amount + 1) + ":C" + (row_amount + 1)).Row(1).Merge();
            worksheet.Cell("D" + (row_amount + 1)).Value = "แนะนำแล้วไปซื้อ : " + refer_buy + " ราย";
            worksheet.Range("D" + (row_amount + 1) + ":F" + (row_amount + 1)).Row(1).Merge();
            worksheet.Cell("G" + (row_amount + 1)).Value = "แนะนำแล้วยังไม่ไปซื้อ : " + refer_not_buy + " ราย";
            worksheet.Range("G" + (row_amount + 1) + ":I" + (row_amount + 1)).Row(1).Merge();
            worksheet.Cell("J" + (row_amount + 1)).Value = "แนะนำแล้วเกิน 1 เดือนยังไม่ไปซื้อ : " + refer_more_than_month + " ราย";
            worksheet.Range("J" + (row_amount + 1) + ":M" + (row_amount + 1)).Row(1).Merge();
            worksheet.Cell("N" + (row_amount + 1)).Value = "ยกเลิก : " + refer_cancel + " ราย";

            for (var i = 1; i <= 14; i++)
            {
                if (i >= 9)
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(252, 248, 227);
                    worksheet.Cell((header + 1), i).Style.Fill.BackgroundColor = XLColor.FromArgb(252, 248, 227);
                }
                else if (i >= 3)
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 237, 247);
                    worksheet.Cell((header + 1), i).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 237, 247);
                }
                else
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
                }
            }
            worksheet.Cell(row, 1).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
            worksheet.Cell(row, 13).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
            worksheet.Cell(row, 14).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);

            worksheet.Columns().AdjustToContents();

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(wb, "ข้อมูลการแนะนำเพื่อน" + DateTime.Now.ToString("dd-MM-yyyy"));
            /*   string myName = Server.UrlEncode("Refer_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
               MemoryStream stream = GetStream(wb);// The method is defined below
               Response.Clear();
               Response.Buffer = true;
               Response.AddHeader("content-disposition",
               "attachment; filename=" + myName);
               Response.ContentType = "application/vnd.ms-excel";
               Response.BinaryWrite(stream.ToArray());
               Response.End();*/
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public void setFormatExcelTmp(IXLWorksheet ws)
        {
            int rowStart = 9;
            string lastColumnName = ws.LastColumnUsed().ColumnLetter();
            int lastRows = ws.LastRowUsed().RowNumber();

            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontSize = 16;
            ws.Range("A" + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontName = "Browallia New";

            //ใส่วันที่ Export
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            lastRows = ws.LastRowUsed().RowNumber();
            lastRows = lastRows + 1;
            DateTime dateNow = DateTime.Now;
            ws.Cell(lastRows, 1).Value = "Export date : " + dateNow.ToString();
            ws.Cell(lastRows, 1).Style.Font.FontSize = 14;
            ws.Cell(lastRows, 1).Style.Font.FontName = "Browallia New";
            ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Row(1).Merge();
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
        }
    }
}