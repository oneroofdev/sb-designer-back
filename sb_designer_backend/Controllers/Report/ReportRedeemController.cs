﻿using ClosedXML.Excel;
using ExporHTMLTOFiles;
using Newtonsoft.Json;
using RestSharp;
using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Report
{
    public class ReportRedeemController : BaseController
    {
        Models.SetExportExcel se = new Models.SetExportExcel();
        public int firstColomn_sum;
        // GET: ReportRedeem
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RedeemStatus()
        {
            return View();
        }
        public void ReportRedeemRewardExcel(string report_h, string start_date, string end_date)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            var data_report = GetDataSummaryRedeem(2, start_date, end_date);
            var ws = workbook.Worksheets.Add("Redeem Reward(สรุป)");
            //SET Header
            ws.Cell(2, 1).Value = report_h;
            ws.Cell(3, 1).Value = "ช่วงวันที่แลก : " + start_date + " - " + end_date;
            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 20;
            ws.Range("A3:M11").Style.Font.FontSize = 14;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;

            //SET Image
            //var imagePath = Server.MapPath("~");
            //imagePath = imagePath + @"\Content\img\new-logo.png";
            //var image = ws.AddPicture(imagePath)
            //    .MoveTo(ws.Cell("L2").Address)
            //    .Scale(0.8); // optional: resize picture
            ws.Range("A6:A6").Value = "จำนวนสมาชิกที่แลกของรางวัล  : "; ws.Range("B6:B6").Value = data_report.NUM_MEMBER.ToString("#,##0"); ws.Range("C6:C6").Value = "ราย";
            ws.Range("A7:A7").Value = "คะแนนที่ใช้แลก  : "; ws.Range("B7:B7").Value = "'" + (data_report.POINT_REDEEM).ToString("#,##0"); ws.Range("C7:C7").Value = "คะแนน";
            ws.Range("A8:A8").Value = "มูลค่าของรางวัล  : "; ws.Range("B8:B8").Value = "'" + Math.Floor(data_report.REWARD_PRICE).ToString("#,##0.00"); ws.Range("C8:C8").Value = "บาท";
            ws.Range("B6:B8").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A6:D8").Style.Font.Bold = true;

            //SET Logo
            //xlWorkSheet.Shapes.AddPicture("C:\\Shashi_keshar.JPG", MsoTriState.msoFalse, MsoTriState.msoCTrue, 50, 50, 300, 45);
            //SET HEADER COLUMN
            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = " Category";
            ws.Cell(Row, 2).Value = "ลำดับที่";
            ws.Cell(Row, 3).Value = "รหัสของรางวัล";
            ws.Cell(Row, 4).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 5).Value = "จำนวน";
            ws.Cell(Row, 6).Value = "คะแนน/หน่วย";
            ws.Cell(Row, 7).Value = "คะแนนที่แลก";
            ws.Cell(Row, 8).Value = "มูลค่าของรางวัล";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontSize = 14;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontName = "Cambria";
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 0;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            var sum_qty = 0;
            var sum_price = 0;
            var sum_point = 0;
            var old_cat = "";
            var new_cat = "";
            foreach (var item in data_report.ListReport)
            {

                new_cat = (item.CATEGORY_NAME == null || item.CATEGORY_NAME.ToString() == "") ? "NULL" : item.CATEGORY_NAME;

                var cat_name = (new_cat == old_cat) ? "" : item.CATEGORY_NAME;
                i = (new_cat == old_cat) ? i + 1 : 1;

                ws.Cell(Row, 1).Value = "'" + cat_name;
                ws.Cell(Row, 2).Value = "'" + i;
                ws.Cell(Row, 3).Value = (item.REWARD_CODE == null) ? "'" : "'" + item.REWARD_CODE.ToString();
                ws.Cell(Row, 4).Value = (item.NAME_TH == null) ? "'" : "'" + item.NAME_TH.ToString();
                ws.Cell(Row, 5).Value = (item.QTY.ToString() == null) ? "'" : "'" + item.QTY.ToString("#,##0");
                ws.Cell(Row, 6).Value = (item.POINT.ToString() == null || item.POINT.ToString() == "") ? "'" : "'" + (item.POINT).ToString("#,##0");
                ws.Cell(Row, 7).Value = (item.POINT_REDEEM.ToString() == null || item.POINT_REDEEM.ToString() == "") ? "'" : "'" + item.POINT_REDEEM.ToString("#,##0");
                ws.Cell(Row, 8).Value = (item.REWARD_PRICE.ToString() == null || item.REWARD_PRICE.ToString() == "") ? "'" : "'" + Math.Floor(item.REWARD_PRICE).ToString("#,##0.00");

                ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Range("B" + Row + ":B" + Row).Style.DateFormat.SetFormat("dd/MM/yyyy");
                //ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

                Row++;
                old_cat = (item.CATEGORY_NAME == null || item.CATEGORY_NAME.ToString() == "") ? "NULL" : item.CATEGORY_NAME;
                sum_qty += item.QTY;
                sum_price += (int)Math.Floor(item.REWARD_PRICE);
                sum_point += (int)item.POINT_REDEEM;
            }


            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวม";
            ws.Range("E" + endRow + ":E" + endRow).Value = "'" + sum_qty.ToString("#,##0");
            ws.Range("G" + endRow + ":G" + endRow).Value = "'" + sum_point.ToString("#,##0");
            ws.Range("H" + endRow + ":H" + endRow).Value = "'" + sum_price.ToString("#,##0.00");
            ws.Range("A" + endRow + ":D" + endRow).Row(1).Merge();
            ws.Range("A" + endRow + ":D" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;


            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontSize = 12;
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontName = "Cambria";
            ws.Range("A" + startRow + ":H" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":H" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":H" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("E" + startRow + ":H" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A" + endRow + ":H" + endRow).Style.Font.Bold = true;

            ws.Column("A").Width = 30;
            ws.Column("B").Width = 15;
            ws.Column("C").Width = 15;
            ws.Column("D").Width = 50;
            ws.Column("E").Width = 15;
            ws.Column("F").Width = 15;
            ws.Columns("G:H").Width = 15;

            Row++;

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h);
        } //Report Redeem Reward Sum 3
        public void ReportRedeemRewardByDetailExcel(string report_h, string start_date, string end_date)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            List<DataReportRedeem> data_report = new List<DataReportRedeem>();
            data_report = GetDataReportRedeem(1, start_date, end_date);
            var datef = start_date.Split('-');
            var txt_datef = datef[2] + "/" + datef[1] + "/" + datef[0];
            var datet = end_date.Split('-');
            var txt_datet = datet[2] + "/" + datet[1] + "/" + datet[0];
            var ws = workbook.Worksheets.Add("Redeem Reward (Detail)");
            //SET Header
            ws.Cell(2, 1).Value = report_h;
            ws.Cell(3, 1).Value = "ช่วงวันที่แลก : " + txt_datef + " - " + txt_datet;
            

            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 20;
            ws.Range("A3:M11").Style.Font.FontSize = 14;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;

            //SET Image
            //var imagePath = Server.MapPath("~");
            //imagePath = imagePath + @"\Content\img\new-logo.png";
            //var image = ws.AddPicture(imagePath)
            //    .MoveTo(ws.Cell("L2").Address)
            //    .Scale(0.8); // optional: resize picture


            //SET Logo
            //xlWorkSheet.Shapes.AddPicture("C:\\Shashi_keshar.JPG", MsoTriState.msoFalse, MsoTriState.msoCTrue, 50, 50, 300, 45);
            //SET HEADER COLUMN
            int Row = ws.LastRowUsed().RowNumber() + 6;
            ws.Cell(Row, 1).Value = "ลำดับที่";
            ws.Cell(Row, 2).Value = "รหัสสมาชิก";
            ws.Cell(Row, 3).Value = "ชื่อสมาชิก";
            ws.Cell(Row, 4).Value = "วันที่แลก";
            ws.Cell(Row, 5).Value = " Category";
            ws.Cell(Row, 6).Value = "รหัสของรางวัล";
            ws.Cell(Row, 7).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 8).Value = " จำนวน";
            ws.Cell(Row, 9).Value =  "คะแนน/หน่วย";
            ws.Cell(Row, 10).Value = "คะแนนที่ใช้แลก";
            ws.Cell(Row, 11).Value = "มูลค่าของรางวัล";
            ws.Cell(Row, 12).Value = "ผู้รับสินค้า";
            ws.Cell(Row, 13).Value = "การจัดส่ง";
            ws.Cell(Row, 14).Value = "หมายเหตุ";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Font.FontSize = 14;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Font.FontName = "Cambria";
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":N" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 0;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            var sum_qty = 0;
            decimal sum_price = 0;
            var sum_point = 0;
            var old_cat = "";
            var new_cat = "";
            var mem = "";
            var countMem = 0;
            foreach (var item in data_report)
            {
                if(mem != item.MEMBER_ID.ToString())
                {
                    countMem++;
                    mem = item.MEMBER_ID.ToString();
                }
                new_cat = (item.CATEGORY_NAME == null || item.CATEGORY_NAME.ToString() == "") ? "NULL" : item.CATEGORY_NAME;
                var cat_name = (new_cat == old_cat) ? "" : item.CATEGORY_NAME;
                i = i + 1;

                ws.Cell(Row, 1).Value = "'" + i;
                ws.Cell(Row, 2).Value = (item.MEMBER_ID.ToString() == null) ? "'" : "'" + item.MEMBER_ID.ToString();
                ws.Cell(Row, 3).Value = (item.MEMBER_NAME == null) ? "'" : "'" + item.MEMBER_NAME.ToString();
                ws.Cell(Row, 4).Value = (item.REDEEM_DATE == null) ? "'" : "'" + item.REDEEM_DATE.ToString();
                ws.Cell(Row, 5).Value = "'" + cat_name;
                ws.Cell(Row, 6).Value = (item.REWARD_CODE == null) ? "'" : "'" + item.REWARD_CODE.ToString();
                ws.Cell(Row, 7).Value = (item.NAME_TH == null) ? "'" : "'" + item.NAME_TH.ToString();
                ws.Cell(Row, 8).Value = (item.QTY == 0) ? "'0" : "'" + item.QTY.ToString("#,##0");
                ws.Cell(Row, 9).Value =  (item.POINT == 0) ? "'0" : "'" + item.POINT.ToString("#,##0");
                ws.Cell(Row, 10).Value = (item.POINT_REDEEM == 0) ? "'0" : "'" + item.POINT_REDEEM.ToString("#,##0");
                ws.Cell(Row, 11).Value = (item.REWARD_PRICE == 0) ? "'0" : "'" + Math.Floor(item.REWARD_PRICE).ToString("#,##0.00");
                ws.Cell(Row, 12).Value = (item.RECEIVE_NAME == null) ? "'" : "'" + item.RECEIVE_NAME.ToString();
                ws.Cell(Row, 13).Value = (item.COURIER_TYPE_NAME == null) ? "'" : "'" + item.COURIER_TYPE_NAME.ToString();
                ws.Cell(Row, 14).Value = (item.REMARK == null) ? "'" : "'" + item.REMARK.ToString();

                ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                Row++;
                old_cat = (item.CATEGORY_NAME == null || item.CATEGORY_NAME.ToString() == "") ? "NULL" : item.CATEGORY_NAME;
                sum_qty += item.QTY;
                sum_price += (decimal)item.REWARD_PRICE;
                sum_point += (int)item.POINT_REDEEM;
            }
            //SET Total Summary
            //ws.Range("A6:A6").Value = "จำนวนผู้แนะนำ : " + data_report.countReferer.ToString("#,##0") + " ราย "; ws.Range("D6:D6").Value = "จำนวนผู้ถูกแนะนำ : " + data_report.countReferee.ToString("#,##0") + " ราย ";
            ws.Range("A6:A6").Value = "จำนวนสมาชิกที่แลกของรางวัล  : "; ws.Range("D6:D6").Value = countMem.ToString("#,##0"); ws.Range("E6:E6").Value = "ราย";
            ws.Range("A7:A7").Value = "คะแนนที่ใช้แลก  : "; ws.Range("D7:D7").Value = "'" + (sum_point).ToString("#,##0"); ws.Range("E7:E7").Value = "คะแนน";
            ws.Range("A8:A8").Value = "มูลค่าของรางวัล  : "; ws.Range("D8:D8").Value = "'" + Math.Floor(sum_price).ToString("#,##0.00"); ws.Range("E8:E8").Value = "บาท";
            ws.Range("D6:D8").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A6:M10").Style.Font.Bold = true;

            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวมทั้งหมด";
            ws.Range("H" + endRow + ":H" + endRow).Value = "'" + sum_qty.ToString("#,##0");
            ws.Range("J" + endRow + ":J" + endRow).Value = "'" + sum_point.ToString("#,##0");
            ws.Range("K" + endRow + ":K" + endRow).Value = "'" + sum_price.ToString("#,##0.00");
            ws.Range("A" + endRow + ":G" + endRow).Row(1).Merge();
            ws.Range("A" + endRow + ":G" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":N" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":N" + endRow).Style.Font.FontSize = 12;
            ws.Range("A" + startRow + ":N" + endRow).Style.Font.FontName = "Cambria";
            ws.Range("A" + startRow + ":N" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":N" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":N" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + startRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A10:N10").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + endRow + ":N" + endRow).Style.Font.Bold = true;
            ws.Range("H" + startRow + ":K" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

            ws.Column("A").Width = 10;
            ws.Column("B").Width = 15;
            ws.Column("C").Width = 25;
            ws.Column("D").Width = 15;
            ws.Column("E").Width = 15;
            ws.Column("F").Width = 15;
            ws.Column("G").Width = 40;
            ws.Columns("H:K").Width = 15;
            ws.Columns("L:N").Width = 35;
            Row++;

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h);
        } //Report Redeem Reward By Detail 4 

        public void ReportRedeemRewardOrderByExcel(string report_h, string start_date, string end_date)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            var data_report = GetDataSummaryRedeem(1, start_date, end_date);
            //data_report = GetDataReportRedeem(start_date, end_date, "5", "A");
            var ws = workbook.Worksheets.Add("Redeem Reward(สรุป)");
            //SET Header
            ws.Cell(2, 1).Value = report_h;
            ws.Cell(3, 1).Value = "ช่วงวันที่แลก : " + start_date + " - " + end_date;
            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 20;
            ws.Range("A3:M11").Style.Font.FontSize = 14;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;

            //SET Image
            //var imagePath = Server.MapPath("~");
            //imagePath = imagePath + @"\Content\img\new-logo.png";
            //var image = ws.AddPicture(imagePath)
            //    .MoveTo(ws.Cell("L2").Address)
            //    .Scale(0.8); // optional: resize picture

            //SET Total Summary
            //ws.Range("A6:A6").Value = "จำนวนผู้แนะนำ : " + data_report.countReferer.ToString("#,##0") + " ราย "; ws.Range("D6:D6").Value = "จำนวนผู้ถูกแนะนำ : " + data_report.countReferee.ToString("#,##0") + " ราย ";
            ws.Range("A6:A6").Value = "จำนวนสมาชิกที่แลกของรางวัล  : "; ws.Range("C6:C6").Value = data_report.NUM_MEMBER.ToString("#,##0"); ws.Range("D6:D6").Value = "ราย";
            ws.Range("A7:A7").Value = "คะแนนที่ใช้แลก  : "; ws.Range("C7:C7").Value = "'" + (data_report.POINT_REDEEM).ToString("#,##0"); ws.Range("D7:D7").Value = "คะแนน";
            ws.Range("A8:A8").Value = "มูลค่าของรางวัล  : "; ws.Range("C8:C8").Value = "'" + Math.Floor(data_report.REWARD_PRICE).ToString("#,##0.00"); ws.Range("D8:D8").Value = "บาท";
            ws.Range("C6:C8").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A6:D8").Style.Font.Bold = true;

            //SET Logo
            //xlWorkSheet.Shapes.AddPicture("C:\\Shashi_keshar.JPG", MsoTriState.msoFalse, MsoTriState.msoCTrue, 50, 50, 300, 45);

            //SET HEADER COLUMN
            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = " ลำดับที่";
            ws.Cell(Row, 2).Value = "รหัสของรางวัล";
            ws.Cell(Row, 3).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 4).Value = "Category";
            ws.Cell(Row, 5).Value = "จำนวน";
            ws.Cell(Row, 6).Value = "คะแนน/หน่วย";
            ws.Cell(Row, 7).Value = "มูลค่าของรางวัล";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Font.FontSize = 14;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Font.FontName = "Cambria";
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":G" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 0;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            var sum_qty = 0;
            var sum_price = 0;
            foreach (var item in data_report.ListReport)
            {
                i = i + 1;
                ws.Cell(Row, 1).Value = "'" + i;
                ws.Cell(Row, 2).Value = (item.REWARD_CODE == null) ? "'" : "'" + item.REWARD_CODE.ToString();
                ws.Cell(Row, 3).Value = (item.NAME_TH == null) ? "'" : "'" + item.NAME_TH.ToString();
                ws.Cell(Row, 4).Value = (item.CATEGORY_NAME == null) ? "'" : "'" + item.CATEGORY_NAME.ToString();
                ws.Cell(Row, 5).Value = (item.QTY.ToString() == null) ? "'" : "'" + item.QTY.ToString("#,##0");
                ws.Cell(Row, 6).Value = (item.POINT_REDEEM.ToString() == null || item.POINT_REDEEM.ToString() == "") ? "'" : "'" + (item.POINT_REDEEM).ToString("#,##0");
                ws.Cell(Row, 7).Value = (item.REWARD_PRICE.ToString() == null || item.REWARD_PRICE.ToString() == "") ? "'" : "'" + item.REWARD_PRICE.ToString("#,##0.00");

                ws.Range("A" + Row + ":A" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Range("B" + Row + ":B" + Row).Style.DateFormat.SetFormat("dd/MM/yyyy");
                //ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

                Row++;
                sum_qty += item.QTY;
                sum_price += (int)item.REWARD_PRICE;
            }


            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวม";
            ws.Range("E" + endRow + ":E" + endRow).Value = "'" + sum_qty.ToString("#,##0");
            ws.Range("G" + endRow + ":G" + endRow).Value = "'" + sum_price.ToString("#,##0.00");
            ws.Range("A" + endRow + ":D" + endRow).Row(1).Merge();
            ws.Range("A" + endRow + ":D" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;


            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":G" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":G" + endRow).Style.Font.FontSize = 12;
            ws.Range("A" + startRow + ":G" + endRow).Style.Font.FontName = "Cambria";
            ws.Range("A" + startRow + ":G" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":G" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":G" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("E" + startRow + ":G" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A" + endRow + ":G" + endRow).Style.Font.Bold = true;


            ws.Column("A").Width = 10;
            ws.Column("B").Width = 15;
            ws.Column("C").Width = 50;
            ws.Column("D").Width = 15;
            ws.Column("E").Width = 15;
            ws.Columns("F:G").Width = 15;

            Row++;
            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h);
        } //Report Redeem Status 5

        public void ReportRedeemRewardStatusExcel(string report_h, string start_date, string end_date, string status)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            DataReportBudget data_report = new DataReportBudget();
            data_report = GetDataReportRedeemStatus(1, start_date, end_date, status);
            var ws = workbook.Worksheets.Add("Redeem Status (Detail)");
            //SET Header
            ws.Cell(2, 1).Value = report_h;
            ws.Cell(3, 1).Value = "ช่วงวันที่แลก : " + start_date + " - " + end_date;


            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 16;
            ws.Range("A3:M11").Style.Font.FontSize = 11;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;

            ws.Range("A7:A7").Value = "จำนวนสมาชิกที่แลกของรางวัล "; ws.Range("B7:B7").Value = (data_report.countOfMember).ToString("#,##0"); ws.Range("C7:C7").Value = "ราย";
            ws.Range("A8:A8").Value = "คะแนนที่ใช้แลก "; ws.Range("B8:B8").Value = "'" + data_report.countOfPoint.ToString("#,##0"); ws.Range("C8:C8").Value = "คะแนน";

            ws.Range("B7:B8").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A7:P8").Style.Font.Bold = true;
            if (status == "2")
            {
                int Row = ws.LastRowUsed().RowNumber() + 2;
                ws.Cell(Row, 1).Value = "ลำดับที่";
                ws.Cell(Row, 2).Value = "รหัสสมาชิก";
                ws.Cell(Row, 3).Value = "ชื่อสมาชิก";
                ws.Cell(Row, 4).Value = " ประเภทสมาชิก";
                ws.Cell(Row, 5).Value = "วันที่แลก";
                ws.Cell(Row, 6).Value = "รหัสของรางวัล";
                ws.Cell(Row, 7).Value = "ชื่อของรางวัล";
                ws.Cell(Row, 8).Value = " จำนวน";
                ws.Cell(Row, 9).Value = "คะแนนที่ใช้แลก";
                ws.Cell(Row, 10).Value = "สถาะการจัดส่ง";
                ws.Cell(Row, 11).Value = "วันที่ส่งของ";
                ws.Cell(Row, 12).Value = "จำนวนวันหลังจากแลก(ที่ยังไม่ได้จัดส่งแล้ว)";
                ws.Cell(Row, 13).Value = "ผู้รับสินค้า";
                ws.Cell(Row, 14).Value = "การจัดส่ง";
                ws.Cell(Row, 15).Value = "หมายเหตุ";

                var lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var lastRows = ws.LastRowUsed().RowNumber();
                firstColomn_sum = lastRows;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontSize = 11;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.Bold = true;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontName = "Cambria";
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


                //SET DETAIL
                Row += 1;
                var i = 0;
                lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var startRow = ws.LastRowUsed().RowNumber() + 1;
                List<ListReportBudget> data_budget = data_report.DataReport;
                var sum_qty = 0;
                var sum_point = 0;
                foreach (var item in data_budget)
                {
                    i = i + 1;
                    ws.Cell(Row, 1).Value = "'" + i;
                    ws.Cell(Row, 2).Value = (item.member_id.ToString() == "") ? "'" : "'" + item.member_id.ToString();
                    ws.Cell(Row, 3).Value = (item.name == null) ? "'" : "'" + item.name.ToString() + " " + item.last_name;
                    ws.Cell(Row, 4).Value = (item.tier_code == null) ? "'" : "'" + item.tier_code.ToString();
                    ws.Cell(Row, 5).Value = (item.redeem_date == null) ? "'" : "'" + item.redeem_date.ToString();
                    ws.Cell(Row, 6).Value = (item.reward_code == null) ? "'" : "'" + item.reward_code.ToString();
                    ws.Cell(Row, 7).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                    ws.Cell(Row, 8).Value = (item.qty == 0) ? "'0" : "'" + item.qty.ToString("#,##0");
                    ws.Cell(Row, 9).Value = (item.point_redeem == 0) ? "'0" : "'" + Math.Floor(item.point_redeem).ToString("#,##0");
                    ws.Cell(Row, 10).Value = (item.status_redeem == null) ? "'" : "'" + item.status_redeem.ToString();
                    ws.Cell(Row, 11).Value = (item.delivery_date == null) ? "'" : "'" + item.delivery_date.ToString();
                    ws.Cell(Row, 12).Value = (item.overdue_date.ToString() == "" || item.overdue_date == 0) ? "'" : "'" + item.overdue_date.ToString();
                    ws.Cell(Row, 13).Value = (item.receive_by == null) ? "'" : "'" + item.receive_by.ToString();
                    ws.Cell(Row, 14).Value = (item.delivery == null) ? "'" : "'" + item.delivery.ToString();
                    ws.Cell(Row, 15).Value = (item.remark == null) ? "'" : "'" + item.remark.ToString();

                    ws.Range("A" + Row + ":A" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    Row++;
                    sum_qty += item.qty;
                    sum_point += (int)Math.Floor(item.point_redeem);
                }

                var endRow = ws.LastRowUsed().RowNumber() + 1;
                // รวมทั้งหมด
                ws.Range("A" + endRow + ":A" + endRow).Value = "รวมทั้งหมด";
                ws.Range("H" + endRow + ":H" + endRow).Value = sum_qty.ToString("#,##0");
                ws.Range("I" + endRow + ":I" + endRow).Value = sum_point.ToString("#,##0");


                //จัดขนาด Font และ ตีเส้นตาราง
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontSize = 11;
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontName = "Cambria";
                ws.Range("A" + startRow + ":O" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + startRow + ":O" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + endRow + ":O" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + endRow + ":O" + endRow).Style.Font.Bold = true;
                ws.Range("H" + startRow + ":I" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Range("L" + startRow + ":L" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                ws.Column("A").Width = 30;
                ws.Column("B").Width = 10;
                ws.Column("C").Width = 25;
                ws.Column("D").Width = 15;
                ws.Column("E").Width = 15;
                ws.Column("F").Width = 15;
                ws.Column("G").Width = 40;
                ws.Columns("H:M").Width = 20;
                ws.Columns("N:O").Width = 35;
                Row++;
            }

            else if (status == "1")
            {
                //SET HEADER COLUMN
                int Row = ws.LastRowUsed().RowNumber() + 2;
                ws.Cell(Row, 1).Value = "ลำดับที่";
                ws.Cell(Row, 2).Value = "รหัสสมาชิก";
                ws.Cell(Row, 3).Value = "ชื่อสมาชิก";
                ws.Cell(Row, 4).Value = " ประเภทสมาชิก";
                ws.Cell(Row, 5).Value = "วันที่แลก";
                ws.Cell(Row, 6).Value = "รหัสของรางวัล";
                ws.Cell(Row, 7).Value = "ชื่อของรางวัล";
                ws.Cell(Row, 8).Value = " จำนวน";
                ws.Cell(Row, 9).Value = "คะแนนที่ใช้แลก";
                ws.Cell(Row, 10).Value = "สถาะการจัดส่ง";
                ws.Cell(Row, 11).Value = "วันที่ส่ง";
                ws.Cell(Row, 12).Value = "จำนวนวันหลังจากแลก(ที่จัดส่งแล้ว)";
                ws.Cell(Row, 13).Value = "ผู้รับสินค้า";
                ws.Cell(Row, 14).Value = "การจัดส่ง";
                ws.Cell(Row, 15).Value = "หมายเหตุ";

                var lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var lastRows = ws.LastRowUsed().RowNumber();
                firstColomn_sum = lastRows;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontSize = 11;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.Bold = true;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontName = "Cambria";
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


                //SET DETAIL
                Row += 1;
                var i = 0;
                lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var startRow = ws.LastRowUsed().RowNumber() + 1;
                List<ListReportBudget> data_budget = data_report.DataReport;
                var sum_qty = 0;
                var sum_point = 0;
                foreach (var item in data_budget)
                {
                    i = i + 1;
                    ws.Cell(Row, 1).Value = "'" + i;
                    ws.Cell(Row, 2).Value = (item.member_id.ToString() == "") ? "'" : "'" + item.member_id.ToString();
                    ws.Cell(Row, 3).Value = (item.name == null) ? "'" : "'" + item.name.ToString() + " " + item.last_name;
                    ws.Cell(Row, 4).Value = (item.tier_code == null) ? "'" : "'" + item.tier_code.ToString();
                    ws.Cell(Row, 5).Value = (item.redeem_date == null) ? "'" : "'" + item.redeem_date.ToString();
                    ws.Cell(Row, 6).Value = (item.reward_code == null) ? "'" : "'" + item.reward_code.ToString();
                    ws.Cell(Row, 7).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                    ws.Cell(Row, 8).Value = (item.qty == 0) ? "'0" : "'" + item.qty.ToString("#,##0");
                    ws.Cell(Row, 9).Value = (item.point_redeem == 0) ? "'0" : "'" + Math.Floor(item.point_redeem).ToString("#,##0");
                    ws.Cell(Row, 10).Value = (item.status_redeem == null) ? "'" : "'" + item.status_redeem.ToString();
                    ws.Cell(Row, 11).Value = (item.delivery_date == null) ? "'" : "'" + item.delivery_date.ToString();
                    ws.Cell(Row, 12).Value = (item.dayofdelivery.ToString() == "" || item.dayofdelivery == 0) ? "'" : "'" + item.dayofdelivery.ToString();
                    ws.Cell(Row, 13).Value = (item.receive_by == null) ? "'" : "'" + item.receive_by.ToString();
                    ws.Cell(Row, 14).Value = (item.delivery == null) ? "'" : "'" + item.delivery.ToString();
                    ws.Cell(Row, 15).Value = (item.remark == null) ? "'" : "'" + item.remark.ToString();

                    ws.Range("A" + Row + ":A" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    Row++;
                    sum_qty += item.qty;
                    sum_point += (int)Math.Floor(item.point_redeem);
                }

                var endRow = ws.LastRowUsed().RowNumber() + 1;
                // รวมทั้งหมด
                ws.Range("A" + endRow + ":A" + endRow).Value = "รวมทั้งหมด";
                ws.Range("H" + endRow + ":H" + endRow).Value = sum_qty.ToString("#,##0");
                ws.Range("I" + endRow + ":I" + endRow).Value = sum_point.ToString("#,##0");


                //จัดขนาด Font และ ตีเส้นตาราง
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontSize = 11;
                ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontName = "Cambria";
                ws.Range("A" + startRow + ":O" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + startRow + ":O" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + endRow + ":O" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + endRow + ":O" + endRow).Style.Font.Bold = true;
                ws.Range("H" + startRow + ":I" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Range("L" + startRow + ":L" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                ws.Column("A").Width = 30;
                ws.Column("B").Width = 10;
                ws.Column("C").Width = 25;
                ws.Column("D").Width = 15;
                ws.Column("E").Width = 15;
                ws.Column("F").Width = 15;
                ws.Column("G").Width = 40;
                ws.Columns("H:M").Width = 20;
                ws.Columns("N:O").Width = 35;
                Row++;
            }
            else
            {
                //SET HEADER COLUMN
                int Row = ws.LastRowUsed().RowNumber() + 2;
                ws.Cell(Row, 1).Value = "ลำดับที่";
                ws.Cell(Row, 2).Value = "รหัสสมาชิก";
                ws.Cell(Row, 3).Value = "ชื่อสมาชิก";
                ws.Cell(Row, 4).Value = " ประเภทสมาชิก";
                ws.Cell(Row, 5).Value = "วันที่แลก";
                ws.Cell(Row, 6).Value = "รหัสของรางวัล";
                ws.Cell(Row, 7).Value = "ชื่อของรางวัล";
                ws.Cell(Row, 8).Value = " จำนวน";
                ws.Cell(Row, 9).Value = "คะแนนที่ใช้แลก";
                ws.Cell(Row, 10).Value = "สถาะการจัดส่ง";
                ws.Cell(Row, 11).Value = "วันที่ส่ง";
                ws.Cell(Row, 12).Value = "จำนวนวันหลังจากแลก(ที่จัดส่งแล้ว)";
                ws.Cell(Row, 13).Value = "จำนวนวันหลังจากแลก(ที่ยังไม่ได้จัดส่ง)";
                ws.Cell(Row, 14).Value = "ผู้รับสินค้า";
                ws.Cell(Row, 15).Value = "การจัดส่ง";
                ws.Cell(Row, 16).Value = "หมายเหตุ";

                var lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var lastRows = ws.LastRowUsed().RowNumber();
                firstColomn_sum = lastRows;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Font.FontSize = 11;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Font.Bold = true;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Font.FontName = "Cambria";
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + (lastRows) + ":P" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


                //SET DETAIL
                Row += 1;
                var i = 0;
                lastColumnName = ws.LastColumnUsed().ColumnLetter();
                var startRow = ws.LastRowUsed().RowNumber() + 1;
                List<ListReportBudget> data_budget = data_report.DataReport;
                var sum_qty = 0;
                var sum_point = 0;
                foreach (var item in data_budget)
                {
                    i = i + 1;
                    ws.Cell(Row, 1).Value = "'" + i;
                    ws.Cell(Row, 2).Value = (item.member_id.ToString() == "") ? "'" : "'" + item.member_id.ToString();
                    ws.Cell(Row, 3).Value = (item.name == null) ? "'" : "'" + item.name.ToString() + " " + item.last_name;
                    ws.Cell(Row, 4).Value = (item.tier_code == null) ? "'" : "'" + item.tier_code.ToString();
                    ws.Cell(Row, 5).Value = (item.redeem_date == null) ? "'" : "'" + item.redeem_date.ToString();
                    ws.Cell(Row, 6).Value = (item.reward_code == null) ? "'" : "'" + item.reward_code.ToString();
                    ws.Cell(Row, 7).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                    ws.Cell(Row, 8).Value = (item.qty == 0) ? "'0" : "'" + item.qty.ToString("#,##0");
                    ws.Cell(Row, 9).Value = (item.point_redeem == 0) ? "'0" : "'" + Math.Floor(item.point_redeem).ToString("#,##0");
                    ws.Cell(Row, 10).Value = (item.status_redeem == null) ? "'" : "'" + item.status_redeem.ToString();
                    ws.Cell(Row, 11).Value = (item.delivery_date == null) ? "'" : "'" + item.delivery_date.ToString();
                    ws.Cell(Row, 12).Value = (item.dayofdelivery.ToString() == "" || item.dayofdelivery == 0) ? "'" : "'" + item.dayofdelivery.ToString();
                    ws.Cell(Row, 13).Value = (item.overdue_date.ToString() == "" || item.overdue_date == 0) ? "'" : "'" + item.overdue_date.ToString();
                    ws.Cell(Row, 14).Value = (item.receive_by == null) ? "'" : "'" + item.receive_by.ToString();
                    ws.Cell(Row, 15).Value = (item.delivery == null) ? "'" : "'" + item.delivery.ToString();
                    ws.Cell(Row, 16).Value = (item.remark == null) ? "'" : "'" + item.remark.ToString();

                    ws.Range("A" + Row + ":A" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    Row++;
                    sum_qty += item.qty;
                    sum_point += (int)Math.Floor(item.point_redeem);
                }

                var endRow = ws.LastRowUsed().RowNumber() + 1;
                // รวมทั้งหมด
                ws.Range("A" + endRow + ":A" + endRow).Value = "รวมทั้งหมด";
                ws.Range("H" + endRow + ":H" + endRow).Value = sum_qty.ToString("#,##0");
                ws.Range("I" + endRow + ":I" + endRow).Value = sum_point.ToString("#,##0");


                //จัดขนาด Font และ ตีเส้นตาราง
                ws.Range("A" + startRow + ":P" + endRow).Style.Font.FontColor = XLColor.Black;
                ws.Range("A" + startRow + ":P" + endRow).Style.Font.FontSize = 11;
                ws.Range("A" + startRow + ":P" + endRow).Style.Font.FontName = "Cambria";
                ws.Range("A" + startRow + ":P" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + startRow + ":P" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A" + endRow + ":P" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
                ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A" + endRow + ":P" + endRow).Style.Font.Bold = true;
                ws.Range("H" + startRow + ":I" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Range("L" + startRow + ":M" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                ws.Column("A").Width = 30;
                ws.Column("B").Width = 10;
                ws.Column("C").Width = 25;
                ws.Column("D").Width = 15;
                ws.Column("E").Width = 15;
                ws.Column("F").Width = 15;
                ws.Column("G").Width = 40;
                ws.Columns("H:L").Width = 15;
                ws.Columns("M:N").Width = 20;
                ws.Columns("O:P").Width = 35;
                Row++;
            }

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h);
        } //Report สถานะการแลกของรางวัล

        public class DataReportBudget
        {
            public int countOfMember { get; set; }
            public int member_all { get; set; }
            public int member_has_point { get; set; }
            public int point_all { get; set; }
            public int countOfPoint { get; set; }
            public decimal countOfPriceReward { get; set; }
            public int countOfGR { get; set; }
            public int countOfGP { get; set; }
            public int countOfPL { get; set; }
            public int countOfPLEMP { get; set; }
            public List<ListReportBudget> DataReport { get; set; }
            public List<SBUBudget> DataSumSBU { get; set; }

        }
        public class SBUBudget
        {
            public string sbu { get; set; }
            public decimal tot_budget { get; set; }
        }
        public class ListReportBudget
        {
            public string cat_name { get; set; }
            public string redeem_no { get; set; }
            public string member_id { get; set; }
            public string name { get; set; }
            public string last_name { get; set; }
            public string tier_code { get; set; }
            public string reward_code { get; set; }
            public string reward_name { get; set; }
            public string redeem_date { get; set; }
            public int qty { get; set; }
            public decimal point_redeem { get; set; }
            public decimal reward_price { get; set; }
            public decimal reward_price_sbu { get; set; }
            public string delivery { get; set; }
            public string remark { get; set; }
            public string sbu { get; set; }
            public string sbu_id { get; set; }
            public decimal tot_point { get; set; }
            public int tot_member { get; set; }
            public decimal tot_budget { get; set; }
            public string referee { get; set; }
            public string rfr_cleansing { get; set; }
            public string status_redeem { get; set; }
            public string delivery_date { get; set; }
            public string receive_date { get; set; }
            public int dayofdelivery { get; set; }
            public int overdue_date { get; set; }
            public int point { get; set; }
            public string receive_by { get; set; }
            public decimal price_for_budget { get; set; }
            public string project_name { get; set; }
            public string unit_number { get; set; }
            public int point_per_item { get; set; }
            public string POINT_TYPE_CODE { get; set; }
            public string ACTIVITY_NAME { get; set; }
            public string ADJUST_POINT_NAME { get; set; }
        }
        public class ReportBudgetParameter
        {
            public string budget_id { get; set; }
            public string type_report { get; set; }
            public string fromType { get; set; }
            public string start_date { get; set; }
            public string end_date { get; set; }
            public string status_redeem { get; set; }
        }
        public class ParamReportRedeem
        {
            public int reportID { get; set; }
            public string start_date { get; set; }
            public string end_date { get; set; }
            public string status { get; set; }
        }
        public class DataSummaryRedeem
        {
            public int QTY { get; set; }
            public int POINT { get; set; }
            public int POINT_REDEEM { get; set; }
            public decimal REWARD_PRICE { get; set; }
            public int NUM_MEMBER { get; set; }
            public List<DataReportRedeem> ListReport { get; set; }
        }
        public class DataReportRedeem
        {
            public int MEMBER_ID { get; set; }
            public string MEMBER_NAME { get; set; }
            public string REDEEM_DATE { get; set; }
            public string CATEGORY_NAME { get; set; }
            public string REWARD_CODE { get; set; }
            public string NAME_TH { get; set; }
            public int QTY { get; set; }
            public int POINT { get; set; }
            public int POINT_REDEEM { get; set; }
            public decimal REWARD_PRICE { get; set; }
            public string RECEIVE_NAME { get; set; }
            public string COURIER_TYPE_NAME { get; set; }
            public string REMARK { get; set; }
            public string DELIVERY_NO { get; set; }
            public string STATUS_REDEEM { get; set; }
            public string DELIVERY_DATE { get; set; }
            public string RECEIVE_DATE { get; set; }
            public string RECEIVE_BY { get; set; }
            public int? DAYOFDELIVERY { get; set; }
            public int? OVERT_DUEDATE { get; set; }

        }

        public static List<DataReportRedeem> GetDataReportRedeem(int reportID, string start_date, string end_date)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["EndPoint"].ToString() + "/Report/getListReportRedeem";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new List<DataReportRedeem>();
            try
            {
                var dataRequest = new ParamReportRedeem();
                dataRequest.reportID = reportID;
                dataRequest.start_date = start_date;
                dataRequest.end_date = end_date;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<List<DataReportRedeem>>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Redeem

        public static DataReportBudget GetDataReportRedeemStatus(int reportID, string start_date, string end_date, string status)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["EndPoint"].ToString() + "/Report/getListReportRedeemStatus";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new DataReportBudget();
            try
            {
                var dataRequest = new ParamReportRedeem();
                dataRequest.reportID = reportID;
                dataRequest.start_date = start_date;
                dataRequest.end_date = end_date;
                dataRequest.status = status;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<DataReportBudget>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Redeem Status

        public static DataSummaryRedeem GetDataSummaryRedeem(int reportID, string start_date, string end_date)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["EndPoint"].ToString() + "/Report/ListSummaryReportRedeem";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new DataSummaryRedeem();
            try
            {
                var dataRequest = new ParamReportRedeem();
                dataRequest.reportID = reportID;
                dataRequest.start_date = start_date;
                dataRequest.end_date = end_date;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<DataSummaryRedeem>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Redeem
        public static DataReportBudget GetDataReportBudget(string budget_id, string report_type)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["EndPoint"].ToString() + "reportbudget/report_budget";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("APP_TOKEN", "d750df98-7fbf-49cc-a3b2-0b3611b986ac");
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new DataReportBudget();
            try
            {
                var dataRequest = new ReportBudgetParameter();
                dataRequest.budget_id = budget_id;
                dataRequest.type_report = report_type;
                dataRequest.fromType = "B";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<DataReportBudget>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Budget

        [HttpPost]
        public string GetDataReportRedeemStatusGraph(string start_date, string end_date, string status, string fromType)
        {
            string rawXml = "{" +
                 "\"start_date\": \"" + start_date + "\"," +
                 "\"end_date\": \"" + end_date + "\"," +
                 "\"status\": \"" + status + "\"," +
                 "\"fromType\": \"B\"" +
             "}";

            var client = new RestClient(EndPoint + "/Report/getListReportRedeemStatusGraph");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}