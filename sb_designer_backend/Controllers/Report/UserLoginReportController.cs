﻿using ClosedXML.Excel;
using ExporHTMLTOFiles;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Report
{
    public class UserLoginReportController : BaseController
    {
        // GET: UserLoginReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDetail(string start_date, string end_date)
        {
            string rawXml = "{" +
                "\"Start_Date\": \"" + start_date + "\"," +
                "\"End_Date\":\"" + end_date + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/get_log_login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);

            var items = new Dictionary<string, Dictionary<string, object>>();
            var log_date_time = "";
            foreach (var user in callback["items"])

            {
                var log_date = user["LogDate"].ToString().Split(' ');
                //
                string array_key = user["Cust_ID"] + "_" + log_date[0];

                if (items.ContainsKey(array_key))
                {
                    if (log_date_time != user["LogDate"].ToString().Substring(0, 16) || log_date_time == "")
                    {
                        log_date_time = user["LogDate"].ToString().Substring(0, 16);
                        items[array_key]["QTY"] = Int32.Parse(items[array_key]["QTY"].ToString()) + 1;
                    }
                    else
                    {
                        log_date_time = user["LogDate"].ToString().Substring(0, 16);
                    }
                }
                else
                {
                    log_date_time = user["LogDate"].ToString().Substring(0, 16);
                    items.Add(array_key, new Dictionary<string, object>()
                    {
                        { "Cust_ID", user["Cust_ID"].ToString() },
                        { "Name", user["Name"].ToString() + " " + user["Surname"].ToString() },
                        { "LogDate", user["LogDate"].ToString()},
                        { "VIP", user["VIP"].ToString() },
                        { "QTY", 1 },
                    });
                }
            }

            List<object> data = new List<object>();
            foreach (var item in items)
            {
                data.Add(item.Value);
            }

            return Json(data);
        }

        public void ExportExcel(string start_date, string end_date)
        {
            //call api
            string rawXml = "{" +
                "\"Start_Date\": \"" + start_date + "\"," +
                "\"End_Date\":\"" + end_date + "\"" +
                "}";
            var client = new RestClient(EndPoint + "/get_log_login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);
            //loop for count login amount
            var items = new Dictionary<string, Dictionary<string, object>>();
            var log_date_time = "";
            List<string> countDistinctMember = new List<string>();
            int totalMember = 0;
            foreach (var user in callback["items"])
            {
                if (!countDistinctMember.Contains(user["Cust_ID"].ToString()))
                {
                    totalMember++;
                }
                countDistinctMember.Add(user["Cust_ID"].ToString());
                var log_date = user["LogDate"].ToString().Split(' ');
                //
                string array_key = user["Cust_ID"] + "_" + log_date[0];

                if (items.ContainsKey(array_key))
                {
                    if(log_date_time != user["LogDate"].ToString().Substring(0, 16) || log_date_time == "")
                    {
                        log_date_time = user["LogDate"].ToString().Substring(0, 16);
                        items[array_key]["QTY"] = Int32.Parse(items[array_key]["QTY"].ToString()) + 1;
                    }
                    else
                    {
                        log_date_time = user["LogDate"].ToString().Substring(0, 16);
                    }
                }
                else
                {
                    log_date_time = user["LogDate"].ToString().Substring(0, 16);
                    items.Add(array_key, new Dictionary<string, object>()
                    {
                        { "Cust_ID", user["Cust_ID"].ToString() },
                        { "Name", user["Name"].ToString() + " " + user["Surname"].ToString() },
                        { "LogDate", user["LogDate"].ToString() },
                        { "VIP", user["VIP"].ToString() },
                        { "QTY", 1 },
                    });
                }
            }

            //generate excel
            ExportToFile ex = new ExportToFile();
            XLWorkbook wb = new XLWorkbook();
            var worksheet = wb.Worksheets.Add("ผู้ใช้งานระบบ"); 
            var imagePath = Server.MapPath("~");
            imagePath = imagePath + @"\Content\img\new-logo.png";
            var image = worksheet.AddPicture(imagePath)
                .MoveTo(worksheet.Cell("F1").Address) 
                .Scale(0.4) ; // optional: resize picture
            worksheet.Cell("A1").Value = "รายงาน : รายงานสมาชิกผู้ใช้งานระบบ";
            worksheet.Range("A1:E1").Row(1).Merge();
            worksheet.Cell("A2").Value = (start_date == "" && end_date == "") ? "ช่วงวันที่ : ทั้งหมด" : "ช่วงวันที่ : " + start_date + " - " + end_date;
            //worksheet.Range("A2:E2").Row(1).Merge();
            ////worksheet.Cell("A3").Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            //worksheet.Range("A3:E3").Row(1).Merge();
            ex.setHeaderExcel(worksheet);
            var seq = 0;
            var row_amount = 5;
            var header = row_amount + 1;
            var row = header + 1;
            var total = 0;

            worksheet.Cell("A" + header).Value = "No.";
            worksheet.Cell("B" + header).Value = "รหัสสมาชิก";
            worksheet.Cell("C" + header).Value = "ชื่อสมาชิก";
            worksheet.Cell("D" + header).Value = "ระดับ";
            worksheet.Cell("E" + header).Value = "วันที่";
            worksheet.Cell("F" + header).Value = "จำนวนครั้ง";

            ex.setTitleExcel(worksheet);
            foreach (var item in items)
            {
                seq++;
                worksheet.Cell("A" + row).Value = seq;
                foreach (var data in item.Value)
                {
                    if (data.Key == "Cust_ID")
                    {
                        worksheet.Cell("B" + row).Value = data.Value;
                    }
                    else if (data.Key == "Name")
                    {
                        worksheet.Cell("C" + row).Value = data.Value;
                    }
                    else if (data.Key == "VIP")
                    { 
                        worksheet.Cell("D" + row).Value = data.Value;
                        /* if (data.Value == "")
                         {
                             worksheet.Cell("D" + row).Value = "";
                         }
                         else
                         {
                             worksheet.Cell("D" + row).Value = "VIP";
                         }*/

                    }
                    else if (data.Key == "LogDate")
                    {
                        string[] date = data.Value.ToString().Split(' ');
                        worksheet.Cell("E" + row).Value = date[0];
                    }
                    else if (data.Key == "QTY")
                    {
                        worksheet.Cell("F" + row).Value = data.Value;

                        total += (int)data.Value;
                    }
                }
                row++;
            }
            worksheet.Cell("A" + row).Value = "Total";
            worksheet.Cell("F" + row).Value = total;
            worksheet.Range("A" + row + ":E" + row).Row(1).Merge();

            worksheet.Cell("A" + row_amount).Value = "จำนวนผู้เข้าใช้ระบบทั้งสิ้น : " + totalMember + " ราย";
            worksheet.Range("A" + row_amount + ":E" + row_amount).Row(1).Merge();

            /*    for (var i = 1; i <= 6; i++)
            {
                worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.Red;
            }

            worksheet.Cell(row, 1).Style.Fill.BackgroundColor = XLColor.Red;
            worksheet.Cell(row, 6).Style.Fill.BackgroundColor = XLColor.Red;

            worksheet.Columns().AdjustToContents();


            string myName = Server.UrlEncode("Register_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();*/

            //จัดขนาด Font และ ตีเส้นตาราง
            ex.setFormatExcel(worksheet);
            //สั่งให้ Export ไฟล์
            ex.ExportExcel(wb, "ข้อมูลการเข้าใข้งานระบบ" + DateTime.Now.ToString("dd-MM-yyyy"));
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
    }
}