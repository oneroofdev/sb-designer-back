﻿using ClosedXML.Excel;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Report
{
    public class PointReportController : BaseController
    {
        // GET: PointReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDetail(string start_date, string end_date)
        {
            string rawXml = "{" +
                    "\"Start_Date\": \"" + start_date + "\"," +
                    "\"End_Date\":\"" + end_date + "\"" +
                    "}";

           // var client = new RestClient("http://sbdesignerclub.com:8080/api/get_point_by_date"); 
            var client = new RestClient(EndPoint + "/get_point_by_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);

            var array_key = 0;
            var items = new Dictionary<int, Dictionary<string, object>>();
            foreach (var user in callback["items"])
            {
                items.Add(array_key, new Dictionary<string, object>()
                {
                    { "Cust_Code", user["Cust_Code"].ToString() },
                    { "Name", user["Name"].ToString() + " " + user["Surname"].ToString() },
                    { "IS_VIP", user["IS_VIP"].ToString() },
                    { "BalancePoint", user["BalancePoint"].ToString() },
                    { "Mobile_Phone", user["Mobile_Phone"].ToString() },
                    { "Email", user["Email"].ToString() },
                    { "Sap_Code", user["Sap_Code"].ToString() },
                    { "TotalRefer", user["TotalRefer"].ToString() },
                    { "Process", user["Process"].ToString() },
                    { "Point", user["Point"].ToString() },
                    { "Amount", user["Amount"].ToString() },
                });
                array_key++;
            }

            List<object> data = new List<object>();
            foreach (var item in items)
            {
                data.Add(item.Value);
            }

            return Json(data);
        }

        public void ExportExcel(string start_date, string end_date)
        {
            //call api
            string rawXml = "{" +
                    "\"Start_Date\": \"" + start_date + "\"," +
                    "\"End_Date\":\"" + end_date + "\"" +
                    "}";

           // var client = new RestClient("http://sbdesignerclub.com:8080/api/get_point_by_date"); 
            var client = new RestClient(EndPoint + "/get_point_by_date");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var callback = JObject.Parse(response.Content);

            var array_key = 0;
            var items = new Dictionary<int, Dictionary<string, object>>();
            foreach (var user in callback["items"])
            {
                items.Add(array_key, new Dictionary<string, object>()
                {
                    { "Cust_Code", user["Cust_Code"].ToString() },
                    { "Name", user["Name"].ToString() + " " + user["Surname"].ToString() },
                    { "IS_VIP", user["IS_VIP"].ToString() },
                    { "BalancePoint", user["BalancePoint"].ToString() },
                    { "Mobile_Phone", user["Mobile_Phone"].ToString() },
                    { "Email", user["Email"].ToString() },
                    { "Sap_Code", user["Sap_Code"].ToString() },
                    { "TotalRefer", user["TotalRefer"].ToString() },
                    { "Process", user["Process"].ToString() },
                    { "Point", user["Point"].ToString() },
                    { "Amount", user["Amount"].ToString() },
                });
                array_key++;
            }

            //generate excel
            XLWorkbook wb = new XLWorkbook();
            var worksheet = wb.Worksheets.Add("Refer Friend");
            worksheet.Cell("A1").Value = "รายงานสรุปคะแนนปัจจุบันของสมาชิก";
            worksheet.Range("A1:E1").Row(1).Merge();
            worksheet.Cell("A2").Value = "ช่วงวันที่ : " + start_date + " - " + end_date;
            worksheet.Range("A2:E2").Row(1).Merge();
            worksheet.Cell("A3").Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            worksheet.Range("A3:E3").Row(1).Merge();

            var seq = 0;
            int merge_seq = 1;
            var row_amount = 6;
            var header = row_amount + 1;
            var row = header + 1;
            int total_point = 0;
            int total_amount = 0;
            var tmp_sap_id = "";
            int tmp_col_merge = 0;
            int tmp_col_merge2 = 0;
            var tmp_process = "";
            var tmp_process_sap_id = "";

            worksheet.Cell("A" + header).Value = "No.";
            worksheet.Cell("B" + header).Value = "รหัสสมาชิก";
            worksheet.Cell("C" + header).Value = "ชื่อสมาชิก";
            worksheet.Cell("D" + header).Value = "ระดับ";
            worksheet.Cell("E" + header).Value = "คะแนนรวมปัจจุบัน";
            worksheet.Cell("F" + header).Value = "Mobile Phone";
            worksheet.Cell("G" + header).Value = "Email";
            worksheet.Cell("H" + header).Value = "Refer SAP ID";
            worksheet.Cell("I" + header).Value = "ผู้ถูกแนะนำทั้งหมด";
            worksheet.Cell("J" + header).Value = "ช่องทางการได้คะแนน";
            worksheet.Cell("K" + header).Value = "คะแนนรวม";
            worksheet.Cell("L" + header).Value = "ยอดซื้อรวม";

            var count_cust = new Dictionary<string, int>();
            foreach (var item in items)
            {
                seq++;
                worksheet.Cell("A" + row).Value = merge_seq;
                foreach (var data in item.Value)
                {
                    if (data.Key == "Cust_Code")
                    {
                        worksheet.Cell("B" + row).Value = data.Value;
                    }
                    else if (data.Key == "Name")
                    {
                        worksheet.Cell("C" + row).Value = data.Value;
                    }
                    else if (data.Key == "IS_VIP")
                    {
                        if (data.Value == "")
                        {
                            worksheet.Cell("D" + row).Value = "";
                        }
                        else
                        {
                            worksheet.Cell("D" + row).Value = "VIP";
                        }
                    }
                    else if (data.Key == "BalancePoint")
                    {
                        worksheet.Cell("E" + row).Value = data.Value;
                    }
                    else if (data.Key == "Mobile_Phone")
                    {
                        worksheet.Cell("F" + row).Value = "'" + data.Value;
                    }
                    else if (data.Key == "Email")
                    {
                        worksheet.Cell("G" + row).Value = data.Value;
                    }
                    else if (data.Key == "Sap_Code")
                    {
                        if (tmp_sap_id != data.Value.ToString())
                        {
                            tmp_col_merge = row;
                            tmp_sap_id = data.Value.ToString();
                            worksheet.Cell("H" + row).Value = data.Value;
                            // horizontal alignment
                            worksheet.Cell("A" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            worksheet.Cell("B" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            worksheet.Cell("C" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                            worksheet.Cell("D" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            worksheet.Cell("E" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                            worksheet.Cell("F" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            worksheet.Cell("G" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                            worksheet.Cell("H" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            worksheet.Cell("I" + row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                            // prepare merge seq
                            merge_seq++;
                        }
                        else
                        {
                            worksheet.Range("A" + tmp_col_merge + ":A" + row).Column(1).Merge();
                            worksheet.Range("B" + tmp_col_merge + ":B" + row).Column(1).Merge();
                            worksheet.Range("C" + tmp_col_merge + ":C" + row).Column(1).Merge();
                            worksheet.Range("D" + tmp_col_merge + ":D" + row).Column(1).Merge();
                            worksheet.Range("E" + tmp_col_merge + ":E" + row).Column(1).Merge();
                            worksheet.Range("F" + tmp_col_merge + ":F" + row).Column(1).Merge();
                            worksheet.Range("G" + tmp_col_merge + ":G" + row).Column(1).Merge();
                            worksheet.Range("H" + tmp_col_merge + ":H" + row).Column(1).Merge();
                            worksheet.Range("I" + tmp_col_merge + ":I" + row).Column(1).Merge();
                            // vertical alignment
                            worksheet.Cell("A" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("B" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("C" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("D" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("E" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("F" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("G" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("H" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            worksheet.Cell("I" + tmp_col_merge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                        }
                        // count reference
                        if (count_cust.ContainsKey(data.Value.ToString()))
                        {
                            count_cust[data.Value.ToString()]++;
                        }
                        else
                        {
                            count_cust.Add(data.Value.ToString(), 1);
                        }
                    }
                    else if (data.Key == "TotalRefer")
                    {
                        worksheet.Cell("I" + row).Value = data.Value;
                    }
                    else if (data.Key == "Process")
                    {
                        if (tmp_sap_id != tmp_process_sap_id)
                        {
                            tmp_col_merge2 = row;
                            tmp_process_sap_id = tmp_sap_id;
                            tmp_process = data.Value.ToString();
                            worksheet.Cell("J" + row).Value = data.Value;
                        }
                        else
                        {
                            if (tmp_process != data.Value.ToString())
                            {
                                tmp_col_merge2 = row;
                                tmp_process = data.Value.ToString();
                                worksheet.Cell("J" + row).Value = data.Value;
                            }
                            else
                            {
                                worksheet.Range("J" + tmp_col_merge2 + ":J" + row).Column(1).Merge();
                                worksheet.Cell("J" + tmp_col_merge2).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                            }
                        }
                    }
                    else if (data.Key == "Point")
                    {
                        worksheet.Cell("K" + row).Value = data.Value;
                        worksheet.Cell(row, "K").Style.NumberFormat.Format = "#,##";
                        total_point += Convert.ToInt32(data.Value);
                    }
                    else if (data.Key == "Amount")
                    {
                        worksheet.Cell("L" + row).Value = data.Value;
                        worksheet.Cell(row, "L").Style.NumberFormat.Format = "#,##";
                        total_amount += Convert.ToInt32(data.Value);
                    }
                }
                row++;
            }
            worksheet.Cell("A" + row).Value = "Total";
            worksheet.Range("A" + row + ":J" + row).Row(1).Merge();
            worksheet.Cell("K" + row).Value = total_point;
            worksheet.Cell("L" + row).Value = total_amount;

            worksheet.Cell("A" + row_amount).Value = "จำนวนสมาชิก : " + count_cust.Count() + " ราย";
            worksheet.Range("A" + row_amount + ":C" + row_amount).Row(1).Merge();
            worksheet.Cell("D" + row_amount).Value = "คะแนนปัจจุบันทั้งสิ้น : " + total_point + " คะแนน";
            worksheet.Range("D" + row_amount + ":F" + row_amount).Row(1).Merge();

            for (var i = 1; i <= 12; i++)
            {
                if (i >= 10)
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(252, 248, 227);
                }
                else if (i >= 2)
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 237, 247);
                }
                else
                {
                    worksheet.Cell(header, i).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
                }
            }
            worksheet.Cell(row, 1).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
            worksheet.Cell(row, 11).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);
            worksheet.Cell(row, 12).Style.Fill.BackgroundColor = XLColor.FromArgb(245, 245, 245);

            worksheet.Columns().AdjustToContents();

            string myName = Server.UrlEncode("Point_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            MemoryStream stream = GetStream(wb);// The method is defined below
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment; filename=" + myName);
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
    }
}