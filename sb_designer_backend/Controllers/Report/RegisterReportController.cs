﻿using ClosedXML.Excel;
using ClosedXML.Extensions;
using ExporHTMLTOFiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Report
{
    public class RegisterReportController : BaseController
    {
        // GET: RegisterReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult VerifyMember(string Cust_ID, string verify_date, int? reason, string IS_Verify)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"Verify_Date\": \"" + verify_date + "\"," +
                "\"Reason\": \"" + reason + "\"," +
                "\"Verify_By\": \"" + username + "\"," +
                "\"IS_Verify\":\"" + IS_Verify + "\"" +
                "}";


            var client = new RestClient(EndPoint + "/verify_member");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var callback = JObject.Parse(response.Content);
            IDictionary<string, string> result = new Dictionary<string, string>();
            result["status"] = callback["success"].ToString();
            result["description"] = callback["description"].ToString();

            if (result["status"] == "True" && IS_Verify == "Y")
            {
                SendMail(Cust_ID);
                SendSMS(Cust_ID);
            }

            return Json(result);
        }
        public string getDataMember(string CustText, string StartDate, string EndDate, string IS_Verify, string IS_VIP)
        {
            string rawXml = "{" +
                "\"CustText\": \"" + CustText + "\"," +
                "\"StartDate\":\"" + StartDate + "\"," +
                "\"EndDate\":\"" + EndDate + "\"," +
                "\"IS_Verify\":\"" + IS_Verify + "\"," +
                "\"IS_VIP\":\"" + IS_VIP + "\"" +
                "}";
            var client = new RestClient(EndPoint + "/customer_backend");
            //var client = new RestClient("http://203.146.21.132:8080/vqsbservice/api/CustomerInfo");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //var callback = JObject.Parse(response.Content);
            return response.Content;
        }
        public void ExportExcel(string CustText, string StartDate, string EndDate, string IS_Verify, string IS_VIP)
        {
            try
            {
                ExportToFile ex = new ExportToFile();
                XLWorkbook wb = new XLWorkbook();
                var worksheet = wb.Worksheets.Add("Register");
                var imagePath = Server.MapPath("~");
                imagePath = imagePath + @"\Content\img\new-logo.png";
                var image = worksheet.AddPicture(imagePath)
                    .MoveTo(worksheet.Cell("L1").Address)
                    .Scale(0.4);
                worksheet.Cell("A1").Value = "รายงาน : การสมัครสมาชิก";
                worksheet.Cell("A2").Value = (StartDate == "01-01-2018" && EndDate == "01-12-2020") ? "ช่วงวันที่ : ทั้งหมด" : "ช่วงวันที่ : " + StartDate + " - " + EndDate;
                worksheet.Range("A1:L1").Row(1).Merge();
                ex.setHeaderExcel(worksheet);
                // worksheet.Cell("A2").Value = "Date : " + DateTime.Now.ToString("dd/MM/yyyy");
                worksheet.Range("A2:L2").Row(1).Merge();

                string rawXml = "{" +
                    "\"CustText\": \"" + CustText + "\"," +
                    "\"StartDate\":\"" + StartDate + "\"," +
                    "\"EndDate\":\"" + EndDate + "\"," +
                    "\"IS_Verify\":\"" + IS_Verify + "\"," +
                    "\"IS_VIP\":\"" + IS_VIP + "\"" +
                    "}";

                var client = new RestClient(EndPoint + "/customer_backend");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
                request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var callback = JObject.Parse(response.Content);

                var seq = 0;
                var header = 5;
                var row = 6;

                worksheet.Cell("A" + header).Value = "Item.";
                worksheet.Cell("B" + header).Value = "ชื่อ-นามสกุล";
                worksheet.Cell("C" + header).Value = "อาชีพ";
                worksheet.Cell("D" + header).Value = "เพศ";
                worksheet.Cell("E" + header).Value = "สถานะ";
                worksheet.Cell("F" + header).Value = "วันเกิด";
                worksheet.Cell("G" + header).Value = "เบอร์โทร";
                worksheet.Cell("H" + header).Value = "E-mail";
                worksheet.Cell("I" + header).Value = "Line";
                worksheet.Cell("J" + header).Value = "วันที่สมัคร";
                worksheet.Cell("K" + header).Value = "ที่อยู่";
                worksheet.Cell("L" + header).Value = "ที่อยู่ที่ทำงาน";
                worksheet.Cell("M" + header).Value = "สาขาที่สมัคร";
                worksheet.Cell("N" + header).Value = "วันที่อนุมัติ";
                worksheet.Cell("O" + header).Value = "ประเภทลูกค้า";
                worksheet.Cell("P" + header).Value = "ชื่อ-นามสกุล (ภาษาอังกฤษ)";
                worksheet.Cell("Q" + header).Value = "ประเภท";
                ex.setTitleExcel(worksheet);
                foreach (var cust in callback["items"])
                {
                    seq++;
                    //byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(cust["Cust_ID"].ToString());
                    //string plainTextBytes = System.Convert.ToBase64String(toEncodeAsBytes);

                    //string rawXml2 = "{" +
                    //"\"Cust_ID\": \"" + cust["Cust_ID"].ToString() + "\"" +
                    //"}";

                    //var client2 = new RestClient(EndPoint + "/get_member");
                    //var request2 = new RestRequest(Method.POST);
                    //request2.AddHeader("cache-control", "no-cache");
                    //request2.AddHeader("content-type", "application/json");
                    //request2.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
                    //request2.AddParameter("application/json", rawXml2, ParameterType.RequestBody);
                    //IRestResponse response2 = client2.Execute(request2);
                    //var callback2 = JObject.Parse(response2.Content);
                    //var detail = callback2["items"];
                    //sequence
                    worksheet.Cell("A" + row).Value = seq;
                    //customer name
                    worksheet.Cell("B" + row).Value = cust["Title_Name"].ToString() + cust["Name"].ToString() + ' ' + cust["Surname"].ToString();
                    //customer job
                    var occ = "";
                    switch (cust["Occupation"].ToString())
                    {
                        case "D":
                            occ = "มัณฑนากร";
                            break;
                        case "A":
                            occ = "สถาปนิก";
                            break;
                        case "S":
                            occ = "สไตล์ลิส";
                            break;
                        default:
                            occ = "อื่นๆ (" + cust["Occupation_other"].ToString() + ")";
                            break;
                    }
                    worksheet.Cell("C" + row).Value = cust["Gender"].ToString() == "M" ? "ชาย" : "หญิง";
                    //customer sex
                    worksheet.Cell("D" + row).Value = occ;
                    //customer status
                    worksheet.Cell("E" + row).Value = cust["Status"].ToString() == "M" ? "แต่งงาน" : "โสด";
                    //customer birthday
                    var bd = cust["Birthday"].ToString();
                    if (bd != null && bd != "")
                    {
                        bd = Convert.ToDateTime(cust["Birthday"]).ToString("dd-MM-yyyy");
                    }
                    worksheet.Cell("F" + row).Value = bd;
                    //customer phone
                    worksheet.Cell("G" + row).Value = "'" + cust["Mobile_Phone"].ToString();
                    //customer email
                    worksheet.Cell("H" + row).Value = cust["Email"].ToString();
                    //customer line
                    worksheet.Cell("I" + row).Value = cust["Line_ID"].ToString();
                    //register date
                    worksheet.Cell("J" + row).Value = "'" + Convert.ToDateTime(cust["Create_Date"]).ToString("dd-MM-yyyy");
                    //address
                    string addr_home = "";
                    string addr_work = "";


                    ///addrwork
                    addr_work = cust["Company_Name"].ToString();
                    if (cust["AddrnoWork"].ToString() != "")
                    {
                        addr_work += " เลขที่ " + cust["AddrnoWork"].ToString();
                    }
                    if (cust["SoiWork"].ToString() != "")
                    {
                        addr_work += " ซ." + cust["SoiWork"].ToString();
                    }
                    if (cust["RoadWork"].ToString() != "")
                    {
                        addr_work += " ถ." + cust["RoadWork"].ToString();
                    }
                    if (cust["TambolWork"].ToString() != "")
                    {
                        addr_work += " ต." + cust["TambolWork"].ToString();
                    }
                    if (cust["AmphurWork"].ToString() != "")
                    {
                        addr_work += " อ." + cust["AmphurWork"].ToString();
                    }
                    if (cust["ProvinceWork"].ToString() != "")
                    {
                        addr_work += " จ." + cust["ProvinceWork"].ToString();
                    }
                    if (cust["PostcodeWork"].ToString() != "")
                    {
                        addr_work += " " + cust["PostcodeWork"].ToString();
                    }
                    addr_work += " Tel." + cust["Tel"].ToString();


                    ///addrhome
                    if (cust["Addrno"].ToString() != "")
                    {
                        addr_home = "เลขที่ " + cust["Addrno"].ToString();
                    }
                    if (cust["Soi"].ToString() != "")
                    {
                        addr_home += " ซ." + cust["Soi"].ToString();
                    }
                    if (cust["Road"].ToString() != "")
                    {
                        addr_home += " ถ." + cust["Road"].ToString();
                    }
                    if (cust["Tambol"].ToString() != "")
                    {
                        addr_home += " ต." + cust["Tambol"].ToString();
                    }
                    if (cust["Amphur"].ToString() != "")
                    {
                        addr_home += " อ." + cust["Amphur"].ToString();
                    }
                    if (cust["Province"].ToString() != "")
                    {
                        addr_home += " จ." + cust["Province"].ToString();
                    }
                    if (cust["Postcode"].ToString() != "")
                    {
                        addr_home += " " + cust["Postcode"].ToString();
                    }


                    /*
                    var clientAddress = new RestClient(EndPoint + "/getmember_address");
                    var requestAddress = new RestRequest(Method.POST);
                    requestAddress.AddHeader("cache-control", "no-cache");
                    requestAddress.AddHeader("content-type", "application/json");
                    requestAddress.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
                    requestAddress.AddParameter("application/json", rawXml2, ParameterType.RequestBody);
                    IRestResponse responseAddress = clientAddress.Execute(requestAddress);
                    var callbackAddress = JObject.Parse(responseAddress.Content);
                    foreach (var i_data in callbackAddress["items"])
                    {
                        if (i_data["Addr_Type"].ToString() == "W")
                        {
                            addr_work = i_data["Company_Name"].ToString();
                            if (i_data["Addrno"].ToString() != "")
                            {
                                addr_work += " เลขที่ " + i_data["Addrno"].ToString();
                            }
                            if (i_data["Soi"].ToString() != "")
                            {
                                addr_work += " ซ." + i_data["Soi"].ToString();
                            }
                            if (i_data["Road"].ToString() != "")
                            {
                                addr_work += " ถ." + i_data["Road"].ToString();
                            }
                            if (i_data["Tambol"].ToString() != "")
                            {
                                addr_work += " ต." + i_data["Tambol"].ToString();
                            }
                            if (i_data["Amphur"].ToString() != "")
                            {
                                addr_work += " อ." + i_data["Amphur"].ToString();
                            }
                            if (i_data["Province"].ToString() != "")
                            {
                                addr_work += " จ." + i_data["Province"].ToString();
                            }
                            if (i_data["Postcode"].ToString() != "")
                            {
                                addr_work += " " + i_data["Postcode"].ToString();
                            }
                            addr_work += " Tel." + i_data["Phone"].ToString();
                        }
                        else
                        {
                            if (i_data["Addrno"].ToString() != "")
                            {
                                addr_home = "เลขที่ " + i_data["Addrno"].ToString();
                            }
                            if (i_data["Soi"].ToString() != "")
                            {
                                addr_home += " ซ." + i_data["Soi"].ToString();
                            }
                            if (i_data["Road"].ToString() != "")
                            {
                                addr_home += " ถ." + i_data["Road"].ToString();
                            }
                            if (i_data["Tambol"].ToString() != "")
                            {
                                addr_home += " ต." + i_data["Tambol"].ToString();
                            }
                            if (i_data["Amphur"].ToString() != "")
                            {
                                addr_home += " อ." + i_data["Amphur"].ToString();
                            }
                            if (i_data["Province"].ToString() != "")
                            {
                                addr_home += " จ." + i_data["Province"].ToString();
                            }
                            if (i_data["Postcode"].ToString() != "")
                            {
                                addr_home += " " + i_data["Postcode"].ToString();
                            }
                        }
                    }*/
                    string BranchApply = cust["Branch_Apply"].ToString() == "B" ? "บางนา กม.4" : cust["Branch_Apply"].ToString() == "R" ? "เดอะคริสตัล เอสบี ราชพฤกษ" : cust["Branch_Apply"].ToString() == "C" ? "คริสตัล ดีไซน์ เซ้นเตอร์" : cust["Branch_Apply"].ToString() == "P" ? "ภูเก็ต" : " - ";
                    worksheet.Cell("K" + row).Value = addr_home;
                    worksheet.Cell("L" + row).Value = addr_work;
                    worksheet.Cell("M" + row).Value = BranchApply;


                    //worksheet.Cell("J" + row).Value = Convert.ToDateTime(cust["Create_Date"]).ToString("dd-MM-yyyy");

                    string Verify_Date = (cust["Verify_Date"].ToString() != "") ? Convert.ToDateTime(cust["Verify_Date"]).ToString("dd-MM-yyyy") : "";
                    //string Verify_Date = cust["Verify_Date"].ToString();
                    worksheet.Cell("N" + row).Value ="'"+ Verify_Date;
                    string isVIP = "";
                    if (cust["IS_VIP"].ToString()  == "Y")
                    {
                        isVIP = "VIP";
                    }
                    else if (cust["IS_52Week"].ToString()   == "Y")
                    {
                        isVIP = "52 Week";
                    }
                    else
                    {
                        isVIP = "ปกติ";
                    }
                    worksheet.Cell("O" + row).Value = isVIP;
                    worksheet.Cell("P" + row).Value = cust["Name_Eng"].ToString() + ' ' + cust["Surname_Eng"].ToString(); 
                    worksheet.Cell("Q" + row).Value = cust["Occupation_Type"].ToString() == "F" ? "Freelance": "พนักงานประจำ" ;
                    row++;
                }
                worksheet.Cell("A3").Value = "จำนวนทั้งหมด : " + seq;
                worksheet.Range("A3:L3").Row(1).Merge();

                ex.setFormatExcel(worksheet);
                ex.ExportExcel(wb, "ข้อมูลการสมัครสมาชิก" + DateTime.Now.ToString("dd-MM-yyyy"));
                /*
                for (var i = 1; i <= 12; i++)
                {
                    worksheet.Cell(5, i).Style.Fill.BackgroundColor = XLColor.Red;
                }

                worksheet.Columns().AdjustToContents();

                string myName = Server.UrlEncode("Register_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();*/
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public void SendSMS(string Cust_ID)
        {
            string rawXml2 = "{" +
                  "\"Cust_ID\": \"" + Cust_ID + "\"" +
                  "}";

            var client2 = new RestClient(EndPoint + "/get_member");
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("cache-control", "no-cache");
            request2.AddHeader("content-type", "application/json");
            request2.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request2.AddParameter("application/json", rawXml2, ParameterType.RequestBody);
            IRestResponse response2 = client2.Execute(request2);
            var callback2 = JObject.Parse(response2.Content);
            var detail = callback2["items"];
            string template = getSmsTemplate("WC");
            sendMsg(template, detail[0]["Mobile_Phone"].ToString());

        }
        public string getSmsTemplate(string code)
        {
            string rawXml = "{" +
                  "\"SMS_Code\": \"" + code + "\"," +
                  "}";
            var client = new RestClient(EndPoint + "/get_sms_template");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            var callback2 = JObject.Parse(response.Content);
            return callback2["items"].ToString();
        }

        public bool sendMsg(string message, string phone)
        {
            string rawXml = "{" +
                  "\"mobilePhoneNo\": \"" + phone + "\"," +
                  "\"message\":\"" + message + "\"," +
                  "}";
            var client = new RestClient(EndPoint + "/send_sms");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            return true;
        }

        public void SendMail(string Cust_ID)
        {
            string rawXml2 = "{" +
                  "\"Cust_ID\": \"" + Cust_ID + "\"" +
                  "}";

            var client2 = new RestClient(EndPoint + "/get_member");
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("cache-control", "no-cache");
            request2.AddHeader("content-type", "application/json");
            request2.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request2.AddParameter("application/json", rawXml2, ParameterType.RequestBody);
            IRestResponse response2 = client2.Execute(request2);
            var callback2 = JObject.Parse(response2.Content);
            var detail = callback2["items"];
            string template = EmailTemplate(detail[0]["Email"].ToString(), detail[0]["Password"].ToString(), detail[0]["Name"].ToString() + ' ' + detail[0]["Surname"].ToString(), detail[0]["IS_52Week"].ToString());

            string sendTo = detail[0]["Email"].ToString();
            string subject = "ขอต้อนรับเข้าสู่ SB Designer Club";

            string rawXml = "{" +
                "\"sendTo\": \"" + sendTo + "\"," +
                "\"subject\": \"" + subject + "\"," +
                "\"body\":\"" + template + "\"" +
                "}";

            var client = new RestClient(EndPoint + "/send_email");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
        }

        public string EmailTemplate(string Email, string Password, string name, string IS_52Week)
        {
            string template = "";
            if (IS_52Week == "Y")
            {
                template = @"
                            <html xmlns='http://www.w3.org/1999/xhtml'>
                            <head>
                                <title></title>
                            </head>
                            <body>
                                <table style='background-color: #f1f1f1;width: 584px;font-size: 12px;' border='0' cellspacing='0'> 
                                     <tr>
                                      <td> &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/logo.png' style='margin-bottom: 10px;' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/banner.png' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2'>
                                            <p style='padding: 45px;line-height: 1.8;'>
                                                คุณ " + name + @"
                                                <br />
                                                <br />
                                                ยินดีต้อนรับและขอบคุณสำหรับการเข้าร่วมเป็น SB Designer Club คลับที่สมาชิกดีไซน์สิทธิประโยชน์ได้ เราขอมอบของขวัญชิ้นพิเศษสุดเอ๊กซ์คลูซีฟสำหรับสมาชิก ด้วยสมุดบันทึกแบรนด์ดัง
                                                เคียงคู่ดีไซเนอร์ มายาวนาน 'Moleskine' ที่สลักชื่อท่านสมาชิกและปากกา Classic Roller Pen พร้อมสิทธิประโยชน์ที่เหนือกว่า สัมผัสประสบการณ์ใหม่ บริการสุดประทับใจ แลกของรางวัลถูกใจ ที่เราตั้งใจคัดสรรเพื่อมอบให้สมาชิก SB Designer Club เท่านั้น
                                                <br />
                                                <br />
                                                เพื่อตรวจสอบสิทธิประโยชน์ต่างๆ ท่านสามารถล็อกอินเข้าสู่ระบบที่เว็บไซต์ www.sbdesignerclub.com พร้อมรับ SB Designer Point 1,000 คะแนน*
                                                ด้วยอีเมลที่ลงทะเบียนไว้ และรหัสผ่านด้านล่างนี้
                                                <br />
                                                <br />
                                                Email : <u> " + Email + @"</u><br />
                                                Password : <u>" + Password + @"</u>
                                                <br />
                                                <br />
                                                โดยสมาชิกสามารถเปลี่ยนรหัสผ่านเองได้ภายหลัง<br />
                                                เมื่อล็อกอินเข้าสู่ระบบเป็นที่เรียบร้อยแล้ว
                                                <br />
                                                <br />
                                                เราหวังเป็นอย่างยิ่งว่าสิทธิประโยชน์ที่เรามอบให้ จะเป็นส่วนหนึ่งในการสร้างแรงบันดาลใจ เพื่อสร้างสรรค์ผลงานและร่วมกันพัฒนาวงการออกแบบ ตกแต่งให้ดียิ่งขึ้นในวันนี้และอนาคต
                                                <br />
                                                <br />
                                                <i style='color: #777777;'>*สำหรับการล็อกอินครั้งแรกเท่านั้น</i>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 292px;'></td>
                                        <td style='text-align: center;padding-top: 20px;padding-bottom: 50px;'>
                                            ขอแสดงความนับถือ <br />
                                            SB Designer Club
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='padding-left: 45px;'>
                                            <table style='font-size: 12px;width: 100%;margin-bottom: 10px;'>
                                                <tr>
                                                    <td style='width: 60px;'>
                                                        <img src='http://sbdesignerclub.com/Content/image/email/LinkSBDesigner.png' style='width: 60px;' />
                                                    </td>
                                                    <td style='padding-top: 5px;'>
                                                        สอบถามเพิ่มเติม <br />
                                                        <ul style='padding-left: 20px;margin-top: 5px;'>
                                                            <li>LINE &#64;sbdesignerclub</li>
                                                            <li>อีเมล sbdesignerclub&#64;sbdesignsquare.com</li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/SB-DSC_Banner-Privilege_Email.jpg' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/footer.jpg' />
                                        </td>
                                    </tr>
                                </table>
                                <table style='width: 584px;'>
                                    <tr>
                                        <td style='text-align: center;'>
                                            <label style='font-size: 12px;'>Copyright &copy; 2017 SB Design Square, All rights reserved.</label>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                            </html>
                            ";
            }
            else
            {
                template = @"
                            <html xmlns='http://www.w3.org/1999/xhtml'>
                            <head>
                                <title></title>
                            </head>
                            <body>
                                <table style='background-color: #f1f1f1;width: 584px;font-size: 12px;' border='0' cellspacing='0'> 
                                     <tr>
                                      <td> &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/logo.png' style='margin-bottom: 10px;' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/banner.png' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2'>
                                            <p style='padding: 45px;line-height: 1.8;'>
                                                คุณ " + name + @"
                                                <br />
                                                <br />
                                                ยินดีต้อนรับและขอบคุณสำหรับการเข้าร่วมเป็น SB Designer Club คลับที่สมาชิกดีไซน์สิทธิประโยชน์ได้ เราขอมอบของขวัญชิ้นพิเศษสุดเอ๊กซ์คลูซีฟสำหรับสมาชิก ด้วยสมุดบันทึกแบรนด์ดัง
                                                เคียงคู่ดีไซเนอร์ มายาวนาน 'Moleskine' ที่สลักชื่อท่านสมาชิก พร้อมสิทธิประโยชน์ที่เหนือกว่า สัมผัสประสบการณ์ใหม่ บริการสุดประทับใจ แลกของรางวัลถูกใจ ที่เราตั้งใจคัดสรรเพื่อมอบให้สมาชิก SB Designer Club เท่านั้น
                                                <br />
                                                <br />
                                                เพื่อตรวจสอบสิทธิประโยชน์ต่างๆ ท่านสามารถล็อกอินเข้าสู่ระบบที่เว็บไซต์ www.sbdesignerclub.com พร้อมรับ SB Designer Point 1,000 คะแนน*
                                                ด้วยอีเมลที่ลงทะเบียนไว้ และรหัสผ่านด้านล่างนี้
                                                <br />
                                                <br />
                                                Email : <u> " + Email + @"</u><br />
                                                Password : <u>" + Password + @"</u>
                                                <br />
                                                <br />
                                                โดยสมาชิกสามารถเปลี่ยนรหัสผ่านเองได้ภายหลัง<br />
                                                เมื่อล็อกอินเข้าสู่ระบบเป็นที่เรียบร้อยแล้ว
                                                <br />
                                                <br />
                                                เราหวังเป็นอย่างยิ่งว่าสิทธิประโยชน์ที่เรามอบให้ จะเป็นส่วนหนึ่งในการสร้างแรงบันดาลใจ เพื่อสร้างสรรค์ผลงานและร่วมกันพัฒนาวงการออกแบบ ตกแต่งให้ดียิ่งขึ้นในวันนี้และอนาคต
                                                <br />
                                                <br />
                                                <i style='color: #777777;'>*สำหรับการล็อกอินครั้งแรกเท่านั้น</i>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 292px;'></td>
                                        <td style='text-align: center;padding-top: 20px;padding-bottom: 50px;'>
                                            ขอแสดงความนับถือ <br />
                                            SB Designer Club
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='padding-left: 45px;'>
                                            <table style='font-size: 12px;width: 100%;margin-bottom: 10px;'>
                                                <tr>
                                                    <td style='width: 60px;'>
                                                        <img src='http://sbdesignerclub.com/Content/image/email/LinkSBDesigner.png' style='width: 60px;' />
                                                    </td>
                                                    <td style='padding-top: 5px;'>
                                                        สอบถามเพิ่มเติม <br />
                                                        <ul style='padding-left: 20px;margin-top: 5px;'>
                                                            <li>LINE &#64;sbdesignerclub</li>
                                                            <li>อีเมล sbdesignerclub&#64;sbdesignsquare.com</li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/SB-DSC_Banner-Privilege_Email.jpg' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' style='text-align: center;'>
                                            <img src='http://sbdesignerclub.com/Content/image/email/footer.jpg' />
                                        </td>
                                    </tr>
                                </table>
                                <table style='width: 584px;'>
                                    <tr>
                                        <td style='text-align: center;'>
                                            <label style='font-size: 12px;'>Copyright &copy; 2017 SB Design Square, All rights reserved.</label>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                            </html>
                            ";
            }
            return template;
        }
    }
}