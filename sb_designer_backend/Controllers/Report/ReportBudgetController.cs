﻿using ClosedXML.Excel;
using ExporHTMLTOFiles;
using Newtonsoft.Json;
using RestSharp;
using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using static sb_designer_backend.Controllers.Report.ReportRedeemController;

namespace sb_designer_backend.Controllers.Report
{
    public class ReportBudgetController : BaseController
    {
        Models.SetExportExcel se = new Models.SetExportExcel();
        public int firstColomn_sum;
        // GET: ReportRedeem
        public ActionResult Index(int budgetid = 0)
        {
            ViewBag.budget_id = budgetid;
            return View();
        }

        public class DataReportBudget
        {
            public int countOfMember { get; set; }
            public int member_all { get; set; }
            public int member_has_point { get; set; }
            public int point_all { get; set; }
            public int countOfPoint { get; set; }
            public decimal countOfPriceReward { get; set; }
            public int countOfGR { get; set; }
            public int countOfGP { get; set; }
            public int countOfPL { get; set; }
            public int countOfPLEMP { get; set; }
            public List<ListReportBudget> DataReport { get; set; }

        } // Data rptbudget
        public class ListReportBudget
        {
            public string cat_name { get; set; }
            public string redeem_no { get; set; }
            public string member_id { get; set; }
            public string name { get; set; }
            public string last_name { get; set; }
            public string tier_code { get; set; }
            public string reward_code { get; set; }
            public string reward_name { get; set; }
            public string redeem_date { get; set; }
            public int qty { get; set; }
            public int point_redeem { get; set; }
            public decimal reward_price { get; set; }
            public string delivery { get; set; }
            public string remark { get; set; }
            public string sbu { get; set; }
            public string sbu_id { get; set; }
            public decimal tot_point { get; set; }
            public int tot_member { get; set; }
            public decimal tot_budget { get; set; }
            public string referee { get; set; }
            public string rfr_cleansing { get; set; }
            public string status_redeem { get; set; }
            public string delivery_date { get; set; }
            public string receive_date { get; set; }
            public int dayofdelivery { get; set; }
            public int overdue_date { get; set; }
            public int point { get; set; }
            public string receive_by { get; set; }
            public decimal price_for_budget { get; set; }
            public string project_name { get; set; }
            public string unit_number { get; set; }
            public int point_per_item { get; set; }
            public string POINT_TYPE_CODE { get; set; }
            public string ACTIVITY_NAME { get; set; }
            public string ADJUST_POINT_NAME { get; set; }
        } //List rpt budget

        public void ReportBudgetSummaryExcel(string report_h, string round_num, string period, string start_date, string end_date)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            var data_report = new DataReportBudget();

            data_report = GetDataReportBudget(round_num, "1");

            var ws = workbook.Worksheets.Add("Redeem Reward(สรุป)");
            //SET Header
            ws.Cell(2, 1).Value = report_h;
            var periods = period;
            if (period == "A")
            {
                periods = "ทั้งหมด";
                ws.Cell(3, 1).Value = "การตั้งเบิกทั้งหมด ";
            }
            else if (!string.IsNullOrEmpty(start_date) && start_date != "")
            {
                ws.Cell(3, 1).Value = "ช่วงวันที่แลก  " + start_date + " - " + end_date;

            }
            else
            {
                ws.Cell(3, 1).Value = "การตั้งเบิกครั้งที่ " + round_num + " : " + "ช่วงวันที่แลก : " + periods;
            }

            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 16;
            ws.Range("A3:M11").Style.Font.FontSize = 11;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;


            ws.Range("A6:A6").Value = "จำนวนสมาชิกที่แลกของรางวัล  "; ws.Range("B6:B6").Value = (data_report.countOfMember).ToString("#,##0"); ws.Range("C6:C6").Value = "ราย";
            ws.Range("A7:A7").Value = "คะแนนที่ใช้แลก  "; ws.Range("B7:B7").Value = "'" + (data_report.countOfPoint).ToString("#,##0"); ws.Range("C7:C7").Value = "คะแนน";
            ws.Range("A8:A8").Value = "มูลค่าของรางวัล  "; ws.Range("B8:B8").Value = "'" + (data_report.countOfPriceReward).ToString("#,##0.00"); ws.Range("C8:C8").Value = "บาท";
            //SET Total Summary
            //ws.Range("A6:A6").Value = "จำนวนผู้แนะนำ : " + data_report.countReferer.ToString("#,##0") + " ราย "; ws.Range("D6:D6").Value = "จำนวนผู้ถูกแนะนำ : " + data_report.countReferee.ToString("#,##0") + " ราย ";
            ws.Range("B6:B10").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("E6:E10").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A6:M10").Style.Font.Bold = true;


            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = " Category";
            ws.Cell(Row, 2).Value = "ลำดับที่";
            ws.Cell(Row, 3).Value = "รหัสของรางวัล";
            ws.Cell(Row, 4).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 5).Value = "จำนวน";
            ws.Cell(Row, 6).Value = "คะแนน/หน่วย";
            ws.Cell(Row, 7).Value = "คะแนนที่แลก";
            ws.Cell(Row, 8).Value = "มูลค่าของรางวัล";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontSize = 11;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Font.FontName = "Cambria";
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":H" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 0;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            List<ListReportBudget> data_budget = data_report.DataReport;
            var sum_qty = 0;
            var sum_price = 0;
            var sum_point = 0;
            var old_cat = "";
            var new_cat = "";
            foreach (var item in data_budget)
            {

                new_cat = (item.cat_name == null || item.cat_name.ToString() == "") ? "NULL" : item.cat_name;

                var cat_name = (new_cat == old_cat) ? "" : item.cat_name;
                i = (new_cat == old_cat) ? i + 1 : 1;

                ws.Cell(Row, 1).Value = "'" + cat_name;
                ws.Cell(Row, 2).Value = "'" + i;
                ws.Cell(Row, 3).Value = (item.reward_code == null) ? "'" : "'" + item.reward_code.ToString();
                ws.Cell(Row, 4).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                ws.Cell(Row, 5).Value = (item.qty.ToString() == null) ? "'" : "'" + item.qty.ToString("#,##0");
                ws.Cell(Row, 6).Value = (item.point.ToString() == null || item.point.ToString() == "") ? "'" : "'" + (item.point).ToString("#,##0");
                ws.Cell(Row, 7).Value = (item.tot_point.ToString() == null || item.tot_point.ToString() == "") ? "'" : "'" + Math.Floor(item.tot_point).ToString("#,##0");
                ws.Cell(Row, 8).Value = (item.reward_price.ToString() == null || item.reward_price.ToString() == "") ? "'" : "'" + item.reward_price.ToString("#,##0.00");

                ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Range("B" + Row + ":B" + Row).Style.DateFormat.SetFormat("dd/MM/yyyy");
                //ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

                Row++;
                old_cat = (item.cat_name == null || item.cat_name.ToString() == "") ? "NULL" : item.cat_name;
                sum_qty += item.qty;
                sum_price += (int)item.reward_price;
                sum_point += (int)Math.Floor(item.tot_point);
            }


            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวม";
            ws.Range("E" + endRow + ":E" + endRow).Value = "'" + sum_qty.ToString("#,##0");
            ws.Range("G" + endRow + ":G" + endRow).Value = "'" + sum_point.ToString("#,##0");
            ws.Range("H" + endRow + ":H" + endRow).Value = "'" + sum_price.ToString("#,##0.00");


            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontSize = 11;
            ws.Range("A" + startRow + ":H" + endRow).Style.Font.FontName = "Cambria";
            ws.Range("A" + startRow + ":H" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":H" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":H" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + endRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("E" + startRow + ":H" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A" + endRow + ":H" + endRow).Style.Font.Bold = true;

            ws.Column("A").Width = 30;
            ws.Column("B").Width = 10;
            ws.Column("C").Width = 15;
            ws.Column("D").Width = 50;
            ws.Column("E").Width = 15;
            ws.Column("F").Width = 15;
            ws.Columns("G:H").Width = 15;

            Row++;

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h + "_ครั้งที่_" + round_num);
        } //ReportBudgetSummaryExcel 1

        public void ReportBudgetDetailExcel(string report_h, string round_num, string period, string start_date, string end_date)
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            var data_report = new DataReportBudget();

            data_report = GetDataReportBudget(round_num, "2");

            var ws = workbook.Worksheets.Add("Budget Redeem (Detail)");
            //SET Header
            ws.Cell(2, 1).Value = report_h + " (เรียงตามชื่อสมาชิก,วันที่แลก)";
            var periods = period;
            if (period == "A")
            {
                periods = "ทั้งหมด";
                ws.Cell(3, 1).Value = "การตั้งเบิกทั้งหมด ";
            }
            else if (!string.IsNullOrEmpty(start_date) && start_date != "")
            {
                ws.Cell(3, 1).Value = "ช่วงวันที่แลก  " + start_date + " - " + end_date;

            }
            else
            {
                ws.Cell(3, 1).Value = "การตั้งเบิกครั้งที่ " + round_num + " : " + "ช่วงวันที่แลก " + periods;
            }

            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:M2").Style.Font.FontSize = 16;
            ws.Range("A3:M11").Style.Font.FontSize = 11;
            ws.Range("A1:M3").Style.Font.Bold = true;
            ws.Range("A1:M11").Style.Font.FontName = "Cambria";
            ws.Range("A1:M11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:M4").Style.Font.FontColor = XLColor.Blue;

            //SET Total Summary
            //ws.Range("A6:A6").Value = "จำนวนผู้แนะนำ : " + data_report.countReferer.ToString("#,##0") + " ราย "; ws.Range("D6:D6").Value = "จำนวนผู้ถูกแนะนำ : " + data_report.countReferee.ToString("#,##0") + " ราย ";
            ws.Range("A6:A6").Value = "จำนวนสมาชิกที่แลกของรางวัล "; ws.Range("B6:B6").Value = (data_report.countOfMember).ToString("#,##0"); ws.Range("C6:C6").Value = "ราย";
            ws.Range("A7:A7").Value = "คะแนนที่ใช้แลก "; ws.Range("B7:B7").Value = "'" + (data_report.countOfPoint).ToString("#,##0"); ws.Range("C7:C7").Value = "คะแนน";
            ws.Range("A8:A8").Value = "มูลค่าของรางวัล "; ws.Range("B8:B8").Value = "'" + (data_report.countOfPriceReward).ToString("#,##0.00"); ws.Range("C8:C8").Value = "บาท";
            ws.Range("B6:B10").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("E6:E10").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A6:M10").Style.Font.Bold = true;

            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = "ลำดับที่";
            ws.Cell(Row, 2).Value = "รหัสสมาชิก";
            ws.Cell(Row, 3).Value = "ชื่อสมาชิก";
            ws.Cell(Row, 4).Value = " ระดับสมาชิก";
            ws.Cell(Row, 5).Value = "วันที่แลก";
            ws.Cell(Row, 6).Value = " Category";
            ws.Cell(Row, 7).Value = "รหัสของรางวัล";
            ws.Cell(Row, 8).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 9).Value = " จำนวน";
            ws.Cell(Row, 10).Value = "คะแนน/หน่วย";
            ws.Cell(Row, 11).Value = "คะแนนที่ใช้แลก";
            ws.Cell(Row, 12).Value = "มูลค่าของรางวัล";
            ws.Cell(Row, 13).Value = "ผู้รับสินค้า";
            ws.Cell(Row, 14).Value = "การจัดส่ง";
            ws.Cell(Row, 15).Value = "หมายเหตุ";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontSize = 11;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Font.FontName = "Cambria";
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":O" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 0;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            List<ListReportBudget> data_budget = data_report.DataReport;
            var sum_qty = 0;
            decimal sum_price = 0;
            var sum_point = 0;
            var old_cat = "";
            var new_cat = "";
            foreach (var item in data_budget)
            {

                new_cat = (item.cat_name == null || item.cat_name.ToString() == "") ? "NULL" : item.cat_name;

                var cat_name = (new_cat == old_cat) ? "" : item.cat_name;
                i = i + 1;

                ws.Cell(Row, 1).Value = "'" + i;
                ws.Cell(Row, 2).Value = (item.member_id == null) ? "'" : "'" + item.member_id.ToString();
                ws.Cell(Row, 3).Value = (item.name == null) ? "'" : "'" + item.name.ToString() + " " + item.last_name;
                ws.Cell(Row, 4).Value = (item.tier_code == null) ? "'" : "'" + item.tier_code.ToString();
                ws.Cell(Row, 5).Value = (item.redeem_date == null) ? "'" : "'" + item.redeem_date.ToString();
                ws.Cell(Row, 6).Value = (item.cat_name == null) ? "'" : "'" + item.cat_name.ToString();
                ws.Cell(Row, 7).Value = (item.reward_code == null) ? "'" : "'" + item.reward_code.ToString();
                ws.Cell(Row, 8).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                ws.Cell(Row, 9).Value = (item.qty == 0) ? "'0" : "'" + item.qty.ToString("#,##0");
                ws.Cell(Row, 10).Value = (item.point_per_item == 0) ? "'0" : "'" + item.point_per_item.ToString("#,##0");
                ws.Cell(Row, 11).Value = (item.point_redeem == 0) ? "'0" : "'" + (item.point_redeem).ToString("#,##0");
                ws.Cell(Row, 12).Value = (item.reward_price == 0) ? "'0" : "'" + Math.Floor(item.reward_price).ToString("#,##0.00");
                ws.Cell(Row, 13).Value = (item.receive_by == null) ? "'" : "'" + item.receive_by.ToString();
                ws.Cell(Row, 14).Value = (item.delivery == null) ? "'" : "'" + item.delivery.ToString();
                ws.Cell(Row, 15).Value = (item.remark == null) ? "'" : "'" + item.remark.ToString();

                ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                Row++;
                old_cat = (item.cat_name == null || item.cat_name.ToString() == "") ? "NULL" : item.cat_name;
                sum_qty += item.qty;
                sum_price += (decimal)item.reward_price;
                sum_point += (int)(item.point_redeem);
            }

            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวมทั้งหมด";
            ws.Range("I" + endRow + ":I" + endRow).Value = "'" + sum_qty.ToString("#,##0");
            ws.Range("K" + endRow + ":K" + endRow).Value = "'" + sum_point.ToString("#,##0");
            ws.Range("L" + endRow + ":L" + endRow).Value = "'" + sum_price.ToString("#,##0.00");


            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontSize = 11;
            ws.Range("A" + startRow + ":O" + endRow).Style.Font.FontName = "Cambria";
            ws.Range("A" + startRow + ":O" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":O" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":O" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + startRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + endRow + ":O" + endRow).Style.Font.Bold = true;
            ws.Range("I" + startRow + ":L" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

            ws.Column("A").Width = 30;
            ws.Column("B").Width = 15;
            ws.Column("C").Width = 25;
            ws.Column("D").Width = 15;
            ws.Column("E").Width = 15;
            ws.Column("F").Width = 15;
            ws.Column("G").Width = 15;
            ws.Column("H").Width = 40;
            ws.Columns("I:M").Width = 15;
            ws.Columns("N:O").Width = 35;
            Row++;

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, report_h + "_ครั้งที่_" + round_num);
        } //ReportBudgetDetailExcel 2

        public static DataReportBudget GetDataReportBudget(string reportID, string type)
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["EndPoint"].ToString() + "/Report/getDataReportBudget";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new DataReportBudget();
            try
            {
                var dataRequest = new ReportBudgetParameter();
                dataRequest.budget_id = reportID;
                dataRequest.type_report = type;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<DataReportBudget>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Redeem Status

        [System.Web.Http.HttpPost]
        public string GetDataReportBudgetGraph(string budget_id)
        {
            string rawXml = "{" +
                 "\"budget_id\": \"" + budget_id + "\"" +
             "}";

            var client = new RestClient(EndPoint + "/Report/getListReportBudgetGraph");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [System.Web.Http.HttpPost]
        public string GetListReportBudget(string budget_id, string fromType)
        {
            string rawXml = "{" +
                 "\"budget_id\": \"" + budget_id + "\"," +
                  "\"fromType\": \"" + fromType + "\"" +
             "}";

            var client = new RestClient(EndPoint + "/Report/getListReportBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    } // report budget controller
}
