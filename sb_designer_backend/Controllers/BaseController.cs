﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers
{
    public class BaseController : Controller
    {
        protected string EndPoint = ConfigurationManager.AppSettings["EndPoint"].ToString();
        protected string EndPointPort = ConfigurationManager.AppSettings["EndPointPort"].ToString();

        protected string username;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            username = System.Web.HttpContext.Current.Session["USERNAME"] as string;
            if (String.IsNullOrEmpty(username))
            {
                Response.Redirect(Url.Action("Index", "Home"));
            }
        }
    }
}
