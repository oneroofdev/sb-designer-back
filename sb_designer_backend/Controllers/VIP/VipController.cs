﻿using Newtonsoft.Json.Linq;
using RestSharp;
using sb_designer_backend.Controllers.Report;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.VIP
{
    public class VipController : BaseController
    {
        // GET: Vip
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult GetProvince()
        {
            var client = new RestClient(EndPoint +"/get_province");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            IRestResponse response = client.Execute(request);

            var callback = JObject.Parse(response.Content);

            IDictionary<string, string> result = new Dictionary<string, string>();
            result["status"] = callback["success"].ToString();
            result["code"] = callback["code"].ToString();
            result["description"] = callback["description"].ToString();
            result["items"] = callback["items"].ToString();

            return Json(result);
        }

        public ActionResult GetAmphur(string province_id)
        {
            string rawXml = "{\"AMP_PRV_ID\":\"" + province_id + "\"}";

            var client = new RestClient(EndPoint +"/get_amphur");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var callback = JObject.Parse(response.Content);

            IDictionary<string, string> result = new Dictionary<string, string>();
            result["status"] = callback["success"].ToString();
            result["code"] = callback["code"].ToString();
            result["description"] = callback["description"].ToString();
            result["items"] = callback["items"].ToString();

            return Json(result);
        }

        public ActionResult GetTambol(string amphur_id)
        {
            string rawXml = "{\"TAM_AMP_ID\":\"" + amphur_id + "\"}";

            var client = new RestClient(EndPoint +"/get_tambol");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var callback = JObject.Parse(response.Content);

            IDictionary<string, string> result = new Dictionary<string, string>();
            result["status"] = callback["success"].ToString();
            result["code"] = callback["code"].ToString();
            result["description"] = callback["description"].ToString();
            result["items"] = callback["items"].ToString();

            return Json(result);
        }

        [HttpPost]
        public ActionResult Save(VipMember member)
        {
            string filename = "";
            string PicUrl = "";
            if (member.Path_Card != null)
            {
                string extension = Path.GetExtension(member.Path_Card.FileName);
                filename = "vip_" + DateTime.Now.ToString("yymmssff") + extension;
                PicUrl = "~/Content/img/vip" + filename;
            }


            string rawXml = "{" +
                "\"Create_By\": \"" + username + "\"," +
                "\"Title_ID\": \"" + member.Title_ID + "\"," +
                "\"Name\":\"" + member.Name + "\"," +
                "\"Surname\":\"" + member.Surname + "\"," +
                "\"Mobile_Phone\":\"" + member.Mobile_Phone + "\"," +
                "\"Email\":\"" + member.Email + "\"," +
                "\"Line_ID\":\"" + member.Line_ID + "\"," +
                "\"Name_Eng\":\"" + member.Name_Eng + "\"," +
                "\"Surname_Eng\":\"" + member.Surname_Eng + "\"," + 
                "\"Birthday\":\"" + member.Birthday + "\"," +
                "\"Path_Card\":\"" + PicUrl + "\"," + 
                "\"IS_Verify\":\"" + member.IS_Verify + "\"," +
                "\"Verify_Date\":\"" + member.Verify_Date + "\"," +
                "\"Recommend_By\":\"" + member.Recommend_By + "\"," +
                "\"Birthday\":\"" + member.Birthday + "\"" + 
                "}";

            var client = new RestClient(EndPoint +"/create_member_vip");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var callback = JObject.Parse(response.Content);

            IDictionary<string, string> result = new Dictionary<string, string>();
            if (callback["success"].ToString() == "True")
            {
                if (member.IS_Verify == "Y")
                {
                    RegisterReportController cm = new RegisterReportController();
                    var CustID = result["items"].ToString();
                    cm.SendMail(CustID);
                    cm.SendSMS(CustID);
                }
                if (member.Path_Card != null)
                {
                    member.Path_Card.SaveAs(Path.Combine(Server.MapPath("~/Content/img/vip"), filename));
                }

                string rawXml2 = "{" +
                "\"Cust_ID\": \"" + callback["items"].ToString() + "\"," +
                "\"Building\": \"" + member.Building + "\"," +
                "\"House_No\":\"" + member.House_No + "\"," +
                "\"Village\":\"" + member.Village + "\"," +
                "\"Moo\":\"" + member.Moo + "\"," +
                "\"Soi\":\"" + member.Soi + "\"," +
                "\"Road\":\"" + member.Road + "\"," +
                "\"Province_ID\":\"" + member.Province_ID + "\"," +
                "\"Amphor_ID\":\"" + member.Amphor_ID + "\"," +
                "\"Tambon_ID\":\"" + member.Tambon_ID + "\"," +
                "\"Post_Code\":\"" + member.Post_Code + "\"" +
                "}";
                var client2 = new RestClient(EndPoint +"/set_address");
                var request2 = new RestRequest(Method.POST);
                request2.AddHeader("cache-control", "no-cache");
                request2.AddHeader("content-type", "application/json");
                request2.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
                request2.AddParameter("application/json", rawXml2, ParameterType.RequestBody);
                IRestResponse response2 = client2.Execute(request2);

                var callback2 = JObject.Parse(response2.Content);

                result["status"] = callback2["success"].ToString();
                result["code"] = callback2["code"].ToString();
                result["description"] = callback2["description"].ToString();
            }
            else
            {
                result["status"] = callback["success"].ToString();
                result["code"] = callback["code"].ToString();
                result["description"] = callback["description"].ToString();
            }

            return Json(result);
        }
    }

    public class VipMember
    {
        public string Title_ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Mobile_Phone { get; set; }
        public string Email { get; set; }
        public string Line_ID { get; set; }
        public string Name_Eng { get; set; }
        public string Surname_Eng { get; set; }
        public string Birthday { get; set; }
        public HttpPostedFileBase Path_Card { get; set; }
        public string Building { get; set; }
        public string House_No { get; set; }
        public string Village { get; set; }
        public string Moo { get; set; }
        public string Soi { get; set; }
        public string Road { get; set; }
        public string IS_Verify { get; set; }
        public string Verify_Date { get; set; }

        public string Recommend_By { get; set; }

        public string Province_ID { get; set; }
        public string Amphor_ID { get; set; }
        public string Tambon_ID { get; set; }
        public string Post_Code { get; set; }
    }
}