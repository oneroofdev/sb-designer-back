﻿using ClosedXML.Excel;
using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Delivery
{
    public class PrintDeliveryController : BaseController
    {
        // GET: PrintDelivery
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Label(string deliveryNo, string project, string deliveryBy)
        {
            string strListBuilder = string.Empty;
            string fontPath = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X v3.2.ttf").Replace(@"\", "/");
            string fontPathBold = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf").Replace(@"\", "/");
            string cssFont = Server.MapPath("~/Content/css/font-DBG.css").Replace(@"\", "/");
            GeneratorHTML genHtml = new GeneratorHTML();
            var dataDelivery = new List<RedeemDeliveryItem>();
            string FileName = string.Empty;
            var jsonReturn = new JsonRespone();
            try
            {
                if (!string.IsNullOrEmpty(deliveryNo) && project.Equals("SB"))
                {
                    dataDelivery = DeliveryManagement.GetDeliveryRedeem(deliveryNo, "/del/label_delivery");
                    //var dataAgent = DeliveryManagement.getAgent(dataDelivery.Select(r => r.SUB_AGENT_CODE).ToList());
                    strListBuilder = genHtml.LabelHtml(dataDelivery, fontPath, fontPathBold, project);
                    FileName = "Label_Delivery_No-" + deliveryNo;
                }
                if (strListBuilder != "")
                {
                    Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                    string strHtml = strListBuilder.ToString();
                    string pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                    string html = string.Empty;
                    html = strHtml.Replace("\r\n", "");
                    html = strHtml.Replace("\0", "");
                    //ExportPDFLabel(html, pdfFileName);
                    Exports export = new Exports();
                    export.SelectPDF(html, pdfFileName);
                    // export.PDF(html, pdfFileName, 5, 5, 16, 5, fontPath);
                    //export.ec(html, pdfFileName);
                    jsonReturn = new JsonRespone { status = true, message = FileName + ".pdf" };

                    //using (FileStream fs = new FileStream(Server.MapPath("~/Content/Document/Test.html"), FileMode.Create))
                    //{
                    //    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    //    {
                    //        sw.WriteLine(strHtml);
                    //    }
                    //}
                }
                else
                {
                    jsonReturn = new JsonRespone { status = false, message = "ไม่มีข้อมูล" };
                }
            }
            catch (Exception ex)
            {
                jsonReturn = new JsonRespone { status = false, message = ex.Message };
            }
            return Json(jsonReturn);
        }

        public ActionResult RewardRedeemSummary(string deliveryNo, string project)
        {
            var jsonReturn = new JsonRespone();
            string strListBuilder = string.Empty;
            string fontPath = Server.MapPath("~/Content/fonts/grillsiam").Replace(@"\", "/");
            string fontPathBold = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf").Replace(@"\", "/");
            string cssFont = Server.MapPath("~/Content/css/font-DBG.css").Replace(@"\", "/");
            string strPathAndQuery = HttpContext.Request.Url.PathAndQuery;
            string domain = HttpContext.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "");
            string app_path = HttpContext.Request.ApplicationPath;
            string imgLogo = domain + app_path + "/Content/img/new-logo.png";
            GeneratorHTML genHtml = new GeneratorHTML();
            var dataDelivery = new List<RedeemDeliveryItem>();
            string FileName = string.Empty;
            //delivery_no = "D00442";
            try
            {
                dataDelivery = DeliveryManagement.GetDeliveryRedeem(deliveryNo, "/del/list_reward_delivery");
                strListBuilder = genHtml.RewardRedeemSummary(dataDelivery, fontPath, fontPathBold, imgLogo);
                FileName = "RewardRedeemSummary_Delivery_No-" + deliveryNo;
                if (strListBuilder != "")
                {
                    Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                    string strHtml = strListBuilder.ToString();
                    string pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                    string html = string.Empty;
                    html = strHtml.Replace("\r\n", "");
                    html = strHtml.Replace("\0", "");
                    Exports export = new Exports();
                    //export.PDFIron(html, pdfFileName, cssFont);
                    //export.PDF(html, pdfFileName, fontPath);
                    //export.PDFiText7(html, pdfFileName, fontPath, fontPathBold);
                    export.SelectPDF(html, pdfFileName);
                    jsonReturn = new JsonRespone { status = true, message = FileName + ".pdf" };
                    //using (FileStream fs = new FileStream(Server.MapPath("~/Content/Delivery/Export/Test.html"), FileMode.Create))
                    //{
                    //    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    //    {
                    //        sw.WriteLine(strHtml);
                    //    }
                    //}
                }
                else
                {
                    jsonReturn = new JsonRespone { status = false, message = "ไม่มีข้อมูล" };
                }
            }
            catch (Exception ex)
            {
                jsonReturn = new JsonRespone { status = false, message = ex.Message };
            }
            return Json(jsonReturn);
        }

        public ActionResult RedeemDetail(string deliveryNo, string project, string deliveryBy)
        {
            var jsonReturn = new JsonRespone();
            string strListBuilder = string.Empty;
            string fontPath = Server.MapPath("~/Content/fonts/grillsiam").Replace(@"\", "/");
            string fontPathBold = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf").Replace(@"\", "/");
            string cssFont = Server.MapPath("~/Content/css/font-DBG.css").Replace(@"\", "/");
            string strPathAndQuery = HttpContext.Request.Url.PathAndQuery;
            string domain = HttpContext.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            string app_path = HttpContext.Request.ApplicationPath;
            string imgLogo = domain + app_path + "/Content/img/new-logo.png";
            //var img = iTextSharp.text.Image.GetInstance(Server.MapPath("../Content/img/new-logo.png"));
            GeneratorHTML genHtml = new GeneratorHTML();
            var dataDelivery = new List<RedeemDeliveryItem>();
            string FileName = string.Empty;
            //delivery_no = "D00444";
            try
            {
                if (project == "SB")
                {
                    dataDelivery = DeliveryManagement.GetDeliveryRedeem(deliveryNo, "/del/redeem_delivery");
                    string courierType = dataDelivery.Select(r => r.courierType).FirstOrDefault();
                    //if (courierType == "เงินสด")
                    //{
                    //    strListBuilder = genHtml.RedeemDetailMoney(dataDelivery, fontPath, fontPathBold, imgLogo);
                    //}
                    //else if (courierType == "supplier")
                    //{
                    //    strListBuilder = genHtml.RedeemDetailForSupplier(dataDelivery, fontPath, fontPathBold, imgLogo);
                    //}
                    //else
                    //{
                    //    strListBuilder = genHtml.RedeemDetail(dataDelivery, fontPath, fontPathBold, imgLogo);
                    //}
                    strListBuilder = genHtml.RedeemDetail(dataDelivery, fontPath, fontPathBold, imgLogo);
                }

                FileName = "RedeemDetail_Delivery_NO-" + deliveryNo;
                if (strListBuilder != "")
                {
                    Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                    string strHtml = strListBuilder.ToString();
                    string pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                    string html = string.Empty;
                    html = strHtml.Replace("\r\n", "");
                    html = strHtml.Replace("\0", "");
                    Exports export = new Exports();
                    //export.PDFLandscape(html, pdfFileName, fontPath);
                    export.SelectPDF_Landscape(html, pdfFileName);
                    //export.PDFiText7(html, pdfFileName, fontPath, fontPathBold);
                    jsonReturn = new JsonRespone { status = true, message = FileName + ".pdf" };
                }
                else
                {
                    jsonReturn = new JsonRespone { status = false, message = "ไม่มีข้อมูล" };
                }
            }
            catch (Exception ex)
            {
                jsonReturn = new JsonRespone { status = false, message = ex.Message };
            }
            return Json(jsonReturn);
        }

        public ActionResult Order(string deliveryNo, string project)
        {
            string strListBuilder = string.Empty;
            string html = string.Empty;
            string strHtml = string.Empty;
            string pdfFileName = string.Empty;

            string fontPath = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X v3.2.ttf").Replace(@"\", "/");
            string fontPathBold = Server.MapPath("~/Content/fonts/grillsiam/DB Gill Siam X Bd v3.2.ttf").Replace(@"\", "/");
            string strPathAndQuery = HttpContext.Request.Url.PathAndQuery;
            string domain = HttpContext.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            string app_path = HttpContext.Request.ApplicationPath;
            string imgIdNo = domain + app_path + "/Content/img/ID-No-block.PNG";
            string imgLogo = domain + app_path + "/Content/img/new-logo.png";

            string FileName = string.Empty;
            string zipFloderName = "Order_Delivery_No-" + deliveryNo;
            string DesZipFile = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + "Order_Delivery_No-" + deliveryNo + ".zip";

            var jsonReturn = new JsonRespone();
            GeneratorHTML genHtml = new GeneratorHTML();
            var dataDelivery = new List<RedeemDeliveryItem>();
            Exports export = new Exports();
            List<string> filePath = new List<string>();
            try
            {
                if (project.Equals("SB"))
                {
                    dataDelivery = DeliveryManagement.GetDeliveryRedeem(deliveryNo, "/del/order_delivery");
                }

                if (dataDelivery.Count > 0)
                {
                    var dataGroup = (from a in dataDelivery
                                     group a by new
                                     {
                                         a.houseNo,
                                         a.amphurEn,
                                         a.amphurTh,
                                         a.deliveryBy,
                                         a.deliveryDate,
                                         a.deliveryNo,
                                         a.email,
                                         a.floor,
                                         a.memberName,
                                         a.receiveName,
                                         a.memberCardNo,
                                         a.memberId,
                                         a.mobileNo,
                                         a.trackingNo,
                                         a.postCode,
                                         a.provNameEn,
                                         a.provNameTh,
                                         a.redeemBy,
                                         a.redeemChannel,
                                         a.redeemDate,
                                         a.road,
                                         a.soi,
                                         a.tambolEn,
                                         a.tambolTh,
                                         a.village,
                                         a.villageNo,
                                         a.bulding,
                                     } into gg
                                     select new
                                     {
                                         gg.Key,
                                         gg
                                     }).ToList();
                    string addr1 = "";
                    string addr2 = "";
                    string rewards = "";
                    int index = 1;
                    int page = 0;
                    int running = 1;
                    //int fileNumber = 1;
                    foreach (var item in dataGroup)
                    {
                        addr1 = item.Key.houseNo + (!string.IsNullOrEmpty(item.Key.bulding) ? " อาคาร " + item.Key.bulding + " " : " ");
                        addr1 += (!string.IsNullOrEmpty(item.Key.soi) ? " ซ." + item.Key.soi + " " : "");
                        addr1 += (!string.IsNullOrEmpty(item.Key.road) ? " ถ." + item.Key.road + " " : "");
                        addr2 = (!string.IsNullOrEmpty(item.Key.tambolTh) ? item.Key.tambolTh + " " : "");
                        addr2 += (!string.IsNullOrEmpty(item.Key.amphurTh) ? item.Key.amphurTh + " " : "");
                        addr2 += (!string.IsNullOrEmpty(item.Key.provNameTh) ? item.Key.provNameTh + " " : "");
                        addr2 += (!string.IsNullOrEmpty(item.Key.postCode) ? item.Key.postCode : "");
                        rewards = "";
                        index = 1;
                        foreach (var i in item.gg)
                        {
                            rewards += @"<div style='width: 100%; text-align: left; style='font-size:15px; padding-bottom:-10px;'><span>" + index + @". RedeemNo. <u> " + i.redeemNo + "</u> รหัสของรางวัล <u>" + i.rewardCode + @"</span></div>
                                 <div style='width: 100%; text-align: left; style='font-size:15px; padding-bottom:-10px;'></u> จำนวน <u> " + i.qty + @"</u> ชิ้น ชื่อสินค้า <span><u>" + i.rewardName + @"</u></span></div>";
                            index++;
                        }
                        string owner_name = string.IsNullOrEmpty(item.Key.memberName) ? "เจ้าของบ้าน" : item.Key.memberName;
                        strListBuilder += genHtml.OrderDelivery(imgLogo, imgIdNo, item.Key.deliveryNo, item.Key.deliveryDate, running, owner_name, item.Key.receiveName, item.Key.memberCardNo, addr1, addr2, item.Key.mobileNo, rewards);

                        page++;
                        //if (page % 500 == 0)
                        //{
                        //    Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                        //    FileName = "DeliveryOrder_" + project + "_NO_" + deliveryNo + "_Part-" + fileNumber;
                        //    strHtml = strListBuilder.ToString();
                        //    pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                        //    html = string.Empty;
                        //    if (strListBuilder != "")
                        //    {
                        //        html = strHtml.Replace("\r\n", "");
                        //        html = strHtml.Replace("\0", "");
                        //        export.PDFiText7(html, pdfFileName, fontPath, fontPathBold);
                        //    }
                        //    strListBuilder = "";
                        //    filePath.Add(pdfFileName);
                        //    fileNumber++;
                        //}
                        //if (page == dataGroup.Count)
                        //{
                        //    Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                        //    FileName = "DeliveryOrder_" + project + "_NO_" + deliveryNo + "_Part-" + fileNumber;
                        //    strHtml = strListBuilder.ToString();
                        //    pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                        //    html = string.Empty;
                        //    if (strListBuilder != "")
                        //    {
                        //        html = strHtml.Replace("\r\n", "");
                        //        html = strHtml.Replace("\0", "");
                        //        export.PDFiText7(html, pdfFileName, fontPath, fontPathBold);
                        //    }
                        //    filePath.Add(pdfFileName);
                        //}
                        running++;
                    }

                    html = string.Empty;
                    if (strListBuilder != "")
                    {
                        Commons.CreateDirectory("~/Content/Delivery/Export/", Server);
                        FileName = "DeliveryOrder_" + project + "_NO_" + deliveryNo;
                        strHtml = strListBuilder.ToString();
                        pdfFileName = Request.PhysicalApplicationPath + "Content\\Delivery\\Export\\" + FileName + ".pdf";
                        html = strHtml.Replace("\r\n", "");
                        html = strHtml.Replace("\0", "");
                        export.PDFiText7(html, pdfFileName, fontPath, fontPathBold);
                        jsonReturn = new JsonRespone { status = true, message = FileName + ".pdf" };
                    }
                    else
                    {
                        jsonReturn = new JsonRespone { status = false, message = "ไม่มีข้อมูล" };
                    }

                    //Exports.ToZip(filePath, DesZipFile, zipFloderName);

                }

            }
            catch (Exception ex)
            {
                return Json(jsonReturn = new JsonRespone { status = false, message = ex.Message });
            }
            // return File(Server.MapPath("~/Content/Delivery/Export/" + zipFloderName + ".zip"), "application/zip", zipFloderName + ".zip");
            return Json(jsonReturn);
        }

        public void DeliveryReport(string deliveryNo, string deliveryBy)
        {
            Exports ex = new Exports();
            XLWorkbook workbook = new XLWorkbook();
            //string delivery_by = "";
            var dataDelivery = DeliveryManagement.GetDeliveryRedeem(deliveryNo, "/del/redeem_delivery");
            //dynamic dataDecode = JsonConvert.DeserializeObject(data);
            string courierType = dataDelivery.Select(r => r.courierType).FirstOrDefault();
            //if (courierType == "เงินสด")
            //{
            //    deliveryBy = ExportExcelMoney(deliveryNo, ex, workbook, dataDelivery);
            //}
            //else
            //{
                
            //}
            deliveryBy = ExportExcelNormal(deliveryNo, ex, workbook, dataDelivery);

            //สั่งให้ Export ไฟล์
            ex.Excel(workbook, "รายงานสรุปการแลกของรางวัล_No-" + deliveryNo);

        }

        private static string ExportExcelNormal(string deliveryNo, Exports ex, XLWorkbook workbook, List<RedeemDeliveryItem> data)
        {
            string deliveryBy;
            var ws = workbook.Worksheets.Add("Target");
            deliveryBy = data.Select(r => r.courierName).FirstOrDefault();
            //set Header
            ws.Cell(1, 1).Value = "รายงานสรุปการแลกของรางวัล ตามการจัดส่ง";
            ws.Cell(2, 1).Value = "เลขที่การจัดส่งของรางวัล  : " + deliveryNo + " วิธีการจัดส่ง : " + deliveryBy;
            ws.Cell(3, 1).Value = "วันที่จัดส่ง : " + data.Select(r => r.deliveryDate).FirstOrDefault();
            ws.Cell(4, 1).Value = "วันที่พิมพ์ : " + DateTime.Now.ToString();
            ex.setHeaderExcel(ws);

            //set Header Title.
            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = "#";
            ws.Cell(Row, 2).Value = "เลขการแลก";
            ws.Cell(Row, 3).Value = "วันที่แลก";
            //ws.Cell(Row, 4).Value = "เลขที่บัตรสมาชิก";
            ws.Cell(Row, 4).Value = "ชื่อลูกค้า/ผู้แลก";
            ws.Cell(Row, 5).Value = "ชื่อผู้รับ";
            ws.Cell(Row, 6).Value = "ที่อยู่";
            ws.Cell(Row, 7).Value = "เบอร์โทร";
            ws.Cell(Row, 8).Value = "จังหวัด";
            ws.Cell(Row, 9).Value = "รหัสของรางวัล";
            ws.Cell(Row, 10).Value = "ของรางวัล";
            ws.Cell(Row, 11).Value = "จำนวน";
            ws.Cell(Row, 12).Value = "หมายเหตุ";
            ex.setTitleExcel(ws);
            int i = 0;
            foreach (var items in data)
            {
                string FullAddress = (!string.IsNullOrEmpty(items.houseNo) ? items.houseNo.ToString() : "");
                //if (!String.IsNullOrEmpty(items.bulding))
                //{
                //    FullAddress = FullAddress + " อาคาร" + items.bulding.ToString();
                //}
                //if (!String.IsNullOrEmpty(items.soi))
                //{
                //    FullAddress = FullAddress + " ซ." + items.soi.ToString();
                //}
                //if (!String.IsNullOrEmpty(items.road))
                //{
                //    FullAddress = FullAddress + " ถ." + items.road.ToString();
                //}
                if (!String.IsNullOrEmpty(items.tambolTh))
                {
                    FullAddress = FullAddress + " " + items.tambolTh.ToString();
                }
                if (!String.IsNullOrEmpty(items.amphurTh))
                {
                    FullAddress = FullAddress + " " + items.amphurTh.ToString();
                }
                if (!String.IsNullOrEmpty(items.provNameTh))
                {
                    FullAddress = FullAddress + " " + items.provNameTh.ToString();
                }
                if (!String.IsNullOrEmpty(items.postCode))
                {
                    FullAddress = FullAddress + " " + items.postCode.ToString();
                }

                string mobile = "";
                if (!String.IsNullOrEmpty(items.mobileNo))
                {
                    mobile = "'" + items.mobileNo.ToString();
                }
                Row++;
                i++;

                ws.Cell(Row, 1).Value = i.ToString();
                ws.Cell(Row, 2).Value = (!string.IsNullOrEmpty(items.redeemNo) ? items.redeemNo.ToString() : "");
                ws.Cell(Row, 3).Value = (!string.IsNullOrEmpty(items.deliveryDate) ? items.deliveryDate.ToString() : "");
                //ws.Cell(Row, 4).Value = (!string.IsNullOrEmpty(items.memberCardNo) ? items.memberCardNo : "");
                //ws.Cell(Row, 4).Style.DateFormat.SetNumberFormatId(1);
                ws.Cell(Row, 4).Value = items.memberName.ToString();
                ws.Cell(Row, 5).Value = (!string.IsNullOrEmpty(items.receiveName) ? items.receiveName.ToString() : "-");
                ws.Cell(Row, 6).Value = FullAddress;
                ws.Cell(Row, 7).Value = mobile;
                ws.Cell(Row, 8).Value = (!string.IsNullOrEmpty(items.provNameTh) ? items.provNameTh.ToString() : "");
                ws.Cell(Row, 9).Value = (!string.IsNullOrEmpty(items.rewardCode) ? items.rewardCode.ToString() : "");
                ws.Cell(Row, 10).Value = (!string.IsNullOrEmpty(items.rewardName) ? items.rewardName.ToString() : "");
                ws.Cell(Row, 11).Value = items.qty.ToString(("#,##0"));
                //FORMAT NUMBER 
                ws.Cell(Row, 11).Style.NumberFormat.Format = "#,##0";
                ws.Cell(Row, 11).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Cell(Row, 12).Value = (!string.IsNullOrEmpty(items.remark) ? items.remark : "");
            }
            //จัดขนาด Font และ ตีเส้นตาราง
            ex.setFormatExcel(ws);
            return deliveryBy;
        }
    }
}