﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Budget
{
    public class BudgetController : BaseController
    {
        // GET: Budget
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string checkQtyGenerateBudget(string DateFrom, string DateTo)
        {
            string rawXml = "{" +
                "\"DateFrom\": \"" + DateFrom + "\"," +
                "\"DateTo\":\"" + DateTo + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/checkQtyGenerateBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveGenerateBudget(string DateFrom, string DateTo,string listRedeem, string CreateBy)
        {
            string rawXml = "{" +
                "\"DateFrom\": \"" + DateFrom + "\"," +
                "\"DateTo\":\"" + DateTo + "\"," +
                "\"listRedeem\":\"" + listRedeem + "\"," +
                "\"CreateBy\":\"" + CreateBy + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/SaveGenerateBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListBudgetByCategory(string BUDGET_ID)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"SBU\": \"\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/GetListBudgetByCategory");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string UpdateApproveBudget(string BUDGET_ID, string username)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"username\": \"" + username + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/UpdateApproveBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListBudget()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/GetListBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListWaitBudget()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/ListWaitBudget");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string DataLastDate()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/DataLastDate");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string getReceiveByBudgetID(string BUDGET_ID)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/getReceiveByBudgetID");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListReceiveRewardID(string BUDGET_ID, string REWARD_CODE)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"REWARD_CODE\": \"" + REWARD_CODE + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/GetListReceiveRewardID");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveReceiveReward(string BUDGET_ID, string REWARD_CODE, int PROJECT_ID, int QTY, string REF_DOC_NO, string COPY_PO, string username)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"REWARD_CODE\": \"" + REWARD_CODE + "\"," +
                "\"PROJECT_ID\": " + PROJECT_ID + "," +
                "\"QTY\": " + QTY + "," +
                "\"REF_DOC_NO\": \"" + REF_DOC_NO + "\"," +
                "\"COPY_PO\": \"" + COPY_PO + "\"," +
                "\"username\": \"" + username + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/SaveReceiveReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListUpdateCopy(string REDEEM_NO)
        {
            string rawXml = "{" +
                "\"REDEEM_NO\": \"" + REDEEM_NO + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/GetListUpdateCopy");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListBanked()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/redeem/ListBanked");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string UpdateRedeemCopy(string BUDGET_ID, string COPY_CARD, string COPY_BANK, string username)
        {
            string rawXml = "{" +
                "\"BUDGET_ID\": \"" + BUDGET_ID + "\"," +
                "\"COPY_CARD\": \"" + COPY_CARD + "\"," +
                "\"COPY_BANK\": \"" + COPY_BANK + "\"," +
                "\"username\": \"" + username + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/UpdateRedeemCopy");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}