﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace sb_designer_backend.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadListImage()
        {
            string fileName = string.Empty;
            var listFileName = new List<string>();
            string year = DateTime.Now.Year + "_";
            string path = "~/Content/img/" + Request.Params["path"] + "/";
            //var strPysicalPath = Commons.RemoveStringFromToEnd(Request.PhysicalApplicationPath, "Backend") + path;
            if (Request.Files.Count > 0)
            {
                for (var i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = Request.Files[i].FileName.Split(new char[] { '\\' });
                        fileName = year + testfiles[testfiles.Length - 1];
                    }
                    else // chrome, firefox, other
                    {
                        fileName = year + Request.Files[i].FileName;
                    }
                    listFileName.Add(fileName);
                    bool exists = Directory.Exists(Server.MapPath(path)); // map path in Server by boolean
                    if (!exists)
                    {
                        // Create folder if it does not exist. 
                        //***And your need change permission is full control on thr server***
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    string fileLocation = Server.MapPath(path) + year + Request.Files[i].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files[i].SaveAs(fileLocation);

                }
            }
            return Json(listFileName, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void UploadImage()
        {
            if (Request.Files.Count > 0)
            {

            }
        }
        [HttpPost]
        public JsonResult UploadListImageReward()
        {
            string fileName = string.Empty;
            var listFileName = new List<string>();
            string year = DateTime.Now.Year + "_";
            string path = "~/Content/img/" + Request.Params["path"] + "/";
            //var strPysicalPath = Commons.RemoveStringFromToEnd(Request.PhysicalApplicationPath, "Backend") + path;
            if (Request.Files.Count > 0)
            {
                for (var i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = Request.Files[i].FileName.Split(new char[] { '\\' });
                        fileName = year + testfiles[testfiles.Length - 1];
                    }
                    else // chrome, firefox, other
                    {
                        fileName = year + Request.Files[i].FileName;
                    }
                    listFileName.Add(fileName);
                    bool exists = Directory.Exists(Server.MapPath(path)); // map path in Server by boolean
                    if (!exists)
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    string fileLocation = Server.MapPath(path) + year + Request.Files[i].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files[i].SaveAs(fileLocation);
                    var imgname = Request.Files[i].FileName.Split('.');
                    string fileLocation_thump = Server.MapPath(path) + year + imgname[0] + "_thumpnail." + imgname[1];
                    Image imgPhoto = Image.FromStream(Request.Files[i].InputStream, true, true);//Image.FromFile(@"C:\Users\...\Pictures\book.jpg");
                    Bitmap image = ResizeImage(imgPhoto, 96, 70);
                    image.Save(fileLocation_thump);
                }
            }
            return Json(listFileName, JsonRequestBehavior.AllowGet);
        }
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }
        public void saveImageReward(string imageData, string filename)
        {
            var temp = HttpRuntime.AppDomainAppPath;
            string fileNameWitPath = temp + @"\Content\img\Reward\edm\" + filename;
            string path = "~/Content/img/Reward/edm/";
            bool exists = Directory.Exists(Server.MapPath(path)); // map path in Server by boolean
            if (!exists)
            {
                // Create folder if it does not exist. 
                //***And your need change permission is full control on thr server***
                Directory.CreateDirectory(Server.MapPath(path));
            }
            if (System.IO.File.Exists(fileNameWitPath))
            {
                System.IO.File.Delete(fileNameWitPath);
            }
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(imageData);//convert from base64
                    bw.Write(data);
                    bw.Close();
                }
            }
        }

        [HttpPost]
        public string UploadImageRedeem()
        {
            string fileName = string.Empty;
            var listFileName = new List<string>();
            //string year = DateTime.Now.Year + "_";
            string year = DateTime.Now.Year + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string path = "~/Content/img/" + Request.Params["path"] + "/";
            //var strPysicalPath = Commons.RemoveStringFromToEnd(Request.PhysicalApplicationPath, "Backend") + path;
            if (Request.Files.Count > 0)
            {
                for (var i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = Request.Files[i].FileName.Split(new char[] { '\\' });
                        fileName = year + Path.GetExtension(testfiles[testfiles.Length - 1]);
                    }
                    else // chrome, firefox, other
                    {
                        fileName = year + Path.GetExtension(Request.Files[i].FileName);
                    }
                    listFileName.Add(fileName);
                    bool exists = Directory.Exists(Server.MapPath(path)); // map path in Server by boolean
                    if (!exists)
                    {
                        // Create folder if it does not exist. 
                        //***And your need change permission is full control on thr server***
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    string fileLocation = Server.MapPath(path) + fileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files[i].SaveAs(fileLocation);

                }
            }
            return fileName;
        }

    }
}
