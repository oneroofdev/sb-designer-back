﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers
{
    public class PointController : BaseController
    {
        // GET: Point
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string BalancePoint(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/point/BalancePoint");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
