﻿using sb_designer_backend.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using UploadImage;

namespace sb_designer_backend.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["Language"] = "TH";
            Session["Cust_ID"] = "";
            Session["Username"] = "";
            return View();
        }
        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    var dataUser = Account.VerifyLogin(username, password);
                    if (dataUser != null)
                    {
                        if (!string.IsNullOrEmpty(dataUser.USERNAME))
                        {
                            if (dataUser.ACTIVE == "Y") // active == Y
                            {
                                Session["USERID"] = dataUser.USER_ID;
                                Session["USERNAME"] = dataUser.USERNAME;
                                Session["GRP_ID"] = dataUser.GRP_ID;
                                Session["USER_TYPE"] = dataUser.USER_TYPE;
                                Session["user_id"] = 0;
                                Session["user_name"] = "";
                                return RedirectToAction("index", "RegisterReport");
                            }
                            else
                            {
                                ViewBag.TextWarning = "ชื่อผู้ใช้งานนี้ ถูกยกเลิกการใช้งาน กรุณาติดต่อ Admin";
                                ViewBag.color = "red";
                            }
                        }
                        else
                        {
                            ViewBag.TextWarning = !dataUser.isSuccess ? dataUser.message : "";//error 500 or 403
                            ViewBag.color = "red";
                        }
                    }
                    else
                    {
                        ViewBag.TextWarning = "Username หรือ Password ไม่ถูกต้อง";
                        ViewBag.color = "red";
                    }
                }
                else
                {
                    ViewBag.TextWarning = "Username หรือ Password ไม่ถูกต้อง";
                    ViewBag.color = "red";
                }
            }
            catch (Exception ex)
            {
                ViewBag.TextWarning = "เกิดข้อผิดพลาด " + ex.Message;
                ViewBag.color = "red";
            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("index");
        }

        public ActionResult AnotherLink()
        {
            return View("Index");
        } 

        public bool UpProfileImg(IEnumerable<HttpPostedFileBase> files)
        {
            Upload ss = new Upload();
            Dictionary<string, string> isThump = new Dictionary<string, string>();
            isThump.Add(key: "Y", value: "60X60");
            string path =  Server.MapPath("~/Content/image/profile/");
            var temp = ss.UpProfileImg(files, "test", path, isThump); 
            return true;
        }
        public void SetSessionCus_ID(string sessionval)
        {
            Session["Cust_ID"] = sessionval;
        }
    }
}
