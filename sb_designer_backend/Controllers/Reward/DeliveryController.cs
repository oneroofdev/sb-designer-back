﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Reward
{
    public class DeliveryController : BaseController
    {
        // GET: Delivery
        public ActionResult Index()
        {
            return View();
        }
        public string getDataRedeemSearch(string rewardType)
        {
            string rawXml = "{" +
             "\"REWARD_TYPE\":\"" + rewardType + "\"" +
         "}";
            var client = new RestClient(EndPoint + "/REW/getRedeemReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;/*
            string json = new JavaScriptSerializer().Serialize(StockReward.getDataStock(rewardType));
            return json;*/
        }

        public string getDataRedeem(string rewardCode)
        {
            string rawXml = "{" +
                 "\"REWARD_CODE\":\"" + rewardCode + "\"" +
             "}";
            var client = new RestClient(EndPoint + "/REW/getRedeemDetail");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;/*
            string json = new JavaScriptSerializer().Serialize(StockReward.getDataStock(rewardType));
            return json;*/
        }
        public string saveDataDelivery(string deliveryDate, string dataDelivery)
        {
            string rawXml = "{" +
                  "\"DELIVERY_DATE\":\"" + deliveryDate + "\"," +
                  "\"DATA_DELIVERY\":\"" + dataDelivery + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/REW/saveDataDelivery");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
             
        } 

        public string getRewardCourier(string RewardCode)
        {
            string rawXml = "{" +
                  "\"REWARD_CODE\":\"" + RewardCode + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/REW/ListRewardCourier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string setEditDeivery(string editCourier, string editRewardCode, string editRedeemNo)
        {
            string rawXml = "{" +
                  "\"REWARD_CODE\":\"" + editRewardCode + "\"," +
                  "\"COURIER_CODE\":\"" + editCourier + "\"," +
                  "\"REDEEM_NO\":\"" + editRedeemNo + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/REW/saveEditDelivery");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
    }
}