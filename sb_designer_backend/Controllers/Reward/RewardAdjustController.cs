﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Web.Mvc;
using ClosedXML.Excel;
using ExporHTMLTOFiles;
using sb_designer_backend.Models;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace sb_designer_backend.Controllers.Reward
{
    public class RewardAdjustController : BaseController
    {
        Models.SetExportExcel se = new Models.SetExportExcel();
        public int firstColomn_sum;
        // GET: RewardAdjust
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string GetListRewardStockByCode(string RewardCode)
        {
            string rawXml = "{" +
                "\"RewardCode\": \"" + RewardCode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListRewardStockByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveAdjustReward(string DATA_ADJUST, string PROJECT_ID, string username)
        {
            string rawXml = "{" +
                "\"DATA_ADJUST\": " + DATA_ADJUST + "," +
                "\"PROJECT_ID\": \"" + PROJECT_ID + "\"," +
                "\"username\": \"" + username + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/budget/SaveAdjustReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string list_project()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/project/list_project");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListRewardStock(int Status_Stock)
        {
            string rawXml = "{" +
                "\"Status_Stock\":\""+ Status_Stock + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListRewardStock");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListRewardHistory(string DateF, string DateT, string RewardCode, string RewardSearch)
        {
            string rawXml = "{" +
                "\"DateF\":\"" + DateF + "\"," +
                "\"DateT\":\"" + DateT + "\"," +
                "\"RewardCode\":\"" + RewardCode + "\"," +
                "\"RewardSearch\":\"" + RewardSearch + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListRewardHistory");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListReasonAdjReward(int type_id)
        {
            string rawXml = "{" +
                "\"type_id\":\"" + type_id + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListReasonAdjReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public void ReportStockExcel()
        {
            ExportToFile ex = new ExportToFile();
            XLWorkbook workbook = new XLWorkbook();
            var data_report = new List<ListReportStock>();
            data_report = GetDataReportStock();
            var ws = workbook.Worksheets.Add("Reward Stock");
            //SET Header
            ws.Cell(2, 1).Value = "รายงานของรางวัลที่มีในสต๊อก";

            ws.Cell(4, 1).Value = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ws.Range("A2:D2").Style.Font.FontSize = 20;
            ws.Range("A3:D11").Style.Font.FontSize = 14;
            ws.Range("A1:D3").Style.Font.Bold = true;
            ws.Range("A1:D11").Style.Font.FontName = "DB Gill Siam X";
            ws.Range("A1:D11").Style.Font.FontColor = XLColor.Black;
            ws.Range("A4:D4").Style.Font.FontColor = XLColor.Blue;

            //SET Logo
            //xlWorkSheet.Shapes.AddPicture("C:\\Shashi_keshar.JPG", MsoTriState.msoFalse, MsoTriState.msoCTrue, 50, 50, 300, 45);
            //SET HEADER COLUMN
            int Row = ws.LastRowUsed().RowNumber() + 2;
            ws.Cell(Row, 1).Value = "ลำดับที่";
            ws.Cell(Row, 2).Value = "รหัสของรางวัล";
            ws.Cell(Row, 3).Value = "ชื่อของรางวัล";
            ws.Cell(Row, 4).Value = "จำนวนสต๊อก";

            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            firstColomn_sum = lastRows;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Font.FontSize = 14;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Font.Bold = true;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Font.FontName = "DB Gill Siam X";
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("A" + (lastRows) + ":D" + lastRows).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;


            //SET DETAIL
            Row += 1;
            var i = 1;
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var startRow = ws.LastRowUsed().RowNumber() + 1;
            List<ListReportStock> data_budget = data_report;
            var sum_qty = 0;
            foreach (var item in data_budget)
            {

                ws.Cell(Row, 1).Value = "'" + i;
                ws.Cell(Row, 2).Value = (item.REWARD_CODE == null) ? "'" : "'" + item.REWARD_CODE.ToString();
                ws.Cell(Row, 3).Value = (item.reward_name == null) ? "'" : "'" + item.reward_name.ToString();
                ws.Cell(Row, 4).Value = (item.sum_stock.ToString() == null) ? "'" : "'" + item.sum_stock.ToString("#,##0");

                ws.Range("B" + Row + ":B" + Row).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                Row++;
                sum_qty += item.sum_stock;
                i = i + 1;
            }


            var endRow = ws.LastRowUsed().RowNumber() + 1;
            // รวมทั้งหมด
            ws.Range("A" + endRow + ":A" + endRow).Value = "รวม";
            ws.Range("D" + endRow + ":D" + endRow).Value = "'" + sum_qty.ToString("#,##0");

            //จัดขนาด Font และ ตีเส้นตาราง
            ws.Range("A" + startRow + ":D" + endRow).Style.Font.FontColor = XLColor.Black;
            ws.Range("A" + startRow + ":D" + endRow).Style.Font.FontSize = 12;
            ws.Range("A" + startRow + ":D" + endRow).Style.Font.FontName = "DB Gill Siam X";
            ws.Range("A" + startRow + ":D" + endRow).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + startRow + ":D" + endRow).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range("A" + endRow + ":D" + endRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#8EB4E3");
            ws.Range("A" + startRow + ":A" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range("D" + startRow + ":D" + endRow).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            ws.Range("A" + endRow + ":D" + endRow).Style.Font.Bold = true;

            ws.Column("A").Width = 15;
            ws.Column("B").Width = 15;
            ws.Column("C").Width = 50;
            ws.Column("D").Width = 15;

            Row++;

            //สั่งให้ Export ไฟล์
            ex.ExportExcel(workbook, "รายงานสต๊อกของรางวัล");
        } //Report Stock
        public static List<ListReportStock> GetDataReportStock()
        {
            var json = new APIResultResponse();
            string endpoint = ConfigurationManager.AppSettings["HostApi"].ToString() + "/report/list_report_stock";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("APP_TOKEN", "d750df98-7fbf-49cc-a3b2-0b3611b986ac");
            httpWebRequest.Headers.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");

            var item = new List<ListReportStock>();
            try
            {
                var dataRequest = new ReportParameter();
                dataRequest.fromType = "B";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string data = JsonConvert.SerializeObject(dataRequest);
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<APIResultResponse>(result);
                    item = JsonConvert.DeserializeObject<List<ListReportStock>>(JsonConvert.SerializeObject(json.items));
                }
            }
            catch (WebException ex)
            {
                using (var sr = new StreamReader(ex.Response.GetResponseStream()))
                {
                    json = JsonConvert.DeserializeObject<APIResultResponse>(sr.ReadToEnd());
                    //item = JsonConvert.DeserializeObject<UserItem>(JsonConvert.SerializeObject(json));
                    //item.isSuccess = false;
                    //item.message = ex.Message;
                }
            }
            return item;
        }  //Get Data Report Stock

        public class ListReportStock
        {
            public string REWARD_CODE { get; set; }
            public int sum_stock { get; set; }
            public string reward_name { get; set; }

        }
        public class ReportParameter
        {
            public string type_report { get; set; }
            public string fromType { get; set; }
        }
    }
}