﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Web.Mvc;

namespace sb_designer_backend.Controllers.Reward
{
    public class RedemptionController : BaseController
    {
        // GET: Redemption
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string CountRedeemForCart(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/CountRedeemForCart");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetCartIDByUser(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetCartIDByUser");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string addRewardToCart(string MEMBER_ID, string REWARD_CODE, string IS_SPECIAL, int QTY, int POINT_REDEEM, string CREATE_BY, string CREATE_DATE, string TRAN_ID, string CART_ID, string CHANNEL, string STATUS, string COURIER_CODE)
        {
            string rawXml = "{" +
                "\"MEMBER_ID\": \"" + MEMBER_ID + "\"," +
                "\"REWARD_CODE\": \"" + REWARD_CODE + "\"," +
                "\"IS_SPECIAL\": \"" + IS_SPECIAL + "\"," +
                "\"QTY\": " + QTY + "," +
                "\"POINT_REDEEM\": " + POINT_REDEEM + "," +
                "\"CREATE_BY\": \"" + CREATE_BY + "\"," +
                "\"CREATE_DATE\": \"" + CREATE_DATE + "\"," +
                "\"TRAN_ID\": \"" + TRAN_ID + "\"," +
                "\"CART_ID\": \"" + CART_ID + "\"," +
                "\"CHANNEL\": \"" + CHANNEL + "\"," +
                "\"STATUS\": \"" + STATUS + "\"," +
                "\"COURIER_CODE\": \"" + COURIER_CODE + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/addRewardToCart");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListCourierByReward(string RewardCode)
        {
            string rawXml = "{" +
                "\"RewardCode\": \"" + RewardCode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCourierByReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListCategory()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCategory");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string getListCatalogue()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/getListCatalogue");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListRewardNew(string ALL_TIER, string START_DATE, string END_DATE, string CATALOGUE_CODE, string CATEGORY_CODE, string IS_HILIGHT,
            string IS_MAINPAGE, string TIER, int POINT, string TAG, string MODEL, string BRAND, string ACTIVE)
        {
            string rawXml = "{" +
                "\"ALL_TIER\": \"" + ALL_TIER + "\"," +
                "\"START_DATE\": \"" + START_DATE + "\"," +
                "\"END_DATE\": \"" + END_DATE + "\"," +
                "\"CATALOGUE_CODE\": \"" + CATALOGUE_CODE + "\"," +
                "\"CATEGORY_CODE\": \"" + CATEGORY_CODE + "\"," +
                "\"IS_HILIGHT\": \"" + IS_HILIGHT + "\"," +
                "\"IS_MAINPAGE\": \"" + IS_MAINPAGE + "\"," +
                "\"TIER\": \"" + TIER + "\"," +
                "\"POINT\": " + POINT + "," +
                "\"TAG\": \"" + TAG + "\"," +
                "\"MODEL\": \"" + MODEL + "\"," +
                "\"BRAND\": \"" + BRAND + "\"," +
                "\"ACTIVE\": \"" + ACTIVE + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListRewardNew");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetRewardDetailByCode(string RewardCode)
        {
            string rawXml = "{" +
                "\"RewardCode\": \"" + RewardCode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetRewardDetailByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string deleteRewardFromCart(string TRAN_ID, string CART_ID, string MEMBER_ID, string REWARD_CODE, string IS_SPECIAL, string QTY, string POINT_REDEEM, string CREATE_BY
            , string CREATE_DATE, string CHANNEL, string STATUS)
        {
            string rawXml = "{" +
                "\"TRAN_ID\": \"" + TRAN_ID + "\"," +
                "\"CART_ID\": \"" + CART_ID + "\"," +
                "\"MEMBER_ID\": \"" + MEMBER_ID + "\"," +
                "\"REWARD_CODE\": \"" + REWARD_CODE + "\"," +
                "\"IS_SPECIAL\": \"" + IS_SPECIAL + "\"," +
                "\"QTY\": \"" + QTY + "\"," +
                "\"POINT_REDEEM\": \"" + POINT_REDEEM + "\"," +
                "\"CREATE_BY\": \"" + CREATE_BY + "\"," +
                "\"CREATE_DATE\": \"" + CREATE_DATE + "\"," +
                "\"CHANNEL\": \"" + CHANNEL + "\"," +
                "\"STATUS\": \"" + STATUS + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/deleteRewardFromCart");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetFristRedeem(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/redeem/GetFristRedeem");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string saveRedeem_BK(string CART_ID, string MEMBER_ID, string REDEEM_BY, string REDEEM_CHANNEL, string RECEIVE_NAME, string HOUSE_NO, string BULDING, string TAMBOL_ID
            , string AMPHUR_ID, string PROVINCE_ID, string POST_CODE, string PHONE, string EMAIL, string CREATE_BY, string CARD_ID, string COPY_CARD_ID, string BANK_CODE
            , string BANK_ACCT_NO, string BANK_BRANCH, string COPY_BOOK_BANK, string PROMPT_PAY_MOBILE, string PROMPT_PAY_CID, string COURIER)
        {
            string rawXml = "{" +
                "\"CART_ID\": " + CART_ID + "," +
                "\"MEMBER_ID\": " + MEMBER_ID + "," +
                "\"BRANCH_CODE\": 0," +
                "\"TOTAL_POINT\": 0," +
                "\"REDEEM_BY\": \"" + REDEEM_BY + "\"," +
                "\"REDEEM_CHANNEL\": \"" + REDEEM_CHANNEL + "\"," +
                "\"RECEIVE_NAME\": \"" + RECEIVE_NAME + "\"," +
                "\"HOUSE_NO\": \"" + HOUSE_NO + "\"," +
                "\"BULDING\": \"" + BULDING + "\"," +
                "\"TAMBOL_ID\": \"" + TAMBOL_ID + "\"," +
                "\"AMPHUR_ID\": \"" + AMPHUR_ID + "\"," +
                "\"PROVINCE_ID\": \"" + PROVINCE_ID + "\"," +
                "\"POST_CODE\": \"" + POST_CODE + "\"," +
                "\"PHONE\": \"" + PHONE + "\"," +
                "\"EMAIL\": \"" + EMAIL + "\"," +
                "\"CREATE_BY\": \"" + CREATE_BY + "\"," +
                "\"CARD_ID\": \"" + CARD_ID + "\"," +
                "\"COPY_CARD_ID\": \"" + COPY_CARD_ID + "\"," +
                "\"BANK_CODE\": \"" + BANK_CODE + "\"," +
                "\"BANK_ACCT_NO\": \"" + BANK_ACCT_NO + "\"," +
                "\"BANK_BRANCH\": \"" + BANK_BRANCH + "\"," +
                "\"COPY_BOOK_BANK\": \"" + COPY_BOOK_BANK + "\"," +
                "\"PROMPT_PAY_MOBILE\": \"" + PROMPT_PAY_MOBILE + "\"," +
                "\"PROMPT_PAY_CID\": \"" + PROMPT_PAY_CID + "\"," +
                "\"COURIER\": " + COURIER + "" +
            "}";

            var client = new RestClient(EndPoint + "/REW/saveRedeem_BK");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string clearCartFirst(string MEMBER_ID)
        {
            string rawXml = "{" +
                "\"MEMBER_ID\": \"" + MEMBER_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/clearCartFirst");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string get_profile(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/MEM/get_profile");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListProvince()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/LOC/ListProvince");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListDistrict(string prv_id)
        {
            string rawXml = "{" +
                "\"prv_id\": \"" + prv_id + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/LOC/ListDistrict");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListSubDistrict(string amp_id)
        {
            string rawXml = "{" +
                "\"amp_id\": \"" + amp_id + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/LOC/ListSubDistrict");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string Zipcode(string tam_id)
        {
            string rawXml = "{" +
                "\"tam_id\": \"" + tam_id + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/LOC/Zipcode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListBanked()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/redeem/ListBanked");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListRewardCart_Draw(string Cust_ID)
        {
            string rawXml = "{" +
                "\"Cust_ID\": \"" + Cust_ID + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListRewardCart_Draw");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}
