﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
//using System.Web.Http;

namespace sb_designer_backend.Controllers.Reward
{
    public class RewardSettingController : BaseController
    {
        // GET: RewardSetting
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string SaveSupplier(string SupplierCode, string SupplierTH, string SupplierEN, string Suppliercontact, string Suppliertel, string Supplieremail, string Supplieraddress
            , string Supplierlineid, string Supplierworking, string Active, string MemberName)
        {
            string rawXml = "{" +
                "\"SupplierCode\": \"" + SupplierCode + "\"," +
                "\"SupplierTH\": \"" + SupplierTH + "\"," +
                "\"SupplierEN\": \"" + SupplierEN + "\"," +
                "\"Suppliercontact\": \"" + Suppliercontact + "\"," +
                "\"Suppliertel\": \"" + Suppliertel + "\"," +
                "\"Supplieremail\": \"" + Supplieremail + "\"," +
                "\"Supplieraddress\": \"" + Supplieraddress + "\"," +
                "\"Supplierlineid\": \"" + Supplierlineid + "\"," +
                "\"Supplierworking\": \"" + Supplierworking + "\"," +
                "\"Active\": \"" + Active + "\"," +
                "\"MemberName\": \"" + MemberName + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/SaveSupplier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListSupplier()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListSupplier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListSupplierByCode(string SupplierCode)
        {
            string rawXml = "{" +
                "\"SupplierCode\": \"" + SupplierCode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListSupplierByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListSupplier_active()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListSupplier_active");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveCourier(string Couriercode, string CourierTH, string CourierEN, string CourierNum, string CourierType, string CourierLogistic, string CourierURL, string Active, string MemberName)
        {
            string rawXml = "{" +
                "\"Couriercode\":\"" + Couriercode + "\"," +
                "\"CourierTH\":\"" + CourierTH + "\"," +
                "\"CourierEN\":\"" + CourierEN + "\"," +
                "\"CourierNum\":\"" + CourierNum + "\"," +
                "\"CourierType\":\"" + CourierType + "\"," +
                "\"CourierLogistic\":\"" + CourierLogistic + "\"," +
                "\"CourierURL\":\"" + CourierURL + "\"," +
                "\"Active\":\"" + Active + "\"," +
                "\"MemberName\":\"" + MemberName + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/SaveCourier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveCategory(string CategoryCode, string CategoryTH, string CategoryEN, string Prefixes, string Active, string DisplaySEQ, string MemberName)
        {
            string rawXml = "{" +
                "\"CategoryCode\":\"" + CategoryCode + "\"," +
                "\"CategoryTH\":\"" + CategoryTH + "\"," +
                "\"CategoryEN\":\"" + CategoryEN + "\"," +
                "\"Prefixes\":\"" + Prefixes + "\"," +
                "\"Active\":\"" + Active + "\"," +
                "\"DisplaySEQ\":\"" + DisplaySEQ + "\"," +
                "\"MemberName\":\"" + MemberName + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/SaveCategory");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string SaveCatalogue(string CatalogueCode, string CatalogueTH, string CatalogueEN, string Active, string DateStart, string DateEnd, string MemberName)
        {
            string rawXml = "{" +
                "\"CatalogueCode\":\"" + CatalogueCode + "\"," +
                "\"CatalogueTH\":\"" + CatalogueTH + "\"," +
                "\"CatalogueEN\":\"" + CatalogueEN + "\"," +
                "\"Active\":\"" + Active + "\"," +
                "\"DateStart\":\"" + DateStart + "\"," +
                "\"DateEnd\":\"" + DateEnd + "\"," +
                "\"MemberName\":\"" + MemberName + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":99," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/SaveCatalogue");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string saveStatusReward(string rewardCode, string rewardStatus, string username)
        {
            string rawXml = "{" +
                "\"rewardCode\":\"" + rewardCode + "\"," +
                "\"rewardStatus\":\"" + rewardStatus + "\"," +
                "\"username\":\"" + username + "\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/saveStatusReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetEditRewardByCode(string RewardCode)
        {
            string rawXml = "{" +
                "\"RewardCode\":\"" + RewardCode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":1," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetEditRewardByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetCourierByCode(string COURIER_CODE)
        {
            string rawXml = "{" +
                "\"COURIER_CODE\":\"" + COURIER_CODE + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":1," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetCourierByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListCatalogueByCode(string itemecode)
        {
            string rawXml = "{" +
                "\"itemecode\":\"" + itemecode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":1," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCatalogueByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string GetListRewardSort(string Mainpage,string Highlight)
        {
            string rawXml = "{" +
                "\"Mainpage\":\"" + Mainpage + "\"," +
                "\"Highlight\":\"" + Highlight + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/GetListRewardSort");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string updateSEQReward(string Mainpage, string Highlight, string ListSEQ)
        {
            string rawXml = "{" +
                "\"Mainpage\":\"" + Mainpage + "\"," +
                "\"Highlight\":\"" + Highlight + "\"," +
                "\"ListSEQ\":" + ListSEQ + "," +
                "\"language\":\"TH\"," +
                "\"maxResults\":1," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/updateSEQReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
//, string rewardCode, string rewardName, string rewardNameEN, string startDate,string endDate,
//            string specialStartDate, string specialEndDate, string point, string specialpoint, string tierpoint,
//            string price, string catalogueCode, string categoryCode, string courierCode, string rewardDetailTH,
//            string rewardDetailEN, string fileNamePic, string qtyFixed, string active, string username,
//            string rewardBrand, string rewardModel, string tier, string mainPage, string hilight,
//            string seq, string supplier, string rewardConditionTH, string rewardConditionEN, string rewardStepTH,
//            string rewardSteplEN, string rewardProject

        //"\"rewardCode\":\"" + rewardCode + "\"," +
        //"\"rewardName\":\"" + rewardName + "\"," +
        //"\"rewardNameEN\":\"" + rewardNameEN + "\"," +
        //"\"startDate\":\"" + startDate + "\"," +
        //"\"endDate\":\"" + endDate + "\"," +
        //"\"specialStartDate\":\"" + specialStartDate + "\"," +
        //"\"specialEndDate\":\"" + specialEndDate + "\"," +
        //"\"point\":" + point + "," +
        //"\"specialpoint\":" + specialpoint + "," +
        //"\"tierpoint\":\"" + tierpoint + "\"," +
        //"\"price\":" + price + "," +
        //"\"catalogueCode\":" + catalogueCode + "," +
        //"\"categoryCode\":" + categoryCode + "," +
        //"\"courierCode\":\"" + courierCode + "\"," +
        //"\"rewardDetailTH\":\"" + rewardDetailTH + "\"," +
        //"\"rewardDetailEN\":\"" + rewardDetailEN + "\"," +
        //"\"fileNamePic\":\"" + fileNamePic + "\"," +
        //"\"qtyFixed\":" + qtyFixed + "," +
        //"\"active\":\"" + active + "\"," +
        //"\"username\":\"" + username + "\"," +
        //"\"rewardBrand\":\"" + rewardBrand + "\"," +
        //"\"rewardModel\":\"" + rewardModel + "\"," +
        //"\"tier\":\"" + tier + "\"," +
        //"\"mainPage\":\"" + mainPage + "\"," +
        //"\"hilight\":\"" + hilight + "\"," +
        //"\"seq\":" + seq + "," +
        //"\"supplier\":\"" + supplier + "\"," +
        //"\"rewardConditionTH\":\"" + rewardConditionTH + "\"," +
        //"\"rewardConditionEN\":\"" + rewardConditionEN + "\"," +
        //"\"rewardStepTH\":\"" + rewardStepTH + "\"," +
        //"\"rewardSteplEN\":\"" + rewardSteplEN + "\"," +
        //"\"rewardProject\":" + rewardProject + "" +

        [HttpPost]
        public string saveNewReward()
        {
            var rowDataJson = Request.Form.Get("DataRew");
            string base64Decoded;
            byte[] data = System.Convert.FromBase64String(rowDataJson);
            base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
            //var json = new JavaScriptSerializer().Serialize(dataRew);
            List <ParamSaveNewReward> Rewrad_N = new List<ParamSaveNewReward>();
            dynamic itemCourier = JsonConvert.DeserializeObject(base64Decoded);
            foreach (var val in itemCourier)
            {
                ParamSaveNewReward aa = new ParamSaveNewReward();
                aa.rewardCode = val.rewardCode;
                aa.rewardName = val.rewardName;
                aa.rewardNameEN = val.rewardNameEN;
                aa.startDate = val.startDate;
                aa.endDate = val.endDate;
                aa.specialStartDate = val.specialStartDate;
                aa.specialEndDate = val.specialEndDate;
                aa.point = val.point;
                aa.specialpoint = val.specialpoint;
                aa.tierpoint = val.tierpoint;
                aa.price = val.price;
                aa.catalogueCode = val.catalogueCode;
                aa.categoryCode = val.categoryCode;
                aa.courierCode = val.courierCode;
                aa.rewardDetailTH = val.rewardDetailTH;
                aa.rewardDetailEN = val.rewardDetailEN;
                aa.fileNamePic = val.fileNamePic;
                aa.qtyFixed = val.qtyFixed;
                aa.active = val.active;
                aa.username = val.username;
                aa.rewardBrand = val.rewardBrand;
                aa.rewardModel = val.rewardModel;
                aa.tier = val.tier;
                aa.mainPage = val.mainPage;
                aa.hilight = val.hilight;
                aa.seq = val.seq;
                aa.supplier = val.supplier;
                aa.rewardConditionTH = val.rewardConditionTH;
                aa.rewardConditionEN = val.rewardConditionEN;
                aa.rewardStepTH = val.rewardStepTH;
                aa.rewardSteplEN = val.rewardSteplEN;
                aa.rewardProject = val.rewardProject;
                Rewrad_N.Add(aa);
            }
            string rawXml = "{" +
                "\"rewardCode\":\"" + Rewrad_N[0].rewardCode + "\"," +
                "\"rewardName\":\"" + Rewrad_N[0].rewardName + "\"," +
                "\"rewardNameEN\":\"" + Rewrad_N[0].rewardNameEN + "\"," +
                "\"startDate\":\"" + Rewrad_N[0].startDate + "\"," +
                "\"endDate\":\"" + Rewrad_N[0].endDate + "\"," +
                "\"specialStartDate\":\"" + Rewrad_N[0].specialStartDate + "\"," +
                "\"specialEndDate\":\"" + Rewrad_N[0].specialEndDate + "\"," +
                "\"point\":" + Rewrad_N[0].point + "," +
                "\"specialpoint\":" + Rewrad_N[0].specialpoint + "," +
                "\"tierpoint\":\"" + Rewrad_N[0].tierpoint + "\"," +
                "\"price\":" + Rewrad_N[0].price + "," +
                "\"catalogueCode\":" + Rewrad_N[0].catalogueCode + "," +
                "\"categoryCode\":" + Rewrad_N[0].categoryCode + "," +
                "\"courierCode\":\"" + Rewrad_N[0].courierCode + "\"," +
                "\"rewardDetailTH\":\"" + Rewrad_N[0].rewardDetailTH + "\"," +
                "\"rewardDetailEN\":\"" + Rewrad_N[0].rewardDetailEN + "\"," +
                "\"fileNamePic\":\"" + Rewrad_N[0].fileNamePic + "\"," +
                "\"qtyFixed\":" + Rewrad_N[0].qtyFixed + "," +
                "\"active\":\"" + Rewrad_N[0].active + "\"," +
                "\"username\":\"" + Rewrad_N[0].username + "\"," +
                "\"rewardBrand\":\"" + Rewrad_N[0].rewardBrand + "\"," +
                "\"rewardModel\":\"" + Rewrad_N[0].rewardModel + "\"," +
                "\"tier\":\"" + Rewrad_N[0].tier + "\"," +
                "\"mainPage\":\"" + Rewrad_N[0].mainPage + "\"," +
                "\"hilight\":\"" + Rewrad_N[0].hilight + "\"," +
                "\"seq\":" + Rewrad_N[0].seq + "," +
                "\"supplier\":\"" + Rewrad_N[0].supplier + "\"," +
                "\"rewardConditionTH\":\"" + Rewrad_N[0].rewardConditionTH + "\"," +
                "\"rewardConditionEN\":\"" + Rewrad_N[0].rewardConditionEN + "\"," +
                "\"rewardStepTH\":\"" + Rewrad_N[0].rewardStepTH + "\"," +
                "\"rewardSteplEN\":\"" + Rewrad_N[0].rewardSteplEN + "\"," +
                "\"rewardProject\":" + Rewrad_N[0].rewardProject + "" +
            "}";

            var client = new RestClient(EndPoint + "/REW/saveNewReward");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public class ParamSaveNewReward
        {
            public string rewardCode { get; set; }
            public string rewardName { get; set; }
            public string rewardNameEN { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string specialStartDate { get; set; }
            public string specialEndDate { get; set; }
            public string point { get; set; }
            public string specialpoint { get; set; }
            public string tierpoint { get; set; }
            public string price { get; set; }
            public string catalogueCode { get; set; }
            public string categoryCode { get; set; }
            public string courierCode { get; set; }
            public string rewardDetailTH { get; set; }
            public string rewardDetailEN { get; set; }
            public string fileNamePic { get; set; }
            public string qtyFixed { get; set; }
            public string stock { get; set; }
            public string active { get; set; }
            public string username { get; set; }
            public string rewardBrand { get; set; }
            public string rewardModel { get; set; }
            public string tier { get; set; }
            public string mainPage { get; set; }
            public string hilight { get; set; }
            public string supplier { get; set; }
            public string seq { get; set; }
            public string rewardConditionTH { get; set; }
            public string rewardConditionEN { get; set; }
            public string rewardStepTH { get; set; }
            public string rewardSteplEN { get; set; }
            public string rewardProject { get; set; }
        }

        [HttpPost]
        public string ListCourier()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCourier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListCourierType()
        {
            string rawXml = "{" +
                "\"language\":\"TH\"," +
                "\"maxResults\":999," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCourierType");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        [HttpPost]
        public string ListCategoryByCode(string itemecode)
        {
            string rawXml = "{" +
                "\"itemecode\":\"" + itemecode + "\"," +
                "\"language\":\"TH\"," +
                "\"maxResults\":1," +
                "\"fromType\":\"B\"" +
            "}";

            var client = new RestClient(EndPoint + "/REW/ListCategoryByCode");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}