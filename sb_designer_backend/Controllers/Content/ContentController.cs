﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace sb_designer_backend.Controllers.Content
{
    public class ContentController : BaseController
    { 
        // GET: Content
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult content_example(int id)
        {
            ViewBag.ContentCode = id;
            return View();
        } 
        public string getContent(string fromType, string contentType, string contentCode)
        {
            string rawXml = "{" +
                  "\"fromType\":\"" + fromType + "\"," +
                  "\"contentType\":\"" + contentType + "\"," +
                  "\"contentCode\":\"" + contentCode + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/content/get_content");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        public string listContent(string fromType, string contentType)
        {
            string rawXml = "{" +
                  "\"fromType\":\"" + fromType + "\"," +
                  "\"contentType\":\"" + contentType + "\"" + 
              "}";
            var client = new RestClient(EndPoint + "/content/list_content");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        public string createContent(string fromType, string contentType, string contentHeaderTh, string contentDescTh, string linkUrl, string isRegister, string isLimitRegister, string LimitRegister, string contentImage, string contentTextTh, string contentTextEn, string startDate, string endDate, string active)
        {
            string rawXml = "{" +
                  //"\"fromType\":\"" + fromType + "\"," +
                  //"\"contentType\":\"" + contentType + "\"," +
                  //"\"contentHeaderTh\":\"" + contentHeaderTh + "\"," + 
                  //"\"contentDescTh\":\"" + contentDescTh + "\"," + 
                  //"\"linkUrl\":\"" + linkUrl + "\"," +
                  //"\"isRegister\":\"" + isRegister + "\"," +
                  //"\"isLimitRegister\":\"" + isLimitRegister + "\"," +
                  //"\"LimitRegister\":\"" + LimitRegister + "\"," +
                  //"\"contentImage\":\"" + contentImage + "\"," +
                  //"\"contentTextTh\":\"" + contentTextTh + "\"," + 
                  //"\"startDate\":\"" + startDate + "\"," +
                  ////"\"endDate\":\"" + endDate + "\"," +
                  //"\"active\":\"" + active + "\"," +
                  "\"createBy\":\"" + username + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/content/create_content");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string updateContent(string fromType, string contentCode, string contentType, string contentHeaderTh, string contentHeaderEn, string contentDescTh, string contentDescEn, string linkUrl, string isRegister, string isLimitRegister, string LimitRegister, string contentImage, string contentTextTh, string contentTextEn, string startDate, string endDate, string active)
        { 
            string rawXml = "{" +
                  "\"fromType\":\"" + fromType + "\"," +
                  "\"contentCode\":\"" + contentCode + "\"," +
                  "\"contentType\":\"" + contentType + "\"," +
                  "\"contentHeaderTh\":\"" + contentHeaderTh + "\"," +
                  "\"contentHeaderEn\":\"" + contentHeaderEn + "\"," +
                  "\"contentDescTh\":\"" + contentDescTh + "\"," +
                  "\"contentDescEn\":\"" + contentDescEn + "\"," +
                  "\"linkUrl\":\"" + linkUrl + "\"," +
                  "\"isRegister\":\"" + isRegister + "\"," +
                  "\"isLimitRegister\":\"" + isLimitRegister + "\"," +
                  "\"LimitRegister\":\"" + LimitRegister + "\"," +
                  "\"contentImage\":\"" + contentImage + "\"," +
                  "\"contentTextTh\":\"" + contentTextTh + "\"," +
                  "\"contentTextEn\":\"" + contentTextEn + "\"," +
                  "\"startDate\":\"" + startDate + "\"," +
                  "\"endDate\":\"" + endDate + "\"," +
                  "\"active\":\"" + active + "\"," +
                  "\"createBy\":\"" + username + "\"" +
              "}";
            var client = new RestClient(EndPoint + "/content/update_content");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic U0JEZXMhZ25lcjpEZXMhZ25lcmNsdWI=");
            request.AddParameter("application/json", rawXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }


    }
}
